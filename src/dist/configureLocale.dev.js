'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true,
});
exports['default'] = void 0;

var _i18next = _interopRequireDefault(require('i18next'));

var _i18nextBrowserLanguagedetector = _interopRequireDefault(require('i18next-browser-languagedetector'));

var _locale_en = _interopRequireDefault(require('./locale/locale_en'));

var _locale_ch = _interopRequireDefault(require('./locale/locale_ch'));

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

_i18next['default'].use(_i18nextBrowserLanguagedetector['default']).init({
    // we init with resources
    resources: {
        en: _locale_en['default'],
        ch: _locale_ch['default'],
    },
    fallbackLng: 'en',
    // have a common namespace used around the full app
    ns: ['translations', 'sidemenu'],
    defaultNS: 'translations',
    keySeparator: false,
    // we use content as keys
    interpolation: {
        escapeValue: false,
        // not needed for react!!
        formatSeparator: ',',
    },
    react: {
        wait: true,
    },
});

var _default = _i18next['default'];
exports['default'] = _default;
