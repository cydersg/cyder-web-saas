import React, { useState, useEffect, useMemo, useCallback } from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';
import moment from 'moment';

import { Container, Form, Input, Row, Col, FormGroup } from 'reactstrap';
import ReactTable from 'react-table';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import { TablePagination } from 'cyderComponents/pagination/';
import DialogModal from 'cyderComponents/modals/DialogModal';
import { CyderSelect, CyderInput, CyderDatePicker } from 'cyderComponents/input/index';
import { ConfirmModalManager } from 'cyderComponents/ConfirmModal';
import LinkButton from 'cyderComponents/buttons/LinkButton';

import { getAllHolidays, createHoliday, updateHoliday, deleteHoliday } from 'actions/settings/cyderLeaveHolidaysAction';

import ActionExecutor from 'js/ActionExecutor';
import { getYearOptions } from 'js/generalUtils';

function StaffLeaveBalanceUsersPage(props) {
    const yearOptions = useMemo(() => getYearOptions(2017), []);
    const [holidays, setHolidays] = useState([]);
    const [isOpen, setIsOpen] = useState(false);
    const [loading, setLoading] = useState(false);
    const [keyword, setKeyword] = useState(moment().format('YYYY'));
    const [data, setData] = useState({});

    const onEdit = useCallback((toggle, data) => {
        setIsOpen(toggle);
        setData(data);
    }, []);

    const getHolidays = useCallback(() => {
        const callback = async () => {
            const data = await props.getAllHolidays(keyword);
            setHolidays(data && data.data);
        };
        ActionExecutor.execute(setLoading || (() => {}), callback);
    }, [keyword]);

    const onDelete = useCallback(
        id => {
            const callback = async () => {
                await props.deleteHoliday(id);
                getHolidays();
                ConfirmModalManager.toggle();
            };
            ConfirmModalManager.show('Warning', 'Are you sure you wan to delete this holiday record?', callback, true);
        },
        [keyword],
    );

    useEffect(getHolidays, [keyword]);

    return (
        <div>
            <SubmitDialog {...props} data={data} isOpen={isOpen} toggle={setIsOpen} setLoading={setLoading} getHolidays={getHolidays} />
            <Container>
                <Row>
                    <Col xs="12" md="3">
                        <CyderSelect
                            formGroupClassName="m-0"
                            inputProps={{
                                options: yearOptions,
                                placeholder: 'Filter by month',
                                value: yearOptions.find(x => x.label === parseInt(keyword)),
                                onChange: year => setKeyword(year.value),
                            }}
                        />
                    </Col>
                    <Col className="text-right d-flex align-items-end">
                        <LinkButton preset="addButton" text="Holiday" onClick={() => onEdit(true, {})} />
                    </Col>
                </Row>
                <RowColWrapper colProps={{ className: 'pt-4' }}>
                    {loading ? (
                        <LoadingSpinner center />
                    ) : (
                        <ReactTable
                            className="-highlight mb-2"
                            data={holidays}
                            pageSize={holidays.length}
                            columns={getColumns(props, onEdit, onDelete)}
                            showPagination={holidays.length > 5}
                            PaginationComponent={TablePagination}
                        />
                    )}
                </RowColWrapper>
            </Container>
        </div>
    );
}

const SubmitDialog = props => {
    const { data, toggle, isOpen, setLoading, getHolidays } = props;
    const [date, setDate] = useState('');
    const [name, setName] = useState('');

    useEffect(() => {
        const { holidaydate, holidayname } = data;
        setDate(holidaydate && moment(holidaydate, 'YYYY-MM-DD'));
        setName(holidayname);
    }, [data]);

    const onSubmit = () => {
        const isAdd = Object.keys(data).length === 0;
        const body = {
            holidayname: name,
            holidaydate: date && date.format('YYYY-MM-DD'),
            id: data.id,
        };

        const callback = async () => {
            // VALIDATION
            const hasError = validate(body);
            if (hasError) {
                ConfirmModalManager.show('Warning', hasError, () => ConfirmModalManager.toggle(), true);
                return;
            }

            // API CALL
            const data = isAdd ? await props.createHoliday(body) : await props.updateHoliday(body);
            const message = isAdd ? 'add' : 'updat';
            ConfirmModalManager.show(
                data.ok ? 'Confirmation' : 'Failed to',
                data.ok ? `Holiday has been ${message}ed` : `Failed to ${message} holiday`,
                () => {
                    getHolidays();
                    ConfirmModalManager.toggle();
                },
                true,
            );
        };
        toggle(false);
        ActionExecutor.execute(setLoading || (() => {}), callback);
    };

    return (
        <DialogModal
            bodyContent
            key="modal"
            modal={isOpen}
            toggle={() => toggle(!isOpen)}
            customHeader="Edit Holiday"
            positiveButtonAction={onSubmit}
            positiveButtonText="Submit"
        >
            <FormGroup>
                <CyderInput
                    mandatory
                    label="Holiday Name"
                    inputProps={{
                        type: 'text',
                        id: 'holidayname',
                        name: 'holidayname',
                        onChange: e => setName(e.target.value),
                        value: name,
                    }}
                />
                <CyderInput mandatory label="Holiday Date" formGroupClassName="m-0">
                    <CyderDatePicker timeFormat={false} onChange={setDate} value={date} placeholder="DD/MM/YYYY" dateFormat="DD/MM/YYYY" />
                </CyderInput>
            </FormGroup>
        </DialogModal>
    );
};

const getColumns = (props, onEdit, onDelete) => [
    {
        Header: 'Name',
        accessor: 'holidayname',
        Cell: props => props.value,
    },
    {
        Header: 'Date',
        accessor: 'holidaydate',
        Cell: props => {
            return moment(props.value, 'YYYY-MM-DD').format('DD MMM YYYY');
        },
    },
    {
        Header: '',
        accessor: 'id',
        width: 130,
        Cell: props => {
            return (
                <div>
                    <i
                        className="mr-2 material-icons align-text-bottom"
                        style={{ cursor: 'pointer' }}
                        onClick={() => onEdit(true, props.original)}
                    >
                        edit
                    </i>
                    <i
                        className="mr-1 material-icons align-text-bottom"
                        style={{ cursor: 'pointer', color: 'red' }}
                        onClick={() => onDelete(props.original.id)}
                    >
                        delete
                    </i>
                </div>
            );
        },
    },
];

const validate = data => {
    if (!data.holidaydate) return 'Holiday date is required';
    if (!data.holidayname) return 'Holiday name is required';
    return false;
};

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = { getAllHolidays, createHoliday, updateHoliday, deleteHoliday };

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(StoredLayout(StaffLeaveBalanceUsersPage));
