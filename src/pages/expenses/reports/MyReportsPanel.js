import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import history from 'history.js';
import moment from 'moment';

import { Badge, Container, Button, Input, Row, Col } from 'reactstrap';
import ReactTable from 'react-table';
import CyderDatePicker from 'cyderComponents/forms/CyderDatePicker';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import { TablePagination } from 'cyderComponents/pagination/';

import { expenseFullReportDownloadPDF } from 'actions/expenses/reports/reportsDetailAction.js';
import { myReportsGetReports } from 'actions/expenses/cyderExpenseMyReportsAction.js';
import { setJumbotronTitle } from 'actions/pagedata';

import cyderlib from 'js/cyderlib.js';
import { layoutLib } from 'js/constlib';

var numeral = require('numeral');

class MyReportsPanel extends Component {
    componentDidMount() {
        this.props.getReports(null, null, 'all', this.props.mode);
    }

    printReportPDF = () => {
        const startdate = document.getElementById('startdate').value;
        const enddate = document.getElementById('enddate').value;
        const status = document.getElementById('status').value;
        const reportParam = {
            startdate,
            enddate,
            status,
        };
        const btoa = require('btoa');
        window.open(
            `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp/' : ''}report/expense/reportsbymonth/print/${btoa(
                JSON.stringify(reportParam),
            )}`,
        );
    };

    printReport = () => {
        const startdate = document.getElementById('startdate').value;
        const enddate = document.getElementById('enddate').value;
        const status = document.getElementById('status').value;

        var csvPayload = `Expense Report,,,Printed on,${moment().format('DD MMMM YYYYY')}\n`;
        csvPayload += ',,,,\n';
        csvPayload += `From,,${startdate},,\n`;
        csvPayload += `To,,${enddate},,\n`;
        csvPayload += `Status,,${status},,\n`;
        csvPayload += ',,,,\n';
        csvPayload += 'Submitted On,Submitted By,Amount,Status,Approver\n';
        csvPayload += ',,,,\n';
        this.props.reports.forEach(data => {
            csvPayload += `${moment(data.createddt).format('DD MMMM YYYY')},${data.createdby},${data.totalamount / 100},${data.status},${
                data.owner
            }\n`;
        });

        var csv = 'data:text/csv;charset=utf-8,' + csvPayload;
        var data = encodeURI(csv);
        var link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('download', 'ExpenseReport.csv');
        link.click();
    };

    getReportsRefresh = () => {
        const startdate = document.getElementById('startdate').value;
        const enddate = document.getElementById('enddate').value;
        const status = document.getElementById('status').value;
        this.props.getReports(startdate, enddate, status, this.props.mode);
    };

    getTdProps = (state, rowInfo) => {
        return {
            onClick: (e, handleOriginal) => {
                if (!rowInfo) return;
                const url = `${(this.props.mode === 'self'
                    ? `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreports/details/`
                    : `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp/' : ''}report/expense/reportsbymonth/details/`) +
                    rowInfo.original.id}`;
                history.push(url);
                if (handleOriginal) handleOriginal();
            },
        };
    };
    render() {
        const { loading, reports, mode } = this.props;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        return (
            <Container className="wideContainer">
                <SearchPanel
                    {...this.props}
                    getReportsRefresh={this.getReportsRefresh}
                    printReport={this.printReport}
                    printReportPDF={this.printReportPDF}
                />
                <Row>
                    <Col xs={12}>
                        {loading ? (
                            <LoadingSpinner center />
                        ) : (
                            <ReactTable
                                minRows={1}
                                data={reports}
                                showPagination={!isMobile}
                                className="-highlight mb-2"
                                columns={getColumns(mode)}
                                NoDataComponent={() => <div className="no-rtData">No reports found</div>}
                                PaginationComponent={TablePagination}
                                getTdProps={this.getTdProps}
                                defaultSorted={[
                                    {
                                        id: 'createddt',
                                        desc: true,
                                    },
                                ]}
                            />
                        )}
                    </Col>
                </Row>
            </Container>
        );
    }
}

const SearchPanel = props => {
    const { mode, getReportsRefresh, printReport, printReportPDF } = props;
    return (
        <Container>
            <div className="d-none d-md-block">
                <Row className="pb-4 ">
                    <div className="mr10 ml10">
                        <strong>From:</strong>
                        <CyderDatePicker id="startdate" dateFormat="DD/MM/YYYY" width="130px" timeFormat={false} placeholder="DD/MM/YYYY" />
                    </div>
                    <div className="mr10 ml10">
                        <strong>To:</strong>
                        <CyderDatePicker id="enddate" dateFormat="DD/MM/YYYY" width="130px" timeFormat={false} placeholder="DD/MM/YYYY" />
                    </div>
                    <div className="mr10 ml10">
                        <strong>Status:</strong>
                        <Input id="status" type="select">
                            <option value="all">All</option>
                            <option value="submitted">Submitted</option>
                            <option value="approved">Approved</option>
                            <option value="pendingpayment">Pending Payment</option>
                            <option value="completed">Completed</option>
                            <option value="paid">Paid</option>
                            <option value="returned">Returned</option>
                        </Input>
                    </div>
                    <div className="mr10 ml10">
                        <div className="invisible">button</div>
                        <Button className="mx-1" color="success" onClick={getReportsRefresh}>
                            Go
                        </Button>
                        <Button className="mx-1" color="info" onClick={printReport}>
                            CSV
                        </Button>
                        <Button className="mx-1" color="info" onClick={printReportPDF}>
                            Print
                        </Button>
                    </div>
                </Row>
            </div>
            <div className="d-block d-md-none">
                <Row className="pb-4">
                    <Col xs={12}>
                        <strong>From:</strong>
                        <CyderDatePicker id="startdate" dateFormat="DD/MM/YYYY" timeFormat={false} placeholder="DD/MM/YYYY" />
                    </Col>
                    <Col xs={12}>
                        <strong>To:</strong>
                        <CyderDatePicker id="enddate" dateFormat="DD/MM/YYYY" timeFormat={false} placeholder="DD/MM/YYYY" />
                    </Col>
                    <Col xs={12}>
                        <strong>Status:</strong>
                        <Input id="status" type="select">
                            <option value="all">All</option>
                            <option value="submitted">Submitted</option>
                            <option value="approved">Approved</option>
                            <option value="pendingpayment">Pending Payment</option>
                            <option value="completed">Completed</option>
                            <option value="paid">Paid</option>
                            <option value="returned">Returned</option>
                        </Input>
                    </Col>
                    <Col xs={12}>
                        <div className="invisible">button</div>
                        <Button className="mx-1" color="success" onClick={getReportsRefresh}>
                            Go
                        </Button>
                        {/* {mode === 'admin' && ( */}
                        <Button className="mx-1" color="info" onClick={printReport}>
                            CSV
                        </Button>
                        {/* )} */}
                        {/* {mode === 'admin' && ( */}
                        <Button className="mx-1" color="info" onClick={printReportPDF}>
                            Print
                        </Button>
                        {/* )} */}
                    </Col>
                </Row>
            </div>
        </Container>
    );
};

const getColumns = mode => {
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    let mobileColumns = [
        {
            filterable: false,
            Header: '',
            headerClassName: 'rt-no-sort-ind',
            accessor: 'id',
            sortable: false,
            Cell: props => {
                return (
                    <div className="boxShadow">
                        <div className="d-flex justify-content-between">
                            <strong>{moment(props.original.createddt).format('DD MMM YYYY')}</strong>
                            <span>{cyderlib.formatCurrencyCent(props.original.totalamount)}</span>
                        </div>
                        {mode !== 'self' && (
                            <>
                                <span className="font14">{props.original.createdby}</span>
                                <br />
                            </>
                        )}
                        <span className="font12">{props.original.remarks}</span>
                        <br />
                        <div className="d-flex">
                            <Badge color={cyderlib.statusBadgeColor(props.original.status)}>{props.original.status}</Badge>
                        </div>
                    </div>
                );
            },
        },
    ];

    let columns = [
        {
            Header: 'Report No.',
            filterable: true,
            accessor: 'reportId',
        },
        {
            Header: 'Submitted On',
            filterable: false,
            accessor: 'createddt',
            Cell: props => moment(props.value).format('DD MMMM YYYY'),
            sortMethod: cyderlib.tableSortDate,
        },
    ];

    if (isMobile) return mobileColumns;

    if (mode === 'self') {
        return columns.concat([
            {
                Header: 'Remarks',
                accessor: 'remarks',
                filterable: true,
                filterMethod: (filter, row) => row[filter.id].toLowerCase().indexOf(filter.value.toLowerCase()) !== -1,
            },
            {
                Header: 'Amount',
                accessor: 'totalamount',
                filterable: false,
                width: 100,
                Cell: props => 'S$' + numeral((props.value / 100).toFixed(2)).format('0,0.00'),
            },
            {
                Header: 'Status',
                accessor: 'status',
                filterable: false,
                width: 90,
                filterMethod: (filter, row) => row[filter.id].toLowerCase().indexOf(filter.value.toLowerCase()) !== -1,
                Cell: props => {
                    return <Badge color={cyderlib.statusBadgeColor(props.value)}>{props.value}</Badge>;
                },
            },
            {
                filterable: false,
                Header: '',
                headerClassName: 'rt-no-sort-ind',
                accessor: 'id',
                sortable: false,
                width: 50,
                Cell: props => {
                    return (
                        <div className="float-right">
                            <Link
                                to={
                                    `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreports/details/` +
                                    props.value
                                }
                            >
                                <Button color="link" size="sm">
                                    <i className="align-middle material-icons">info_outline</i>
                                </Button>
                            </Link>
                        </div>
                    );
                },
            },
        ]);
    } else {
        return columns.concat([
            {
                Header: 'Submitted By',
                accessor: 'createdby',
                filterable: true,
                filterMethod: (filter, row) => {
                    return row[filter.id].toLowerCase().indexOf(filter.value.toLowerCase()) !== -1;
                },
            },
            {
                Header: 'Amount',
                accessor: 'totalamount',
                width: 100,
                filterable: false,
                Cell: props => 'S$' + numeral((props.value / 100).toFixed(2)).format('0,0.00'),
            },
            {
                Header: 'Approved By',
                accessor: 'owner',
                filterable: true,
                filterMethod: (filter, row) => {
                    return row[filter.id].toLowerCase().indexOf(filter.value.toLowerCase()) !== -1;
                },
            },
            {
                Header: 'Status',
                accessor: 'status',
                width: 90,
                filterable: false,
                filterMethod: (filter, row) => row[filter.id].toLowerCase().indexOf(filter.value.toLowerCase()) !== -1,
                Cell: props => {
                    return <Badge color={cyderlib.statusBadgeColor(props.value)}>{props.value}</Badge>;
                },
            },
            {
                filterable: false,
                Header: '',
                headerClassName: 'rt-no-sort-ind',
                accessor: 'id',
                sortable: false,
                width: 50,
                Cell: props => {
                    return (
                        <div className="float-right">
                            <Link
                                to={`/${
                                    window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp/' : ''
                                }report/expense/reportsbymonth/details/${props.value}`}
                            >
                                <Button color="link" size="sm">
                                    <i className="align-middle material-icons">info_outline</i>
                                </Button>
                            </Link>
                        </div>
                    );
                },
            },
        ]);
    }
};

const mapStateToProps = state => {
    return {
        ...state.cyderExpenseMyReportsReducer,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        downloadExpenseReport: (startdate, enddate, status) => {
            return dispatch(expenseFullReportDownloadPDF(startdate, enddate, status));
        },
        setJumbotronTitle: title => dispatch(setJumbotronTitle(title)),
        getReports: (startdate, enddate, status, mode) => dispatch(myReportsGetReports(startdate, enddate, status, mode)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyReportsPanel);
