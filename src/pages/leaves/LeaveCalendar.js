import React, { useEffect, useState, useCallback, useMemo } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import { Container } from 'reactstrap';
import Calendar from 'react-awesome-calendar';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

import { getAllLeaveSummary } from 'actions/timesheet/monthlyTimesheetAction';
import { getAllHolidays } from 'actions/settings/cyderLeaveHolidaysAction';
import { setJumbotronTitle } from 'actions/pagedata';

import { useGetAllActiveUsers } from 'pages/settings/leave/LeaveBalanceUserSettingsPage';
import { cyderUsersSearchAction } from 'actions/users/cyderUsersSearchAction';
import ActionExecutor from 'js/ActionExecutor';
import { randDarkColor, getYearOptions } from 'js/generalUtils';
import { layoutLib } from 'js/constlib';

const useGetAllHolidays = (props, years) => {
    const [loading, setLoading] = useState(false);
    const [holidays, setHolidays] = useState([]);

    const getAllHolidays = async () => {
        const callback = async year => {
            const { data } = await props.getAllHolidays(year);
            if (!data) return;
            const parsedData = processHolidays(data);
            return parsedData;
        };

        const callbacks = years.map(year => ActionExecutor.execute(setLoading, async () => await callback(year.value)));
        Promise.all(callbacks).then(x => {
            const days = x.reduce((a, b) => [...a, ...b], []);
            setHolidays(days);
        });
    };

    const processHolidays = data => {
        if (!data) return [];
        return data
            .map(x => {
                if (x.status !== 'Active') return;
                return {
                    id: x.id,
                    color: 'blue',
                    from: moment(x.holidaydate, 'YYYY-MM-DD'),
                    to: moment(x.holidaydate, 'YYYY-MM-DD'),
                    title: `${x.holidayname}`,
                };
            })
            .filter(x => x);
    };

    useEffect(() => {
        getAllHolidays();
    }, []);

    return [holidays, loading];
};

const useGetMonthlyLeaveSummary = (props, years) => {
    const users = useGetAllActiveUsers(props);
    const [loading, setLoading] = useState(false);
    const [leaves, setLeaves] = useState([]);

    const getAllLeaveSummary = () => {
        const callback = async () => {
            const { data } = await props.getAllLeaveSummary();
            if (!data) return;
            const parsedData = processLeaves(data.reverse());
            setLeaves(parsedData);
        };
        ActionExecutor.execute(setLoading, callback);
    };

    const processLeaves = data => {
        if (!data) return [];

        return data
            .map(x => {
                if (x.status !== 'Active') return;
                const { lastname, firstname } = users.find(y => y.email === x.owner) || {};

                return {
                    status: x.status,
                    fromdate: x.fromdate,
                    todate: x.todate,
                    id: x.id,
                    color: randDarkColor(),
                    from: x.fromdate,
                    to: moment(x.todate, 'YYYY-MM-DD').add(1, 'days'),
                    title: `${x.leaveTypeCode} ${firstname && lastname && `by ${lastname} ${firstname}`}`,
                };
            })
            .filter(x => x);
    };

    useEffect(() => {
        getAllLeaveSummary();
    }, [users]);

    return [leaves, loading];
};

const LeaveCalendar = props => {
    const years = useMemo(() => getYearOptions(2019).filter(x => x.value));
    const [holidays, holidaysLoading] = useGetAllHolidays(props, years);
    const [leaves, leavesLoading] = useGetMonthlyLeaveSummary(props);

    useEffect(() => {
        props.setJumbotronTitle('Leave Calendar');
    }, []);

    if ((holidaysLoading, leavesLoading)) return <LoadingSpinner center />;
    return (
        <Container>
            <Calendar events={[...leaves, ...holidays]} />
        </Container>
    );
};

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {
        getAllLeaveSummary() {
            return dispatch(getAllLeaveSummary());
        },
        getAllHolidays(year) {
            return dispatch(getAllHolidays(year));
        },
        setJumbotronTitle(title) {
            return dispatch(setJumbotronTitle(title));
        },
        getUsers() {
            return dispatch(cyderUsersSearchAction());
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LeaveCalendar);
