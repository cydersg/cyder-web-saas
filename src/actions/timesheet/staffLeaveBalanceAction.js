import withQuery from 'with-query';

import fetchlib from '../../js/fetchlib';
import { CYDER_LEAVE_API_ROOT } from '../../config';

export function getStaffLeaveBalance(owner) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/leave`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getuserleavebalanceforadmin',
        });
        const body = {
            owner,
        };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}

export function updateStaffLeaveBalance(owner, balance, leaveTypeCode) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/leave`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'updateleavebalance',
        });
        const body = { owner, balance, leaveTypeCode };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}

export function getCurrentLeaveBalance() {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/leave`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getmyleavebalance',
        });
        return fetchlib.fetch(dispatch, getState, 'GET', url);
    };
}

export function generateLeaveBalance(owner) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/leave`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'generateleavebalanceindividual',
        });
        const body = { owner };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}
