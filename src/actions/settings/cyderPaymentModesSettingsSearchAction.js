import {
    CYDER_PAYMENT_MODES_SETTINGS_SEARCH_SUCCESS,
    CYDER_PAYMENT_MODES_SETTINGS_SEARCH_REQUEST,
    CYDER_PAYMENT_MODES_SETTINGS_SEARCH_FAILURE,
    CYDER_PAYMENT_MODES_SETTINGS_SEARCH_SET_FORCE_REFRESH_FLAG,
    CYDER_PAYMENT_MODES_SETTINGS_MODAL_SET_PAYMENT_MODE,
    CYDER_PAYMENT_MODES_SETTINGS_MODAL_TOGGLE,
    CYDER_PAYMENT_MODES_SETTINGS_MODAL_CHANGE_VALUE,
    CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_ADD_REQUEST,
    CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_ADD_SUCCESS,
    CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_ADD_FAILURE,
    CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_UPDATE_REQUEST,
    CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_UPDATE_SUCCESS,
    CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_UPDATE_FAILURE,
    CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_DELETE_REQUEST,
    CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_DELETE_SUCCESS,
    CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_DELETE_FAILURE,
} from '../constants/actionTypes';
import withQuery from 'with-query';

import { DJARVIS_COMMON_API_ROOT } from '../../config';

/**
 * Security action creators
 *
 * Author: Fernando
 */

/**
 * Login function
 *
 * @param {*} fields
 * @param {*} callback
 */
export function cyderPaymentModesSettingsSearchAction(body) {
    // Redux Thunk will inject dispatch here:
    return (dispatch, getState) => {
        // Reducers may handle this to set a flag like isFetching
        dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_SEARCH_REQUEST });
        dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_SEARCH_SET_FORCE_REFRESH_FLAG, val: false });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/paymentmode', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'partialsearch',
        });
        // const bodyWithAppId = Object.assign({appId: getState().cyderProfileReducer.profile.appId}, body);
        const options = {
            body: JSON.stringify(body),
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
        };
        return fetch(url, options)
            .then(response => response.json())
            .then(
                response => {
                    const id = 1;
                    // Reducers may handle this to show the data and reset isFetching
                    dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_SEARCH_SUCCESS, id, response });
                    // if we return stuff here, we can use it as a promise
                    return response;
                },
                error => {
                    // TODO
                    // Reducers may handle this to reset isFetching
                    // dispatch({ type: 'GET_USER_FAILURE', id, error })
                    dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_SEARCH_FAILURE, error });
                    // Rethrow so returned Promise is rejected
                    throw error;
                },
            )
            .catch(error => {
                //TODO
                console.log('in catch block');
                console.log(error);
                throw error;
            });
    };
}
export function cyderPaymentModesSettingsEditModalSetPaymentMode(id, code, name) {
    return {
        type: CYDER_PAYMENT_MODES_SETTINGS_MODAL_SET_PAYMENT_MODE,
        id,
        code,
        name,
    };
}
export function cyderPaymentModesSettingsModalToggleAction(modalAction) {
    return {
        type: CYDER_PAYMENT_MODES_SETTINGS_MODAL_TOGGLE,
        modalAction,
    };
}
export function cyderPaymentModesSettingsEditModalChangeValue(key, value) {
    return {
        type: CYDER_PAYMENT_MODES_SETTINGS_MODAL_CHANGE_VALUE,
        key,
        value,
    };
}
export function cyderPaymentModesSettingsAddPaymentModeAction() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_ADD_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/paymentmode', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'add',
        });
        const body = {
            ...getState().cyderPaymentModesSettingsSearchReducer.modalPaymentMode,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_ADD_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_ADD_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_ADD_FAILURE, error });
                return error;
            });
    };
}
export function cyderPaymentModesSettingsUpdatePaymentModeAction() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_UPDATE_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/paymentmode', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'update',
        });
        const body = {
            ...getState().cyderPaymentModesSettingsSearchReducer.modalPaymentMode,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_UPDATE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_UPDATE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_UPDATE_FAILURE, error });
                return error;
            });
    };
}
export function cyderPaymentModesSettingsDeletePaymentModeAction() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_DELETE_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/paymentmode', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'delete',
        });
        const code = getState().cyderPaymentModesSettingsSearchReducer.modalPaymentMode.code;
        console.log(code);
        const body = {
            code,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_DELETE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_DELETE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_PAYMENT_MODES_SETTINGS_PAYMENT_MODE_DELETE_FAILURE, error });
                return error;
            });
    };
}
