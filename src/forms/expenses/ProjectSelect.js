/**
 * Project Select
 * 
 * Author: Fernando
 */

import React, {Component} from 'react'
import Select from 'react-select'
import {connect} from 'react-redux'
// import history from '../../history'
import projects from '../../json/projects.json'
import {selectProject} from '../../actions/expenses/expenseCommonAction'

class ProjectSelect extends Component {
  render() {
    return ( 
      <Select
        autosize={false}
        placeholder="Select project"
        autofocus
        options={projects}
        simpleValue
        clearable={this.props.expense.projectValue.clearable}
        name="selected-project"
        disabled={this.props.expense.projectValue.disabled}
        value={this.props.expense.projectValue.value}
        onChange={this.props.updateValue}
        searchable={this.props.expense.projectValue.searchable}
      />
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    expense: state.cyderExpenseReducer
  }
}
const mapDispatchToProps = dispatch => {
  return {
    updateValue: (e) => {
      dispatch(selectProject(e));
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProjectSelect)