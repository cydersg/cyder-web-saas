import {
    CYDER_USERS_DETAIL_REQUEST,
    CYDER_USERS_DETAIL_SUCCESS,
    CYDER_USERS_DETAIL_FAILURE,
    CYDER_USERS_DETAIL_CHANGE_FIELD,
    CYDER_USERS_DETAIL_SAVE_CHANGES_REQUEST,
    CYDER_USERS_DETAIL_SAVE_CHANGES_SUCCESS,
    CYDER_USERS_DETAIL_SAVE_CHANGES_FAILURE,
    CYDER_USERS_DETAIL_DELETE_REQUEST,
    CYDER_USERS_DETAIL_DELETE_SUCCESS,
    CYDER_USERS_DETAIL_DELETE_FAILURE,
    CYDER_USERS_DETAIL_MODAL_TOGGLE,
    CYDER_USERS_SEARCH_SET_FORCE_REFRESH_FLAG,
    CYDER_USERS_DETAIL_FORCE_VALIDATE,
    CYDER_PROFILE_REFRESH_OVERRIDE_REQUEST,
    CYDER_PROFILE_REFRESH_OVERRIDE_SUCCESS,
    CYDER_PROFILE_REFRESH_OVERRIDE_FAILURE,
} from '../constants/actionTypes';

import { CYDER_COMMON_API_ROOT } from '../../config';

import withQuery from 'with-query';

/**
 * Login function
 *
 * @param {*} fields
 * @param {*} callback
 */
export function forceValidate() {
    return dispatch => {
        dispatch({ type: CYDER_USERS_DETAIL_FORCE_VALIDATE });
        // return this for async actions
        return Promise.resolve();
    };
}
export function cyderUsersDetailChangeFieldAction(key, value) {
    return {
        type: CYDER_USERS_DETAIL_CHANGE_FIELD,
        key,
        value,
    };
}
export function cyderUsersDetailGetUserDetailsAction(userid, currentUser) {
    // Redux Thunk will inject dispatch here:
    return (dispatch, getState) => {
        // Reducers may handle this to set a flag like isFetching
        if (currentUser) {
            dispatch({ type: CYDER_PROFILE_REFRESH_OVERRIDE_REQUEST });
        } else {
            dispatch({ type: CYDER_USERS_DETAIL_REQUEST });
        }

        // Perform the actual API call
        const url = withQuery(CYDER_COMMON_API_ROOT + '/users', {
            appid: getState().cyderProfileReducer.profile.appId,
            id: currentUser ? getState().cyderProfileReducer.profile.id : userid,
            action: 'findbyid',
        });
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
        };
        const sanitizeResponse = response => {
            for (var key in response) {
                if (response[key] === null) {
                    response[key] = '';
                }
            }
            return response;
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        if (currentUser) {
                            dispatch({ type: CYDER_PROFILE_REFRESH_OVERRIDE_FAILURE, error });
                        } else {
                            dispatch({ type: CYDER_USERS_DETAIL_FAILURE, error });
                        }
                        return error;
                    });
                } else {
                    return response.json().then(response => {
                        if (currentUser) {
                            dispatch({ type: CYDER_PROFILE_REFRESH_OVERRIDE_SUCCESS, userData: sanitizeResponse(response) });
                        } else {
                            dispatch({ type: CYDER_USERS_DETAIL_SUCCESS, response: sanitizeResponse(response) });
                        }
                        return response;
                    });
                }
            })
            .catch(error => {
                if (currentUser) {
                    dispatch({ type: CYDER_PROFILE_REFRESH_OVERRIDE_FAILURE, error });
                } else {
                    dispatch({ type: CYDER_USERS_DETAIL_FAILURE, error });
                }
                return error;
            });
    };
}
export function cyderUsersDetailSaveChangesAction() {
    // Redux Thunk will inject dispatch here:
    return (dispatch, getState) => {
        const setSavingStateAction = {
            type: CYDER_USERS_DETAIL_SAVE_CHANGES_REQUEST,
        };
        dispatch(setSavingStateAction);
        const url = withQuery(CYDER_COMMON_API_ROOT + '/users', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'update',
        });
        let theUser = getState().cyderUsersDetailReducer.user;
        const department_code = theUser.department_code.value;
        theUser.department_code = department_code;
        delete theUser.firstname_lower;
        delete theUser.lastname_lower;
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(theUser),
            method: 'POST',
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_USERS_DETAIL_SAVE_CHANGES_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_USERS_DETAIL_SAVE_CHANGES_FAILURE, error });
                return error;
            });
    };
}
export function cyderUsersDetailDeleteUserAction() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_USERS_DETAIL_DELETE_REQUEST });

        // Perform the actual API call
        const url = withQuery(CYDER_COMMON_API_ROOT + '/users', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'delete',
        });
        const body = {
            username: getState().cyderUsersDetailReducer.user.username,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_USERS_DETAIL_DELETE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_USERS_SEARCH_SET_FORCE_REFRESH_FLAG, val: true });
                        dispatch({ type: CYDER_USERS_DETAIL_DELETE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_USERS_DETAIL_DELETE_FAILURE, error });
                return error;
            });
    };
}
export function cyderUsersDetailModalToggleAction(modalAction) {
    return {
        type: CYDER_USERS_DETAIL_MODAL_TOGGLE,
        modalAction,
    };
}
