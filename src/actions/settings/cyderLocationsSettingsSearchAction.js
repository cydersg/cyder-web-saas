import {
    CYDER_LOCATIONS_SETTINGS_SEARCH_SUCCESS,
    CYDER_LOCATIONS_SETTINGS_SEARCH_REQUEST,
    CYDER_LOCATIONS_SETTINGS_SEARCH_FAILURE,
    CYDER_LOCATIONS_SETTINGS_SEARCH_SET_FORCE_REFRESH_FLAG,
    CYDER_LOCATIONS_SETTINGS_MODAL_SET_LOCATION,
    CYDER_LOCATIONS_SETTINGS_MODAL_TOGGLE,
    CYDER_LOCATIONS_SETTINGS_MODAL_CHANGE_VALUE,
    CYDER_LOCATIONS_SETTINGS_LOCATION_ADD_REQUEST,
    CYDER_LOCATIONS_SETTINGS_LOCATION_ADD_SUCCESS,
    CYDER_LOCATIONS_SETTINGS_LOCATION_ADD_FAILURE,
    CYDER_LOCATIONS_SETTINGS_LOCATION_UPDATE_REQUEST,
    CYDER_LOCATIONS_SETTINGS_LOCATION_UPDATE_SUCCESS,
    CYDER_LOCATIONS_SETTINGS_LOCATION_UPDATE_FAILURE,
    CYDER_LOCATIONS_SETTINGS_LOCATION_DELETE_REQUEST,
    CYDER_LOCATIONS_SETTINGS_LOCATION_DELETE_SUCCESS,
    CYDER_LOCATIONS_SETTINGS_LOCATION_DELETE_FAILURE,
    CYDER_LOCATIONS_SETTINGS_LOCATION_MAP_SET_LOC,
    CYDER_LOCATIONS_SETTINGS_CHANGE_FIELD_VALUE,
    CYDER_LOCATIONS_SETTINGS_CHANGE_LAT_LNG,
    CYDER_LOCATIONS_SETTINGS_QR_OPEN,
} from '../constants/actionTypes';
import withQuery from 'with-query';

import { DJARVIS_COMMON_API_ROOT } from '../../config';

/**
 * Security action creators
 *
 * Author: Fernando
 */

/**
 * Login function
 *
 * @param {*} fields
 * @param {*} callback
 */
export function cyderLocationsSearchAction(body) {
    // Redux Thunk will inject dispatch here:
    return (dispatch, getState) => {
        // Reducers may handle this to set a flag like isFetching
        dispatch({ type: CYDER_LOCATIONS_SETTINGS_SEARCH_REQUEST });
        dispatch({ type: CYDER_LOCATIONS_SETTINGS_SEARCH_SET_FORCE_REFRESH_FLAG, val: false });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/location', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'partialsearch',
        });
        // const bodyWithAppId = Object.assign({appId: getState().cyderProfileReducer.profile.appId}, body);
        const options = {
            body: JSON.stringify(body),
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
        };
        return fetch(url, options)
            .then(response => response.json())
            .then(
                response => {
                    const id = 1;
                    // Reducers may handle this to show the data and reset isFetching
                    dispatch({ type: CYDER_LOCATIONS_SETTINGS_SEARCH_SUCCESS, id, response });
                    // if we return stuff here, we can use it as a promise
                    return response;
                },
                error => {
                    // TODO
                    // Reducers may handle this to reset isFetching
                    // dispatch({ type: 'GET_USER_FAILURE', id, error })
                    dispatch({ type: CYDER_LOCATIONS_SETTINGS_SEARCH_FAILURE, error });
                    // Rethrow so returned Promise is rejected
                    throw error;
                },
            )
            .catch(error => {
                //TODO
                console.log('in catch block');
                console.log(error);
                throw error;
            });
    };
}
export function cyderLocationsSettingsEditModalSetLocation(id, code, name, address, lat, lng) {
    return (dispatch, getState) => {
        dispatch({
            type: CYDER_LOCATIONS_SETTINGS_MODAL_SET_LOCATION,
            id,
            code,
            name,
            address,
            lat,
            lng,
        });
        return Promise.resolve();
    };
}
export function cyderLocationsSettingsModalToggleAction(modalAction) {
    return {
        type: CYDER_LOCATIONS_SETTINGS_MODAL_TOGGLE,
        modalAction,
    };
}
export function cyderLocationsSettingsEditModalChangeValue(key, value) {
    return {
        type: CYDER_LOCATIONS_SETTINGS_MODAL_CHANGE_VALUE,
        key,
        value,
    };
}
export function cyderLocationsSettingsAddLocationAction() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_LOCATIONS_SETTINGS_LOCATION_ADD_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/location', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'add',
        });
        const body = {
            ...getState().cyderLocationsSettingsSearchReducer.modalLocation,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_LOCATIONS_SETTINGS_LOCATION_ADD_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_LOCATIONS_SETTINGS_LOCATION_ADD_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_LOCATIONS_SETTINGS_LOCATION_ADD_FAILURE, error });
                return error;
            });
    };
}
export function cyderLocationsSettingsUpdateLocationAction() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_LOCATIONS_SETTINGS_LOCATION_UPDATE_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/location', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'update',
        });
        const body = {
            ...getState().cyderLocationsSettingsSearchReducer.modalLocation,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_LOCATIONS_SETTINGS_LOCATION_UPDATE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_LOCATIONS_SETTINGS_LOCATION_UPDATE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_LOCATIONS_SETTINGS_LOCATION_UPDATE_FAILURE, error });
                return error;
            });
    };
}
export function cyderLocationsSettingsDeleteLocationAction() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_LOCATIONS_SETTINGS_LOCATION_DELETE_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/location', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'delete',
        });
        const code = getState().cyderLocationsSettingsSearchReducer.modalLocation.code;
        console.log(code);
        const body = {
            code,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_LOCATIONS_SETTINGS_LOCATION_DELETE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_LOCATIONS_SETTINGS_LOCATION_DELETE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_LOCATIONS_SETTINGS_LOCATION_DELETE_FAILURE, error });
                return error;
            });
    };
}
export function cyderLocationsSettingsSetMapLocAction(place, marker) {
    return dispatch => {
        dispatch({
            type: CYDER_LOCATIONS_SETTINGS_LOCATION_MAP_SET_LOC,
            place,
            marker,
        });
        return Promise.resolve();
    };
}
export function cyderLocationsSettingsPinPointLocAction(lat, lng) {
    return {
        type: CYDER_LOCATIONS_SETTINGS_CHANGE_LAT_LNG,
        lat,
        lng,
    };
}
export function cyderLocationsSettingsChangeFieldValueAction(key, value) {
    return {
        type: CYDER_LOCATIONS_SETTINGS_CHANGE_FIELD_VALUE,
        key,
        value,
    };
}

export function cyderLocationsSettingsOpenQrCodeAction(open, location) {
    return {
        type: CYDER_LOCATIONS_SETTINGS_QR_OPEN,
        open,
        location,
    };
}
