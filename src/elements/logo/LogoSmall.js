import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Media } from 'reactstrap';
import { withTranslation } from 'react-i18next';
import '../../scss/elements/logo.scss';
const pjson = require('../../../package.json');

class LogoSmall extends React.Component {
    render() {
        const logoStyle = {
            width: '60%',
        };
        return (
            <Link to="/home" className="logo d-flex justify-content-start align-items-center flex-nowrap">
                <Media style={logoStyle} object src="/assets/images/header_small.png" />
                <span
                    style={{
                        paddingTop: '5px',
                        marginLeft: '50px',
                    }}
                >
                    {pjson.version}
                </span>
            </Link>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const isNavBarCollapsed = state.config.layout === 'collapsed-sidebar-1';
    return {
        isNavBarCollapsed,
    };
};

export default withTranslation()(connect(mapStateToProps, null)(Logo));
