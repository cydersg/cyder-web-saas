import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import withQuery from 'with-query';
import moment from 'moment';
import history from 'history.js';

import { Row, Col, ListGroup, ListGroupItem } from 'reactstrap';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import IconCardWidget from 'cyderComponents/cardWidget/IconCardWidget';
import { SimplyLinkButton } from 'pages/home/shared/SimplyButtons';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import ConciergeHomeTable from 'pages/home/shared/ConciergeHomeTable';
import StatusBadge from 'cyderComponents/common/StatusBadge';

import {
    expenseSharedGetProjects,
    expenseSharedGetLocations,
    expenseSharedGetPaymentModes,
    expenseSharedGetExpenseCategories,
    expenseSharedGetMileageCategories,
    expenseSharedGetDashboardItems,
    expenseSharedGetSettingsItems,
} from 'actions/expenses/cyderExpenseClaimSharedAction';
import { myTasksGetTasks } from 'actions/workflow/myTasksAction.js';
import { myReceiptsGetReceipts, myReceiptsRenderReceipts } from 'actions/expenses/cyderExpenseMyReceiptsAction';
import { setJumbotronTitle } from 'actions/pagedata';
import { forceLoading } from 'actions/common/homeAction';
import { layoutLib } from 'js/constlib';
import cyderlib from 'js/cyderlib';

class HomePage extends Component {
    componentDidMount() {
        this.props.setJumbotronTitle();
        this.props.getRelevantStuff();
    }
    render() {
        const { loading, profile, receipts, dashboardItems, settingsCount, expenseCategoriesMap, mileageCategoriesMap, tasks } = this.props;

        return (
            <div>
                <Row className="mb-3">
                    <Col xl={12}>
                        <StatusReportPanel data={dashboardItems} tasksCount={tasks.length} />
                    </Col>
                    <Col lg={12}>
                        {loading ? (
                            <LoadingSpinner center />
                        ) : (
                            <div>
                                <ExpensesStatus data={receipts} cats={expenseCategoriesMap} mcats={mileageCategoriesMap} />
                            </div>
                        )}
                    </Col>
                </Row>
                {profile.role > 0 ? <SetupPanel settingsCount={settingsCount} /> : null}
            </div>
        );
    }
}

const StatusReportPanel = ({ data, tasksCount }) => {
    const cards = [
        {
            icon: 'assignment_turned_in',
            color: 'tomato',
            title: tasksCount || 0,
            label: 'Tasks',
            link: `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'workflow'}/mytasks`,
        },
        {
            icon: 'check',
            color: 'blue',
            title: data.Submitted || 0,
            label: 'Submitted',
            link: 'Submitted',
        },
        {
            icon: 'layers_clear',
            color: 'salmon',
            title: data.Returned || 0,
            label: 'Returned',
            link: 'Returned',
        },
        {
            icon: 'mood',
            color: 'darksalmon',
            title: data.Approved || 0,
            label: 'Approved',
            link: 'Approved',
        },
        // {
        //     icon: 'monetization_on',
        //     color: 'orange',
        //     title: data.Payment || 0,
        //     label: 'Payment',
        //     link: 'PendingPayment',
        // },
    ];
    return (
        <Row className="mt-2 mb-3">
            <Col xs={12} md={2}>
                <QuickActionButtons />
            </Col>
            <Col xs={12} md={10} className="mt10">
                <Row>
                    {cards.map((item, i) =>
                        item.label === 'Tasks' ? (
                            <IconCardWidget key={i} {...item} url={item.link} xs={6} md={3} lg={3} sm={6} />
                        ) : (
                            <IconCardWidget
                                xs={6}
                                md={3}
                                lg={3}
                                sm={6}
                                key={i}
                                {...item}
                                url={withQuery(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts`, {
                                    status: item.link,
                                })}
                            />
                        ),
                    )}
                </Row>
            </Col>
        </Row>
    );
};

const ExpensesStatus = ({ data, cats, mcats }) => {
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;
    const columns = [
        {
            Header: '',
            accessor: 'expensedate',
            Cell: props => {
                const receiptType = props.row._original.type.toLowerCase();
                const link = withQuery(
                    `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts/edit/${receiptType}`,
                    {
                        receiptid: props.original.id,
                        index: props.index,
                    },
                );

                return (
                    <Link
                        to={link}
                        style={{
                            color: 'unset',
                        }}
                    >
                        <div className="boxShadow">
                            <div className="d-flex justify-content-between">
                                <strong>{moment(props.original.expensedate).format('DD MMM YYYY')}</strong>
                                <span>{cyderlib.formatCurrencyCent(props.original.amount)}</span>
                            </div>
                            <span>
                                {props.original.type === 'Mileage' ? mcats[props.original.category] : cats[props.original.category]}
                            </span>
                            <br />
                            {props.original.remarks}
                            <br />
                            <div className="d-flex">
                                <StatusBadge status={props.original.status} />
                            </div>
                        </div>
                    </Link>
                );
            },
            show: isMobile,
        },
        {
            Header: 'Date',
            accessor: 'expensedate',
            Cell: props => moment(props.value).format('DD MMM YYYY'),
            show: !isMobile,
        },
        {
            Header: 'Particulars',
            Cell: props => {
                return (
                    <div>
                        <span>
                            <strong>
                                {props.original.type === 'Mileage' ? mcats[props.original.category] : cats[props.original.category]}
                            </strong>
                        </span>
                        <br />
                        {props.original.remarks}
                    </div>
                );
            },
            show: !isMobile,
        },
        {
            Header: 'Amount',
            accessor: 'amount',
            Cell: props => cyderlib.formatCurrencyCent(props.value),
            show: !isMobile,
        },
        {
            Header: 'Status',
            width: 80,
            filterable: false,
            filterMethod: cyderlib.filterIgnoreCase,
            accessor: 'status',
            Cell: props => {
                return (
                    <div className="d-flex justify-content-center">
                        <StatusBadge status={props.value} />
                    </div>
                );
            },
            show: !isMobile,
        },
        {
            Header: '',
            width: 80,
            filterable: false,
            filterMethod: cyderlib.filterIgnoreCase,
            accessor: 'id',
            Cell: props => {
                const receiptType = props.row._original.type.toLowerCase();
                const link = withQuery(`/expense/myreceipts/edit/${receiptType}`, {
                    receiptid: props.value,
                    index: props.index,
                });
                return (
                    <div className="d-flex justify-content-center text-bold">
                        <Link to={link}>View</Link>
                    </div>
                );
            },
            show: !isMobile,
        },
    ];
    return <ConciergeHomeTable title="Expenses" columns={columns} data={data} defaultPageSize={5} />;
};

const SetupPanel = props => {
    const { project, location, payment_mode, expense_category, mileage_category } = props.settingsCount;
    const hasProjects = project > 0;
    const hasLocations = location > 0;
    const hasPaymentModes = payment_mode > 0;
    const hasExpenseCategories = expense_category > 0;
    const hasMileageCategories = mileage_category > 0;
    const items = [
        {
            item: 'Expense Categories',
            has: hasExpenseCategories,
            link: 'expensecategories',
        },
        {
            item: 'Mileage Categories',
            has: hasMileageCategories,
            link: 'mileagecategories',
        },
        {
            item: 'Projects',
            has: hasProjects,
            link: 'projects',
        },
        {
            item: 'Locations',
            has: hasLocations,
            link: 'locations',
        },
        {
            item: 'Payment Modes',
            has: hasPaymentModes,
            link: 'paymentmodes',
        },
    ];
    return (
        <RowColWrapper colProps={{ className: 'mb-2', xs: 12 }}>
            <ListGroup className="unstriped">
                {items.map((x, i) => {
                    const { item, link, has } = x;
                    if (has) return null;
                    const to = `/settings/${link}`;
                    const label = `You have not set up any ${item}. Please click here to set up ${item}`;
                    return (
                        <ListGroupItem key={i}>
                            <Link to={to}>{label}</Link>
                        </ListGroupItem>
                    );
                })}
            </ListGroup>
        </RowColWrapper>
    );
};

const QuickActionButtons = () => (
    <Row>
        <Col className="d-flex flex-column mt-2">
            <SimplyLinkButton
                text="Receipt"
                iconName="add"
                to={`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts/new/receipt`}
            />
            <SimplyLinkButton
                text="Mileage"
                iconName="add"
                to={`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts/new/mileage`}
            />
            <SimplyLinkButton
                text="Report"
                iconName="add"
                to={`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts/myreceipts`}
            />
            <SimplyLinkButton
                text="Reports"
                iconName="library_books"
                to={`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreports`}
            />
        </Col>
    </Row>
);

const mapStateToProps = state => {
    return {
        ...state.cyderExpenseSharedReducer,
        loading: state.homeReducer.loading,
        profile: state.cyderProfileReducer.profile,
        receipts: state.cyderExpenseMyReceiptsReducer.receipts,
        tasks: state.myTasksReducer.receipts,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        navigateTo: url => history.push(url),
        forceLoading: loading => dispatch(forceLoading(loading)),
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Personal')),
        getRelevantStuff: () => {
            dispatch(forceLoading(true));
            const items = [
                dispatch(myReceiptsGetReceipts('Attention')),
                dispatch(expenseSharedGetDashboardItems()),
                dispatch(expenseSharedGetSettingsItems()),
                dispatch(expenseSharedGetProjects()),
                dispatch(expenseSharedGetLocations()),
                dispatch(expenseSharedGetExpenseCategories()),
                dispatch(expenseSharedGetPaymentModes()),
                dispatch(expenseSharedGetMileageCategories()),
                dispatch(myTasksGetTasks('Expense Report')),
            ];
            return Promise.all(items).then(res => {
                dispatch(myReceiptsRenderReceipts(res[0]));
                return dispatch(forceLoading(false));
            });
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
