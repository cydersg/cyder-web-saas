import React, { Component } from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';
import ReportsDetailPanel from '../../pages/expenses/reports/ReportsDetailPanel';

class ReportsByMonthDetails extends Component {
    render() {
        return <ReportsDetailPanel {...this.props} />;
    }
}

const mapStateToProps = (state, ownProps) => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(StoredLayout(ReportsByMonthDetails));
