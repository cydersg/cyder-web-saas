import React from 'react';
import { Button, Col } from 'reactstrap';
import { layoutLib } from 'js/constlib';
const isMobile = window.innerWidth <= layoutLib.mobileWidth;

const ButtonPrimaryRoundImage = ({ onClick, faIcon, disabled, style, label }) => {
    return (
        <Col className="mycorp-col-big-icon">
            <Button
                disabled={disabled}
                style={style}
                className={disabled ? 'mycorp-add-button-circle-disabled ' : ' mycorp-add-button-circle'}
                onClick={onClick}
                color="primary"
            >
                {faIcon && <i className={`fa fa-${faIcon} fa-lg mycorp-big-icon`}></i>}
            </Button>
            {label && <div className="mycorp-col-big-text">{label}</div>}
        </Col>
    );
};

export default ButtonPrimaryRoundImage;
