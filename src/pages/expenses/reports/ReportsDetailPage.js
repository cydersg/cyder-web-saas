import React, { Component } from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';
import ReportsDetailPanel from './ReportsDetailPanel';

class ReportDetailPage extends Component {
    render() {
        return <ReportsDetailPanel {...this.props} />;
    }
}

const mapStateToProps = (state, ownProps) => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(StoredLayout(ReportDetailPage));
