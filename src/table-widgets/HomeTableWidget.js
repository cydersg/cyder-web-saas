/**
 * Home Table dashboard
 * 
 * Author: Fernando
 */

import React from 'react'
import HomeDashboard from './HomeDashboard'
import homenotifications from '../json/home-notification.json'

const HomeTableWidget = () => <HomeDashboard items={homenotifications} />

export default HomeTableWidget
