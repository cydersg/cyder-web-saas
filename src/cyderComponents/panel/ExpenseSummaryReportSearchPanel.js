/**
 * Expense summary report search panel
 * Common table used for reporting
 *
 * Author: Fenando Karnagi
 */
import React from 'react';
import { connect } from 'react-redux';

import { Col, Row, Container, Button, Label } from 'reactstrap';
import CyderDatePicker from 'cyderComponents/forms/CyderDatePicker';
import { CyderSelect } from 'cyderComponents/input/index';

import { expenseStartDateChanged, expenseEndDateChanged, expenseStatusChanged } from 'actions/report/expenseReportAction';
import { layoutLib } from 'js/constlib';

import '../../css/forms/default-forms.css';

class ExpenseSummaryReportSearchPanel extends React.Component {
    render() {
        this.statuses = [
            {
                value: 'Saved',
                label: 'Draft',
            },
            {
                value: 'Submitted',
                label: 'Submitted',
            },
            {
                value: 'Approved',
                label: 'Approved',
            },
            {
                value: 'Returned',
                label: 'Returned',
            },
            {
                value: 'Paid',
                label: 'Paid',
            },
        ];

        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        return (
            <Container className="pb-4">
                <Row>
                    <Col xs={12} md={3} className={isMobile ? 'mb10' : null}>
                        <Label className="text-bold">Start Date</Label>
                        <CyderDatePicker
                            onChange={event => this.props.onStartDateChanged(event)}
                            value={this.props.report.search.startDate}
                            placeholder="DD/MM/YYYY"
                            dateFormat="DD/MM/YYYY"
                            timeFormat={false}
                        />
                    </Col>
                    <Col xs={12} md={3} className={isMobile ? 'mb10' : null}>
                        <Label className="text-bold">End Date</Label>
                        <CyderDatePicker
                            onChange={event => this.props.onEndDateChanged(event)}
                            value={this.props.report.search.endDate}
                            placeholder="DD/MM/YYYY"
                            dateFormat="DD/MM/YYYY"
                            timeFormat={false}
                        />
                    </Col>
                    <Col xs={12} md={3} className={isMobile ? 'mb10' : null}>
                        <CyderSelect
                            label="Status"
                            formGroupClassName="m-0"
                            inputProps={{
                                autosize: false,
                                placeholder: 'Select status',
                                autofocus: true,
                                options: this.statuses,
                                simpleValue: true,
                                clearable: this.props.report.search.clearable,
                                name: 'selected-status',
                                disabled: this.props.report.search.disabled,
                                value: this.props.report.search.status.selectValue,
                                onChange: this.props.onStatusChanged,
                                searchable: this.props.report.search.searchable,
                            }}
                        />
                    </Col>
                    <Col xs={12} md={3} className="d-flex align-items-end">
                        <Button onClick={this.props.search} tabIndex="-1" className={isMobile ? 'w-100p' : null}>
                            Search
                        </Button>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        ownProps,
        report: state.cyderReportExpenseReducer,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onStatusChanged: newValue => {
            dispatch(expenseStatusChanged(newValue));
        },
        onStartDateChanged: newValue => {
            dispatch(expenseStartDateChanged(newValue));
        },
        onEndDateChanged: newValue => {
            dispatch(expenseEndDateChanged(newValue));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExpenseSummaryReportSearchPanel);
