import React, { Fragment, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import StoredLayout from 'layouts/StoredLayout';

import ReactTable from 'react-table';
import { Container, Badge, FormGroup, Form, Row, Col } from 'reactstrap';
import { CyderInput } from 'cyderComponents/input/index';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import LinkButton from 'cyderComponents/buttons/LinkButton';
import { TablePagination } from 'cyderComponents/pagination/';

import { getStaffLeaveBalance, updateStaffLeaveBalance, generateLeaveBalance } from 'actions/timesheet/staffLeaveBalanceAction';

import ActionExecutor from 'js/ActionExecutor';
import cyderlib from 'js/cyderlib';

function getTableColumns(onEditClicked) {
    const columns = [
        {
            Header: 'Code',
            accessor: 'code',
        },
        {
            Header: 'Description',
            accessor: 'description',
        },
        {
            Header: 'Entitlement',
            accessor: 'default_entitlement',
        },
        {
            Header: 'Balance',
            accessor: 'balance',
        },
        {
            Header: 'Carry Amount',
            accessor: 'amount_carry_forward',
        },
        {
            Header: 'Period',
            accessor: 'period',
        },
        {
            Header: 'Approval',
            accessor: 'requireapproval',
            Cell: props => {
                const label = props.value === 'Y' ? 'Required' : 'Not Required';
                return <Badge color={cyderlib.statusBadgeColor(props.value)}>{label}</Badge>;
            },
        },
        {
            Header: '',
            accessor: 'id',
            width: 130,
            Cell: props => {
                return (
                    <div className="btn">
                        <i className="material-icons" onClick={() => onEditClicked(props.value)}>
                            edit
                        </i>
                    </div>
                );
            },
        },
    ];
    return columns;
}

function useCallEffects(callback) {
    useEffect(callback, []);
}

function StaffLeaveBalancePage(props) {
    // STATES
    const { email } = props.match.params;
    const [loading, setLoading] = useState(false);
    const [leaveBalance, setLeaveBalance] = useState([]);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [modalData, setModalData] = useState({
        modalHeader: email ? 'Leave Balance' : 'Apply Leave',
        modalBody: '',
        onlyOneButton: false,
        positiveButtonText: email ? 'Update' : 'Submit',
        updateLeaveBalance: false,
    });
    const [updateLeaveBalanceDataStore, setUpdateLeaveBalanceData] = useState({
        id: null,
        code: '',
        balance: 0,
    });

    // METHODS
    const onEditClicked = leaveBalanceId => {
        const data = leaveBalance.find(x => x.id === leaveBalanceId);
        const { code, balance } = data;
        setUpdateLeaveBalanceData({
            code,
            balance,
        });
        setIsModalOpen(!isModalOpen);
    };

    const onUpdateTextChanged = e => {
        const { value, id } = e.target;
        let inputDataClone = Object.assign({}, updateLeaveBalanceDataStore);
        inputDataClone[id] = value;
        setUpdateLeaveBalanceData(inputDataClone);
    };

    const getStaffLeaveBalance = () => {
        const callback = async () => {
            const { data } = await props.getStaffLeaveBalance(email);
            if (data) setLeaveBalance(data);
        };
        ActionExecutor.execute(setLoading, callback);
    };

    const updateStaffLeaveBalance = () => {
        const callback = async () => {
            const { code, balance } = updateLeaveBalanceDataStore;
            await props.updateStaffLeaveBalance(email, balance, code);
            getStaffLeaveBalance();
        };
        ActionExecutor.execute(setLoading, callback).then(() => getStaffLeaveBalance());
    };

    const generateLeaveBalance = () => {
        const callback = async () => {
            await props.generateLeaveBalance(email);
            getStaffLeaveBalance();
        };
        ActionExecutor.execute(setLoading, callback).then(() => getStaffLeaveBalance());
    };

    const negativeButtonAction = () => {
        setIsModalOpen(!isModalOpen);
        resetModal();
    };

    const positiveButtonAction = () => {
        // STAFF UPDATE LEAVE BALANCE
        if (!modalData.updateLeaveBalance) {
            updateStaffLeaveBalance();
            getStaffLeaveBalance();
            setIsModalOpen(!isModalOpen);
            return;
        }
        // STAFF RESET LEAVE BALANCE
        if (modalData.updateLeaveBalance) {
            generateLeaveBalance();
            setIsModalOpen(!isModalOpen);
            resetModal();
            return;
        }
    };

    const resetModal = () => {
        setModalData({
            modalHeader: 'Leave Balance',
            modalBody: '',
            onlyOneButton: false,
            positiveButtonText: 'Update',
            updateLeaveBalance: false,
        });
    };

    const effects = () => {
        getStaffLeaveBalance();
    };

    // EFFECTS
    useCallEffects(effects);

    // RENDER
    const customModalBody = <UpdateLeaveBalanceForm data={updateLeaveBalanceDataStore} onTextChanged={onUpdateTextChanged} />;
    return (
        <Fragment>
            <DialogModal
                key="actionModal"
                modal={isModalOpen}
                toggle={setIsModalOpen}
                customHeader={modalData.modalHeader}
                customModalBody={modalData.modalBody || customModalBody}
                onlyOneButton={modalData.onlyOneButton}
                positiveButtonText={modalData.positiveButtonText}
                negativeButtonAction={negativeButtonAction}
                positiveButtonAction={positiveButtonAction}
            />
            <Container>
                <Row>
                    <Col xs="6" className="mb-4">
                        <Form inline>
                            <Link to="../leavebalance" tabIndex="-1">
                                <i className="align-middle material-icons material-icons-3x">chevron_left</i>
                            </Link>
                        </Form>
                    </Col>
                </Row>
                <Row className="mb-2">
                    <Col className="d-flex">
                        <LinkButton
                            color="success"
                            altColor="light"
                            className="ml-auto"
                            materialIcon="update"
                            text="Reset Leave Balance"
                            onClick={() => {
                                setModalData({
                                    modalHeader: 'Reset Leave Balance',
                                    modalBody: 'Are you sure you want to reset leave balance?',
                                    onlyOneButton: false,
                                    positiveButtonText: 'Update',
                                    updateLeaveBalance: true,
                                });
                                setIsModalOpen(!isModalOpen);
                            }}
                        />
                    </Col>
                </Row>
                <br />
                <RowColWrapper>
                    {loading ? (
                        <LoadingSpinner />
                    ) : (
                        <ReactTable
                            className="-highlight mb-2"
                            data={leaveBalance}
                            columns={getTableColumns(onEditClicked)}
                            pageSize={leaveBalance.length}
                            PaginationComponent={TablePagination}
                        />
                    )}
                </RowColWrapper>
            </Container>
        </Fragment>
    );
}

// FOR STAFF LEAVE BALANCE
const UpdateLeaveBalanceForm = props => {
    const { onTextChanged = () => {}, data } = props;
    return (
        <FormGroup>
            <CyderInput
                label="Code"
                inputProps={{
                    type: 'text',
                    id: 'code',
                    name: 'code',
                    disabled: true,
                    onChange: onTextChanged,
                    value: data.code,
                }}
            />
            <CyderInput
                label="Balance"
                inputProps={{
                    type: 'number',
                    id: 'balance',
                    name: 'balance',
                    onChange: onTextChanged,
                    value: data.balance,
                }}
            />
        </FormGroup>
    );
};

const mapDispatchToProps = dispatch => {
    return {
        getStaffLeaveBalance: owner => {
            return dispatch(getStaffLeaveBalance(owner));
        },
        updateStaffLeaveBalance: (owner, balance, leaveTypeCode) => {
            return dispatch(updateStaffLeaveBalance(owner, balance, leaveTypeCode));
        },
        generateLeaveBalance: owner => {
            return dispatch(generateLeaveBalance(owner));
        },
    };
};

export default connect(
    () => ({}),
    mapDispatchToProps,
)(StoredLayout(StaffLeaveBalancePage));
