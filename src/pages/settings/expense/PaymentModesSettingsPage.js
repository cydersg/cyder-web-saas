import React from 'react';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import StoredLayout from 'layouts/StoredLayout';

import ReactTable from 'react-table';
import LinkButton from 'cyderComponents/buttons/LinkButton';
import { Alert, Input, Label, Button, Container, Row, Col, FormGroup } from 'reactstrap';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

import {
    cyderPaymentModesSettingsSearchAction,
    cyderPaymentModesSettingsEditModalSetPaymentMode,
    cyderPaymentModesSettingsModalToggleAction,
    cyderPaymentModesSettingsEditModalChangeValue,
    cyderPaymentModesSettingsAddPaymentModeAction,
    cyderPaymentModesSettingsUpdatePaymentModeAction,
    cyderPaymentModesSettingsDeletePaymentModeAction,
} from 'actions/settings/cyderPaymentModesSettingsSearchAction';
import DialogModal from 'cyderComponents/modals/DialogModal';
import { setJumbotronTitle } from 'actions/pagedata';

import reactTableFilter from 'js/reactTableFilter';
import cyderlib from 'js/cyderlib';

class PaymentModesSettingsPage extends React.Component {
    constructor(props) {
        super(props);
        this.onFilteredChange = this.onFilteredChange.bind(this);
        this.filterAllFunc = this.filterAllFunc.bind(this);
        this.state = {
            filterAll: '',
        };
        this.getOptions = this.getOptions.bind(this);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.toggleAction = this.toggleAction.bind(this);
        this.validateThenSearch = this.validateThenSearch.bind(this);
    }
    componentDidMount() {
        if (this.props.forceRefresh) {
            this.validateThenSearch();
        }
        this.validateThenSearch();
        // this.props.getAllPaymentModes();
    }
    handleClick(user) {
        console.log(user);
    }
    validateThenSearch(e) {
        if (e) {
            e.preventDefault();
        }
        // this.props.clearErrorMessage();

        const body = {
            keyword: '',
            status: 'Active',
        };
        this.props.searchPaymentModes(body);
    }
    setupEditModal(project) {
        this.props.setModalPaymentMode(project);
        this.props.toggleModal('update');
    }
    setupDeleteModal(project) {
        this.props.setModalPaymentMode(project);
        this.props.toggleModal('delete');
    }
    getOptions(input) {
        // const username = document.getElementById('searchUsername').value;
        return this.props.getOptions(input, this.props.usersReference);
    }
    toggleAction() {
        this.handleToggleModal();
        if (this.props.modalAction === 'refresh') {
            this.validateThenSearch();
        }
    }
    calcCurrentPositiveButtonText() {
        if (this.props.modalAction === 'update') {
            return 'Update';
        } else if (this.props.modalAction === 'add') {
            return 'Add';
        } else if (this.props.modalAction === 'delete') {
            return 'Delete';
        } else if (this.props.modalAction === 'close') {
            return 'Ok';
        } else {
            return 'Ok';
        }
    }
    calcCurrentPositiveButtonAction() {
        if (this.props.modalAction === 'update') {
            return this.props.handleUpdate;
        } else if (this.props.modalAction === 'refresh') {
            return () => {
                this.handleToggleModal();
                this.validateThenSearch();
            };
        } else if (this.props.modalAction === 'add') {
            return this.props.handleAdd;
        } else if (this.props.modalAction === 'delete') {
            return this.props.handleDelete;
        } else if (this.props.modalAction === 'close') {
            return this.handleToggleModal;
        } else {
            return this.handleToggleModal;
        }
    }
    handleToggleModal(modalAction) {
        this.props.toggleModal(modalAction);
    }
    onFilteredChange(filtered) {
        this.setState(reactTableFilter.onFilteredChange(filtered));
    }
    filterAllFunc(e) {
        this.setState(reactTableFilter.filterAllFunc(e));
    }
    render() {
        const customModalBody = (
            <div>
                <FormGroup>
                    <Label for="exampleText">Payment Mode Code</Label>
                    <Input
                        onChange={event => this.props.changeModalTextAreaValue(event)}
                        disabled={this.props.modalAction === 'update'}
                        type="text"
                        maxLength={10}
                        id="code"
                        value={this.props.modalPaymentMode.code}
                    />
                    <Label for="exampleText">Payment Mode Name</Label>
                    <Input
                        onChange={event => this.props.changeModalTextAreaValue(event)}
                        type="text"
                        maxLength={100}
                        id="name"
                        value={this.props.modalPaymentMode.name}
                    />
                </FormGroup>
            </div>
        );
        const columns = [
            reactTableFilter.generateAllColumnFilter(),
            {
                Header: 'Payment Mode Code',
                accessor: 'code',
                filterMethod: cyderlib.filterIgnoreCase,
            },
            {
                Header: 'Payment Mode Name',
                accessor: 'name',
                filterMethod: cyderlib.filterIgnoreCase,
            },
            {
                Header: '',
                accessor: 'id',
                filterable: false,
                Cell: props => {
                    const thisPaymentMode = this.props.paymentmodes[props.index];
                    return (
                        <div className="float-right">
                            <Button
                                color="link"
                                size="sm"
                                onClick={() => {
                                    this.setupEditModal(thisPaymentMode);
                                }}
                            >
                                <i className="align-middle material-icons">edit</i>
                            </Button>
                            <Button
                                color="link"
                                className="ml-1"
                                size="sm"
                                onClick={() => {
                                    this.setupDeleteModal(thisPaymentMode);
                                }}
                            >
                                <i className="text-danger align-middle material-icons">delete</i>
                            </Button>
                        </div>
                    );
                },
            },
        ];
        return (
            <div key={0}>
                <Container>
                    <Row className="mb-2">
                        <Col className="d-flex">
                            <Input className="w-300" placeholder="Search" onChange={this.filterAllFunc} value={this.state.filterAll} />
                            <LinkButton preset="addButton" onClick={() => this.handleToggleModal('add')} text="Payment Mode" />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            {this.props.loading ? (
                                <LoadingSpinner center />
                            ) : this.props.paymentmodes.length > 0 ? (
                                <ReactTable
                                    filtered={this.state.filtered}
                                    onFilteredChange={this.onFilteredChange}
                                    filterable={false}
                                    defaultFilterMethod={reactTableFilter.defaultFilterMethod}
                                    className="-highlight mb-2"
                                    data={this.props.paymentmodes}
                                    columns={columns}
                                    minRows={0}
                                    showPagination={false}
                                />
                            ) : this.props.errorMessage ? (
                                <Alert color="danger">{this.props.errorMessage}</Alert>
                            ) : null}
                        </Col>
                    </Row>
                </Container>
                <DialogModal
                    key="modal"
                    modal={this.props.modalOpen}
                    toggle={this.toggleAction}
                    customHeader={this.props.modalHeader}
                    customModalBody={
                        this.props.saving ? <LoadingSpinner /> : this.props.modalMessage ? this.props.modalMessage : customModalBody
                    }
                    positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                    positiveButtonText={this.calcCurrentPositiveButtonText()}
                    onlyOneButton={this.props.saved}
                    buttonDisabler={this.props.saving}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ...state.cyderPaymentModesSettingsSearchReducer,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        searchPaymentModes: body => {
            dispatch(cyderPaymentModesSettingsSearchAction(body)).then(res => {
                console.log(res);
                console.log('this is a promise');
            });
        },
        setModalPaymentMode: project => {
            // TODO: change to id, code, name
            // dispatch(cyderPaymentModesSettingsEditModalSetPaymentMode(project.id, project.status, project.email));
            dispatch(cyderPaymentModesSettingsEditModalSetPaymentMode(project.id, project.code, project.name));
        },
        toggleModal: modalAction => {
            if (modalAction === 'add') {
                // dispatch(cyderPaymentModesSettingsEditModalSetPaymentMode("", "", "", [{id: 'aaa'},{id: 'bbb'},{id: 'ccc'},{id: 'ddd'}]))
                dispatch(cyderPaymentModesSettingsEditModalSetPaymentMode('', '', '', []));
            }
            dispatch(cyderPaymentModesSettingsModalToggleAction(modalAction ? modalAction : null));
        },
        changeModalTextAreaValue: event => {
            const value = event.target.value;
            const key = event.target.id;
            dispatch(cyderPaymentModesSettingsEditModalChangeValue(key, value));
        },
        getOptions: input => {
            // const username = document.getElementById('searchUsername').value;
            console.log(input);
            return Promise.resolve(['a', 'b', 'c', 'd']);
        },
        handleAdd: () => {
            dispatch(cyderPaymentModesSettingsAddPaymentModeAction());
        },
        handleUpdate: () => {
            dispatch(cyderPaymentModesSettingsUpdatePaymentModeAction());
        },
        handleDelete: () => dispatch(cyderPaymentModesSettingsDeletePaymentModeAction()),
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Group Detail')),
    };
};
// translate() is if we want to use HOC to perform t()
export default withTranslation()(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(StoredLayout(PaymentModesSettingsPage)),
);
