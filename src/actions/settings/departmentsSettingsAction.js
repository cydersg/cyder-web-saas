import {
    SETTINGS_DEPARTMENTS_SEARCH_SUCCESS,
    SETTINGS_DEPARTMENTS_SEARCH_REQUEST,
    SETTINGS_DEPARTMENTS_SEARCH_FAILURE,
    SETTINGS_DEPARTMENTS_SEARCH_SET_FORCE_REFRESH_FLAG,
    SETTINGS_DEPARTMENTS_MODAL_SET_PROJECT,
    SETTINGS_DEPARTMENTS_MODAL_TOGGLE,
    SETTINGS_DEPARTMENTS_MODAL_CHANGE_VALUE,
    SETTINGS_DEPARTMENTS_PROJECT_ADD_REQUEST,
    SETTINGS_DEPARTMENTS_PROJECT_ADD_SUCCESS,
    SETTINGS_DEPARTMENTS_PROJECT_ADD_FAILURE,
    SETTINGS_DEPARTMENTS_PROJECT_UPDATE_REQUEST,
    SETTINGS_DEPARTMENTS_PROJECT_UPDATE_SUCCESS,
    SETTINGS_DEPARTMENTS_PROJECT_UPDATE_FAILURE,
    SETTINGS_DEPARTMENTS_PROJECT_DELETE_REQUEST,
    SETTINGS_DEPARTMENTS_PROJECT_DELETE_SUCCESS,
    SETTINGS_DEPARTMENTS_PROJECT_DELETE_FAILURE,
} from '../constants/actionTypesSettings';
import withQuery from 'with-query';

import { DJARVIS_COMMON_API_ROOT } from '../../config';

export function departmentsSettingsSearchAction(body, searchbox) {
    if (!body) {
        body = {
            keyword: '',
            status: 'Active',
        };
    }
    // Redux Thunk will inject dispatch here:
    return (dispatch, getState) => {
        // Reducers may handle this to set a flag like isFetching
        dispatch({ type: SETTINGS_DEPARTMENTS_SEARCH_REQUEST });
        dispatch({ type: SETTINGS_DEPARTMENTS_SEARCH_SET_FORCE_REFRESH_FLAG, val: false });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/department', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'partialsearch',
        });
        // const bodyWithAppId = Object.assign({appId: getState().cyderProfileReducer.profile.appId}, body);
        const options = {
            body: JSON.stringify(body),
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: SETTINGS_DEPARTMENTS_SEARCH_FAILURE, error });
                        return error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: SETTINGS_DEPARTMENTS_SEARCH_SUCCESS, response });
                        if (searchbox) {
                            let newResponse = [];
                            response.forEach(dpt => {
                                newResponse.push({
                                    value: dpt.code,
                                    label: dpt.code + ' ' + dpt.name,
                                });
                            });
                            return newResponse;
                        }
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: SETTINGS_DEPARTMENTS_SEARCH_FAILURE, error });
                return error;
            });
    };
}
export function departmentsSettingsEditModalSetDepartment(modalDepartment) {
    return {
        ...modalDepartment,
        type: SETTINGS_DEPARTMENTS_MODAL_SET_PROJECT,
    };
}
export function departmentsSettingsModalToggleAction(modalAction) {
    return {
        type: SETTINGS_DEPARTMENTS_MODAL_TOGGLE,
        modalAction,
    };
}
export function departmentsSettingsEditModalChangeValue(key, value) {
    return {
        type: SETTINGS_DEPARTMENTS_MODAL_CHANGE_VALUE,
        key,
        value,
    };
}
export function departmentsSettingsAddDepartmentAction(approvers) {
    return (dispatch, getState) => {
        dispatch({ type: SETTINGS_DEPARTMENTS_PROJECT_ADD_REQUEST });

        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/department', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'add',
        });
        let approverList = Object.values(approvers);
        let approversParsed = '';
        let approversArray = [];
        if (approverList.length === 0 || approverList.indexOf(null) !== -1) {
            approversParsed = '';
        } else {
            approverList.forEach((approver, i) => {
                approversArray.push(approver.value);
                approversParsed = approversParsed + approver.value;
                if (i < approverList.length - 1) {
                    approversParsed = approversParsed + ',';
                }
            });
        }
        const body = Object.assign(
            {
                ...getState().departmentSettingsReducer.modalDepartment,
            },
            {
                approvers: approversArray,
            },
        );
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: SETTINGS_DEPARTMENTS_PROJECT_ADD_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: SETTINGS_DEPARTMENTS_PROJECT_ADD_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: SETTINGS_DEPARTMENTS_PROJECT_ADD_FAILURE, error });
                return error;
            });
    };
}
export function departmentsSettingsUpdateDepartmentAction(approvers) {
    return (dispatch, getState) => {
        dispatch({ type: SETTINGS_DEPARTMENTS_PROJECT_UPDATE_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/department', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'update',
        });
        const modalDepartment = getState().departmentSettingsReducer.modalDepartment;
        let approverList = Object.values(approvers);
        let approversArray = [];
        if (approverList.length === 0 || approverList.indexOf(null) !== -1) {
            approversArray = [];
        } else {
            approverList.forEach(approver => {
                approversArray.push(approver.value);
            });
        }
        const body = {
            code: modalDepartment.code,
            name: modalDepartment.name,
            users: modalDepartment.users,
            approvers: approversArray,
            overrideexpenseapproval: modalDepartment.overrideexpenseapproval,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: SETTINGS_DEPARTMENTS_PROJECT_UPDATE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: SETTINGS_DEPARTMENTS_PROJECT_UPDATE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: SETTINGS_DEPARTMENTS_PROJECT_UPDATE_FAILURE, error });
                return error;
            });
    };
}
export function departmentsSettingsDeleteDepartmentAction() {
    return (dispatch, getState) => {
        dispatch({ type: SETTINGS_DEPARTMENTS_PROJECT_DELETE_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/department', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'delete',
        });
        const code = getState().departmentSettingsReducer.modalDepartment.code;
        console.log(code);
        const body = {
            code,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: SETTINGS_DEPARTMENTS_PROJECT_DELETE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: SETTINGS_DEPARTMENTS_PROJECT_DELETE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: SETTINGS_DEPARTMENTS_PROJECT_DELETE_FAILURE, error });
                return error;
            });
    };
}
