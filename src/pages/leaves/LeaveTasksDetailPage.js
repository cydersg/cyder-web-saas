import React, { Component, useState, useEffect, useCallback } from 'react';
import qs from 'query-string';
import moment from 'moment';
import history from 'history.js';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { CyderFileUploader } from 'cyderComponents/input';

import StoredLayout from 'layouts/StoredLayout';
import { Button, Container, Row, Col, Badge, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { CyderInput } from 'cyderComponents/input/index';
import DialogModal from 'cyderComponents/modals/DialogModal';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';

import { setJumbotronTitle } from 'actions/pagedata';
import * as Action from 'actions/leaves/leaveTaskAction';
import { cyderUsersSearchAction } from 'actions/users/cyderUsersSearchAction';
import { getAllHolidays } from 'actions/settings/cyderLeaveHolidaysAction';

import ActionExecutor from 'js/ActionExecutor';
import { capitalize, calNumOfDays } from 'js/generalUtils';
import cyderlib from 'js/cyderlib';
import { layoutLib } from 'js/constlib';

// import WorkflowHistoryTable from 'cyderComponents/common/WorkflowHistoryTable';
// import { expenseNewReceiptClaimGetHistory } from 'actions/expenses/cyderExpenseNewReceiptClaimAction';

const { getTaskDetail, rejectLeaveTask, approveLeaveTask } = Action;

class LeaveTaskDetailPage extends Component {
    state = {
        loading: false,
        taskDetail: {},
        users: [],
        // Modal
        modalLoading: false,
        isModalFormOpen: false,
        modalFormConfig: {
            modalAction: null,
            modalType: '', // Either 'approve' or 'reject'
        },
        isModalOpen: false,
        modalConfig: {
            modalHeader: 'Confirmation',
            modaleBody: '',
            onlyOneButton: false,
        },
    };

    componentDidMount() {
        const { taskid } = this.props.match.params;
        if (taskid) this.getTaskDetails(taskid);
        this.props.setJumbotronTitle('Leave Info');
        this.getUsers();
    }

    showLoading = loading => {
        this.setState({ loading });
    };

    showModalLodaing = modalLoading => {
        this.setState({ modalLoading });
    };

    getTaskDetails = taskid => {
        const callback = async () => {
            const { data } = await this.props.getTaskDetail(taskid);
            this.setState({ taskDetail: data });
        };
        ActionExecutor.execute(this.showLoading, callback);
    };

    getUsers = () => {
        const callback = async () => {
            const data = await this.props.cyderUsersSearchAction({ keyword: '', status: 'Active' });
            this.setState({ users: data });
        };
        ActionExecutor.execute(this.showLoading, callback);
    };

    onApprove = () => {
        const { location, approveLeaveTask } = this.props;

        this.showModalForm('approve', remarks => {
            const callback = async () => {
                const realid = qs.parse(location.search.substring(1)).realid;
                const { ok, status } = await approveLeaveTask(realid, remarks);
                this.toggleModalForm();

                // Modal Alert
                if (ok && status === 200) {
                    this.showModal(
                        'Confirmation',
                        'Successfully approved leave task.',
                        () =>
                            history.push(
                                `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'leavetype'}/leaves/leaveapproval`,
                            ),
                        true,
                    );
                    return;
                }
                this.showModal('Error', 'Failed to approve leave task.', null, true);
            };
            ActionExecutor.execute(this.showModalLodaing, callback);
        });
    };

    onReject = () => {
        const { location, rejectLeaveTask } = this.props;

        this.showModalForm('reject', remarks => {
            const callback = async () => {
                const realid = qs.parse(location.search.substring(1)).realid;
                const { ok, status } = await rejectLeaveTask(realid, remarks);
                this.toggleModalForm();

                // Modal Alert
                if (ok && status === 200) {
                    this.showModal(
                        'Confirmation',
                        'Successfully rejected leave task.',
                        () =>
                            history.push(
                                `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'leavetype'}/leaves/leaveapproval`,
                            ),
                        true,
                    );
                    return;
                }
                this.showModal('Error', 'Failed to reject leave task.', null, true);
            };
            ActionExecutor.execute(this.showModalLodaing, callback);
        });
    };

    toggleModalForm = () => {
        this.setState({ isModalFormOpen: !this.state.isModalFormOpen });
    };

    showModalForm = (type, action) => {
        const modalFormConfig = {
            ...this.state.modalFormConfig,
            modalAction: action,
            modalType: type,
        };
        this.setState({ modalFormConfig });
        this.toggleModalForm();
    };

    toggleModal = () => {
        this.setState({ isModalOpen: !this.state.isModalOpen });
    };

    showModal = (header, body, action, onlyOneButton) => {
        const modalConfig = {
            ...this.state.modalConfig,
            modalHeader: header,
            modalBody: body,
            modalAction: action,
            onlyOneButton,
        };
        this.setState({ modalConfig });
        this.toggleModal();
    };

    render() {
        const { taskDetail, users, modalFormConfig, isModalFormOpen, isModalOpen, modalConfig, modalLoading } = this.state;

        const userInfo = users.find(x => x.email === taskDetail.owner) || {};
        return (
            <div>
                <ModalForm
                    isOpen={isModalFormOpen}
                    loading={modalLoading}
                    toggle={this.toggleModalForm}
                    type={modalFormConfig.modalType}
                    onSubmit={modalFormConfig.modalAction}
                />
                <DialogModal
                    key="confirmModal"
                    modal={isModalOpen}
                    toggle={this.toggleModal}
                    customHeader={modalConfig.modalHeader}
                    customModalBody={modalConfig.modalBody}
                    positiveButtonAction={modalConfig.modalAction}
                    onlyOneButton={modalConfig.onlyOneButton}
                />
                <Container>
                    <RowColWrapper rowProps={{ className: 'mb-2 mb-4' }}>
                        <Link to="../../leaveapproval" tabIndex="-1">
                            <i className="align-middle material-icons material-icons-3x">chevron_left</i>
                        </Link>
                    </RowColWrapper>
                    <DetailPanel
                        {...this.props}
                        taskDetail={taskDetail}
                        userInfo={userInfo}
                        onApprove={this.onApprove}
                        onReject={this.onReject}
                    />
                </Container>
            </div>
        );
    }
}

function DetailPanel(props) {
    const { taskDetail, userInfo, onApprove, onReject } = props;
    const [holidays, setHolidays] = useState({});

    const formatDate = date => {
        if (!date) return 'Not date found';
        return moment(date, 'YYYY-MM-DD').format('DD MMM YYYY');
    };

    const getAllHolidays = async year => {
        const callback = async () => {
            const { data } = await props.getAllHolidays(year);
            return data;
        };
        return ActionExecutor.execute(() => {}, callback);
    };

    const getHolidays = async (from, to) => {
        if (!from || !to) return;
        const fromYear = from.format('YYYY');
        const toYear = to.format('YYYY');
        const fromHolidays = await getAllHolidays(fromYear);
        const toHolidays = fromYear !== toYear ? await getAllHolidays(toYear) : [];
        const [numOfDays, totalNumOfDays, deductedNumOfDays, deductedList] = calNumOfDays(from, to, [...fromHolidays, ...toHolidays]);

        setHolidays({
            numOfDays,
            totalNumOfDays,
            deductedNumOfDays,
            holidays: deductedList.holidaysList,
            weekends: deductedList.weekendsList,
        });
    };

    useEffect(() => {
        const { fromDate, toDate } = taskDetail;
        const from = fromDate && moment(fromDate, 'YYYY-MM-DD');
        const to = toDate && moment(toDate, 'YYYY-MM-DD');
        getHolidays(from, to);
    }, [taskDetail]);

    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return (
        <div className="wideContainer">
            <Row>
                <Col>
                    <h2 className="font-weight-bold d-none d-md-block">{taskDetail.leaveTypeCode}</h2>
                    <h4 className="font-weight-bold d-block d-md-none">{taskDetail.leaveTypeCode}</h4>
                    <Badge color={cyderlib.statusBadgeColor(taskDetail.status)}>{taskDetail.status}</Badge>
                </Col>
            </Row>
            <br />
            <Row>
                <Col xs="12">
                    <Row>
                        <Col xs="12" sm="4">
                            <InfoField label="From" value={formatDate(taskDetail.fromDate) + ' - ' + taskDetail.fromSession} />
                        </Col>
                        <Col xs="12" sm="4">
                            <InfoField label="To" value={formatDate(taskDetail.toDate) + ' - ' + taskDetail.toSession} />
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row>
                <Col xs="12" sm="4">
                    <InfoField label="Owner" value={userInfo.firstname + ' ' + userInfo.lastname} />
                </Col>
                <Col xs="12" sm="4">
                    <InfoField label="Number of Days" value={taskDetail.numOfDays} />
                </Col>
            </Row>
            <Row>
                <Col xs="12" sm="4">
                    <InfoField label="Email" value={taskDetail.owner} />
                </Col>
                <Col xs="12" sm="4">
                    <InfoField
                        label="Holidays in between"
                        value={
                            holidays.holidays && holidays.holidays.length > 0
                                ? holidays.holidays.map((x, i) => <div key={i}>{x.holidayname}</div>)
                                : '-'
                        }
                    />
                </Col>
            </Row>
            <Row>
                <Col xs="6">
                    <InfoField label="Remarks" value={taskDetail.remarks} />
                </Col>
            </Row>
            {taskDetail.leaveTypeCode === 'MEDICAL' ? (
                <Row>
                    <Col xs="6">
                        <InfoField label="Attachment">
                            <h6 className="font-weight-bold">
                                {taskDetail.Body ? (
                                    <CyderFileUploader
                                        disabled
                                        id={taskDetail.filename}
                                        imageFile={{
                                            b64img: taskDetail.Body,
                                        }}
                                    ></CyderFileUploader>
                                ) : (
                                    'No attachment file'
                                )}
                            </h6>
                        </InfoField>
                    </Col>
                </Row>
            ) : null}
            <br />
            <Row>
                <Col xs="12" sm="4">
                    <Button color="success" className={isMobile ? 'w-100p mb10' : 'm-2'} onClick={onApprove}>
                        Approve
                    </Button>
                    <Button color="danger" className={isMobile ? 'w-100p mb10' : 'm-2'} onClick={onReject}>
                        Reject
                    </Button>
                </Col>
            </Row>
        </div>
    );
}

function ModalForm(props) {
    const { isOpen, loading, toggle, type, onSubmit } = props;
    const [remarks, setRemarks] = useState('');

    return (
        <Modal isOpen={isOpen}>
            <ModalHeader className="border-0" toggle={toggle}>
                <b>{capitalize(type)} Leave Task</b>
            </ModalHeader>
            <ModalBody>
                {loading ? (
                    <LoadingSpinner />
                ) : (
                    <div>
                        <RowColWrapper>Are you sure you want to {type} this leave task?</RowColWrapper>
                        {type !== 'approve' && (
                            <CyderInput
                                inputProps={{
                                    rows: 4,
                                    type: 'textarea',
                                    placeholder: 'Please provide your remarks',
                                    value: remarks,
                                    onChange: e => setRemarks(e.target.value),
                                }}
                            />
                        )}
                    </div>
                )}
            </ModalBody>
            <ModalFooter>
                <Button color="success" onClick={() => onSubmit(remarks)}>
                    {type === 'approve' ? 'Approve' : 'Reject'}
                </Button>{' '}
                <Button color="light" className="color-danger" onClick={toggle}>
                    Cancel
                </Button>
            </ModalFooter>
        </Modal>
    );
}

const InfoField = props => (
    <div>
        <span className="text-muted font-weight-bold">{props.label}</span>
        <h6 className="font-weight-bold">{props.value} </h6>
    </div>
);

const mapStateToProps = state => {
    return {};
};

const actionlist = {
    getAllHolidays,
    getTaskDetail,
    setJumbotronTitle,
    cyderUsersSearchAction,
    rejectLeaveTask,
    approveLeaveTask,
};

export default withTranslation()(connect(mapStateToProps, actionlist)(StoredLayout(LeaveTaskDetailPage)));
