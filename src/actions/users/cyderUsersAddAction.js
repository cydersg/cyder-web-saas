import withQuery from 'with-query';
import {
    CYDER_USERS_ADD_CHANGE_FIELD,
    CYDER_USERS_ADD_ADD_USER_REQUEST,
    CYDER_USERS_ADD_ADD_USER_SUCCESS,
    CYDER_USERS_ADD_ADD_USER_FAILURE,
    CYDER_USERS_ADD_ADD_USER_FORCE_VALIDATE,
    CYDER_USERS_ADD_MESSAGE_MODAL_TOGGLE,
    CYDER_USERS_ADD_CLEAR_FIELDS,
} from '../constants/actionTypes';

import { CYDER_COMMON_API_ROOT } from '../../config';

export function forceValidate() {
    return dispatch => {
        dispatch({ type: CYDER_USERS_ADD_ADD_USER_FORCE_VALIDATE });
        // return this for async actions
        return Promise.resolve();
    };
}
export function changeFieldValueAction(key, value) {
    return {
        type: CYDER_USERS_ADD_CHANGE_FIELD,
        key,
        value,
    };
}
export function addUserAction(data, callback) {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_USERS_ADD_ADD_USER_REQUEST });
        const appId = getState().cyderProfileReducer.profile.appId;
        const url = withQuery(CYDER_COMMON_API_ROOT + '/users', {
            appid: appId,
            action: 'add',
        });

        // const dataWithAppId = Object.assign({appId}, data);
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'content-type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(data),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        // error
                        throw error;
                    });
                } else {
                    return response.json().then(res => {
                        dispatch({ type: CYDER_USERS_ADD_ADD_USER_SUCCESS, response: res });
                        if (callback) callback();
                        return res;
                    });
                }
            })
            .catch(error => {
                // error
                dispatch({ type: CYDER_USERS_ADD_ADD_USER_FAILURE, response: error });
                return error;
            });
    };
}
export function cyderUsersAddMessageModalToggleAction(modalAction) {
    return {
        type: CYDER_USERS_ADD_MESSAGE_MODAL_TOGGLE,
        modalAction,
    };
}
export function clearFields() {
    return {
        type: CYDER_USERS_ADD_CLEAR_FIELDS,
    };
}
