export IMAGE_NAME=registry.cyder.com.sg/cydertech/cyder-web-saas:prod

docker rmi -f $IMAGE_NAME
docker build -f Dockerfile -t $IMAGE_NAME --build-arg ENV=PROD .
docker push $IMAGE_NAME

kubectl --kubeconfig cyder-prod-cluster.yaml --insecure-skip-tls-verify=true set env --namespace djarvis-app deployment/djarvis-app -e DEPLOY_TIMESTAMP="$(date +%s)"