/**
 * Expense Report action
 *
 * Author: Fernando Karnagi
 */

import {
    CYDER_REPORT_EXPENSE_RECEIPT_BY_CATEGORY,
    CYDER_REPORT_EXPENSE_MILEAGE_BY_CATEGORY,
    CYDER_REPORT_EXPENSE_RECEIPT_BY_PROJECT,
    CYDER_REPORT_EXPENSE_MILEAGE_BY_PROJECT,
    CYDER_REPORT_EXPENSE_LOADING_SHOW,
    CYDER_REPORT_EXPENSE_LOADING_HIDE,
    CYDER_REPORT_EXPENSE_START_DATE_CHANGED,
    CYDER_REPORT_EXPENSE_END_DATE_CHANGED,
    CYDER_REPORT_EXPENSE_STATUS_CHANGED,
    CYDER_REPORT_EXPENSE_RESET_FORM,
} from '../constants/actionTypes';
import moment from 'moment';
import withQuery from 'with-query';

import { DJARVIS_REPORT_API_ROOT } from '../../config';

export function retrieveSummaryReportAction(action, callback) {
    return (dispatch, getState) => {
        const { apiKey } = getState().config;
        const { appId, authorizationToken } = getState().cyderProfileReducer.profile;
        const { status, startDate, endDate } = getState().cyderReportExpenseReducer.search;
        const momentStartdate = moment(startDate);
        const momentEnddate = moment(endDate);

        const body = {
            status: status.selectValue.value,
            startdate: momentStartdate.isValid() ? momentStartdate.format('DD/MM/YYYY') : '',
            enddate: momentEnddate.isValid() ? momentEnddate.format('DD/MM/YYYY') : '',
            appId,
        };

        const url = withQuery(DJARVIS_REPORT_API_ROOT, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: action,
        });

        const options = {
            body: JSON.stringify(body),
            method: 'POST',
            headers: {
                'x-api-key': apiKey,
                'Content-Type': 'application/json',
                Authorization: authorizationToken,
            },
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    response.json().then(error => {
                        console.log(error);
                        callback(error);
                    });
                } else {
                    response.json().then(response => callback(null, response));
                }
            })
            .catch(error => {
                console.log(error);
                callback(error);
            });
    };
}

export function updateReceiptByCategory(report) {
    return {
        type: CYDER_REPORT_EXPENSE_RECEIPT_BY_CATEGORY,
        report,
    };
}

export function updateMileageByCategory(report) {
    return {
        type: CYDER_REPORT_EXPENSE_MILEAGE_BY_CATEGORY,
        report,
    };
}

export function updateReceiptByProject(report) {
    return {
        type: CYDER_REPORT_EXPENSE_RECEIPT_BY_PROJECT,
        report,
    };
}

export function updateMileageByProject(report) {
    return {
        type: CYDER_REPORT_EXPENSE_MILEAGE_BY_PROJECT,
        report,
    };
}

// UI new -----

export function expenseLoadingShow() {
    return {
        type: CYDER_REPORT_EXPENSE_LOADING_SHOW,
    };
}

export function expenseLoadingHide() {
    return {
        type: CYDER_REPORT_EXPENSE_LOADING_HIDE,
    };
}

export function expenseStartDateChanged(startDate) {
    return {
        type: CYDER_REPORT_EXPENSE_START_DATE_CHANGED,
        startDate,
    };
}

export function expenseEndDateChanged(endDate) {
    return {
        type: CYDER_REPORT_EXPENSE_END_DATE_CHANGED,
        endDate,
    };
}

export function expenseStatusChanged(status) {
    return {
        type: CYDER_REPORT_EXPENSE_STATUS_CHANGED,
        status,
    };
}

export function expenseResetForm() {
    return {
        type: CYDER_REPORT_EXPENSE_RESET_FORM,
    };
}
