export default {
    init: function () {
        const func = function(d, src, c) {
            var t = d.scripts[d.scripts.length - 1],
                s = d.createElement("script");
            s.id = "la_x2s6df8d";
            s.async = true;
            s.src = src;
            s.onload = s.onreadystatechange = function() {
                var rs = this.readyState;
                if (rs && rs != "complete" && rs != "loaded") {
                    return;
                }
                c(this);
            };
            t.parentElement.insertBefore(s, t.nextSibling);
        }
        
        func(document, "https://blue.ladesk.com/scripts/track.js", function(e) {
            window.LiveAgent.createButton("a2b097b9", e);
        });
    }
};

