import { connect } from 'react-redux';
import { compose, branch, renderComponent, renderNothing } from 'recompose';
import MyCorpTopNavigation from './MyCorpTopNavigation';

const layouts = ['top-navigation-1'];

const Component = compose(
    connect(state => {
        return {
            navigation: state.navigation,
            layout: state.config.layout,
        };
    }),
    branch(({ layout }) => layouts.includes(layout), renderComponent(MyCorpTopNavigation), renderNothing),
)(MyCorpTopNavigation);

export default Component;
