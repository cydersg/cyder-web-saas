import React, { useState, useEffect, Fragment } from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';
import moment from 'moment';
import history from 'history.js';
import { Link } from 'react-router-dom';
import Table from 'react-table';
import { Button, Badge, Row, Col, Container } from 'reactstrap';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import { TablePagination } from 'cyderComponents/pagination/';
import { setJumbotronTitle } from 'actions/pagedata';
import * as Actions from 'actions/timesheet/monthlyTimesheetAction';
import SubmitLeaveForm from 'pages/leaves/SubmitLeaveForm';
import YearMonthSearchPanel from 'cyderComponents/panel/YearMonthSearchPanel';
import { isFutureDate } from 'js/generalUtils';
import ActionExecutor from 'js/ActionExecutor';
import cyderlib from 'js/cyderlib';
import { layoutLib } from 'js/constlib';

const { getMonthlyLeaveSummary, deleteLeave } = Actions;

// Store submit leave onClick function
let submitLeavePlanFn;

const useLeaveSummaryEffect = callback => {
    useEffect(callback, []);
};

const LeaveSummaryPage = props => {
    const [loading, setLoading] = useState(false);
    const [leaves, setLeaves] = useState([]);
    const [applied, setApplied] = useState(false);
    const [month, setMonth] = useState(null);
    const [year, setYear] = useState(null);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [modalConfig, setModalConfig] = useState({
        header: 'Month Leave Timesheet',
        body: '',
        onlyOneButton: false,
    });

    const onSearch = (month, year) => {
        month = month && month.value;
        year = year && year.value;

        setYear(year);
        setMonth(month);
        getMonthlyLeaveSummaryFn(month, year);
    };

    const showModal = (header, body, action, onlyOneButton) => {
        setIsModalOpen(true);
        setModalConfig({
            ...modalConfig,
            header: header || modalConfig.header,
            body: body || modalConfig.body,
            action: action || null,
            onlyOneButton: onlyOneButton || modalConfig.onlyOneButton,
        });
    };

    const showModalIf = (header, body, action, condition) => {
        if (!condition) return;
        showModal(header, body, action, true);
    };

    const getMonthlyLeaveSummaryFn = (month, year) => {
        const body = {
            month,
            year,
        };

        const callback = async () => {
            props.setJumbotronTitle('Leave Plan');
            const { data } = await props.getMonthlyLeaveSummary(body);
            showModalIf('Error', 'Failed to retrive monthly leave plan.', null, !data);
            if (!data) return;
            setLeaves(data.reverse());
        };

        ActionExecutor.execute(setLoading, callback);
    };

    const deleteLeave = id => {
        const callback = async () => {
            setModalConfig({
                body: <LoadingSpinner />,
            });
            const { ok } = await props.deleteLeave(id);
            showModalIf('Error', 'Failed to delete leave plan.', null, !ok);
            showModalIf('Success', 'Leave plan has been successfully deleted', null, ok);
            if (!ok) return;
            getMonthlyLeaveSummaryFn(month, year);
        };

        showModal(
            'Confirmation',
            'Are you sure you want to delete this leave plan?',
            () => ActionExecutor.execute(setLoading, callback),
            true,
        );
    };

    const positiveButtonAction = async () => {
        if (modalConfig.action) {
            await modalConfig.action();
            setIsModalOpen(!isModalOpen);
        } else {
            // For submit leave plan only
            const ret = submitLeavePlanFn && (await submitLeavePlanFn());
            if (!ret) return;
            if (ret) getMonthlyLeaveSummaryFn(month, year);
            setIsModalOpen(!isModalOpen);
        }
    };

    const effect = () => {
        const { apply } = props.match.params;
        getMonthlyLeaveSummaryFn(month, year);

        // Check whether should trigger apply leave modal
        if (apply && apply === '1' && !applied) {
            showModal('Apply New Leave', <SubmitLeaveForm submitFn={fn => storeFn(fn)} />, null, false);
            setApplied(true);
        }
    };

    const storeFn = fn => {
        submitLeavePlanFn = fn;
    };

    // USE EFFECT
    useLeaveSummaryEffect(effect);

    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    // RENDER
    return (
        <Container className="wideContainer">
            <DialogModal
                key="modal"
                modal={isModalOpen}
                toggle={setIsModalOpen}
                customHeader={modalConfig.header}
                customModalBody={modalConfig.body}
                onlyOneButton={modalConfig.onlyOneButton}
                positiveButtonAction={positiveButtonAction}
            />
            <Row>
                <YearMonthSearchPanel
                    hasMonth
                    hasYear
                    onSearch={onSearch}
                    button1={
                        <Button
                            className={isMobile ? 'w-100p mt10' : 'ml10'}
                            color="success"
                            onClick={() => showModal('Apply New Leave', <SubmitLeaveForm submitFn={fn => storeFn(fn)} />, false)}
                        >
                            Apply Leave
                        </Button>
                    }
                ></YearMonthSearchPanel>
            </Row>
            <RowColWrapper colProps={{ className: 'pt-4' }}>
                {loading ? (
                    <LoadingSpinner center />
                ) : (
                    <Table
                        className="-highlight mb-2"
                        data={leaves}
                        showPagination={!isMobile}
                        columns={getColumns(deleteLeave)}
                        pageSize={leaves.length > 20 ? 20 : leaves.length}
                        PaginationComponent={TablePagination}
                    />
                )}
            </RowColWrapper>
        </Container>
    );
};

const getColumns = deleteLeaveFn => {
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return [
        {
            Header: 'Leave Type',
            width: 120,
            show: !isMobile,
            accessor: 'leave_type_code',
        },
        {
            Header: 'Duration',
            accessor: 'fromdate',
            show: !isMobile,
            width: 150,
            Cell: props => {
                const { fromsession, tosession } = props.original;
                return (
                    <>
                        <div className="font-weight-bold">
                            {moment(props.original.fromdate).format('DD-MMM-YYYY')} {fromsession}
                        </div>
                        <div className="font-weight-bold">
                            {moment(props.original.todate).format('DD-MMM-YYYY')} {tosession}
                        </div>
                    </>
                );
            },
        },
        {
            Header: 'Days',
            show: !isMobile,
            width: 50,
            accessor: 'num_of_days',
        },
        {
            Header: 'Remarks',
            accessor: 'remarks',
            show: !isMobile,
        },
        {
            Header: 'Status',
            show: !isMobile,
            width: 100,
            accessor: 'status',
            Cell: props => (
                <Badge color={cyderlib.statusBadgeColor(props.value)}>{props.value === 'Active' ? 'Approved' : props.value}</Badge>
            ),
        },
        {
            Header: '',
            show: !isMobile,
            accessor: 'id',
            width: 100,
            Cell: props => {
                const { fromdate } = props.original;
                return (
                    <Fragment>
                        <Button
                            color="link"
                            size="sm"
                            className="ml-1"
                            onClick={e =>
                                history.push(
                                    `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'leavetype'}/leaves/leaveplan/detail/${
                                        props.value
                                    }`,
                                )
                            }
                        >
                            <i className="align-middle material-icons">remove_red_eye</i>
                        </Button>
                        {isFutureDate(fromdate) && (
                            <Button color="link" className="ml-1" size="sm" onClick={e => deleteLeaveFn(props.value)}>
                                <i className="text-danger align-middle material-icons">delete</i>
                            </Button>
                        )}
                    </Fragment>
                );
            },
        },
        {
            Header: '',
            show: isMobile,
            accessor: 'id',
            Cell: props => {
                const { fromsession, tosession } = props.original;
                return (
                    <Link
                        style={{
                            color: 'unset',
                        }}
                        to={`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'leavetype'}/leaves/leaveplan/detail/${
                            props.value
                        }`}
                    >
                        <div className="boxShadow">
                            <span>
                                <div className="font-weight-bold">
                                    {moment(props.original.fromdate).format('DD-MMM-YYYY')} {fromsession}
                                </div>
                                <div className="font-weight-bold">
                                    {moment(props.original.todate).format('DD-MMM-YYYY')} {tosession}
                                </div>
                            </span>
                            <span>
                                <strong>{props.original.leave_type_code}</strong>
                            </span>
                            <br />
                            <span>{props.original.remarks}</span>
                            <br />
                            <Badge color={cyderlib.statusBadgeColor(props.original.status)}>
                                {props.original.status === 'Active' ? 'Approved' : props.original.status}
                            </Badge>
                            {/* {isFutureDate(props.original.fromdate) && (
                                <Button color="link" className="ml-1" size="sm" onClick={e => deleteLeaveFn(props.value)}>
                                    <i className="text-danger align-middle material-icons">delete</i>
                                </Button>
                            )} */}
                        </div>
                    </Link>
                );
            },
        },
    ];
};

const stateToProps = state => {
    return {
        profile: state.cyderProfileReducer.profile,
    };
};

const actionList = {
    setJumbotronTitle,
    getMonthlyLeaveSummary,
    deleteLeave,
};

export default connect(stateToProps, actionList)(StoredLayout(LeaveSummaryPage));
