import {
    CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_REQUEST,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_SUCCESS,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_FAILURE,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_SET_FORCE_REFRESH_FLAG,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MODAL_TOGGLE,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MODAL_SET_MILEAGE_CATEGORY,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MODAL_CHANGE_VALUE,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_REQUEST,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_SUCCESS,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_FAILURE,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_REQUEST,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_SUCCESS,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_FAILURE,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_REQUEST,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_SUCCESS,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_FAILURE,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_ADD_APPROVER,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_REMOVE_APPROVER,
} from '../../actions/constants/actionTypes' 

// defaults
var defaultState = {
    modalMileageCategory: {
        id: "",
        code: "",
        name: "",
        rate: "",
        currency: "SGD",
        rateunit: "km",
        approvers: [],
    },
    approversReference: {},
    saving: false,
    saved: false,
    errorMessage: null,
    modalOpen: false,
    modalAction: null,
    modalMessage: "",
    modalHeader: "",
    loading: false,
    mileagecategories: [],
    forceRefresh: false,
}

export function cyderMileageCategoriesSettingsSearchReducer(state = defaultState, action) {
    let newState = Object.assign({}, state);
    let approversReference;
    let indexOfUserToRemove;
    let obj = {};
    switch (action.type) {
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_REQUEST:
            newState.loading = true;
            newState.errorMessage = null;
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_SUCCESS:
            newState.loading = false;
            newState.mileagecategories = action.response;
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_FAILURE:
            newState.loading = false;
            newState.errorMessage = action.error.message;
            newState.mileagecategories = [];
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_REQUEST:
            newState.saving = true;
            newState.errorMessage = null;
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_SUCCESS:
            newState.modalMessage = action.response.message ? action.response.message : "Mileage Category has been successfully added";
            newState.saving = false;
            newState.saved = true;
            newState.modalAction = "refresh";
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_FAILURE:
            newState.modalMessage = "Failed to add project: " + action.error.message;
            newState.saving = false;
            newState.saved = true;
            newState.modalAction = "close";
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_REQUEST:
            newState.saving = true;
            newState.errorMessage = null;
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_SUCCESS:
            newState.modalMessage = action.response.message ? action.response.message : "Mileage Category has been successfully updated";
            newState.saving = false;
            newState.saved = true;
            newState.modalAction = "refresh";
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_FAILURE:
            newState.modalMessage = "Failed to save changes: " + action.error.message;
            newState.saving = false;
            newState.saved = true;
            newState.modalAction = "close";
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_REQUEST:
            newState.saving = true;
            newState.errorMessage = null;
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_SUCCESS:
            newState.modalMessage = action.response.message ? action.response.message : "Mileage Category has been successfully deleted";
            newState.saving = false;
            newState.saved = true;
            newState.modalAction = "refresh";
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_FAILURE:
            newState.modalMessage = "Failed to save changes: " + action.error.message;
            newState.saving = false;
            newState.saved = true;
            newState.modalAction = "close";
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_SET_FORCE_REFRESH_FLAG:
            newState.forceRefresh = action.val;
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_MODAL_SET_MILEAGE_CATEGORY:
            newState.modalMileageCategory = {
                id: action.id,
                code: action.code,
                name: action.name,
                approvalvotescount: action.approvalvotescount,
                rate: action.rate,
                currency: action.currency,
                rateunit: action.rateunit,
                approvers: action.approvers,
            }
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_MODAL_TOGGLE:
            newState.modalOpen = !state.modalOpen;
            newState.modalAction = action.modalAction;
            if (action.modalAction === "update") {
                newState.modalMessage = null;
                newState.saved = false;
                newState.modalHeader = "Update Mileage Category";
            } else if (action.modalAction === "add") {
                newState.modalMessage = null;
                newState.saved = false;
                newState.modalHeader = "Add Mileage Category";
            } else if (action.modalAction === "delete") {
                newState.modalMessage = "Are you sure you want to delete this Mileage Category?";
                newState.saved = false;
                newState.modalHeader = "Delete Mileage Category";
            }
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_MODAL_CHANGE_VALUE:
            newState.modalMileageCategory = Object.assign({}, state.modalMileageCategory);
            newState.modalMileageCategory[action.key] = action.value
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_ADD_APPROVER:
            obj[action.username] = true;
            approversReference = Object.assign(obj, state.approversReference);
            approversReference[action.username] = true;
            newState.modalMileageCategory = Object.assign({}, state.modalMileageCategory);
            var approvers = state.modalMileageCategory.approvers.concat(action.username);
            newState.modalMileageCategory.approvers = approvers;
            newState.approversReference = approversReference;
            newState.changesMade = true;
            return newState;
        case CYDER_MILEAGE_CATEGORIES_SETTINGS_REMOVE_APPROVER:
            indexOfUserToRemove = state.modalMileageCategory.approvers.indexOf(action.username);
            approversReference = Object.assign({}, state.approversReference);
            approversReference[action.username] = false;
            newState.approversReference = Object.assign({}, approversReference);
            newState.modalMileageCategory.approvers = state.modalMileageCategory.approvers.slice(0, indexOfUserToRemove).concat(state.modalMileageCategory.approvers.slice(indexOfUserToRemove+1));
            newState.changesMade = true;
            return newState;
        default:
            return state;
    }
}
