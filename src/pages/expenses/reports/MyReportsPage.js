import React, { Component } from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';
import MyReportsPanel from './MyReportsPanel';

class MyReports extends Component {
    render() {
        return <MyReportsPanel mode="self" />;
    }
}

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(StoredLayout(MyReports));
