import React, { Fragment } from 'react';
import classnames from 'classnames';
import { Row, Col, FormGroup, Label, Input } from 'reactstrap';

const CyderInput = ({
    inputProps = {},
    mandatory = '',
    labelClassName = '',
    className,
    readOnly,
    children,
    label,
    customLabelComponent,
    formGroupClassName,
    colClassName,
    colstyle,
    error,
    mutetext,
}) => {
    const { type, id, value, checked } = inputProps;

    // FOR CHECKBOX TYPE ONLY
    const isCheckboxType = type === 'checkbox';
    const checkedVal = checked ? 'Yes' : 'No';

    // COMPONENT
    const PreviewComponent = <div>{isCheckboxType ? checkedVal : value}</div>;
    const LabelComponent = customLabelComponent || (
        <Label
            for={id}
            className={classnames('text-bold', labelClassName, {
                mandatory: mandatory ? true : false,
            })}
        >
            {label} {mandatory && <span>*</span>}
        </Label>
    );

    return (
        <Row className={className || 'mt-2 mb-2'}>
            <Col className={colClassName || 'col-12'} style={colstyle}>
                <FormGroup className={formGroupClassName} check={isCheckboxType}>
                    {!isCheckboxType && label && LabelComponent}
                    {!readOnly && (children || <Input {...inputProps} />)}
                    {isCheckboxType && LabelComponent}
                    {readOnly && PreviewComponent}
                    {mutetext && <div className="text-muted">{mutetext}</div>}
                    {error && <span className="help-text">{error}</span>}
                </FormGroup>
            </Col>
        </Row>
    );
};

export default CyderInput;
