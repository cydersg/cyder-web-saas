import React, { Component, useState } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import StoredLayout from 'layouts/StoredLayout';

import Table from 'react-table';
import { Row, Button, Col, Badge, Container } from 'reactstrap';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import YearMonthSearchPanel from 'cyderComponents/panel/YearMonthSearchPanel';
import DateRangeSearchPanel from 'cyderComponents/panel/DateRangeSearchPanel';
import { TablePagination } from 'cyderComponents/pagination/';

import { clockOut, addLocationToFavourite, removeLocationFromFavourite, getAllFavourites } from 'actions/timesheet/clockAction';
import * as Actions from 'actions/timesheet/monthlyTimesheetAction';
import { setJumbotronTitle } from 'actions/pagedata';
import { layoutLib } from 'js/constlib';
import ActionExecutor from 'js/ActionExecutor';

import MyCorpTimesheetBottomNav from './MyCorpTimesheetBottomNav';
const { getDateRangeActivities, getApprovers, submitTimesheetReport, getProjects, deleteTimesheetActivity } = Actions;

const defaultOption = { value: 0, label: 'None' };

class MyCorpTimesheetHistory extends Component {
    state = {
        loading: false,
        activities: [],
        approvers: [],
        isModalOpen: false,
        isSubmitModalOpen: false,
        modal: {
            header: 'Monthly Timesheet',
            body: '',
            action: null,
            onlyOneButton: false,
        },
    };

    componentDidMount() {
        const start = '01/' + moment().format('MM/YYYY');
        const end = moment(
            '01/' +
                moment()
                    .add(1, 'M')
                    .format('MM/YYYY'),
            'DD/MM/YYYY',
        )
            .add(-1, 'days  ')
            .format('DD/MM/YYYY');
        this.setState({ start, end });
        this.props.setJumbotronTitle('Monthly Timesheet');
        this.getMonthlyActivities(start, end);
        this.props.getAllFavourites(this.props.profile.username);
        this.getProjects();
        this.getApproversList();
    }

    addFavourite = async location => {
        await this.props.addLocationToFavourite(location);
        this.props.getAllFavourites(this.props.profile.username);
    };

    removeFavourite = async id => {
        await this.props.removeLocationFromFavourite(id);
        this.props.getAllFavourites(this.props.profile.username);
    };

    clockOut = async id => {
        await this.props.clockOut(id);
        this.getMonthlyActivities(this.state.start, this.state.end);
    };

    setLoading = loading => {
        this.setState({ loading });
    };

    setStartEnd = (start, end) => {
        this.setState({ start, end });
    };

    getMonthlyActivities = (start, end) => {
        const body = {
            start: moment(start, 'DD/MM/YYYY').format('YYYY/MM/DD') + ' 00:00',
            end: moment(end, 'DD/MM/YYYY').format('YYYY/MM/DD') + ' 23:59',
        };
        this.setStartEnd(start, end);
        const callback = async () => {
            const { data } = await this.props.getDateRangeActivities(body);
            if (!data) {
                this.showModal('Error', 'Failed to retrive timesheet', null, true);
                return;
            }
            this.setState({ activities: data });
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    getApproversList = () => {
        const callback = async () => {
            const { data } = await this.props.getApprovers();
            this.setState({ approvers: data });
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    getProjects = () => {
        const callback = async () => {
            const { data } = await this.props.getProjects();
            this.setState({ projects: data });
        };
        ActionExecutor.execute(() => {}, callback);
    };

    deleteActivity = id => {
        const callback = async () => {
            const { ok } = await this.props.deleteTimesheetActivity(id);
            if (ok) {
                this.showModal('Confirmation', 'Successfully deleted activity record.', null, true);
                this.getMonthlyActivities(this.state.start, this.state.end);
                return;
            }
            this.showModal('Error', 'Failed to delete activity record.', null, true);
        };
        this.showModal('Confirmation', 'Are you sure you want to delete this record?', () => ActionExecutor.execute(() => {}, callback));
    };

    submitTimesheetReport = (month, year, approvers) => {
        const body = {
            reportMonth: month,
            reportYear: year,
            approvers,
        };
        const callback = async () => {
            const { ok } = await this.props.submitTimesheetReport(body);
            if (ok) {
                this.showModal('Confirmation', 'Successfully submitted monthly timesheet report.', null, true);
                return;
            }
            this.showModal('Error', 'Failed to submit monthly timesheet report.', null, true);
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    setIsModalOpen = (isModalOpen, id) => {
        let stateCopy = Object.assign({}, this.state);
        const key = id === 'submitModal' ? 'isSubmitModalOpen' : 'isModalOpen';
        stateCopy[key] = isModalOpen;
        this.setState(stateCopy);
    };

    showModal = (header, body, action, onlyOneButton) => {
        this.setIsModalOpen(true);
        const { modal } = this.state;
        const modalConfig = {
            ...this.state.modal,
            header: header || modal.header,
            body: body || modal.body,
            action: action || null,
            onlyOneButton: onlyOneButton || false,
        };
        this.setState({ modal: modalConfig });
    };

    render() {
        const { start, end, approvers, activities, projects, loading, modal, isModalOpen, isSubmitModalOpen } = this.state;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        return (
            <Container className="wideContainer">
                <DialogModal
                    key="confirmModal"
                    modal={isModalOpen}
                    toggle={isOpen => this.setIsModalOpen(isOpen, 'confirmModal')}
                    customHeader={modal.header}
                    customModalBody={modal.body}
                    positiveButtonAction={modal.action}
                    onlyOneButton={modal.onlyOneButton}
                />
                <Row className="justify-content-center">
                    <Col
                        className="ph0"
                        style={{
                            textAlign: 'center',
                        }}
                    >
                        {/* <b>MyCorp Check In</b> */}
                        <hr className="mycorp-hr-small" />
                    </Col>
                </Row>
                <Row>
                    <DateRangeSearchPanel start={start} end={end} onSearch={this.getMonthlyActivities} />
                </Row>
                <RowColWrapper colProps={{ className: ' mycorp-list' }} classNames="mycorp-list">
                    {loading ? (
                        <LoadingSpinner center />
                    ) : (
                        <div>
                            <Table
                                className="-highlight mb-2"
                                showPagination={false}
                                data={activities}
                                TheadComponent={props => null}
                                columns={getColumns(
                                    projects,
                                    this.deleteActivity,
                                    this.addFavourite,
                                    this.removeFavourite,
                                    this.props.allFavourites,
                                    this.clockOut,
                                )}
                                pageSize={activities.length}
                                PaginationComponent={TablePagination}
                            />
                        </div>
                    )}
                </RowColWrapper>
                <MyCorpTimesheetBottomNav page="history" />
            </Container>
        );
    }
}

export const getColumns = (projects = [], deleteActivity, addFavourite, removeFavourite, myFavourites, onClockOut, showActualLoc) => {
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;
    const formatData = date => moment(date, 'YYYY-MM-DD hh:mm:ss').format('DD/MM/YYYY hh:mm a');

    const TheadComponent = props => null;
    return [
        {
            TheadComponent: { TheadComponent },
            headerClassName: 'rt-no-sort-ind',
            accessor: 'id',
            sortable: false,
            filterable: false,
            Cell: props => {
                const project = projects.find(x => x.code === props.original.project);
                const {
                    starttime,
                    endtime,
                    status,
                    purpose,
                    id,
                    location_name,
                    location_address,
                    lat,
                    lng,
                    actual_lat,
                    actual_lng,
                } = props.original;

                const momentStartTime = moment(starttime, 'YYYY-MM-DD hh:mm:ss');
                const momentEndTime = moment(endtime, 'YYYY-MM-DD hh:mm:ss');

                const favLocation =
                    location_name &&
                    myFavourites &&
                    myFavourites.length > 0 &&
                    myFavourites.filter(e => e.name.toLowerCase() === location_name.toLowerCase());
                const isFavourite = favLocation && favLocation.length > 0 ? true : false;

                return (
                    <div className="boxShadow">
                        <div>
                            <div
                                style={{
                                    display: 'flex',
                                    width: '100%',
                                }}
                            >
                                <div
                                    style={{
                                        fontSize: 12,
                                        marginRight: 10,
                                        marginBottom: 5,
                                        width: '100%',
                                    }}
                                >
                                    <span
                                        className="font-weight-bold"
                                        style={{
                                            fontSize: 16,
                                        }}
                                    >
                                        {project ? project.name : props.original.project}
                                    </span>
                                    <br />
                                    <span>In&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;{momentStartTime.format('DD MMM YYYY HH:mm')}</span>
                                    <br />
                                    {endtime && <span>Out&nbsp;:&nbsp;{momentEndTime.format('DD MMM YYYY HH:mm')}</span>}
                                </div>

                                {!endtime && onClockOut && (
                                    <Row
                                        style={{
                                            height: '100%',
                                        }}
                                    >
                                        <Col className="mycorp-col-big-icon mr0">
                                            <Button className={' mycorp-add-button-circle'} onClick={() => onClockOut(id)} color="primary">
                                                <i class="fa fa-sign-out fa-lg  mycorp-big-icon"></i>
                                            </Button>
                                        </Col>
                                    </Row>
                                )}
                            </div>

                            {purpose && (
                                <div>
                                    <div
                                        style={{
                                            fontSize: 12,
                                            marginBottom: 5,
                                        }}
                                    >
                                        {purpose}
                                    </div>
                                </div>
                            )}

                            {lat && lng && (
                                <div
                                    style={{
                                        display: 'flex',
                                        width: '100%',
                                    }}
                                >
                                    <div
                                        style={{
                                            width: '100%',
                                        }}
                                    >
                                        <p className=" text-muted">
                                            <b>{location_name || location_address}</b>
                                            {location_name && (
                                                <p
                                                    className="mycorp-p-slim"
                                                    style={{
                                                        fontSize: 12,
                                                    }}
                                                >
                                                    {location_address}
                                                </p>
                                            )}

                                            {showActualLoc && actual_lat && actual_lng && (
                                                <p
                                                    className="mycorp-p-slim"
                                                    style={{
                                                        fontSize: 12,
                                                    }}
                                                >
                                                    {' '}
                                                    Lat: {actual_lat}, Lng: {actual_lng}
                                                </p>
                                            )}
                                        </p>
                                    </div>

                                    {location_name && addFavourite && removeFavourite && (
                                        <i
                                            onClick={e => {
                                                if (isFavourite) {
                                                    removeFavourite(favLocation[0].id);
                                                } else {
                                                    addFavourite(props.original);
                                                }
                                            }}
                                            className="align-middle material-icons"
                                            style={{
                                                marginRight: 25,
                                                fontSize: 20,
                                                paddingTop: 5,
                                                color: 'orange',
                                            }}
                                        >
                                            {isFavourite ? 'star' : 'star_outline'}
                                        </i>
                                    )}
                                </div>
                            )}
                        </div>
                    </div>
                );
            },
        },
    ];
};

const mapStateToProps = state => {
    return {
        ...state.clockReducer,
        profile: state.cyderProfileReducer.profile,
    };
};

const actionList = {
    setJumbotronTitle,
    getDateRangeActivities,
    getApprovers,
    getProjects,
    submitTimesheetReport,
    deleteTimesheetActivity,
    addLocationToFavourite,
    removeLocationFromFavourite,
    getAllFavourites,
    clockOut,
};

export default connect(mapStateToProps, actionList)(StoredLayout(MyCorpTimesheetHistory));
