import {
    HOME_PAGE_FORCE_LOADING
} from '../../actions/constants/actionTypesHome' 

export function forceLoading(loading) {
    return (dispatch, getState) => {
        dispatch({
            type: HOME_PAGE_FORCE_LOADING,
            loading,
        })
        return Promise.resolve();
    }
}
