import React, { Component } from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';
import MyReportsPanel from '../expenses/reports/MyReportsPanel';

class MyReports extends Component {
    render() {
        return <MyReportsPanel mode="admin" />;
    }
}

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(StoredLayout(MyReports));
