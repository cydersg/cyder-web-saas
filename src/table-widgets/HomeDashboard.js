/**
 * Home dashboard table structure
 * 
 * Author: Fernando
 */

import React from 'react'
import PropTypes from 'prop-types'

const HomeDashboard = ({items}) => (
  <div className="table-widget-4">
    <table className="table table-unbordered table-striped">
      <thead>
        <tr>
          <th>Category</th>
          <th>Date</th>
          <th>Message</th>
          <th>Sender</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {items.map((item, i) => (
          <tr key={i}>
            <td>
              <span className={`badge badge-${item.color} badge-outline`}>
                {item.category}
              </span>
            </td>
            <td>{item.date}</td>
            <td>{item.message}</td>
            <td>{item.sender}</td>  
            <td><i className="material-icons">{item.action}</i></td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
)

HomeDashboard.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object)
}

export default HomeDashboard
