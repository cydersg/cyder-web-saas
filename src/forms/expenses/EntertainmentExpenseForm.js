/**
 * Entertainment Expense Form
 * 
 * Author: Fernando
 */
import React from 'react'
import {connect} from 'react-redux'
import {cancelExpense, selectExpenseDate} from '../../actions/expenses/expenseCommonAction'
import ProjectSelect from '../../forms/expenses/ProjectSelect'
import CyderDatePicker from '../../cyderComponents/forms/CyderDatePicker'

const EntertainmentExpenseForm = ({submit, expense, cancel, selectDateOfExpense}) => (
  <div>
    {expense.inprogress && expense.entertainmentExpense.show && 
      <form onSubmit={e => e.preventDefault()}>
        <div>
          <div className="row">
            <div className="col-12 col-lg-6">
              <div className="form-group">
                <label>When</label>
                <div className="input-group">
                  <span className="input-group-addon rounded-left">
                    <i className='material-icons'>date_range</i>
                  </span> 
                    <CyderDatePicker onChange={selectDateOfExpense}/>
                </div>
                <small className="form-text text-muted">Please enter date of expense</small>
              </div>
            </div>
            <div className="col-12 col-lg-6">
              <div className="form-group">
                <label>How Much</label>
                <div className="input-group">
                  <span className="input-group-addon rounded-left">
                    <i className='material-icons'>attach_money</i>
                  </span> 
                  <input type="text" className="form-control rounded-right" placeholder="" />
                </div>
                <small className="form-text text-muted">Please enter how much is your expense</small>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <div className="form-group">
                <label>Purpose</label> 
                <ProjectSelect />
                <small className="form-text text-muted">Please select project</small>
                <br/>
                <textarea className="form-control" rows="6" />
                <small className="form-text text-muted">Please enter remarks</small>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-12 col-lg-1"> 
              <button
                className="btn btn-primary btn-rounded btn-outline"
                type="submit"
                onClick={() => submit()}>
                Submit
              </button>
            </div>
            <div className="col-12 col-lg-1"> 
              <button
                className="btn btn-primary btn-rounded btn-outline"
                type="submit"
                onClick={() => cancel()}>
                Cancel
              </button>
            </div>
          </div>

        </div>
      
      </form>
    }
  </div>
)

const mapStateToProps = (state, ownProps) => {
  return {
    expense: state.cyderExpenseReducer
  }
}
const mapDispatchToProps = dispatch => {
  return {
    submit: () => {
      // This function ONLY does the binding. Validation should be done in the reducer
      
      console.log("submitting entertainment exepense");
      //history.push(url);
    },
    cancel: () => {
      dispatch(cancelExpense());
    },
    selectDateOfExpense: (e) => {
      dispatch(selectExpenseDate(e.format('DD/MM/YYYY')));
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(EntertainmentExpenseForm)
