import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import StoredLayout from '../../layouts/StoredLayout';
import moment from 'moment';
import { Link } from 'react-router-dom';

import Table from 'react-table';
import { Col, Row, Badge, Container } from 'reactstrap';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import { TablePagination } from 'cyderComponents/pagination/';
import { ConfirmModalManager } from 'cyderComponents/ConfirmModal';
import { InfoField } from 'cyderComponents/input/CyderPreview';

import { setJumbotronTitle } from 'actions/pagedata';
import * as Actions from 'actions/timesheet/monthlyTimesheetAction';
import { cyderUsersSearchAction } from 'actions/users/cyderUsersSearchAction';
import { expenseNewReceiptClaimGetHistory } from 'actions/expenses/cyderExpenseNewReceiptClaimAction';
import { layoutLib } from 'js/constlib';

import cyderlib from 'js/cyderlib';
import ActionExecutor from 'js/ActionExecutor';
import { getColumns } from 'pages/timesheet/MonthlyTimesheetPage';
import dateutils from 'js/dateutils';

const { findMonthlyActivity, getProjects } = Actions;

class YearlyTimesheetPage extends Component {
    state = {
        loading: false,
        year: '',
        returnedRemarks: '',
        reports: [],
        projects: [],
        users: [],
    };

    componentDidMount() {
        this.props.setJumbotronTitle('Report');
        this.getUsers();
        this.getProjects();
        this.findMonthlyActivity();
        this.getResourceHistory();
    }

    setLoading = loading => {
        this.setState({ loading });
    };

    findMonthlyActivity = () => {
        const { taskid } = this.props.match.params;
        const callback = async () => {
            const { data } = await this.props.findMonthlyActivity(taskid);
            if (!data) {
                ConfirmModalManager.show('Error', 'Failed to retrived report details.', () => ConfirmModalManager.toggle(), true);
                return;
            }
            this.setState({ reports: data });
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    getProjects = () => {
        const callback = async () => {
            const { data } = await this.props.getProjects();
            this.setState({ projects: data });
        };
        ActionExecutor.execute(() => {}, callback);
    };

    getUsers = () => {
        const callback = async () => {
            const data = await this.props.cyderUsersSearchAction({ keyword: '', status: 'Active' });
            this.setState({ users: data });
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    getResourceHistory = () => {
        const { taskid } = this.props.match.params;
        if (!taskid) return;
        const callback = async () => {
            const data = await this.props.expenseNewReceiptClaimGetHistory(taskid);
            if (!data) return;
            const returnedInfo = data.find(x => x.status === 'Returned') || {};
            this.setState({ returnedRemarks: returnedInfo.remarks });
        };
        ActionExecutor.execute(() => {}, callback);
    };

    render() {
        const { returnedRemarks, users, reports, projects, loading } = this.state;
        const { activities = [], title, reportMonth, reportYear, owner, createddt, status } = reports;
        const userInfo = users.find(x => x.email === owner) || {};
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        return (
            <Container className="wideContainer">
                <RowColWrapper>
                    {loading ? (
                        <LoadingSpinner center />
                    ) : (
                        <Fragment>
                            <RowColWrapper rowProps={{ className: 'mb-2 mb-4 ph0' }}>
                                <Link to="../yearlytimesheet" tabIndex="-1">
                                    <i className="align-middle material-icons material-icons-3x">chevron_left</i>
                                </Link>
                            </RowColWrapper>
                            <div>
                                <Row className="mb-4 ph0">
                                    <Col>
                                        <h2 className="font-weight-bold d-none d-md-block">{title}</h2>
                                        <h4 className="font-weight-bold d-block d-md-none">{title}</h4>
                                        <small className="font-weight-bold">
                                            Submitted on {moment(createddt, 'YYYY-MM-DD').format('DD-MMM-YYYY')}{' '}
                                        </small>
                                        <br />
                                        <Badge color={cyderlib.statusBadgeColor(status)}>{status}</Badge>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="12" md="6">
                                        <InfoField label="Owner" value={userInfo.firstname + ' ' + userInfo.lastname} />
                                    </Col>
                                    <Col xs="12" md="6">
                                        <InfoField
                                            className="d-none d-sm-block"
                                            label="Report Month"
                                            value={dateutils.monthsArray[reportMonth - 1]}
                                        />
                                        <InfoField className="d-block d-sm-none" label="Email" value={owner} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="12" md="6">
                                        <InfoField className="d-none d-sm-block" label="Email" value={owner} />
                                        <InfoField
                                            className="d-block d-sm-none"
                                            label="Report Month"
                                            value={dateutils.monthsArray[reportMonth - 1]}
                                        />
                                    </Col>
                                    <Col xs="12" md="6">
                                        <InfoField label="Report Year" value={reportYear} />
                                    </Col>
                                </Row>
                                {returnedRemarks && status === 'Returned' && (
                                    <Row className="mb-4">
                                        <Col xs="12">
                                            <InfoField label="Returned Remarks" value={returnedRemarks} />
                                        </Col>
                                    </Row>
                                )}
                                <br />
                            </div>
                            <Table
                                className="-highlight mb-2 ph0"
                                data={activities}
                                showPagination={!isMobile}
                                columns={getColumns(projects)}
                                pageSize={activities.length > 10 ? 10 : activities.length}
                                PaginationComponent={TablePagination}
                            />
                        </Fragment>
                    )}
                </RowColWrapper>
            </Container>
        );
    }
}

const reducer = state => {
    return {};
};

const actionList = {
    cyderUsersSearchAction,
    setJumbotronTitle,
    findMonthlyActivity,
    getProjects,
    expenseNewReceiptClaimGetHistory,
};

export default connect(reducer, actionList)(StoredLayout(YearlyTimesheetPage));
