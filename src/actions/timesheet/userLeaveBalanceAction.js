import withQuery from 'with-query';

import fetchlib from '../../js/fetchlib';
import { CYDER_LEAVE_API_ROOT } from '../../config';

export function submitLeaveAppl(body) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/leave`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'submitleave',
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}
