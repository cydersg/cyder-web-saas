import React, { Fragment, Component } from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';

import ReactTable from 'react-table';
import { Input, Container, Row, Col, FormGroup } from 'reactstrap';
import { withTranslation } from 'react-i18next';
import { CyderInput } from 'cyderComponents/input/index';
import LinkButton from 'cyderComponents/buttons/LinkButton';
import Select from 'react-select';

import { getLeaveTypeDetail, addLeaveType, updateLeaveType, getLeaveType, deleteLeaveType } from 'actions/settings/cyderLeaveTypesAction';

import DialogModal from 'cyderComponents/modals/DialogModal';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

import actionExecutor from 'js/ActionExecutor';

const data = {
    id: 0,
    code: 0,
    description: '',
    requireApproval: { value: '', label: '' },
    amountCarryForward: '',
    defaultEntitlement: '',
};

class LeaveTypesSettingsPage extends Component {
    _isMounted = true;

    state = {
        loading: false,
        idAdd: false,
        keyword: '',
        leaveTypes: [],
        isModalOpen: false,
        deleteLeaveId: '',
        data: Object.assign({}, data),
        modal: {
            type: '',
            modalBody: '',
            modalHeader: '',
            positiveButtonText: '',
            onlyOneButton: false,
        },
    };

    showLoading = loading => {
        this.setState({ loading });
    };

    clearDataState = () => {
        this.setState({ data: Object.assign({}, data) });
    };

    componentDidMount() {
        this.getLeaveTypes();
    }

    getLeaveTypes = () => {
        const callback = async () => {
            const { data } = await this.props.getLeaveType(this.state.keyword);
            if (!data) return;
            this.setState({
                leaveTypes: data,
            });
        };
        actionExecutor.execute(this.showLoading, callback);
    };

    addLeaveType = () => {
        const { code, description, requireApproval, amountCarryForward, defaultEntitlement } = this.state.data;

        // Validation
        const hasError = this.validate();
        if (hasError) {
            this.showModal('Error', hasError, null, true);
            return;
        }

        const callback = async () => {
            const { ok } = await this.props.addLeaveType(code, description, requireApproval.value, amountCarryForward, defaultEntitlement);
            if (!ok) this.showModal('Error', 'Failed to add leave type', null, true);
            this.showModal('Confirmation', 'Successfully added leave type', null, true);
            this.getLeaveTypes();
        };
        actionExecutor.execute(this.showLoading, callback);
    };

    updateLeaveType = () => {
        const { id, description, requireApproval, amountCarryForward, defaultEntitlement } = this.state.data;

        // Validation
        const hasError = this.validate();
        if (hasError) {
            this.showModal('Error', hasError, null, true);
            return;
        }

        const callback = async () => {
            const { ok } = await this.props.updateLeaveType(id, description, requireApproval.value, amountCarryForward, defaultEntitlement);
            if (!ok) this.showModal('Error', 'Failed to update leave type', null, true);
            this.showModal('Confirmation', 'Successfully updated leave type', null, true);
            this.getLeaveTypes();
        };
        actionExecutor.execute(this.showLoading, callback);
    };

    validate = () => {
        const { code, description, requireApproval, amountCarryForward, defaultEntitlement } = this.state.data;
        if (!code) return 'Code is required';
        if (!description) return 'Description is required';
        if (!requireApproval) return 'Require Approval is required';
        if (amountCarryForward !== 0 && !amountCarryForward) return 'Amount Carry Forward is required';
        if (defaultEntitlement !== 0 && !defaultEntitlement) return 'Default Entitlement is required';
        return false;
    };

    onDeleteLeaveTypeConfirmation = name => {
        this.showModal('Confirmation', `Are you sure to delete ${name} leave type?`, 'delete');
    };

    deleteLeaveType = () => {
        const { deleteLeaveId } = this.state;
        const callback = async () => {
            await this.props.deleteLeaveType(deleteLeaveId);
            this.getLeaveTypes();
        };
        actionExecutor.execute(this.showLoading, callback);
    };

    onKeywordChange = e => {
        this.setState({ keyword: e.target.value }, this.getLeaveTypes);
    };

    onTextChanged = e => {
        const id = e.target.id;
        const val = e.target.value;
        let { data } = this.state;
        data[id] = val;
        this.setState({ data });
    };

    onSelectChanged = (value, id) => {
        let { data } = this.state;
        data[id] = value;
        this.setState({ data });
    };

    modalToggle = () => {
        this.setState({ isModalOpen: !this.state.isModalOpen });
    };

    modalAction = () => {
        const { type } = this.state.modal;
        if (type === 'update') this.updateLeaveType();
        if (type === 'add') this.addLeaveType();
        if (type === 'delete') this.deleteLeaveType();
        this.modalToggle();
    };

    showModal = (header, body, type, onlyOneButton, positiveText) => {
        const { modal } = this.state;
        modal.modalHeader = header;
        modal.modalBody = body;
        modal.type = type;
        modal.onlyOneButton = onlyOneButton;
        modal.positiveButtonText = positiveText;
        this.setState({ modal }, this.modalToggle);
    };

    onEdit = id => {
        const data = this.state.leaveTypes.find(x => x.id === id);
        if (data) {
            this.setState({
                data: {
                    ...data,
                    requireApproval: { value: data.requireapproval, label: data.requireapproval },
                    amountCarryForward: data.amount_carry_forward,
                    defaultEntitlement: data.default_entitlement,
                },
            });
        }
        this.showModal('Update Leave Type', null, 'update', null, 'Update');
    };

    onDelete = data => {
        const { id, code } = data;
        this.setState({ deleteLeaveId: id });
        this.onDeleteLeaveTypeConfirmation(code);
    };

    render() {
        const modalBody = (
            <CustomModalBody
                isAdd={this.state.modal.type === 'add'}
                data={this.state.data}
                onTextChanged={this.onTextChanged}
                onSelectChanged={this.onSelectChanged}
            />
        );

        return (
            <Fragment>
                <DialogModal
                    key="modal"
                    modal={this.state.isModalOpen}
                    toggle={this.modalToggle}
                    customHeader={this.state.modal.modalHeader}
                    customModalBody={this.state.modal.modalBody || modalBody}
                    onlyOneButton={this.state.modal.onlyOneButton}
                    positiveButtonAction={this.modalAction}
                    positiveButtonText={this.state.modal.positiveButtonText}
                />
                <Container>
                    <Row className="mb-4">
                        <Col className="d-flex">
                            <Input className="w-300" placeholder="Search" onChange={this.onKeywordChange} value={this.state.keyword} />
                            <LinkButton
                                preset="addButton"
                                text="Leave Type"
                                onClick={() => {
                                    this.clearDataState();
                                    this.showModal('Add Leave Type', null, 'add', null, 'Add');
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            {this.state.loading ? (
                                <LoadingSpinner center />
                            ) : (
                                <ReactTable
                                    minRows={0}
                                    filterable={false}
                                    showPagination={false}
                                    className="-highlight mb-2"
                                    data={this.state.leaveTypes}
                                    columns={getColumns(this.onEdit, this.onDelete)}
                                />
                            )}
                        </Col>
                    </Row>
                </Container>
            </Fragment>
        );
    }
}

const CustomModalBody = props => {
    const { onTextChanged = () => {}, onSelectChanged = () => {}, data, isAdd } = props;
    return (
        <FormGroup>
            <CyderInput
                mandatory
                label="Code"
                inputProps={{
                    type: 'text',
                    id: 'code',
                    name: 'code',
                    disabled: !isAdd,
                    onChange: onTextChanged,
                    value: data.code,
                }}
            />
            <CyderInput
                mandatory
                label="Description"
                inputProps={{
                    type: 'text',
                    id: 'description',
                    name: 'description',
                    onChange: onTextChanged,
                    value: data.description,
                }}
            />
            <CyderInput
                mandatory
                label="Default Entitlement"
                inputProps={{
                    type: 'text',
                    id: 'defaultEntitlement',
                    name: 'defaultEntitlement',
                    onChange: onTextChanged,
                    value: data.defaultEntitlement,
                }}
            />
            <CyderInput
                mandatory
                label="Amount Carry Forward"
                inputProps={{
                    type: 'text',
                    id: 'amountCarryForward',
                    name: 'amountCarryForward',
                    onChange: onTextChanged,
                    value: data.amountCarryForward,
                }}
            />
            <CyderInput mandatory label="Require Approval">
                <Select
                    autofocus
                    name="requireApproval"
                    placeholder="Select value..."
                    options={[{ value: 'Y', label: 'Y' }, { value: 'N', label: 'N' }]}
                    onChange={value => onSelectChanged(value, 'requireApproval')}
                    value={data.requireApproval}
                />
            </CyderInput>
        </FormGroup>
    );
};

const getColumns = (onEdit, onDelete) => {
    return [
        {
            Header: 'Code',
            accessor: 'code',
        },
        {
            Header: 'Description',
            accessor: 'description',
        },
        {
            Header: 'Default Entitlement',
            accessor: 'default_entitlement',
        },
        {
            Header: 'Carry Forward',
            accessor: 'amount_carry_forward',
        },
        {
            Header: 'Require Approval',
            accessor: 'requireapproval',
            Cell: props => {
                return props.value === 'Y' ? 'Yes' : 'No';
            },
        },
        {
            Header: 'Status',
            accessor: 'status',
        },
        {
            Header: '',
            accessor: '',
            maxWidth: 100,
            Cell: props => {
                return (
                    <div>
                        <i
                            className="mr-2 material-icons align-text-bottom"
                            style={{ cursor: 'pointer' }}
                            onClick={() => onEdit(props.original.id)}
                        >
                            edit
                        </i>
                        <i
                            className="mr-1 material-icons align-text-bottom"
                            style={{ cursor: 'pointer', color: 'red' }}
                            onClick={() => onDelete(props.original)}
                        >
                            delete
                        </i>
                    </div>
                );
            },
        },
    ];
};

const mapStateToProps = state => {
    return {
        userProfile: state.cyderProfileReducer.profile,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getLeaveTypeDetail: id => {
            return dispatch(getLeaveTypeDetail(id));
        },
        addLeaveType: (code, description, requireapproval, amountCarryForward, defaultEntitlement) => {
            return dispatch(addLeaveType(code, description, requireapproval, amountCarryForward, defaultEntitlement));
        },
        updateLeaveType: (id, description, requireapproval, amountCarryForward, defaultEntitlement) => {
            return dispatch(updateLeaveType(id, description, requireapproval, amountCarryForward, defaultEntitlement));
        },
        getLeaveType: keyword => {
            return dispatch(getLeaveType(keyword));
        },
        deleteLeaveType: id => {
            return dispatch(deleteLeaveType(id));
        },
    };
};

export default withTranslation()(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(StoredLayout(LeaveTypesSettingsPage)),
);
