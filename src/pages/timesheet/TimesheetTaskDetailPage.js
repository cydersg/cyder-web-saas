import React, { Component, useState } from 'react';
import qs from 'query-string';
import moment from 'moment';
import history from 'history.js';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import StoredLayout from 'layouts/StoredLayout';
import { Button, Container, Row, Col, Card, CardBody, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { CyderInput } from 'cyderComponents/input/index';
import DialogModal from 'cyderComponents/modals/DialogModal';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import { TablePagination } from 'cyderComponents/pagination/';
import Table from 'react-table';

import { setJumbotronTitle } from 'actions/pagedata';
import * as Action from 'actions/leaves/leaveTaskAction';
import { cyderUsersSearchAction } from 'actions/users/cyderUsersSearchAction';
import { getMonthlyActivities } from 'actions/timesheet/monthlyTimesheetAction';
import dateutils from 'js/dateutils';

import ActionExecutor from 'js/ActionExecutor';
import { capitalize } from 'js/generalUtils';
import { getColumns } from 'pages/timesheet/MonthlyTimesheetPage';
import { layoutLib } from 'js/constlib';

const { getTaskDetail, rejectLeaveTask, approveTimesheetTask } = Action;

class LeaveTaskDetailPage extends Component {
    state = {
        loading: false,
        taskDetail: {},
        users: [],
        activities: [],

        // Modal
        modalLoading: false,
        isModalFormOpen: false,
        modalFormConfig: {
            modalAction: null,
            modalType: '', // Either 'approve' or 'reject'
        },
        isModalOpen: false,
        modalConfig: {
            modalHeader: 'Confirmation',
            modaleBody: '',
            onlyOneButton: false,
        },
    };

    componentDidMount() {
        this.props.setJumbotronTitle('Report');
        const { taskid } = this.props.match.params;
        if (taskid) this.getTaskDetails(taskid);
        this.getUsers();
    }

    showLoading = loading => {
        this.setState({ loading });
    };

    showModalLodaing = modalLoading => {
        this.setState({ modalLoading });
    };

    getTaskDetails = taskid => {
        const callback = async () => {
            const { data } = await this.props.getTaskDetail(taskid);
            this.getMonthlyActivities(data.owner, data.reportMonth, data.reportYear);
            this.setState({ taskDetail: data });
        };
        ActionExecutor.execute(this.showLoading, callback);
    };

    getMonthlyActivities = (owner, month, year) => {
        if (!month || !year) return;
        const callback = async () => {
            const { data } = await this.props.getMonthlyActivities({
                owner,
                month,
                year,
            });
            if (!data) return;
            this.setState({ activities: data });
        };
        ActionExecutor.execute(this.showLoading, callback);
    };

    getUsers = () => {
        const callback = async () => {
            const data = await this.props.cyderUsersSearchAction({ keyword: '', status: 'Active' });
            this.setState({ users: data });
        };
        ActionExecutor.execute(this.showLoading, callback);
    };

    onApprove = () => {
        const { location, approveTimesheetTask } = this.props;
        this.showModalForm('approve', () => {
            const callback = async () => {
                const taskId = qs.parse(location.search.substring(1)).realid;
                const { ok, status } = await approveTimesheetTask(taskId);
                this.toggleModalForm();

                // Modal Alert
                if (ok && status === 200) {
                    this.showModal(
                        'Confirmation',
                        'Successfully approved timesheet task.',
                        () => history.push('../timesheet/timesheetapproval'),
                        true,
                    );
                    return;
                }
                this.showModal('Error', 'Failed to approve timesheet task.', null, true);
            };
            ActionExecutor.execute(this.showModalLodaing, callback);
        });
    };

    onReject = () => {
        const { location, rejectLeaveTask } = this.props;

        this.showModalForm('reject', remarks => {
            const callback = async () => {
                const realid = qs.parse(location.search.substring(1)).realid;
                const { ok, status } = await rejectLeaveTask(realid, remarks);
                this.toggleModalForm();

                // Modal Alert
                if (ok && status === 200) {
                    this.showModal(
                        'Confirmation',
                        'Successfully rejected timesheet task.',
                        () => history.push('../timesheet/timesheetapproval'),
                        true,
                    );
                    return;
                }
                this.showModal('Error', 'Failed to reject timesheet task.', null, true);
            };
            ActionExecutor.execute(this.showModalLodaing, callback);
        });
    };

    toggleModalForm = () => {
        this.setState({ isModalFormOpen: !this.state.isModalFormOpen });
    };

    showModalForm = (type, action) => {
        const modalFormConfig = {
            ...this.state.modalFormConfig,
            modalAction: action,
            modalType: type,
        };
        this.setState({ modalFormConfig });
        this.toggleModalForm();
    };

    toggleModal = () => {
        this.setState({ isModalOpen: !this.state.isModalOpen });
    };

    showModal = (header, body, action, onlyOneButton) => {
        const modalConfig = {
            ...this.state.modalConfig,
            modalHeader: header,
            modalBody: body,
            modalAction: action,
            onlyOneButton,
        };
        this.setState({ modalConfig });
        this.toggleModal();
    };

    render() {
        const { taskDetail, activities, users, modalFormConfig, isModalFormOpen, isModalOpen, modalConfig, modalLoading } = this.state;

        const userInfo = users.find(x => x.email === taskDetail.owner) || {};
        return (
            <div>
                <ModalForm
                    isOpen={isModalFormOpen}
                    loading={modalLoading}
                    toggle={this.toggleModalForm}
                    type={modalFormConfig.modalType}
                    onSubmit={modalFormConfig.modalAction}
                />
                <DialogModal
                    key="confirmModal"
                    modal={isModalOpen}
                    toggle={this.toggleModal}
                    customHeader={modalConfig.modalHeader}
                    customModalBody={modalConfig.modalBody}
                    positiveButtonAction={modalConfig.modalAction}
                    onlyOneButton={modalConfig.onlyOneButton}
                />
                <Container className="wideContainer">
                    <RowColWrapper rowProps={{ className: 'mb-2 mb-4' }}>
                        <Link to="../../timesheetapproval" tabIndex="-1">
                            <i className="align-middle material-icons material-icons-3x">chevron_left</i>
                        </Link>
                    </RowColWrapper>
                    <DetailPanel
                        {...this.state}
                        {...this.props}
                        activities={activities}
                        taskDetail={taskDetail}
                        userInfo={userInfo}
                        onApprove={this.onApprove}
                        onReject={this.onReject}
                    />
                </Container>
            </div>
        );
    }
}

const DetailPanel = props => {
    const { activities, taskDetail, userInfo, onApprove, onReject } = props;
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return (
        <Container className="wideContainer">
            <Row className="mb-4">
                <Col>
                    <h2 className="font-weight-bold d-none d-md-block">{taskDetail.title}</h2>
                    <h4 className="font-weight-bold d-block d-md-none">{taskDetail.title}</h4>
                    <small className="font-weight-bold">
                        Submitted on {moment(taskDetail.createddt, 'YYYY-MM-DD').format('DD-MMM-YYYY')}{' '}
                    </small>
                </Col>
            </Row>
            <Row>
                <Col xs="12" md="6">
                    <InfoField label="Owner" value={userInfo.firstname + ' ' + userInfo.lastname} />
                </Col>
                <Col xs="12" md="6">
                    <InfoField
                        className="d-none d-sm-block"
                        label="Report Month"
                        value={dateutils.monthsArray[taskDetail.reportMonth - 1]}
                    />
                    <InfoField className="d-block d-sm-none" label="Email" value={taskDetail.owner} />
                </Col>
            </Row>
            <Row>
                <Col xs="12" md="6">
                    <InfoField className="d-none d-sm-block" label="Email" value={taskDetail.owner} />
                    <InfoField
                        className="d-block d-sm-none"
                        label="Report Month"
                        value={dateutils.monthsArray[taskDetail.reportMonth - 1]}
                    />
                </Col>
                <Col xs="12" md="6">
                    <InfoField label="Report Year" value={taskDetail.reportYear} />
                </Col>
            </Row>
            <Row className="mb-4">
                <Col className="text-right">
                    <Button color="success" className="m-2" onClick={onApprove}>
                        Approve
                    </Button>
                    <Button color="danger" className="m-2" onClick={onReject}>
                        Reject
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Table
                        className="-highlight mb-2"
                        showPagination={!isMobile}
                        data={activities}
                        columns={getColumns()}
                        NoDataComponent={() => <div className="rt-noData">No results found</div>}
                        pageSize={activities.length > 10 ? 10 : activities.length}
                        PaginationComponent={TablePagination}
                    />
                </Col>
            </Row>
        </Container>
    );
};

const ModalForm = props => {
    const { isOpen, loading, toggle, type, onSubmit } = props;
    const [remarks, setRemarks] = useState('');

    return (
        <Modal isOpen={isOpen}>
            <ModalHeader className="border-0" toggle={toggle}>
                <b>{capitalize(type)} Timesheet Task</b>
            </ModalHeader>
            <ModalBody>
                {loading ? (
                    <LoadingSpinner />
                ) : (
                    <div>
                        <RowColWrapper>Are you sure you want to {type} this timesheet task?</RowColWrapper>
                        {type !== 'approve' && (
                            <CyderInput
                                inputProps={{
                                    rows: 4,
                                    type: 'textarea',
                                    placeholder: 'Please provide your remarks',
                                    value: remarks,
                                    onChange: e => setRemarks(e.target.value),
                                }}
                            />
                        )}
                    </div>
                )}
            </ModalBody>
            <ModalFooter>
                <Button color="success" onClick={() => onSubmit(remarks)}>
                    {type === 'approve' ? 'Approve' : 'Reject'}
                </Button>
                <Button color="light" className="color-danger" onClick={toggle}>
                    Cancel
                </Button>
            </ModalFooter>
        </Modal>
    );
};

export const InfoField = props => (
    <div className={`${props.className}kom`}>
        <span className="text-muted font-weight-bold">{props.label}</span>
        <h6 className="font-weight-bold">{props.value} </h6>
    </div>
);

const mapStateToProps = state => {
    return {};
};

const actionlist = {
    getTaskDetail,
    setJumbotronTitle,
    cyderUsersSearchAction,
    rejectLeaveTask,
    approveTimesheetTask,
    getMonthlyActivities,
};

export default withTranslation()(connect(mapStateToProps, actionlist)(StoredLayout(LeaveTaskDetailPage)));
