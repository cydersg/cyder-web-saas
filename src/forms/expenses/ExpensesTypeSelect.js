/**
 * Expense Type Select
 * 
 * Author: Fernando
 */

import React, {Component} from 'react'
import Select from 'react-select'
import {connect} from 'react-redux'
// import history from '../../history'
import {updateSelectedCategoryAction} 
  from '../../actions/expenses/expenseSelectCategoryAction'

class ExpenseTypeSelect extends Component {
  render() {
    return ( 
      <div className="row">
        <div className="col">
          <div className="form-group">
            <label>Expense Category</label>
            <Select
              autosize={false}
              placeholder="Select expense category"
              autofocus
              options={this.props.expenseCategories.options}
              simpleValue
              clearable={this.props.expenseCategories.itemValue.clearable}
              name="selected-expense-category"
              disabled={this.props.expenseCategories.itemValue.disabled}
              value={this.props.expenseCategories.itemValue.value}
              onChange={this.props.updateValue}
              searchable={this.props.expenseCategories.itemValue.searchable}
            />
            <small className="form-text text-muted">Please select expense category</small>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    expenseCategories: state.cyderExpenseReducer.categories
  }
}
const mapDispatchToProps = dispatch => {
  return {
    updateValue: (e) => {
      dispatch(updateSelectedCategoryAction(e));
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ExpenseTypeSelect)