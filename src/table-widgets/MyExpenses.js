/**
 * My Expenses table structure
 * 
 * Author: Fernando
 */

import React from 'react'
import PropTypes from 'prop-types'
import myexpenses from '../json/myexpenses.json'

const MyExpenses = ({items}) => (
  <div className="table-widget-4">
    <table className="table table-unbordered table-striped">
      <thead>
        <tr>
          <th>Category</th>
          <th>Date</th>
          <th>Remarks</th>
          <th>Amount</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {items.map((item, i) => (
          <tr key={i}>
            <td>
              <span className={`badge badge-${item.color} badge-outline`}>
                {item.category}
              </span>
            </td>
            <td>{item.date}</td>
            <td>{item.remarks}</td>
            <td>{item.amount}</td>  
            <td>
              <i className="material-icons">search</i>
              <i className="material-icons">delete</i>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
)

MyExpenses.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object)
}

export const MyExpensesTableWidget = () => <MyExpenses items={myexpenses} />
