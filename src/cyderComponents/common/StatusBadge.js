import React from 'react';

import { Badge, FormText } from 'reactstrap';

const statusBadgeColor = status => {
    switch (status) {
        case 'AutoRec':
            return 'default';
        case 'Attention':
            return 'secondary';
        case 'Ready':
            return 'warning';
        case 'Returned':
            return 'danger';
        case 'Approved':
            return 'success';
        case 'Pending Payment':
            return 'info';
        case 'PendingPayment':
            return 'info';
        case 'Paid':
            return 'success';
        case 'Submitted':
        default:
            return 'primary';
    }
};

const StatusBadge = ({ status, rejected }) => {
    return (
        <div className="d-flex justify-content-center">
            <div>
                <Badge color={statusBadgeColor(status)}>{status}</Badge>
                {rejected === 'Y' ? <FormText className="text-center text-danger">Rejected</FormText> : null}
            </div>
        </div>
    );
};
export default StatusBadge;
