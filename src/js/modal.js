export default {
    calcCurrentPositiveButtonText(modalAction) {
        switch (modalAction) {
            case "update":
                return "Update";
            case "add":
                return "Add";
            case "save":
                return "Save";
            case "remove":
                return "Remove";
            case "delete":
                return "Delete";
            case "redirect":
                return "Ok";
            case "close":
                return "Ok";
            default:
                return "Ok"
        }
    },
};
