import React from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';
import debounce from 'es6-promise-debounce';
import moment from 'moment';
import { withTranslation } from 'react-i18next';

import ReactTable from 'react-table';
import { Async } from 'react-select';
import { Alert, Input, Label, Button, Container, Row, Col, FormGroup } from 'reactstrap';
import DialogModal from 'cyderComponents/modals/DialogModal';
import LinkButton from 'cyderComponents/buttons/LinkButton';
import CyderDatePicker from 'cyderComponents/forms/CyderDatePicker';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

import {
    delegationsSearchAction,
    delegationsEditModalSetDelegation,
    delegationsModalToggleAction,
    delegationsEditModalChangeValue,
    delegationsAddDelegationAction,
    delegationsUpdateDelegationAction,
    delegationsDeleteDelegationAction,
} from 'actions/workflow/delegationsAction';
import { cyderGroupsAddAddUserSearchAction } from 'actions/groups/cyderGroupsAddAction';
import { setJumbotronTitle } from 'actions/pagedata';

import cyderlib from 'js/cyderlib';
import reactTableFilter from 'js/reactTableFilter';

import '../../css/forms/react-datetime.css';

class DelegationsPage extends React.Component {
    constructor(props) {
        super(props);
        this.onFilteredChange = this.onFilteredChange.bind(this);
        this.filterAllFunc = this.filterAllFunc.bind(this);
        this.state = {
            filterAll: '',
            approvers: {},
            delegateUsers: {},
        };
        this.calcCurrentPositiveButtonAction = this.calcCurrentPositiveButtonAction.bind(this);
        this.setDelegateUser = this.setDelegateUser.bind(this);
        this.getOptionsDelegateUsers = this.getOptionsDelegateUsers.bind(this);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.toggleAction = this.toggleAction.bind(this);
        this.filterOptionsUsers = this.filterOptionsUsers.bind(this);
        this.filterOptionsApprovers = this.filterOptionsApprovers.bind(this);
        this.handleSwitchChange = this.handleSwitchChange.bind(this);
    }
    handleSwitchChange(checked) {
        const synthEvent = {
            target: {
                id: 'overrideexpenseapproval',
                value: checked,
            },
        };
        this.props.changeModalInputValue(synthEvent);
    }
    componentDidMount() {
        if (this.props.forceRefresh) {
            this.props.getDelegations();
        }
        this.props.getDelegations();
        this.props.setJumbotronTitle();
        // this.props.getAllProjects();
    }
    setupEditModal(delegation) {
        this.props.setModalDelegation(delegation);
        let newDelegateUsersObj = {
            1: null,
            2: null,
        };
        if (delegation.delegator) {
            newDelegateUsersObj[1] = {
                label: delegation.delegator,
                value: delegation.delegator,
            };
        }
        if (delegation.delegatee) {
            newDelegateUsersObj[2] = {
                label: delegation.delegatee,
                value: delegation.delegatee,
            };
        }
        this.setState({
            delegateUsers: newDelegateUsersObj,
        });
        this.props.toggleModal('update');
    }
    setupDeleteModal(delegation) {
        this.props.setModalDelegation(delegation);
        this.props.toggleModal('delete');
    }
    getOptionsDelegateUsers(input) {
        // const username = document.getElementById('searchUsername').value;
        return this.props.getOptions(input, this.props.approversReference);
    }
    toggleAction() {
        this.handleToggleModal();
        if (this.props.modalAction === 'refresh') {
            this.props.getDelegations();
        }
    }
    calcCurrentPositiveButtonText() {
        if (this.props.modalAction === 'update') {
            return 'Update';
        } else if (this.props.modalAction === 'add') {
            return 'Add';
        } else if (this.props.modalAction === 'delete') {
            return 'Delete';
        } else if (this.props.modalAction === 'close') {
            return 'Ok';
        } else {
            return 'Ok';
        }
    }
    calcCurrentPositiveButtonAction() {
        if (this.props.modalAction === 'update') {
            return () => {
                this.props.handleUpdate(this.state.delegateUsers);
            };
        } else if (this.props.modalAction === 'refresh') {
            return () => {
                this.handleToggleModal();
                this.props.getDelegations();
            };
        } else if (this.props.modalAction === 'add') {
            return () => {
                this.props.handleAdd(this.state.delegateUsers);
            };
        } else if (this.props.modalAction === 'delete') {
            return this.props.handleDelete;
        } else if (this.props.modalAction === 'close') {
            return this.handleToggleModal;
        } else {
            return this.handleToggleModal;
        }
    }
    handleToggleModal(modalAction) {
        if (modalAction === 'add') {
            let delegateUsers = {};
            if (this.props.userProfile.role === 0) {
                delegateUsers[1] = {
                    value: this.props.userProfile.email,
                    label: this.props.userProfile.email,
                };
            }
            this.setState({
                delegateUsers,
            });
        }
        this.props.toggleModal(modalAction);
    }
    filterOptionsUsers(options) {
        const results = options.filter(item => !this.props.usersReference[item.value]);
        return results;
    }
    filterOptionsApprovers(options) {
        const results = options.filter(item => {
            let delegateUsers = Object.values(this.state.delegateUsers);
            const hasOption = delegateUsers.findIndex(approver => {
                return approver !== null && approver.value === item.value;
            });
            return hasOption === -1;
        });
        return results;
    }
    setDelegateUser(val, i) {
        let newDelegators = {};
        newDelegators[i] = val;
        this.setState({
            delegateUsers: Object.assign(this.state.delegateUsers, newDelegators),
        });
    }
    handleExpenseDateChange(momentOrStringVal, id) {
        const synthEvent = {
            target: {
                id,
                value: momentOrStringVal.format ? momentOrStringVal.format('DD/MM/YYYY') : momentOrStringVal,
            },
        };
        this.props.changeModalInputValue(synthEvent);
    }
    onFilteredChange(filtered) {
        this.setState(reactTableFilter.onFilteredChange(filtered));
    }
    filterAllFunc(e) {
        this.setState(reactTableFilter.filterAllFunc(e));
    }
    render() {
        const customModalBody = (
            <div>
                <FormGroup>
                    <Label for="exampleText">Start Date</Label>
                    <CyderDatePicker
                        onChange={event => this.handleExpenseDateChange(event, 'start_date')}
                        value={this.props.modalDelegation.start_date}
                        inputProps={{ placeholder: 'DD/MM/YYYY' }}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleText">End Date</Label>
                    <CyderDatePicker
                        onChange={event => this.handleExpenseDateChange(event, 'end_date')}
                        value={this.props.modalDelegation.end_date}
                        inputProps={{ placeholder: 'DD/MM/YYYY' }}
                    />
                </FormGroup>
                <FormGroup>
                    <Label>Delegator</Label>
                    <Async
                        disabled={this.props.userProfile.role === 0}
                        placeholder="Search user to add..."
                        autoload={false}
                        value={this.state.delegateUsers[1]}
                        onChange={val => {
                            this.setDelegateUser(val, 1);
                        }}
                        name="async"
                        filterOptions={this.filterOptionsApprovers}
                        loadOptions={debounce(this.getOptionsDelegateUsers, 500)}
                    />
                </FormGroup>
                <FormGroup>
                    <Label>Delegatee</Label>
                    <Async
                        placeholder="Search user to add..."
                        autoload={false}
                        value={this.state.delegateUsers[2]}
                        onChange={val => {
                            this.setDelegateUser(val, 2);
                        }}
                        name="async"
                        filterOptions={this.filterOptionsApprovers}
                        loadOptions={debounce(this.getOptionsDelegateUsers, 500)}
                    />
                </FormGroup>
            </div>
        );
        const columns = [
            reactTableFilter.generateAllColumnFilter(['start_date', 'end_date', 'delegator', 'delegatee']),
            {
                Header: 'Start Date',
                accessor: 'start_date',
                sortMethod: cyderlib.tableSortDate,
                Cell: props => moment(props.value).format('DD/MM/YYYY'),
            },
            {
                Header: 'End Date',
                accessor: 'end_date',
                sortMethod: cyderlib.tableSortDate,
                Cell: props => moment(props.value).format('DD/MM/YYYY'),
            },
            {
                Header: 'Delegator',
                accessor: 'delegator',
            },
            {
                Header: 'Delegatee',
                accessor: 'delegatee',
            },
            {
                Header: '',
                accessor: 'id',
                filterable: false,
                Cell: props => {
                    const thisDelegation = this.props.delegations[props.index];
                    let showStyle = { visibility: 'hidden' };
                    if (this.props.userProfile.role === 0) {
                        // user is not admin. selectively show delete button
                        if (props.row.delegator === this.props.userProfile.email) {
                            // show button if user is delegator
                            showStyle = { visibility: 'unset' };
                        }
                    } else {
                        showStyle = { visibility: 'unset' };
                    }
                    return (
                        <div className="float-right">
                            <Button
                                style={showStyle}
                                color="link"
                                size="sm"
                                onClick={() => {
                                    this.setupEditModal(thisDelegation);
                                }}
                            >
                                <i className="align-middle material-icons">edit</i>
                            </Button>
                            <Button
                                style={showStyle}
                                color="link"
                                className="ml-1"
                                size="sm"
                                onClick={() => {
                                    this.setupDeleteModal(thisDelegation);
                                }}
                            >
                                <i className="text-danger align-middle material-icons">delete</i>
                            </Button>
                        </div>
                    );
                },
            },
        ];
        return (
            <div key={0}>
                <Container>
                    <Row className="mb-2">
                        <Col className="d-flex">
                            <Input className="w-300" placeholder="Search" onChange={this.filterAllFunc} value={this.state.filterAll} />
                            <LinkButton preset="addButton" onClick={() => this.handleToggleModal('add')} text="Delegation" />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            {this.props.loading ? (
                                <LoadingSpinner center />
                            ) : this.props.delegations.length > 0 ? (
                                <ReactTable
                                    filtered={this.state.filtered}
                                    onFilteredChange={this.onFilteredChange}
                                    filterable={false}
                                    defaultFilterMethod={reactTableFilter.defaultFilterMethod}
                                    className="-highlight mb-2"
                                    data={this.props.delegations}
                                    columns={columns}
                                    minRows={0}
                                    showPagination={false}
                                />
                            ) : this.props.errorMessage ? (
                                <Alert color="danger">{this.props.errorMessage}</Alert>
                            ) : null}
                        </Col>
                    </Row>
                </Container>
                <DialogModal
                    key="modal"
                    modal={this.props.modalOpen}
                    toggle={this.toggleAction}
                    customHeader={this.props.modalHeader}
                    customModalBody={
                        this.props.saving ? <LoadingSpinner /> : this.props.modalMessage ? this.props.modalMessage : customModalBody
                    }
                    positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                    positiveButtonText={this.calcCurrentPositiveButtonText()}
                    onlyOneButton={this.props.saved}
                    buttonDisabler={this.props.saving}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ...state.delegationsReducer,
        userProfile: state.cyderProfileReducer.profile,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        getDelegations: () => {
            dispatch(delegationsSearchAction());
        },
        setModalDelegation: delegation => {
            dispatch(delegationsEditModalSetDelegation(delegation));
        },
        toggleModal: modalAction => {
            if (modalAction === 'add') {
                const modalDelegation = {
                    delegator: '',
                    delegatee: '',
                    start_date: moment().format('DD/MM/YYYY'),
                    end_date: moment().format('DD/MM/YYYY'),
                };
                dispatch(delegationsEditModalSetDelegation(modalDelegation));
            }
            dispatch(delegationsModalToggleAction(modalAction ? modalAction : null));
        },
        changeModalInputValue: event => {
            const value = event.target.value;
            const key = event.target.id;
            dispatch(delegationsEditModalChangeValue(key, value));
        },
        getOptions: input => {
            // const username = document.getElementById('searchUsername').value;
            return dispatch(cyderGroupsAddAddUserSearchAction(input));
        },
        handleAdd: delegateUsers => {
            dispatch(delegationsAddDelegationAction(delegateUsers));
        },
        handleUpdate: delegateUsers => {
            dispatch(delegationsUpdateDelegationAction(delegateUsers));
        },
        handleDelete: () => dispatch(delegationsDeleteDelegationAction()),
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Settings')),
    };
};
// translate() is if we want to use HOC to perform t()
export default withTranslation()(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(StoredLayout(DelegationsPage)),
);
