import withQuery from 'with-query';
import fetchlib from 'js/fetchlib';
import { CYDER_COMMON_API_ROOT } from 'config.js';

export function getTaskDetail(instanceid) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_COMMON_API_ROOT}/workflow`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'gettaskdetails',
            instanceid,
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, null, null, null, null, true);
    };
}

export function approveTimesheetTask(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_COMMON_API_ROOT}/workflow`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'approveTimesheet',
        });
        const body = {
            id,
        };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body, null, null, null, true);
    };
}

export function approveLeaveTask(id, remarks) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_COMMON_API_ROOT}/workflow`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'approve',
        });
        const body = {
            id,
            remarks,
        };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body, null, null, null, true);
    };
}

export function rejectLeaveTask(id, remarks) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_COMMON_API_ROOT}/workflow`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'reject',
        });
        const body = {
            id,
            remarks,
            data: [id],
        };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body, null, null, null, true);
    };
}

export function deleteTask(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_COMMON_API_ROOT}/workflow`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'delete',
        });
        const body = {
            id,
        };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body, null, null, null, true);
    };
}
