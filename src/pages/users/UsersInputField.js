import React from 'react'
import {
    Input,
    // Table,
    Col,
    FormFeedback,
} from "reactstrap";
const InputField = ({
    sm,
    inputProps,
    formFeedbackText,
    formFeedbackValid,
    validInput,
    value,
    valueKey,
    type,
    handleChange,
    noFeedback,
    disabled
}) => {
    return (
        <Col sm={sm || 10}>
            <Input
                disabled={disabled}
                value={value}
                type={type}
                id={valueKey}
                valid={validInput(valueKey)}
                invalid={validInput(valueKey) === false}
                onChange={(event) => handleChange(event)}
                {...inputProps}
            />
            {noFeedback ? null : (
                <FormFeedback
                    valid={formFeedbackValid(valueKey)}
                >
                    {formFeedbackText(valueKey)}
                </FormFeedback>
            )}
        </Col>
    )
}

export default InputField;
