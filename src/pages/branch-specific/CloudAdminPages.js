import React from 'react';
import PrivateRoute from '../../cyderComponents/common/PrivateRoute'
import AccountsSearchPage from '../accounts/AccountsSearchPage'
import AccountsDetailPage from '../accounts/AccountsDetailPage'
import { Switch } from 'react-router'

const CloudAdminPages = () => (
    <Switch>
        <PrivateRoute exact path="/accounts" component={AccountsSearchPage} />
        <PrivateRoute exact path="/accounts/details/:accountid" component={AccountsDetailPage} />
    </Switch>
)

export default CloudAdminPages
