import React, { Component, Fragment, useState } from 'react';
import { connect } from 'react-redux';
import StoredLayout from '../../layouts/StoredLayout';
import moment from 'moment';
import { Link } from 'react-router-dom';
import DialogModal from 'cyderComponents/modals/DialogModal';
import Table from 'react-table';
import { Col, Row, Badge, Container, Modal, ModalBody, ModalFooter, Button, ModalHeader } from 'reactstrap';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import { TablePagination } from 'cyderComponents/pagination/';
import { ConfirmModalManager } from 'cyderComponents/ConfirmModal';
import { InfoField } from 'cyderComponents/input/CyderPreview';
import { capitalize } from 'js/generalUtils';
import { rejectLeaveTask, approveTimesheetTask, deleteTask } from 'actions/leaves/leaveTaskAction';
import { setJumbotronTitle } from 'actions/pagedata';
import * as Actions from 'actions/timesheet/monthlyTimesheetAction';
import { cyderUsersSearchAction } from 'actions/users/cyderUsersSearchAction';
import { expenseNewReceiptClaimGetHistory } from 'actions/expenses/cyderExpenseNewReceiptClaimAction';
import { layoutLib } from 'js/constlib';
import { CyderInput } from 'cyderComponents/input/index';
import cyderlib from 'js/cyderlib';
import ActionExecutor from 'js/ActionExecutor';
import { getColumns } from 'pages/timesheet/MyCorpTimesheetHistory';
import MyCorpTimesheetBottomNav from './MyCorpTimesheetBottomNav';
import ButtonPrimaryRound from 'cyderComponents/buttons/ButtonPrimaryRound';
import ButtonSecondaryRound from 'cyderComponents/buttons/ButtonSecondaryRound';

const { findMonthlyActivityByDateRange, findMonthlyActivityByReportId, getProjects, deleteTimesheetReport } = Actions;

class MyCorpTimesheetReportDetailPage extends Component {
    state = {
        loading: false,
        year: '',
        returnedRemarks: '',
        reports: [],
        projects: [],
        users: [],
        isModalOpen: false,
        isModalFormOpen: false,
        modalFormConfig: {
            modalAction: null,
            modalType: '', // Either 'approve' or 'reject'
        },
        modalConfig: {
            modalHeader: 'Confirmation',
            modaleBody: '',
            onlyOneButton: false,
        },
        modal: {
            header: '',
            body: '',
            action: null,
            onlyOneButton: false,
        },
    };

    componentDidMount() {
        this.props.setJumbotronTitle('Report');
        this.getUsers();
        this.getProjects();
        this.findMonthlyActivity();
        this.getResourceHistory();
    }

    setLoading = loading => {
        this.setState({ loading });
    };

    findMonthlyActivity = () => {
        const { reportId } = this.props.match.params;
        const callback = async () => {
            const { data } = await this.props.findMonthlyActivityByReportId(reportId);
            if (!data) {
                ConfirmModalManager.show('Error', 'Failed to retrived report details.', () => ConfirmModalManager.toggle(), true);
                return;
            }
            this.setState({ reports: data });
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    deleteTimesheetReport = () => {
        const callback = async () => {
            const { ok } = await this.props.deleteTimesheetReport(this.state.reports.id);
            if (ok) {
                this.showModal('Confirmation', 'Timesheet report successfully deleted', null, true);
                this.props.history.goBack();
                return;
            }
            this.showModal('Error', 'Timesheet report was not deleted.', null, true);
        };
        this.showModal(
            'Confirmation',
            'Are you sure you want to delete this report?',
            () => ActionExecutor.execute(this.setLoading, callback),
            false,
        );
    };

    deleteTask = () => {
        const { history } = this.props;
        this.showModalForm('delete', () => {
            const callback = async () => {
                const taskId = this.props.match.params.taskId;
                const { ok, status } = await this.props.deleteTask(taskId);
                this.toggleModalForm();

                // Modal Alert
                if (ok && status === 200) {
                    this.showModal(
                        'Confirmation',
                        'Timesheet report deleted successfully',
                        () => history.push('/mycorp/timesheet/approval'),
                        true,
                    );
                    return;
                }
                this.showModal('Error', 'Failed to delete timesheet task.', null, true);
            };
            ActionExecutor.execute(this.showModalLoading, callback);
        });
    };

    setIsModalOpen = (isModalOpen, id) => {
        let stateCopy = Object.assign({}, this.state);
        const key = id === 'submitModal' ? 'isSubmitModalOpen' : 'isModalOpen';
        stateCopy[key] = isModalOpen;
        this.setState(stateCopy);
    };

    showModal = (header, body, action, onlyOneButton) => {
        this.setIsModalOpen(true);
        const { modal } = this.state;
        const modalConfig = {
            ...this.state.modal,
            header: header || modal.header,
            body: body || modal.body,
            action: action || null,
            onlyOneButton: onlyOneButton || modal.onlyOneButton,
        };
        this.setState({ modal: modalConfig });
    };

    getProjects = () => {
        const callback = async () => {
            const { data } = await this.props.getProjects();
            this.setState({ projects: data });
        };
        ActionExecutor.execute(() => {}, callback);
    };

    getUsers = () => {
        const callback = async () => {
            const data = await this.props.cyderUsersSearchAction({ keyword: '', status: 'Active' });
            this.setState({ users: data });
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    getResourceHistory = () => {
        const { reportId } = this.props.match.params;
        if (!reportId) return;
        const callback = async () => {
            const data = await this.props.expenseNewReceiptClaimGetHistory(reportId);
            if (!data) return;
            const returnedInfo = data.find(x => x.status === 'Returned') || {};
            this.setState({ returnedRemarks: returnedInfo.remarks });
        };
        ActionExecutor.execute(() => {}, callback);
    };

    onApprove = () => {
        const { approveTimesheetTask, history } = this.props;
        this.showModalForm('approve', () => {
            const callback = async () => {
                const taskId = this.props.match.params.taskId;
                const { ok, status } = await approveTimesheetTask(taskId);
                this.toggleModalForm();

                // Modal Alert
                if (ok && status === 200) {
                    this.showModal(
                        'Confirmation',
                        'Timesheet report approved successfully',
                        () => history.push('/mycorp/timesheet/approval'),
                        true,
                    );
                    return;
                }
                this.showModal('Error', 'Failed to approve timesheet task.', null, true);
            };
            ActionExecutor.execute(this.showModalLoading, callback);
        });
    };

    showModalLoading = modalLoading => {
        this.setState({ modalLoading });
    };

    onReject = () => {
        const { rejectLeaveTask, history } = this.props;

        this.showModalForm('reject', remarks => {
            const callback = async () => {
                const taskId = this.props.match.params.taskId;
                const { ok, status } = await rejectLeaveTask(taskId, remarks);
                this.toggleModalForm();

                // Modal Alert
                if (ok && status === 200) {
                    this.showModal(
                        'Confirmation',
                        'Timesheet report rejected successfully.',
                        () => history.push('/mycorp/timesheet/approval'),
                        true,
                    );
                    return;
                }
                this.showModal('Error', 'Failed to reject timesheet task.', null, true);
            };
            ActionExecutor.execute(this.showModalLoading, callback);
        });
    };

    showModalForm = (type, action) => {
        const modalFormConfig = {
            ...this.state.modalFormConfig,
            modalAction: action,
            modalType: type,
        };
        this.setState({ modalFormConfig });
        this.toggleModalForm();
    };

    toggleModal = () => {
        this.setState({ isModalOpen: !this.state.isModalOpen });
    };

    toggleModalForm = () => {
        this.setState({ isModalFormOpen: !this.state.isModalFormOpen });
    };

    render() {
        const {
            returnedRemarks,
            users,
            reports,
            projects,
            loading,
            modal,
            modalFormConfig,
            isModalFormOpen,
            isModalOpen,
            modalConfig,
            modalLoading,
        } = this.state;
        const { activities = [], title, reportMonth, reportYear, owner, createddt, status } = reports;
        const userInfo = users.find(x => x.email === owner) || {};
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        const isDeletable =
            !this.props.match.params.taskId && this.props.profile.username === reports.owner && reports.status === 'Pending';

        // TODO: to add checking based on the current assignee
        const isApprovable = this.props.match.params.taskId && reports.status === 'Pending';

        return (
            <Container className="wideContainer">
                <DialogModal
                    key="confirmModal"
                    modal={isModalOpen}
                    toggle={isOpen => this.setIsModalOpen(isOpen, 'confirmModal')}
                    customHeader={modal.header}
                    customModalBody={modal.body}
                    positiveButtonAction={modal.action}
                    onlyOneButton={modal.onlyOneButton}
                />
                <ModalForm
                    isOpen={isModalFormOpen}
                    loading={modalLoading}
                    toggle={this.toggleModalForm}
                    type={modalFormConfig.modalType}
                    onSubmit={modalFormConfig.modalAction}
                />
                <Row className="justify-content-center">
                    <Col
                        className="ph0"
                        style={{
                            textAlign: 'center',
                        }}
                    >
                        {/* <b>MyCorp Check In</b> */}
                        <hr className="mycorp-hr-small" />
                    </Col>
                </Row>
                <RowColWrapper>
                    {loading ? (
                        <LoadingSpinner center />
                    ) : reports.activities && reports.activities.length <= 0 ? (
                        <RowColWrapper rowProps={{ className: 'ph0' }}>
                            <Link to={this.props.match.params.taskId ? '../../approval' : '../report'} tabIndex="-1">
                                <i className="align-middle material-icons material-icons-2x">chevron_left</i>
                            </Link>

                            <h4>Opps, report does not exist</h4>
                            <span>
                                This report does not exist. Please contact the requester, as this could have been deleted, or contact
                                MyCorp.
                            </span>
                            <Row className="mycorp-big-icon-panel">
                                <ButtonPrimaryRound onClick={this.deleteTask} faIcon="trash" />
                            </Row>
                        </RowColWrapper>
                    ) : (
                        <Fragment>
                            <RowColWrapper rowProps={{ className: 'ph0' }}>
                                <Link to={this.props.match.params.taskId ? '../../approval' : '../report'} tabIndex="-1">
                                    <i className="align-middle material-icons material-icons-2x">chevron_left</i>
                                </Link>
                            </RowColWrapper>
                            <div>
                                <Row className="mb-4 ph0">
                                    <Col>
                                        <h3 className="font-weight-bold d-none d-md-block">{title}</h3>
                                        <h6 className="font-weight-bold d-block d-md-none">{title}</h6>
                                        <small>Submitted on {moment(createddt, 'YYYY-MM-DD').format('DD-MMM-YYYY')} </small>
                                        <br />
                                        <Badge color={cyderlib.statusBadgeColor(status)}>{status}</Badge>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="12" md="6">
                                        <InfoField label="Owner" value={userInfo.firstname + ' ' + userInfo.lastname} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="12" md="6">
                                        <InfoField label="Email" value={owner} />
                                    </Col>
                                </Row>
                                {returnedRemarks && status === 'Returned' && (
                                    <>
                                        <Row>
                                            <Col xs="12">
                                                <InfoField label="Returned Remarks" value={returnedRemarks} />
                                            </Col>
                                        </Row>
                                        <br />
                                    </>
                                )}
                            </div>
                            {(isDeletable || isApprovable) && (
                                <Row className="mycorp-big-icon-panel">
                                    {isDeletable && <ButtonPrimaryRound onClick={this.deleteTimesheetReport} faIcon="trash" />}
                                    {isApprovable && <ButtonPrimaryRound onClick={this.onApprove} faIcon="thumbs-up" />}
                                    {isApprovable && <ButtonSecondaryRound onClick={this.onReject} faIcon="thumbs-down" />}
                                </Row>
                            )}
                            <RowColWrapper colProps={{ className: ' mycorp-list ph0' }}>
                                <Table
                                    className="-highlight mb-2 ph0"
                                    data={activities}
                                    TheadComponent={props => null}
                                    showPagination={false}
                                    columns={getColumns(projects, null, null, null, null, null, true)}
                                    pageSize={activities.length}
                                    PaginationComponent={TablePagination}
                                />
                            </RowColWrapper>
                        </Fragment>
                    )}
                </RowColWrapper>
                <MyCorpTimesheetBottomNav page="report" />
            </Container>
        );
    }
}

const ModalForm = props => {
    const { isOpen, loading, toggle, type, onSubmit } = props;
    const [remarks, setRemarks] = useState('');

    return (
        <Modal isOpen={isOpen}>
            <ModalHeader className="border-0" toggle={toggle}>
                <b>{capitalize(type)} Report</b>
            </ModalHeader>
            <ModalBody>
                {loading ? (
                    <LoadingSpinner />
                ) : (
                    <div>
                        <RowColWrapper>Are you sure you want to {type} this report?</RowColWrapper>
                        {type === 'reject' && (
                            <CyderInput
                                inputProps={{
                                    rows: 4,
                                    type: 'textarea',
                                    placeholder: 'Please provide your remarks',
                                    value: remarks,
                                    onChange: e => setRemarks(e.target.value),
                                }}
                            />
                        )}
                    </div>
                )}
            </ModalBody>
            <ModalFooter>
                <Button color="success" onClick={() => onSubmit(remarks)}>
                    {type === 'approve' ? 'Approve' : type === 'delete' ? 'Delete' : 'Reject'}
                </Button>
                <Button color="light" className="color-danger" onClick={toggle}>
                    Cancel
                </Button>
            </ModalFooter>
        </Modal>
    );
};

const mapStateToProps = state => {
    return {
        profile: state.cyderProfileReducer.profile,
    };
};

const actionList = {
    cyderUsersSearchAction,
    setJumbotronTitle,
    findMonthlyActivityByDateRange,
    findMonthlyActivityByReportId,
    getProjects,
    deleteTimesheetReport,
    expenseNewReceiptClaimGetHistory,
    rejectLeaveTask,
    approveTimesheetTask,
    deleteTask,
};

export default connect(mapStateToProps, actionList)(StoredLayout(MyCorpTimesheetReportDetailPage));
