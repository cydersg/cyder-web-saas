export default {
    arrayBufferToBlob: dataArray => {
        var byteArray = new Uint8Array(dataArray);
        var blob = new Blob([byteArray], { type: 'application/pdf' });
        // OLD BROWSER SUPPORT
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob);
            return;
        }
        // NEW BROWSER SUPPORT
        var objectUrl = URL.createObjectURL(blob);
        window.open(objectUrl);
    },

    arrayBufferToZip: dataArray => {
        var byteArray = new Uint8Array(dataArray);
        var blob = new Blob([byteArray], { type: 'application/zip' });
        // OLD BROWSER SUPPORT
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob);
            return;
        }
        // NEW BROWSER SUPPORT
        var objectUrl = URL.createObjectURL(blob);

        window.open(objectUrl);
    },
};
