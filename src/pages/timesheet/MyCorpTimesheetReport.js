import React, { Component, Fragment, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import StoredLayout from '../../layouts/StoredLayout';
import moment from 'moment';
import { Link } from 'react-router-dom';
import Table from 'react-table';
import { Row, Badge, Col, Container, Button, Modal, ModalHeader, ModalFooter, ModalBody, Alert } from 'reactstrap';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import YearMonthSearchSubmitReportPanel from 'cyderComponents/panel/YearMonthSearchSubmitReportPanel';
import { TablePagination } from 'cyderComponents/pagination/';
import { setJumbotronTitle } from 'actions/pagedata';
import * as Actions from 'actions/timesheet/monthlyTimesheetAction';
import ActionExecutor from 'js/ActionExecutor';
import cyderlib from 'js/cyderlib';
import { layoutLib } from 'js/constlib';
import { CyderSelect, CyderRadioInput } from 'cyderComponents/input/index';
import MyCorpTimesheetBottomNav from './MyCorpTimesheetBottomNav';
import CyderDatePicker from 'cyderComponents/forms/CyderDatePicker';
import { CyderInput } from 'cyderComponents/input/index';

const { getYearlyReportsByDateRange, deleteTimesheetReport, getApprovers, submitTimesheetReportByDateRange } = Actions;

export const inboxOutboxOptions = [
    {
        value: 'My Reports',
        label: 'My Reports',
    },
    {
        value: 'Approval',
        label: 'Approval',
    },
];

export const navigateTo = (history, value) => {
    if (value === 'myreports') {
        history.push('/mycorp/timesheet/report');
    } else {
        history.push('/mycorp/timesheet/approval');
    }
};

const defaultOption = { value: 0, label: 'None' };

class MyCorpTimesheetReport extends Component {
    state = {
        loading: false,
        reports: [],
        approvers: [],
        isModalOpen: false,
        isSubmitModalOpen: false,
        year: '',
        modal: {
            header: 'Yearly Timesheet',
            body: '',
            action: null,
            onlyOneButton: false,
        },
    };

    componentDidMount() {
        this.props.setJumbotronTitle('Timesheet Report');
        this.getYearlyReports();
        this.getApproversList();
    }

    onInboxOutboxRadioChange = (value, id) => {
        navigateTo(this.props.history, value);
    };

    getApproversList = () => {
        const callback = async () => {
            const { data } = await this.props.getApprovers();
            this.setState({ approvers: data });
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    setLoading = loading => {
        this.setState({ loading });
    };

    setYear = year => {
        this.setState({ year });
    };

    getYearlyReports = year => {
        const body = {
            reportYear: year && year.label ? year.value : moment().format('YYYY'),
        };
        this.setYear(body.reportYear);
        const callback = async () => {
            const { data } = await this.props.getYearlyReportsByDateRange(body);
            if (!data) {
                this.showModal('Error', 'Failed to retrive timesheet reports.', null, true);
                return;
            }
            this.setState({ reports: data });
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    deleteTimesheetReport = id => {
        const { year } = this.state;
        const callback = async () => {
            const { ok } = await this.props.deleteTimesheetReport(id);
            if (ok) {
                this.showModal('Confirmation', 'Successfully remove yearly timesheet report.', null, true);
                this.getYearlyReports(year);
                return;
            }
            this.showModal('Error', 'Failed to remove yearly timesheet report.', null, true);
        };
        this.showModal(
            'Confirmation',
            'Are you sure you want to delete this report?',
            () => ActionExecutor.execute(this.setLoading, callback),
            true,
        );
    };

    setIsModalOpen = (isModalOpen, id) => {
        let stateCopy = Object.assign({}, this.state);
        const key = id === 'submitModal' ? 'isSubmitModalOpen' : 'isModalOpen';
        stateCopy[key] = isModalOpen;
        this.setState(stateCopy);
    };

    showModal = (header, body, action, onlyOneButton) => {
        this.setIsModalOpen(true);
        const { modal } = this.state;
        const modalConfig = {
            ...this.state.modal,
            header: header || modal.header,
            body: body || modal.body,
            action: action || null,
            onlyOneButton: onlyOneButton || modal.onlyOneButton,
        };
        this.setState({ modal: modalConfig });
    };

    submitTimesheetReport = (startDate, endDate, approvers) => {
        const { year } = this.state;
        const body = {
            start: moment(startDate, 'DD/MM/YYYY').format('YYYY/MM/DD'),
            end: moment(endDate, 'DD/MM/YYYY').format('YYYY/MM/DD'),
            approvers,
        };
        const callback = async () => {
            const { ok } = await this.props.submitTimesheetReportByDateRange(body);
            if (ok) {
                this.showModal('Confirmation', 'Timesheet report submitted successfully.', null, true);
                this.getYearlyReports(year);
                return;
            }
            this.showModal('Error', 'Timesheet report was not submitted.', null, true);
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    render() {
        const { year, reports, approvers, isSubmitModalOpen, loading, modal, isModalOpen } = this.state;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        const sortedReports = reports.sort((a, b) => {
            if (a.status === b.status) {
                return 0;
            }
            if (a.status === 'Pending') {
                return -1;
            }
            if (a.status === 'Approved' || b.status === 'Rejected') {
                return 1;
            }
        });
        return (
            <Container className="wideContainer">
                <SubmitModal
                    approvers={approvers}
                    isOpen={isSubmitModalOpen}
                    toggle={isOpen => this.setIsModalOpen(!isSubmitModalOpen, 'submitModal')}
                    onSubmit={this.submitTimesheetReport}
                />
                <DialogModal
                    key="confirmModal"
                    modal={isModalOpen}
                    toggle={isOpen => this.setIsModalOpen(isOpen, 'confirmModal')}
                    customHeader={modal.header}
                    customModalBody={modal.body}
                    positiveButtonAction={modal.action}
                    onlyOneButton={modal.onlyOneButton}
                />
                <Row className="justify-content-center">
                    <Col
                        className="ph0"
                        style={{
                            textAlign: 'center',
                        }}
                    >
                        {/* <b>MyCorp Check In</b> */}
                        <hr className="mycorp-hr-small" />
                    </Col>
                </Row>
                <TooglePanel module="myreports" history={this.props.history} />
                <Row>
                    <YearMonthSearchSubmitReportPanel
                        year={year}
                        onSearch={this.getYearlyReports}
                        onSubmit={() => this.setIsModalOpen(true, 'submitModal')}
                    />
                </Row>
                <RowColWrapper colProps={{ className: ' mycorp-list' }}>
                    {loading ? (
                        <LoadingSpinner center />
                    ) : (
                        <Table
                            className="-highlight mb-2"
                            data={sortedReports}
                            TheadComponent={props => null}
                            showPagination={false}
                            columns={getColumns(this.props, this.deleteTimesheetReport)}
                            pageSize={sortedReports.length}
                            PaginationComponent={TablePagination}
                        />
                    )}
                </RowColWrapper>
                <MyCorpTimesheetBottomNav page="report" />
            </Container>
        );
    }
}

const SubmitModal = props => {
    const { approvers, isOpen, toggle, onSubmit } = props;
    const [startState, setStartState] = useState(null);
    const [endState, setEndState] = useState(null);
    const [selectedApprovers, setSelectedApprovers] = useState([]);
    const [isValid, setIsValid] = useState({
        valid: true,
        fieldName: '',
    });

    useEffect(() => {
        const start = '01/' + moment().format('MM/YYYY');
        const end = moment(
            '01/' +
                moment()
                    .add(1, 'M')
                    .format('MM/YYYY'),
            'DD/MM/YYYY',
        )
            .add(-1, 'days')
            .format('DD/MM/YYYY');

        setStartState(start);
        setEndState(end);
    }, []);

    const checkIsValid = () => {
        // Valid
        if (selectedApprovers.length !== 0) return true;

        // Invalid
        let fieldName = '';
        // if (!month.value) fieldName = 'Month';
        // if (!year.value) fieldName = 'Year';
        if (selectedApprovers.length === 0) fieldName = 'Approvers';
        setIsValid({
            valid: false,
            fieldName,
        });
        return false;
    };

    const onSubmitReport = () => {
        const valid = checkIsValid();
        if (!valid) return;
        toggle();
        const emails = selectedApprovers.map(x => x.email);
        onSubmit(startState, endState, emails);
    };

    return (
        <Modal isOpen={isOpen}>
            <ModalHeader className="border-0" toggle={toggle}>
                <b>Submit Report</b>
            </ModalHeader>
            <ModalBody>
                <Row>
                    <Col xs="12" sm="5">
                        <CyderInput label="From" formGroupClassName="m-0">
                            <CyderDatePicker
                                value={startState}
                                placeholder="DD/MM/YYYY"
                                dateFormat="DD/MM/YYYY"
                                onChange={event => {
                                    setStartState(event.format('DD/MM/YYYY'));
                                }}
                            />
                        </CyderInput>
                    </Col>
                    <Col xs="12" sm="5">
                        <CyderInput label="Until" formGroupClassName="m-0">
                            <CyderDatePicker
                                value={endState}
                                placeholder="DD/MM/YYYY"
                                dateFormat="DD/MM/YYYY"
                                onChange={event => {
                                    setEndState(event.format('DD/MM/YYYY'));
                                }}
                            />
                        </CyderInput>
                    </Col>
                </Row>
                <CyderSelect
                    mandatory
                    label="Approvers"
                    className="mb-4"
                    inputProps={{
                        isMulti: true,
                        name: 'approvers',
                        options: approvers,
                        value: selectedApprovers,
                        getOptionValue: option => option.id,
                        getOptionLabel: option => option.firstname + ' ' + option.lastname,
                        onChange: setSelectedApprovers,
                    }}
                />
                {!isValid.valid ? (
                    <Alert color="danger" className="text-bold">
                        {`Invalid input '${isValid.fieldName}' found`}
                    </Alert>
                ) : null}
            </ModalBody>
            <ModalFooter>
                <Button color="success" onClick={onSubmitReport}>
                    Submit
                </Button>{' '}
                <Button color="light" className="color-danger" onClick={toggle}>
                    Cancel
                </Button>
            </ModalFooter>
        </Modal>
    );
};

export const TooglePanel = ({ module, history }) => {
    return (
        <div className="mycorp-toggle-panel-button-container">
            <div
                className={
                    module === 'myreports'
                        ? 'mycorp-toggle-panel-button-item mycorp-toggle-panel-button-item-selected'
                        : 'mycorp-toggle-panel-button-item mycorp-toggle-panel-button-item'
                }
                onClick={e => {
                    navigateTo(history, 'myreports');
                }}
            >
                <i
                    className="align-middle material-icons"
                    style={{
                        marginLeft: 10,
                        fontSize: 20,
                        marginTop: -4,
                    }}
                >
                    inbox
                </i>
                <h5
                    className={
                        module === 'myreports'
                            ? 'mycorp-toggle-panel-button-label mycorp-toggle-panel-button-label-selected'
                            : 'mycorp-toggle-panel-button-label mycorp-toggle-panel-button-item'
                    }
                >
                    My Reports
                </h5>
            </div>
            <div
                className={
                    module === 'approval'
                        ? 'mycorp-toggle-panel-button-item mycorp-toggle-panel-button-item-selected'
                        : 'mycorp-toggle-panel-button-item mycorp-toggle-panel-button-item'
                }
                onClick={e => {
                    navigateTo(history, 'approval');
                }}
            >
                <i
                    className="align-middle material-icons"
                    style={{
                        marginLeft: 10,
                        fontSize: 20,
                        marginTop: -4,
                    }}
                >
                    launch
                </i>
                <h5
                    className={
                        module === 'approval'
                            ? 'mycorp-toggle-panel-button-label mycorp-toggle-panel-button-label-selected'
                            : 'mycorp-toggle-panel-button-label mycorp-toggle-panel-button-item'
                    }
                >
                    Approval
                </h5>
            </div>
        </div>
    );
};

const getColumns = (props, deleteReport) => {
    const { history } = props;
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return [
        {
            Header: null,
            accessor: 'id',
            Cell: props => {
                const { status } = props.original;
                return (
                    <Link
                        style={{
                            color: 'unset',
                        }}
                        to={`../timesheet/report/${props.value}`}
                    >
                        <div className="boxShadow">
                            <span>
                                <strong>{props.original.title}</strong>
                            </span>
                            <br />
                            <span
                                style={{
                                    fontSize: 12,
                                }}
                            >
                                ({moment(props.original.createddt, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY')}, {props.original.owner})
                            </span>
                            <br />
                            <Badge color={cyderlib.statusBadgeColor(props.original.status)}>{props.original.status}</Badge>
                        </div>
                    </Link>
                );
            },
        },
    ];
};

const reducer = state => {
    return {};
};

const actionList = {
    setJumbotronTitle,
    getYearlyReportsByDateRange,
    deleteTimesheetReport,
    getApprovers,
    submitTimesheetReportByDateRange,
};

export default connect(reducer, actionList)(StoredLayout(MyCorpTimesheetReport));
