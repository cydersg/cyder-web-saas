import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';
import history from 'history.js';

import { Container, Form, Input } from 'reactstrap';
import ReactTable from 'react-table';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import { TablePagination } from 'cyderComponents/pagination/';

import { cyderUsersSearchAction } from 'actions/users/cyderUsersSearchAction';

import ActionExecutor from 'js/ActionExecutor';

const columns = [
    {
        Header: 'Name',
        accessor: 'firstname',
        Cell: props => props.value + ' ' + props.original.lastname,
    },
    {
        Header: 'Department',
        accessor: 'department_code',
        Cell: props => (props.value ? props.value.toLowerCase() : '-'),
    },
    {
        Header: 'Email',
        accessor: 'email',
    },
    {
        Header: '',
        accessor: 'id',
        width: 130,
        Cell: props => <i className="material-icons">edit</i>,
    },
];

export function useGetAllActiveUsers(props, setLoading, keyword) {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        const callback = async () => {
            const data = await props.getUsers(keyword);
            setUsers(data);
        };
        ActionExecutor.execute(setLoading || (() => {}), callback);
    }, [keyword, props, setLoading]);

    return users;
}

function StaffLeaveBalanceUsersPage(props) {
    const [loading, setLoading] = useState(false);
    const [keyword, setKeyword] = useState('');
    const activeUsers = useGetAllActiveUsers(props, setLoading, keyword);

    const getTdProps = (state, rowInfo, column, instance) => {
        return {
            onClick: (e, handleOriginal) => {
                if (!rowInfo) return;
                history.push(`/settings/leave/leavebalance/${rowInfo.original.email}`);
                if (handleOriginal) handleOriginal();
            },
        };
    };

    return (
        <Container>
            <RowColWrapper colProps={{ xs: 12 }}>
                <Form inline>
                    <Input className="w-300" placeholder="Search" onChange={e => setKeyword(e.target.value)} value={keyword} />
                </Form>
            </RowColWrapper>
            <RowColWrapper colProps={{ className: 'pt-4' }}>
                {loading ? (
                    <LoadingSpinner center />
                ) : (
                    <ReactTable
                        className="-highlight mb-2"
                        data={activeUsers}
                        columns={columns}
                        pageSize={activeUsers.length}
                        showPagination={activeUsers.length > 5}
                        getTdProps={getTdProps}
                        PaginationComponent={TablePagination}
                    />
                )}
            </RowColWrapper>
        </Container>
    );
}

const mapStateToProps = state => {
    return {};
};
const mapDispatchToProps = dispatch => {
    return {
        getUsers: keyword => {
            const body = {
                keyword,
                status: 'Active',
            };
            return dispatch(cyderUsersSearchAction(body));
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(StaffLeaveBalanceUsersPage));
