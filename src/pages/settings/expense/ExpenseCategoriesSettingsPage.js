import React from 'react';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';

import ReactTable from 'react-table';
import StoredLayout from 'layouts/StoredLayout';
import LinkButton from 'cyderComponents/buttons/LinkButton';
import cyderlib from 'js/cyderlib.js';
import { Alert, Input, Label, Button, Container, Row, Col, Form, FormGroup } from 'reactstrap';

import {
    cyderExpenseCategoriesSettingsSearchAction,
    cyderExpenseCategoriesSettingsEditModalSetExpenseCategory,
    cyderExpenseCategoriesSettingsModalToggleAction,
    cyderExpenseCategoriesSettingsEditModalChangeValue,
    cyderExpenseCategoriesSettingsAddExpenseCategoryAction,
    cyderExpenseCategoriesSettingsUpdateExpenseCategoryAction,
    cyderExpenseCategoriesSettingsDeleteExpenseCategoryAction,
    cyderExpenseCategoriesSettingsAddApproverAction,
    cyderExpenseCategoriesSettingsRemoveApproverAction,
} from 'actions/settings/cyderExpenseCategoriesSettingsSearchAction';
import DialogModal from 'cyderComponents/modals/DialogModal';
import { setJumbotronTitle } from 'actions/pagedata';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

import reactTableFilter from 'js/reactTableFilter';

class ExpenseCategoriesSettingsPage extends React.Component {
    constructor(props) {
        super(props);
        this.onFilteredChange = this.onFilteredChange.bind(this);
        this.filterAllFunc = this.filterAllFunc.bind(this);
        this.state = {
            filterAll: '',
        };
        this.filterOptionsApprovers = this.filterOptionsApprovers.bind(this);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.toggleAction = this.toggleAction.bind(this);
        this.validateThenSearch = this.validateThenSearch.bind(this);
    }
    componentDidMount() {
        if (this.props.forceRefresh) {
            this.validateThenSearch();
        }
        this.validateThenSearch();
        // this.props.getAllExpenseCategories();
        this.props.setJumbotronTitle();
    }
    validateThenSearch(e) {
        if (e) {
            e.preventDefault();
        }
        // this.props.clearErrorMessage();

        const keyword = document.getElementById('keyword').value;
        const status = document.getElementById('status').value;

        const body = {
            keyword,
            status,
        };
        this.props.searchExpenseCategories(body);
    }
    setupEditModal(project) {
        this.props.setModalExpenseCategory(project);
        this.props.toggleModal('update');
    }
    setupDeleteModal(project) {
        this.props.setModalExpenseCategory(project);
        this.props.toggleModal('delete');
    }
    toggleAction() {
        this.handleToggleModal();
        if (this.props.modalAction === 'refresh') {
            this.validateThenSearch();
        }
    }
    calcCurrentPositiveButtonText() {
        if (this.props.modalAction === 'update') {
            return 'Update';
        } else if (this.props.modalAction === 'add') {
            return 'Add';
        } else if (this.props.modalAction === 'delete') {
            return 'Delete';
        } else if (this.props.modalAction === 'close') {
            return 'Ok';
        } else {
            return 'Ok';
        }
    }
    calcCurrentPositiveButtonAction() {
        if (this.props.modalAction === 'update') {
            return this.props.handleUpdate;
        } else if (this.props.modalAction === 'refresh') {
            return () => {
                this.handleToggleModal();
                this.validateThenSearch();
            };
        } else if (this.props.modalAction === 'add') {
            return this.props.handleAdd;
        } else if (this.props.modalAction === 'delete') {
            return this.props.handleDelete;
        } else if (this.props.modalAction === 'close') {
            return this.handleToggleModal;
        } else {
            return this.handleToggleModal;
        }
    }
    handleToggleModal(modalAction) {
        this.props.toggleModal(modalAction);
    }
    filterOptionsApprovers(options) {
        const results = options.filter(item => !this.props.approversReference[item.value]);
        return results;
    }
    onFilteredChange(filtered) {
        this.setState(reactTableFilter.onFilteredChange(filtered));
    }
    filterAllFunc(e) {
        this.setState(reactTableFilter.filterAllFunc(e));
    }
    render() {
        const customModalBody = (
            <div>
                <FormGroup>
                    <Label for="exampleText">Expense Category Code</Label>
                    <Input
                        onChange={event => this.props.changeModalInputValue(event)}
                        disabled={this.props.modalAction === 'update'}
                        type="text"
                        maxLength={10}
                        id="code"
                        value={this.props.modalExpenseCategory.code}
                        onKeyPress={e => {
                            cyderlib.handleOnKeyPress(
                                e,
                                [this.props.modalExpenseCategory.name, this.props.modalExpenseCategory.code],
                                this.calcCurrentPositiveButtonAction(),
                            );
                        }}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleText">Expense Category Name</Label>
                    <Input
                        onChange={event => this.props.changeModalInputValue(event)}
                        type="text"
                        maxLength={100}
                        id="name"
                        value={this.props.modalExpenseCategory.name}
                        onKeyPress={e => {
                            cyderlib.handleOnKeyPress(
                                e,
                                [this.props.modalExpenseCategory.name, this.props.modalExpenseCategory.code],
                                this.calcCurrentPositiveButtonAction(),
                            );
                        }}
                    />
                </FormGroup>
            </div>
        );
        const columns = [
            reactTableFilter.generateAllColumnFilter(),
            {
                Header: 'Category Code',
                accessor: 'code',
            },
            {
                Header: 'Category Name',
                accessor: 'name',
            },
            {
                Header: '',
                accessor: 'id',
                Cell: props => {
                    const thisExpenseCategory = this.props.expensecategories[props.index];
                    return (
                        <div className="float-right">
                            <Button
                                color="link"
                                size="sm"
                                onClick={() => {
                                    this.setupEditModal(thisExpenseCategory);
                                }}
                            >
                                <i className="align-middle material-icons">edit</i>
                            </Button>
                            <Button
                                color="link"
                                className="ml-1"
                                size="sm"
                                onClick={() => {
                                    this.setupDeleteModal(thisExpenseCategory);
                                }}
                            >
                                <i className="text-danger align-middle material-icons">delete</i>
                            </Button>
                        </div>
                    );
                },
            },
        ];
        return (
            <div key={0}>
                <Container>
                    <Row className="d-none">
                        <Col xs={12}>
                            <Form onSubmit={this.validateThenSearch}>
                                <FormGroup>
                                    <Input placeholder="Keyword" id="keyword" />
                                </FormGroup>
                                <FormGroup>
                                    <Input type="select" id="status">
                                        <option>Active</option>
                                        <option>Inactive</option>
                                    </Input>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                    <Row className="mb-2">
                        <Col className="d-flex">
                            <Input className="w-300" placeholder="Search" onChange={this.filterAllFunc} value={this.state.filterAll} />
                            <LinkButton preset="addButton" onClick={() => this.handleToggleModal('add')} text="Expense Category" />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            {this.props.loading ? (
                                <LoadingSpinner center />
                            ) : this.props.expensecategories.length > 0 ? (
                                <ReactTable
                                    filtered={this.state.filtered}
                                    onFilteredChange={this.onFilteredChange}
                                    filterable={false}
                                    defaultFilterMethod={reactTableFilter.defaultFilterMethod}
                                    className="-highlight mb-2"
                                    data={this.props.expensecategories}
                                    columns={columns}
                                    minRows={0}
                                    showPagination={false}
                                />
                            ) : this.props.errorMessage ? (
                                <Alert color="danger">{this.props.errorMessage}</Alert>
                            ) : null}
                        </Col>
                    </Row>
                </Container>
                <DialogModal
                    key="modal"
                    modal={this.props.modalOpen}
                    toggle={this.toggleAction}
                    customHeader={this.props.modalHeader}
                    customModalBody={
                        this.props.saving ? <LoadingSpinner /> : this.props.modalMessage ? this.props.modalMessage : customModalBody
                    }
                    positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                    positiveButtonText={this.calcCurrentPositiveButtonText()}
                    onlyOneButton={this.props.saved}
                    buttonDisabler={this.props.saving}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ...state.cyderExpenseCategoriesSettingsSearchReducer,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        searchExpenseCategories: body => {
            dispatch(cyderExpenseCategoriesSettingsSearchAction(body));
        },
        setModalExpenseCategory: project => {
            dispatch(
                cyderExpenseCategoriesSettingsEditModalSetExpenseCategory(
                    project.id,
                    project.code,
                    project.name,
                    project.approvalvotescount,
                    project.approvers,
                ),
            );
        },
        toggleModal: modalAction => {
            if (modalAction === 'add') {
                dispatch(cyderExpenseCategoriesSettingsEditModalSetExpenseCategory('', '', '', 0, []));
            }
            dispatch(cyderExpenseCategoriesSettingsModalToggleAction(modalAction ? modalAction : null));
        },
        changeModalInputValue: event => {
            const value = event.target.value;
            const key = event.target.id;
            dispatch(cyderExpenseCategoriesSettingsEditModalChangeValue(key, value));
        },
        handleAdd: () => {
            dispatch(cyderExpenseCategoriesSettingsAddExpenseCategoryAction());
        },
        handleUpdate: () => {
            dispatch(cyderExpenseCategoriesSettingsUpdateExpenseCategoryAction());
        },
        handleDelete: () => dispatch(cyderExpenseCategoriesSettingsDeleteExpenseCategoryAction()),
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Settings')),
        addApprover: val => {
            if (val === null) return;
            const username = val.value;
            dispatch(cyderExpenseCategoriesSettingsAddApproverAction(username));
        },
        removeApprover: username => {
            dispatch(cyderExpenseCategoriesSettingsRemoveApproverAction(username));
        },
    };
};
// withTranslation() is if we want to use HOC to perform t()
export default withTranslation()(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(StoredLayout(ExpenseCategoriesSettingsPage)),
);
