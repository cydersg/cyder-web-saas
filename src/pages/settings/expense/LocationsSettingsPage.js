import React from 'react';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';

import Switch from 'react-switch';
import ReactTable from 'react-table';
import StoredLayout from 'layouts/StoredLayout';
import LinkButton from 'cyderComponents/buttons/LinkButton';
import { Alert, Input, Label, Button, Container, Row, Col, Form, FormGroup, InputGroup } from 'reactstrap';
import GoogleMapReact from 'google-map-react';
import SearchBox from 'cyderComponents/map/SearchBox';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';

import {
    cyderLocationsSearchAction,
    cyderLocationsSettingsEditModalSetLocation,
    cyderLocationsSettingsModalToggleAction,
    cyderLocationsSettingsEditModalChangeValue,
    cyderLocationsSettingsAddLocationAction,
    cyderLocationsSettingsUpdateLocationAction,
    cyderLocationsSettingsDeleteLocationAction,
    cyderLocationsSettingsSetMapLocAction,
    cyderLocationsSettingsChangeFieldValueAction,
    cyderLocationsSettingsPinPointLocAction,
    cyderLocationsSettingsOpenQrCodeAction,
} from 'actions/settings/cyderLocationsSettingsSearchAction';
import { setJumbotronTitle } from 'actions/pagedata';
import { layoutLib } from 'js/constlib';
import cyderlib from 'js/cyderlib';
import reactTableFilter from 'js/reactTableFilter';

import QRCode from 'qrcode.react';

class LocationsSettingsPage extends React.Component {
    constructor(props) {
        super(props);
        this.onFilteredChange = this.onFilteredChange.bind(this);
        this.filterAllFunc = this.filterAllFunc.bind(this);
        this.state = {
            filterAll: '',
            approvers: {},
            gloc: false,
            ppLoc: false,
        };
        this.setupEditModal = this.setupEditModal.bind(this);
        this.setupAddModal = this.setupAddModal.bind(this);
        this.refreshLoc = this.refreshLoc.bind(this);
        this.setMarkerFromLatLng = this.setMarkerFromLatLng.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.toggleAction = this.toggleAction.bind(this);
        this.validateThenSearch = this.validateThenSearch.bind(this);
        this.handleSwitchChange = this.handleSwitchChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.ppLoc = this.ppLoc.bind(this);
    }
    handleSwitchChange(checked) {
        if (this.props.locMarker !== null) {
            // remove previous marker if there is one
            this.props.locMarker.setMap();
        }
        this.setState({ gloc: checked });
    }
    componentDidMount() {
        if (this.props.forceRefresh) {
            this.validateThenSearch();
        }
        this.validateThenSearch();
        this.props.setJumbotronTitle();
        // this.props.getAllLocations();
    }
    validateThenSearch(e) {
        if (e) {
            e.preventDefault();
        }
        // this.props.clearErrorMessage();

        const keyword = document.getElementById('keyword').value;
        const status = document.getElementById('status').value;

        const body = {
            keyword,
            status,
        };
        this.props.searchLocations(body);
    }
    setupAddModal() {
        this.setState({ gloc: false });
        this.props.toggleModal('add');
    }
    setupEditModal(location) {
        this.setState({ gloc: false });
        this.props.setModalLocation(location).then(() => {
            this.props.toggleModal('update');
        });
    }
    setupDeleteModal(location) {
        this.props.setModalLocation(location);
        this.props.toggleModal('delete');
    }
    toggleAction() {
        this.handleToggleModal();
        if (this.props.modalAction === 'refresh') {
            this.validateThenSearch();
        }
    }
    calcCurrentPositiveButtonText() {
        if (this.props.modalAction === 'update') {
            return 'Update';
        } else if (this.props.modalAction === 'add') {
            return 'Add';
        } else if (this.props.modalAction === 'delete') {
            return 'Delete';
        } else if (this.props.modalAction === 'close') {
            return 'Ok';
        } else {
            return 'Ok';
        }
    }
    calcCurrentPositiveButtonAction() {
        if (this.props.modalAction === 'update') {
            return this.props.handleUpdate;
        } else if (this.props.modalAction === 'refresh') {
            return () => {
                this.handleToggleModal();
                this.validateThenSearch();
            };
        } else if (this.props.modalAction === 'add') {
            return this.props.handleAdd;
        } else if (this.props.modalAction === 'delete') {
            return this.props.handleDelete;
        } else if (this.props.modalAction === 'close') {
            return this.handleToggleModal;
        } else {
            return this.handleToggleModal;
        }
    }
    handleToggleModal(modalAction) {
        this.props.toggleModal(modalAction);
    }
    setLocMarker(loc) {
        if (this.props.locMarker !== null) {
            // remove previous marker if there is one
            this.props.locMarker.setMap();
        }
        var position = {
            lat: loc.geometry.location.lat(),
            lng: loc.geometry.location.lng(),
        };
        var marker = new this.state.maps.Marker({
            position,
            map: this.state.map,
            title: loc.name,
        });
        marker.setMap(this.state.map);
        this.props.setLatLng(loc.geometry.location.lat(), loc.geometry.location.lng());
        this.props.setLocMarker(loc, marker).then(() => {
            console.log('set location marker');
        });
    }
    setMarkerFromLatLng(e) {
        if (this.props.locMarker !== null) {
            // remove previous marker if there is one
            this.props.locMarker.setMap();
        }
        var position = {
            lat: e.lat,
            lng: e.lng,
        };
        var marker = new this.state.maps.Marker({
            position,
            map: this.state.map,
            title: 'Custom Coordinates',
        });
        marker.setMap(this.state.map);
        this.props.setLocMarker(null, marker).then(() => {
            console.log('set location marker for lat lng only');
        });
    }
    handleClick(e) {
        this.props.setLatLng(e.lat, e.lng);
        this.setMarkerFromLatLng(e);
    }
    initGoogleMaps(map, maps) {
        this.setState({
            map,
            maps,
        });
        this.refreshLoc();
    }
    ppLoc(event) {
        event.preventDefault();
        this.setState({ ppLoc: !this.state.ppLoc });
    }
    refreshLoc(event) {
        if (event) event.preventDefault();
        if (this.props.locMarker !== null) {
            // remove previous marker if there is one
            this.props.locMarker.setMap();
        }
        var position = {
            lat: parseFloat(this.props.modalLocation.lat),
            lng: parseFloat(this.props.modalLocation.lng),
        };
        var marker = new this.state.maps.Marker({
            position,
            map: this.state.map,
            title: 'Custom Coordinates',
        });
        marker.setMap(this.state.map);
        this.props.setLocMarker(null, marker).then(() => {
            console.log('refreshed location based on lat/lng fields');
        });
    }
    handleChange(event) {
        // remove marker from map, it is no longer accurate
        if (this.props.locMarker !== null) {
            // remove previous marker if there is one
            this.props.locMarker.setMap();
        }
        this.props.handleChange(event);
    }
    onFilteredChange(filtered) {
        this.setState(reactTableFilter.onFilteredChange(filtered));
    }
    filterAllFunc(e) {
        this.setState(reactTableFilter.filterAllFunc(e));
    }
    render() {
        const style = {
            height: '400px',
            width: '100%',
        };
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;
        const customModalBody = (
            <div>
                <Col style={{ width: '100%' }}>
                    <div style={style}>
                        <GoogleMapReact
                            resetBoundsOnResize={true}
                            onGoogleApiLoaded={({ map, maps }) => this.initGoogleMaps(map, maps)}
                            yesIWantToUseGoogleMapApiInternals
                            onClick={this.state.ppLoc ? this.handleClick : null}
                            bootstrapURLKeys={{
                                key: 'AIzaSyBr4vviN07QGsh6OtH0mvRd9RRWL9YtSWo',
                            }}
                            center={{ lat: 1.3521, lng: 103.8198 }}
                            defaultZoom={11}
                        />
                    </div>
                </Col>
                <Col className="mb-0">
                    <Form>
                        <FormGroup className="mt-2">
                            <FormGroup row>
                                <Label sm="auto">Use Google Places Search</Label>
                                <Col sm={{ size: 'auto' }}>
                                    <Label>
                                        <Switch
                                            onChange={this.handleSwitchChange}
                                            checked={this.state.gloc}
                                            uncheckedIcon={null}
                                            id="gloc"
                                        />
                                    </Label>
                                </Col>
                            </FormGroup>
                            {this.state.gloc ? (
                                <InputGroup>
                                    <SearchBox
                                        setLoc={loc => this.setLocMarker(loc)}
                                        inputProps={{
                                            className: 'form-control',
                                            placeholder: 'Search Location',
                                        }}
                                    />
                                </InputGroup>
                            ) : (
                                <div>
                                    <Button color={this.state.ppLoc ? 'primary' : 'secondary'} onClick={this.ppLoc} size="sm">
                                        {this.state.ppLoc ? 'Enter Manual Coordinates' : 'Pinpoint Location On Map'}
                                        <i className="align-middle material-icons">pin_drop</i>
                                    </Button>
                                    <Button className="float-right" onClick={this.refreshLoc} size="sm">
                                        Refresh Location
                                    </Button>
                                    <FormGroup>
                                        <Label for="exampleText">Lat</Label>
                                        <Input
                                            disabled={this.state.ppLoc}
                                            onChange={event => this.handleChange(event)}
                                            id="lat"
                                            value={this.props.modalLocation.lat}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="exampleText">Long</Label>
                                        <Input
                                            disabled={this.state.ppLoc}
                                            onChange={event => this.handleChange(event)}
                                            id="lng"
                                            value={this.props.modalLocation.lng}
                                        />
                                    </FormGroup>
                                </div>
                            )}
                        </FormGroup>
                    </Form>
                    <FormGroup>
                        <Label for="exampleText">Code</Label>
                        <Input
                            onChange={event => this.props.changeModalTextAreaValue(event)}
                            disabled={this.props.modalAction === 'update'}
                            type="text"
                            maxLength={10}
                            id="code"
                            value={this.props.modalLocation.code}
                        />
                        <Label for="exampleText">Name</Label>
                        <Input
                            onChange={event => this.props.changeModalTextAreaValue(event)}
                            type="text"
                            maxLength={100}
                            id="name"
                            value={this.props.modalLocation.name}
                        />
                        <Label for="exampleText">Address</Label>
                        <Input
                            onChange={event => this.props.changeModalTextAreaValue(event)}
                            type="text"
                            maxLength={500}
                            id="address"
                            value={this.props.modalLocation.address}
                        />
                    </FormGroup>
                </Col>
            </div>
        );
        const columns = [
            reactTableFilter.generateAllColumnFilter(),
            {
                Header: 'Location Code',
                show: !isMobile,
                accessor: 'code',
                filterMethod: cyderlib.filterIgnoreCase,
            },
            {
                Header: 'Location Name',
                show: !isMobile,
                accessor: 'name',
                filterMethod: cyderlib.filterIgnoreCase,
            },
            {
                Header: 'Location Address',
                accessor: 'address',
                show: !isMobile,
                filterMethod: cyderlib.filterIgnoreCase,
            },
            {
                Header: '',
                accessor: 'id',
                show: !isMobile,
                filterable: false,
                Cell: props => {
                    const thisLocation = this.props.locations[props.index];
                    return (
                        <div className="float-right">
                            <Button
                                color="link"
                                size="sm"
                                onClick={() => {
                                    this.props.openQr(true, thisLocation);
                                }}
                            >
                                <i className="align-middle material-icons">crop_free</i>
                            </Button>
                            <Button
                                color="link"
                                size="sm"
                                onClick={() => {
                                    this.setupEditModal(thisLocation);
                                }}
                            >
                                <i className="align-middle material-icons">edit</i>
                            </Button>
                            <Button
                                color="link"
                                className="ml-1"
                                size="sm"
                                onClick={() => {
                                    this.setupDeleteModal(thisLocation);
                                }}
                            >
                                <i className="text-danger align-middle material-icons">delete</i>
                            </Button>
                        </div>
                    );
                },
            },
            {
                Header: '',
                accessor: 'id',
                show: isMobile,
                filterable: false,
                Cell: props => {
                    const thisLocation = this.props.locations[props.index];
                    return (
                        <div className="float-right">
                            <Button
                                color="link"
                                size="sm"
                                onClick={() => {
                                    this.props.openQr(true, thisLocation);
                                }}
                            >
                                <i className="align-middle material-icons">crop_free</i>
                            </Button>
                            <Button
                                color="link"
                                size="sm"
                                onClick={() => {
                                    this.setupEditModal(thisLocation);
                                }}
                            >
                                <i className="align-middle material-icons">edit</i>
                            </Button>
                            <Button
                                color="link"
                                className="ml-1"
                                size="sm"
                                onClick={() => {
                                    this.setupDeleteModal(thisLocation);
                                }}
                            >
                                <i className="text-danger align-middle material-icons">delete</i>
                            </Button>
                        </div>
                    );
                },
            },
        ];
        return (
            <div key={0}>
                <Container>
                    <Row className="d-none">
                        <Col xs={12}>
                            <Form onSubmit={this.validateThenSearch}>
                                <FormGroup>
                                    <Input placeholder="Keyword" id="keyword" />
                                </FormGroup>
                                <FormGroup>
                                    <Input type="select" id="status">
                                        <option>Active</option>
                                        <option>Inactive</option>
                                    </Input>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                    <Row className="mb-2">
                        <Col className="d-flex">
                            <Input className="w-300" placeholder="Search" onChange={this.filterAllFunc} value={this.state.filterAll} />
                            <LinkButton preset="addButton" onClick={() => this.handleToggleModal('add')} text="Location" />
                        </Col>
                    </Row>

                    <Row>
                        <Col xs={12}>
                            {this.props.loading ? (
                                <LoadingSpinner center />
                            ) : this.props.locations.length > 0 ? (
                                <ReactTable
                                    filtered={this.state.filtered}
                                    onFilteredChange={this.onFilteredChange}
                                    filterable={false}
                                    defaultFilterMethod={reactTableFilter.defaultFilterMethod}
                                    className="-highlight mb-2"
                                    data={this.props.locations}
                                    columns={columns}
                                    minRows={0}
                                    showPagination={false}
                                />
                            ) : this.props.errorMessage ? (
                                <Alert color="danger">{this.props.errorMessage}</Alert>
                            ) : null}
                        </Col>
                    </Row>
                </Container>
                <DialogModal
                    size="lg"
                    key="modal"
                    modal={this.props.modalOpen}
                    toggle={this.toggleAction}
                    customHeader={this.props.modalHeader}
                    customModalBody={
                        this.props.saving ? <LoadingSpinner /> : this.props.modalMessage ? this.props.modalMessage : customModalBody
                    }
                    positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                    positiveButtonText={this.calcCurrentPositiveButtonText()}
                    onlyOneButton={this.props.saved}
                    buttonDisabler={this.props.saving}
                />
                <DialogModal
                    size="sm"
                    key="modal"
                    modal={this.props.qrOpen}
                    toggle={e => {
                        this.props.openQr(false, null);
                    }}
                    customHeader={<b>{this.props.qrLocation && this.props.qrLocation.name}</b>}
                    customModalBody={
                        <div>
                            {this.props.qrLocation && (
                                <p
                                    style={{
                                        textAlign: 'center',
                                    }}
                                >
                                    <QRCode value={this.props.qrLocation.id} />
                                </p>
                            )}
                            <p
                                style={{
                                    textAlign: 'center',
                                }}
                            >
                                {this.props.qrLocation && this.props.qrLocation.address}
                            </p>
                        </div>
                    }
                    // positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                    // positiveButtonText={this.calcCurrentPositiveButtonText()}
                    onlyOneButton={true}
                    // buttonDisabler={this.props.saving}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ...state.cyderLocationsSettingsSearchReducer,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        handleChange: event => {
            const key = event.target.id;
            const value = event.target.value;
            // console.log(approve.value(value, rules));
            dispatch(cyderLocationsSettingsChangeFieldValueAction(key, value));
        },
        openQr: (open, location) => {
            dispatch(cyderLocationsSettingsOpenQrCodeAction(open, location));
        },
        setLocMarker: (loc, marker) => dispatch(cyderLocationsSettingsSetMapLocAction(loc, marker)),
        searchLocations: body => dispatch(cyderLocationsSearchAction(body)),
        setModalLocation: location => {
            return dispatch(
                cyderLocationsSettingsEditModalSetLocation(
                    location.id,
                    location.code,
                    location.name,
                    location.address,
                    location.lat,
                    location.lng,
                ),
            );
        },
        toggleModal: modalAction => {
            if (modalAction === 'add') {
                // dispatch(cyderLocationsSettingsEditModalSetLocation("", "", "", [{id: 'aaa'},{id: 'bbb'},{id: 'ccc'},{id: 'ddd'}]))
                dispatch(cyderLocationsSettingsEditModalSetLocation('', '', '', []));
            }
            dispatch(cyderLocationsSettingsModalToggleAction(modalAction ? modalAction : null));
        },
        changeModalTextAreaValue: event => {
            const value = event.target.value;
            const key = event.target.id;
            dispatch(cyderLocationsSettingsEditModalChangeValue(key, value));
        },
        handleAdd: () => {
            dispatch(cyderLocationsSettingsAddLocationAction());
        },
        handleUpdate: () => {
            dispatch(cyderLocationsSettingsUpdateLocationAction());
        },
        handleDelete: () => dispatch(cyderLocationsSettingsDeleteLocationAction()),
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Settings')),
        setLatLng: (lat, lng) => {
            dispatch(cyderLocationsSettingsPinPointLocAction(lat, lng));
        },
    };
};
// translate() is if we want to use HOC to perform t()
export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(StoredLayout(LocationsSettingsPage)));
