import React, { Component, useState } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import StoredLayout from 'layouts/StoredLayout';

import Table from 'react-table';
import { Row, Alert, Button, Container, Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import { CyderSelect } from 'cyderComponents/input/index';
import YearMonthSearchPanel from 'cyderComponents/panel/YearMonthSearchPanel';
import { TablePagination } from 'cyderComponents/pagination/';

import * as Actions from 'actions/timesheet/monthlyTimesheetAction';
import { setJumbotronTitle } from 'actions/pagedata';
import { expenseDeletableStatuses, urlRecurring, urlReceipt, layoutLib } from 'js/constlib';
import { getMonthOptions, getYearOptions } from 'js/generalUtils';
import ActionExecutor from 'js/ActionExecutor';
import dateutils from 'js/dateutils';

const { getMonthlyActivities, getApprovers, submitTimesheetReport, getProjects, deleteTimesheetActivity } = Actions;

const defaultOption = { value: 0, label: 'None' };

class MonthlyTimesheetPage extends Component {
    state = {
        loading: false,
        activities: [],
        approvers: [],
        isModalOpen: false,
        isSubmitModalOpen: false,
        modal: {
            header: 'Monthly Timesheet',
            body: '',
            action: null,
            onlyOneButton: false,
        },
    };

    componentDidMount() {
        this.props.setJumbotronTitle('Monthly Timesheet');
        this.getMonthlyActivities();
        this.getProjects();
        this.getApproversList();
    }

    setLoading = loading => {
        this.setState({ loading });
    };

    setMonthAndYear = (month, year) => {
        this.setState({ month, year });
    };

    getMonthlyActivities = (month, year) => {
        const body = {
            month: month && month.label ? month.value : moment().format('M'),
            year: year && year.label ? year.value : moment().format('YYYY'),
        };

        this.setMonthAndYear(body.month, body.year);
        const callback = async () => {
            const { data } = await this.props.getMonthlyActivities(body);
            if (!data) {
                this.showModal('Error', 'Failed to retrive timesheet activities', null, true);
                return;
            }
            this.setState({ activities: data });
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    getApproversList = () => {
        const callback = async () => {
            const { data } = await this.props.getApprovers();
            this.setState({ approvers: data });
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    getProjects = () => {
        const callback = async () => {
            const { data } = await this.props.getProjects();
            this.setState({ projects: data });
        };
        ActionExecutor.execute(() => {}, callback);
    };

    deleteActivity = id => {
        const callback = async () => {
            const { ok } = await this.props.deleteTimesheetActivity(id);
            if (ok) {
                this.showModal('Confirmation', 'Successfully deleted monthly timesheet report.', null, true);
                this.getMonthlyActivities(defaultOption, defaultOption);
                return;
            }
            this.showModal('Error', 'Failed to delete monthly timesheet report.', null, true);
        };
        this.showModal('Confirmation', 'Are you sure you want to delete this activity?', () => ActionExecutor.execute(() => {}, callback));
    };

    submitTimesheetReport = (month, year, approvers) => {
        const body = {
            reportMonth: month,
            reportYear: year,
            approvers,
        };
        const callback = async () => {
            const { ok } = await this.props.submitTimesheetReport(body);
            if (ok) {
                this.showModal('Confirmation', 'Successfully submitted monthly timesheet report.', null, true);
                return;
            }
            this.showModal('Error', 'Failed to submit monthly timesheet report.', null, true);
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    setIsModalOpen = (isModalOpen, id) => {
        let stateCopy = Object.assign({}, this.state);
        const key = id === 'submitModal' ? 'isSubmitModalOpen' : 'isModalOpen';
        stateCopy[key] = isModalOpen;
        this.setState(stateCopy);
    };

    showModal = (header, body, action, onlyOneButton) => {
        this.setIsModalOpen(true);
        const { modal } = this.state;
        const modalConfig = {
            ...this.state.modal,
            header: header || modal.header,
            body: body || modal.body,
            action: action || null,
            onlyOneButton: onlyOneButton || false,
        };
        this.setState({ modal: modalConfig });
    };

    render() {
        const { month, year, approvers, activities, projects, loading, modal, isModalOpen, isSubmitModalOpen } = this.state;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        return (
            <Container className="wideContainer">
                <SubmitModal
                    approvers={approvers}
                    isOpen={isSubmitModalOpen}
                    toggle={isOpen => this.setIsModalOpen(!isSubmitModalOpen, 'submitModal')}
                    onSubmit={this.submitTimesheetReport}
                />
                <DialogModal
                    key="confirmModal"
                    modal={isModalOpen}
                    toggle={isOpen => this.setIsModalOpen(isOpen, 'confirmModal')}
                    customHeader={modal.header}
                    customModalBody={modal.body}
                    positiveButtonAction={modal.action}
                    onlyOneButton={modal.onlyOneButton}
                />
                <Row>
                    <YearMonthSearchPanel hasMonth hasYear month={month} year={year} onSearch={this.getMonthlyActivities} />
                </Row>
                <RowColWrapper colProps={{ className: 'pt-4 ph0' }}>
                    {loading ? (
                        <LoadingSpinner center />
                    ) : (
                        <div>
                            {activities && activities.length > 0 && (
                                <Button
                                    className={isMobile ? 'w-100p' : null}
                                    color="primary"
                                    onClick={() => this.setIsModalOpen(true, 'submitModal')}
                                >
                                    Submit Timesheet Report
                                </Button>
                            )}
                            <Table
                                className="-highlight mb-2"
                                showPagination={!isMobile}
                                data={activities}
                                columns={getColumns(projects, this.deleteActivity)}
                                pageSize={activities.length > 20 ? 20 : activities.length}
                                PaginationComponent={TablePagination}
                            />
                        </div>
                    )}
                </RowColWrapper>
            </Container>
        );
    }
}

const SubmitModal = props => {
    const { approvers, isOpen, toggle, onSubmit } = props;
    const [month, setMonth] = useState(defaultOption);
    const [year, setYear] = useState(defaultOption);
    const [selectedApprovers, setSelectedApprovers] = useState([]);
    const [isValid, setIsValid] = useState({
        valid: true,
        fieldName: '',
    });

    const checkIsValid = () => {
        // Valid
        if (month.value && year.value && selectedApprovers.length !== 0) return true;

        // Invalid
        let fieldName = '';
        if (!month.value) fieldName = 'Month';
        if (!year.value) fieldName = 'Year';
        if (selectedApprovers.length === 0) fieldName = 'Approvers';
        setIsValid({
            valid: false,
            fieldName,
        });
        return false;
    };

    const onSubmitReport = () => {
        const valid = checkIsValid();
        if (!valid) return;
        toggle();
        const emails = selectedApprovers.map(x => x.email);
        onSubmit(month.value, year.value, emails);
    };

    const monthOptionsTmp = React.useMemo(getMonthOptions, []);
    const monthOptions = monthOptionsTmp.map(item => {
        return {
            value: item.value,
            label: item.value ? dateutils.monthsArray[item.value - 1] : item.label,
        };
    });

    return (
        <Modal isOpen={isOpen}>
            <ModalHeader className="border-0" toggle={toggle}>
                <b>Submit Monthly Timesheet Report</b>
            </ModalHeader>
            <ModalBody>
                <CyderSelect
                    mandatory
                    label="Month"
                    className="mb-4"
                    formGroupClassName="m-0"
                    inputProps={{
                        options: monthOptions,
                        placeholder: 'Please select',
                        onChange: setMonth,
                    }}
                />
                <CyderSelect
                    mandatory
                    label="Year"
                    className="mb-4"
                    formGroupClassName="m-0"
                    inputProps={{
                        options: getYearOptions(),
                        placeholder: 'Please select',
                        onChange: setYear,
                    }}
                />
                <CyderSelect
                    mandatory
                    label="Approvers"
                    className="mb-4"
                    inputProps={{
                        isMulti: true,
                        name: 'approvers',
                        options: approvers,
                        value: selectedApprovers,
                        getOptionValue: option => option.id,
                        getOptionLabel: option => option.firstname + ' ' + option.lastname,
                        onChange: setSelectedApprovers,
                    }}
                />
                {!isValid.valid ? (
                    <Alert color="danger" className="text-bold">
                        {`Invalid input '${isValid.fieldName}' found`}
                    </Alert>
                ) : null}
            </ModalBody>
            <ModalFooter>
                <Button color="success" onClick={onSubmitReport}>
                    Submit
                </Button>{' '}
                <Button color="light" className="color-danger" onClick={toggle}>
                    Cancel
                </Button>
            </ModalFooter>
        </Modal>
    );
};

export const getColumns = (projects = [], deleteActivity) => {
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;
    const formatData = date => moment(date, 'YYYY-MM-DD hh:mm:ss').format('DD/MM/YYYY hh:mm a');

    return [
        {
            Header: 'Start Date',
            accessor: 'starttime',
            show: !isMobile,
            Cell: props => {
                return (
                    <>
                        <span className="font-weight-bold">{moment(props.value).format('DD-MMM-YYYY')}</span> <br />
                        <span className="font-weight-bold text-muted">{moment(props.value).format('h:mm a')}</span>
                    </>
                );
            },
        },
        {
            Header: 'End Time',
            accessor: 'endtime',
            show: !isMobile,
            Cell: props => {
                return (
                    <>
                        <span className="font-weight-bold">{moment(props.value).format('DD-MMM-YYYY')}</span> <br />
                        <span className="font-weight-bold text-muted">{moment(props.value).format('h:mm a')}</span>
                    </>
                );
            },
        },
        {
            Header: 'GPS Location',
            accessor: 'id',
            show: !isMobile,
            Cell: props => {
                return (
                    <>
                        {props.original.lat && props.original.lng ? (
                            <span>
                                Lat {props.original.lat}
                                <br />
                                Lng {props.original.lng}
                            </span>
                        ) : (
                            '-'
                        )}
                    </>
                );
            },
        },
        {
            Header: 'Project',
            show: !isMobile,
            accessor: 'project',
            Cell: props => {
                const project = projects.find(x => x.code === props.value);
                return project ? project.name : props.value;
            },
        },
        {
            Header: 'Purpose',
            show: !isMobile,
            accessor: 'purpose',
        },
        {
            Header: '',
            accessor: 'id',
            show: !isMobile,
            width: 50,
            Cell: props => {
                if (!deleteActivity) return null;
                return (
                    <Button
                        color="link"
                        className="ml-1"
                        size="sm"
                        onClick={e => {
                            deleteActivity(props.value);
                        }}
                    >
                        <i className="text-danger align-middle material-icons">delete</i>
                    </Button>
                );
            },
        },
        {
            Header: '',
            headerClassName: 'rt-no-sort-ind',
            accessor: 'id',
            sortable: false,
            show: isMobile,
            filterable: false,
            Cell: props => {
                const project = projects.find(x => x.code === props.original.project);

                return (
                    <div className="boxShadow">
                        <span>
                            <strong>{formatData(props.original.starttime)}</strong>
                        </span>
                        <br />
                        <span>
                            <strong>{formatData(props.original.endtime)}</strong>
                        </span>
                        <br />
                        {props.original.lat && props.original.lng && (
                            <>
                                <span>
                                    Lat {props.original.lat}
                                    <br />
                                    Lng {props.original.lng}
                                </span>
                                <br />
                            </>
                        )}
                        <span>{project ? project.name : props.original.project}</span>
                        <br />
                        <span>{props.original.purpose}</span>
                        {deleteActivity && (
                            <Button
                                // color="link"
                                className="w-100p mt20"
                                // size="sm"
                                onClick={e => {
                                    deleteActivity(props.value);
                                }}
                            >
                                Delete
                                {/* <i className="text-danger align-middle material-icons">delete</i> */}
                            </Button>
                        )}
                    </div>
                );
            },
        },
    ];
};

const actionList = {
    setJumbotronTitle,
    getMonthlyActivities,
    getApprovers,
    getProjects,
    submitTimesheetReport,
    deleteTimesheetActivity,
};

export default connect(null, actionList)(StoredLayout(MonthlyTimesheetPage));
