import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import sha512 from 'js-sha512';
import history from 'history.js';

import { Button } from 'reactstrap';
import CyderForm from 'pages/CyderForm';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import MaterialDesignInput from 'cyderComponents/forms/MaterialDesignInput';

import {
    cyderLoginAction,
    cyderLoginSetErrorMessageAction,
    cyderLoginClearErrorMessageAction,
    cyderLoginResetState,
    cyderResetProfileAction,
    cyderLoginSubmitOTPAction,
} from 'actions/security/cyderSecurityAction';

class Login extends Component {
    componentDidMount() {
        this.props.resetLoginPageState();
    }

    handleOnKeyPress = e => {
        if (e.key !== 'Enter') return;
        if (this.props.tfatoken === null) {
            this.validateThenSubmit();
            return;
        }
        this.submitOTP();
    };

    validateThenSubmit = () => {
        this.props.clearErrorMessage();
        const username = document.getElementById(this.props.fields[0].id).value;
        const password = document.getElementById(this.props.fields[1].id).value;

        if (username === '' || password === '') {
            this.props.setErroMessage('Please enter both username and password');
            return false;
        }
        this.props.loginSubmit(username, password);
    };

    submitOTP = () => {
        const token = document.getElementById(this.props.fields[2].id).value;
        this.props.submitOTP(token);
    };

    render() {
        const style = this.props.loginError ? null : { display: 'none' };
        return (
            <div className="d-flex row justify-content-between">
                <CyderForm
                    style={{ width: '100%' }}
                    title="Account Login"
                    logoPath
                    className="align-self-center semi-transparent"
                    description=""
                >
                    <div className="form-group">
                        <div style={style} className="color-danger text-center mb-4 font-weight-bold">
                            {this.props.errorMessage || null}
                        </div>
                        {this.props.requesting ? (
                            <LoadingSpinner />
                        ) : this.props.tfatoken ? (
                            <div>
                                <MaterialDesignInput field={this.props.fields[2]} onKeyPress={this.handleOnKeyPress} />
                            </div>
                        ) : (
                            <div>
                                <MaterialDesignInput field={this.props.fields[0]} onKeyPress={this.handleOnKeyPress} />
                                <MaterialDesignInput field={this.props.fields[1]} onKeyPress={this.handleOnKeyPress} />
                            </div>
                        )}
                    </div>
                    <div className="d-flex">
                        {this.props.tfatoken ? (
                            <Button block color="info" className="mb-2" disabled={this.props.requesting} onClick={() => this.submitOTP()}>
                                Submit OTP
                            </Button>
                        ) : (
                            <Button
                                block
                                color="info"
                                className="mb-2"
                                disabled={this.props.requesting}
                                onClick={() => this.validateThenSubmit()}
                            >
                                Login
                            </Button>
                        )}
                    </div>
                    <br />
                    <div>
                        <div className="col-xs-12 text-align-center">
                            <Link className="align-self-center ml-auto text-muted font-weight-bold" to="/forgotpassword">
                                Forgot Password
                            </Link>
                        </div>
                        {/* <div className="col-xs-12 col-md-6">
                            <a
                                className="mt-auto text-center mb-2 color-primary font-weight-bold"
                                style={{ height: '1px', width: '100%' }}
                                href={this.props.djarvisWebsite}
                            >
                                Register Now
                            </a>
                        </div> */}
                    </div>
                </CyderForm>
            </div>
        );
    }
}

const parseAuthResponse = res => {
    var navigateChangePassword = () => {
        var navigate = '/changepassword';
        history.replace(navigate);
    };
    var navigateHome = () => {
        var navigate = '/home';
        history.replace(navigate);
    };
    if (!res.authorizationToken) return;
    // if successful login, will have authorizationToken
    if (res.needchangepassword === 'Y' || res.firstlogin === 'Y') {
        navigateChangePassword();
        return;
    }
    navigateHome();
};

const mapStateToProps = state => {
    return {
        fields: state.cyderLoginReducer.fields,
        loginError: state.cyderLoginReducer.loginError,
        errorMessage: state.cyderLoginReducer.errorMessage,
        requesting: state.cyderLoginReducer.requesting,
        djarvisWebsite: state.config.djarvisWebsite,
        tfatoken: state.cyderLoginReducer.tfatoken,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        clearErrorMessage: () => {
            dispatch(cyderLoginClearErrorMessageAction());
        },
        setErroMessage: errorMessage => {
            dispatch(cyderLoginSetErrorMessageAction(errorMessage));
        },
        submitOTP: token => {
            dispatch(cyderLoginSubmitOTPAction(token)).then(res => parseAuthResponse(res));
        },
        loginSubmit: (username, password) => {
            const hashedpw = sha512(password);

            const data = {
                username,
                password: hashedpw,
                system: 'djarvis',
            };
            dispatch(cyderLoginAction(data)).then(res => parseAuthResponse(res));
        },
        resetLoginPageState: () => {
            dispatch(cyderLoginResetState());
            dispatch(cyderResetProfileAction());
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Login);
