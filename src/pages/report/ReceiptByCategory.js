import React from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';

import ExpenseSummaryReportTable from 'cyderComponents/table/ExpenseSummaryReportTable';
import ExpenseSummaryReportSearchPanel from 'cyderComponents/panel/ExpenseSummaryReportSearchPanel';

import { setJumbotronTitle } from 'actions/pagedata';
import {
    retrieveSummaryReportAction,
    updateReceiptByCategory,
    expenseLoadingShow,
    expenseLoadingHide,
    expenseResetForm,
} from 'actions/report/expenseReportAction';

class ReceiptByCategory extends React.Component {
    componentDidMount() {
        this.props.load();
    }
    render() {
        return (
            <div>
                <ExpenseSummaryReportSearchPanel search={this.props.search} />
                <ExpenseSummaryReportTable loading={this.props.report.loading} reports={this.props.report.receiptByCategory} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        report: state.cyderReportExpenseReducer,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        load: () => {
            dispatch(expenseResetForm());
            dispatch(expenseLoadingShow());
            var callback = function(error, reportData) {
                dispatch(updateReceiptByCategory(reportData));
                dispatch(expenseLoadingHide());
            };
            dispatch(retrieveSummaryReportAction('receipt-by-category', callback));
        },
        search: () => {
            dispatch(expenseLoadingShow());
            var callback = function(error, reportData) {
                dispatch(updateReceiptByCategory(reportData));
                dispatch(expenseLoadingHide());
            };
            dispatch(retrieveSummaryReportAction('receipt-by-category', callback));
        },
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Receipts by Category')),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(StoredLayout(ReceiptByCategory));
