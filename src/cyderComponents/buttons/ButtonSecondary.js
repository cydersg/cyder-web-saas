import React from 'react';
import { Button } from 'reactstrap';
import { layoutLib } from 'js/constlib';
const isMobile = window.innerWidth <= layoutLib.mobileWidth;

const ButtonSecondary = ({ onClick, faIcon, label, disabled, style }) => {
    return (
        <Button disabled={disabled} className={isMobile ? 'w-100p' : ''} style={style} onClick={onClick} color="secondary">
            {faIcon && <i class={`fa fa-${faIcon} fa-lg`}></i>}
            {label}
        </Button>
    );
};

export default ButtonSecondary;
