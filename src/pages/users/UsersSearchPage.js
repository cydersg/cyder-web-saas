import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import StoredLayout from 'layouts/StoredLayout';

import { Alert, Input, Button, Container, Row, Col, Form, FormGroup } from 'reactstrap';
import ReactTable from 'react-table';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

import { cyderUsersSearchAction } from 'actions/users/cyderUsersSearchAction';
import { setJumbotronTitle } from 'actions/pagedata';

import cyderlib from 'js/cyderlib';
import roles from 'json/roles.json';

class UsersSearchPage extends React.Component {
    constructor(props) {
        super(props);
        this.validateThenSearch = this.validateThenSearch.bind(this);
    }
    componentDidMount() {
        this.validateThenSearch();
    }
    validateThenSearch(e) {
        if (e) {
            e.preventDefault();
        }
        if (this.props.loading) return;
        // this.props.clearErrorMessage();

        const keyword = document.getElementById('keyword').value;
        const status = document.getElementById('status').value;

        /*
        // TODO: validation
        if (firstname === "" || lastname === "") {
            this.props.setErroMessage("Please enter both username and password");
            return false;
        }
        */
        const body = {
            keyword,
            status,
        };
        this.props.searchUsers(body);
    }
    render() {
        const data = this.props.users;

        const columns = [
            {
                Header: 'First Name',
                accessor: 'firstname',
                filterMethod: cyderlib.filterIgnoreCase,
            },
            {
                Header: 'Last Name',
                accessor: 'lastname',
                filterMethod: cyderlib.filterIgnoreCase,
            },
            {
                Header: 'Email',
                accessor: 'email',
                filterMethod: cyderlib.filterIgnoreCase,
            },
            {
                Header: 'Mobile Number',
                accessor: 'mobilenumber',
                filterMethod: cyderlib.filterIgnoreCase,
            },
            {
                Header: 'Role',
                accessor: 'role',
                filterMethod: cyderlib.filterIgnoreCase,
                Cell: props => {
                    return <div>{roles[props.value].title}</div>;
                },
            },
            {
                Header: '',
                headerClassName: 'rt-no-sort-ind',
                accessor: 'id',
                sortable: false,
                filterable: false,
                width: 150,
                Cell: props => {
                    const url = '/administration/users/details/' + props.value;
                    return (
                        <Link to={url} tabIndex="-1">
                            <i className="color-dark align-middle material-icons">edit</i>
                        </Link>
                    );
                },
            },
        ];
        return (
            <div key={0}>
                <Container>
                    <Row>
                        <Col xs={12}>
                            <Form onSubmit={this.validateThenSearch}>
                                <FormGroup>
                                    <Input placeholder="Keyword" id="keyword" />
                                </FormGroup>
                                <FormGroup>
                                    <Input type="select" id="status">
                                        <option>Active</option>
                                        <option>Inactive</option>
                                    </Input>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                    <Row className="mb-2">
                        <Col>
                            <Button disabled={this.props.loading} onClick={() => this.validateThenSearch()} tabIndex="-1">
                                Search
                            </Button>
                            <Link to="/administration/users/add" className="btn btn-success" style={{ float: 'right' }} tabIndex="-1">
                                Add New User
                            </Link>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            {this.props.loading ? (
                                <LoadingSpinner center />
                            ) : this.props.users.length > 0 ? (
                                <ReactTable
                                    className="-highlight mb-2"
                                    data={data}
                                    columns={columns}
                                    minRows={1}
                                    filterable={data.length > 0}
                                    showPagination={false}
                                />
                            ) : this.props.errorMessage ? (
                                <Alert color="danger">{this.props.errorMessage}</Alert>
                            ) : null}
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ...state.cyderUsersSearchReducer,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        searchUsers: body => {
            dispatch(cyderUsersSearchAction(body));
        },
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Group Detail')),
    };
};
// translate() is if we want to use HOC to perform t()
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(StoredLayout(UsersSearchPage));
