import React from 'react';

export const InfoField = props => {
    const { children } = props;
    return (
        <div className={`${props.className} py-2`}>
            <span className="text-muted font-weight-bold">{props.label}</span>
            {children || <h6 className="font-weight-bold">{props.value} </h6>}
        </div>
    );
};
