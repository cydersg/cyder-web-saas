import {
    CYDER_WORKFLOW_MYTASKS_GET_MYTASKS_REQUEST,
    CYDER_WORKFLOW_MYTASKS_GET_MYTASKS_SUCCESS,
    CYDER_WORKFLOW_MYTASKS_GET_MYTASKS_FAILURE,
    CYDER_WORKFLOW_MYTASKS_MODAL_TOGGLE,
} from 'actions/constants/actionTypesWorkflow';
import withQuery from 'with-query';

import { CYDER_COMMON_API_ROOT } from 'config';
import fetchlib from 'js/fetchlib';

export function myTasksGetTasks(resourceType) {
    return (dispatch, getState) => {
        const url = withQuery(CYDER_COMMON_API_ROOT + '/workflow', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getmytasks',
        });

        const body = {
            status: 'Saved',
            resourceType,
        };

        return fetchlib.fetch(
            dispatch,
            getState,
            'POST',
            url,
            body,
            CYDER_WORKFLOW_MYTASKS_GET_MYTASKS_REQUEST,
            CYDER_WORKFLOW_MYTASKS_GET_MYTASKS_SUCCESS,
            CYDER_WORKFLOW_MYTASKS_GET_MYTASKS_FAILURE,
            true,
        );
    };
}

export function expenseMyReceiptsToggleModal(modalAction, data) {
    return {
        type: CYDER_WORKFLOW_MYTASKS_MODAL_TOGGLE,
        modalAction,
        data,
    };
}
