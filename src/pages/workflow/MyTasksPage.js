import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import withQuery from 'with-query';
import moment from 'moment';
import history from 'history.js';

import { Badge, Container } from 'reactstrap';
import ReactTable from 'react-table';
import StoredLayout from 'layouts/StoredLayout';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import { TablePagination } from 'cyderComponents/pagination/';

import { myTasksGetTasks, expenseMyReceiptsToggleModal } from 'actions/workflow/myTasksAction.js';
import { setJumbotronTitle } from 'actions/pagedata';

import cyderlib from 'js/cyderlib.js';
import 'css/forms/default-forms.css';
import { expenseDeletableStatuses, urlRecurring, urlReceipt, layoutLib } from 'js/constlib';

class MyTasksPage extends Component {
    componentDidMount() {
        this.props.getTasks();
    }

    toggleAction = () => {
        const { modalAction, getTasks } = this.props;
        if (modalAction === 'refresh') getTasks();
        if (modalAction === 'redirect') history.push('/expense/myreceipts');
        this.handleToggleModal();
    };

    calcCurrentPositiveButtonText = () => {
        switch (this.props.modalAction) {
            case 'save':
                return 'Save';
            case 'delete':
                return 'Delete';
            default:
                return 'Ok';
        }
    };

    calcCurrentPositiveButtonAction() {
        const { modalAction, getTasks, handleDelete } = this.props;
        switch (modalAction) {
            case 'save':
                return () => {
                    const getElementValueById = id => document.getElementById(id).value;
                    this.props.handleSave({
                        category: getElementValueById('category'),
                        paymentmode: getElementValueById('paymentmode'),
                        project: getElementValueById('project'),
                        currency: this.state.selectedCurrency,
                    });
                };
            case 'delete':
                return handleDelete;
            case 'refresh':
                return () => {
                    getTasks();
                    this.handleToggleModal();
                };
            case 'redirect':
                return () => {
                    history.push(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'workflow'}/myreceipts`);
                    this.handleToggleModal();
                };
            default:
                return this.handleToggleModal;
        }
    }

    handleToggleModal = (modalAction, data) => {
        this.props.toggleModal(modalAction, data);
    };

    getTdProps = (state, rowInfo, column, instance) => {
        return {
            onClick: (e, handleOriginal) => {
                if (!rowInfo) return;
                let url = withQuery(
                    `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'workflow'}/task/${rowInfo.original.instance_id}`,
                    {
                        realid: rowInfo.original.id,
                    },
                );
                history.push(url);
                if (handleOriginal) handleOriginal();
            },
        };
    };

    render() {
        const { loading, receipts } = this.props;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        if (loading) return <LoadingSpinner center />;
        return (
            <Container className="wideContainer">
                <div key="rt-table">
                    <ReactTable
                        // className="-highlight mb-2"
                        showPagination={!isMobile}
                        filterable={false}
                        data={receipts}
                        columns={getColumns()}
                        filterable={receipts.length > 0 && !isMobile}
                        getTdProps={this.getTdProps}
                        pageSize={receipts.length > 20 ? 20 : receipts.length}
                        PaginationComponent={TablePagination}
                        NoDataComponent={() => <div className="rt-noData">No tasks found</div>}
                    />
                </div>
                <DialogModal
                    key="modal"
                    modal={this.props.modalOpen}
                    toggle={this.toggleAction}
                    customHeader={this.props.modalHeader || 'Confirmation'}
                    customModalBody={this.props.saving ? <LoadingSpinner /> : this.props.modalMessage}
                    positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                    positiveButtonText={this.calcCurrentPositiveButtonText()}
                    onlyOneButton={this.props.saved}
                    buttonDisabler={this.props.saving}
                />
            </Container>
        );
    }
}

const getColumns = props => {
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return [
        {
            Header: '',
            accessor: 'type',
            width: 50,
            show: false,
            sortable: true,
            filterable: false,
            Cell: ({ value }) => {
                const isMileage = value === 'Mileage';
                return (
                    <div>
                        <i className="align-middle material-icons">{isMileage ? 'directions_car' : 'receipt'}</i>
                    </div>
                );
            },
        },
        {
            Header: 'Date',
            accessor: 'createddt',
            width: 100,
            show: !isMobile,
            filterable: false,
            sortMethod: (a, b, desc) => cyderlib.tableSortDate(a, b, desc, 'YYYY-MM-DD HH:mm:ss'),
            Cell: props => {
                return moment(props.value, 'YYYY-MM-DD HH:mm:ss').format('DD-MMM-YYYY');
            },
        },
        {
            Header: 'Type',
            show: !isMobile,
            accessor: 'resource_type',
            filterMethod: cyderlib.filterIgnoreCase,
        },
        {
            Header: 'Originator',
            show: !isMobile,
            accessor: 'originator_fullname',
            filterMethod: cyderlib.filterIgnoreCase,
        },
        {
            Header: 'Status',
            show: !isMobile,
            accessor: 'status',
            width: 80,
            filterable: false,
            filterMethod: cyderlib.filterIgnoreCase,
            Cell: props => <Badge color={cyderlib.statusBadgeColor(props.value)}>{props.value}</Badge>,
        },
        {
            Header: '',
            headerClassName: 'rt-no-sort-ind',
            accessor: 'instance_id',
            sortable: false,
            show: !isMobile,
            filterable: false,
            width: 100,
            Cell: props => {
                const url = withQuery(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'workflow'}/task/${props.value}`, {
                    realid: props.row._original.id,
                });
                return (
                    <Link className="font-weight-bold" to={url}>
                        View
                    </Link>
                );
            },
        },
        {
            Header: '',
            headerClassName: 'rt-no-sort-ind',
            accessor: 'id',
            sortable: false,
            show: isMobile,
            filterable: false,
            Cell: props => {
                const url = withQuery(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'workflow'}/task/${props.value}`, {
                    realid: props.row._original.id,
                });
                return (
                    <Link
                        style={{
                            color: 'unset',
                        }}
                        to={url}
                    >
                        <div className="boxShadow">
                            <span>
                                <strong>{props.original.resource_type}</strong>
                            </span>
                            <br />
                            <span>{moment(props.original.createddt, 'YYYY-MM-DD HH:mm:ss').format('DD-MMM-YYYY')}</span>
                            <br />
                            <span>{props.original.originator_fullname}</span>
                            <br />
                            <Badge color={cyderlib.statusBadgeColor(props.original.status)}>{props.original.status}</Badge>
                        </div>
                    </Link>
                );
            },
        },
    ];
};

const mapStateToProps = state => {
    return {
        ...state.myTasksReducer,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        toggleModal: (modalAction, data) => {
            dispatch(expenseMyReceiptsToggleModal(modalAction ? modalAction : null, data));
        },
        setJumbotronTitle: title => dispatch(setJumbotronTitle(title)),
        getTasks: () => dispatch(myTasksGetTasks('Expense Report')),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(MyTasksPage));
