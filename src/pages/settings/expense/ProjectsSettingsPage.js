import React from 'react';
import { connect } from 'react-redux';
import debounce from 'es6-promise-debounce';
import { withTranslation } from 'react-i18next';
import StoredLayout from 'layouts/StoredLayout';

import ReactTable from 'react-table';
import { Async } from 'react-select';
import { Alert, Input, Label, Button, Container, Row, Col, Form, FormGroup } from 'reactstrap';
import DialogModal from 'cyderComponents/modals/DialogModal';
import ButtonFloatingBottom from 'cyderComponents/buttons/ButtonFloatingBottom';
import MyCorpSettingsCard from 'cyderComponents/cardWidget/MyCorpSettingsCard';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

import {
    cyderProjectsSearchAction,
    cyderProjectsSettingsEditModalSetProject,
    cyderProjectsSettingsModalToggleAction,
    cyderProjectsSettingsEditModalChangeValue,
    cyderProjectsSettingsAddProjectAction,
    cyderProjectsSettingsUpdateProjectAction,
    cyderProjectsSettingsDeleteProjectAction,
    cyderProjectsSettingsAddUserAction,
    cyderProjectsSettingsRemoveUserAction,
    cyderProjectsSettingsAddApproverAction,
    cyderProjectsSettingsRemoveApproverAction,
} from 'actions/settings/cyderProjectsSettingsSearchAction';
import { cyderGroupsAddAddUserSearchAction } from 'actions/groups/cyderGroupsAddAction';
import { setJumbotronTitle } from 'actions/pagedata';
import { layoutLib } from 'js/constlib';

import reactTableFilter from 'js/reactTableFilter';
import MyCorpTimesheetBottomNav from '../../timesheet/MyCorpTimesheetBottomNav';

class ProjectsSettingsPage extends React.Component {
    constructor(props) {
        super(props);
        this.onFilteredChange = this.onFilteredChange.bind(this);
        this.filterAllFunc = this.filterAllFunc.bind(this);
        this.state = {
            filterAll: '',
            approvers: {},
        };
        this.calcCurrentPositiveButtonAction = this.calcCurrentPositiveButtonAction.bind(this);
        this.setApprover = this.setApprover.bind(this);
        this.getOptionsUsers = this.getOptionsUsers.bind(this);
        this.getOptionsApprovers = this.getOptionsApprovers.bind(this);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.toggleAction = this.toggleAction.bind(this);
        this.validateThenSearch = this.validateThenSearch.bind(this);
        this.filterOptionsUsers = this.filterOptionsUsers.bind(this);
        this.filterOptionsApprovers = this.filterOptionsApprovers.bind(this);
        this.handleSwitchChange = this.handleSwitchChange.bind(this);
    }
    handleSwitchChange(checked) {
        const synthEvent = {
            target: {
                id: 'overrideexpenseapproval',
                value: checked,
            },
        };
        this.props.changeModalInputValue(synthEvent);
    }
    componentDidMount() {
        if (this.props.forceRefresh) {
            this.validateThenSearch();
        }
        this.validateThenSearch();
        this.props.setJumbotronTitle();
        // this.props.getAllProjects();
    }
    validateThenSearch(e) {
        if (e) {
            e.preventDefault();
        }
        // this.props.clearErrorMessage();

        const keyword = document.getElementById('keyword').value;
        const status = document.getElementById('status').value;

        const body = {
            keyword,
            status,
        };
        this.props.searchProjects(body);
    }
    setupEditModal(project) {
        this.props.setModalProject(project);
        let newApproversObj = {
            1: null,
            2: null,
        };
        if (project.approvers[0]) {
            newApproversObj[1] = {
                label: project.approvers[0],
                value: project.approvers[0],
            };
        }
        if (project.approvers[1]) {
            newApproversObj[2] = {
                label: project.approvers[1],
                value: project.approvers[1],
            };
        }
        this.setState({
            approvers: newApproversObj,
        });
        this.props.toggleModal('update');
    }
    setupDeleteModal(project) {
        this.props.setModalProject(project);
        this.props.toggleModal('delete');
    }
    getOptionsUsers(input) {
        // const username = document.getElementById('searchUsername').value;
        return this.props.getOptions(input, this.props.usersReference);
    }
    getOptionsApprovers(input) {
        // const username = document.getElementById('searchUsername').value;
        return this.props.getOptions(input, this.props.approversReference);
    }
    toggleAction() {
        this.handleToggleModal();
        if (this.props.modalAction === 'refresh') {
            this.validateThenSearch();
        }
    }
    calcCurrentPositiveButtonText() {
        if (this.props.modalAction === 'update') {
            return 'Update';
        } else if (this.props.modalAction === 'add') {
            return 'Add';
        } else if (this.props.modalAction === 'delete') {
            return 'Delete';
        } else if (this.props.modalAction === 'close') {
            return 'Ok';
        } else {
            return 'Ok';
        }
    }
    calcCurrentPositiveButtonAction() {
        if (this.props.modalAction === 'update') {
            return () => {
                this.props.handleUpdate(this.state.approvers);
            };
        } else if (this.props.modalAction === 'refresh') {
            return () => {
                this.handleToggleModal();
                this.validateThenSearch();
            };
        } else if (this.props.modalAction === 'add') {
            return () => {
                this.props.handleAdd(this.state.approvers);
            };
        } else if (this.props.modalAction === 'delete') {
            return this.props.handleDelete;
        } else if (this.props.modalAction === 'close') {
            return this.handleToggleModal;
        } else {
            return this.handleToggleModal;
        }
    }
    handleToggleModal(modalAction) {
        if (modalAction === 'add') {
            this.setState({
                approvers: {},
            });
        }
        this.props.toggleModal(modalAction);
    }
    filterOptionsUsers(options) {
        const results = options.filter(item => !this.props.usersReference[item.value]);
        return results;
    }
    filterOptionsApprovers(options) {
        const results = options.filter(item => {
            let approvers = Object.values(this.state.approvers);
            const hasOption = approvers.findIndex(approver => {
                return approver !== null && approver.value === item.value;
            });
            return hasOption === -1;
        });
        return results;
    }
    setApprover(val, i) {
        if (i === 1 && val === null) {
            // clear both fields when 1st officer is cleared.
            // previously wouldn't clear 2nd officer
            this.setState({
                approvers: {},
            });
            return;
        }
        let newApprovers = {};
        newApprovers[i] = val;
        this.setState({
            approvers: Object.assign(this.state.approvers, newApprovers),
        });
    }
    onFilteredChange(filtered) {
        this.setState(reactTableFilter.onFilteredChange(filtered));
    }
    filterAllFunc(e) {
        this.setState(reactTableFilter.filterAllFunc(e));
    }
    render() {
        const customModalBody = (
            <div>
                <FormGroup>
                    <Label for="exampleText">Project Code</Label>
                    <Input
                        onChange={event => this.props.changeModalInputValue(event)}
                        disabled={this.props.modalAction === 'update'}
                        type="text"
                        maxLength={10}
                        id="code"
                        value={this.props.modalProject.code}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleText">Project Name</Label>
                    <Input
                        onChange={event => this.props.changeModalInputValue(event)}
                        type="text"
                        maxLength={100}
                        id="name"
                        value={this.props.modalProject.name}
                    />
                </FormGroup>
                {/* <FormGroup>
                    <Label>Approving Officer 1</Label>
                    <Async
                        placeholder="Search user to add..."
                        autoload={false}
                        value={this.state.approvers[1]}
                        onChange={val => {
                            this.setApprover(val, 1);
                        }}
                        name="async"
                        filterOptions={this.filterOptionsApprovers}
                        loadOptions={debounce(this.getOptionsApprovers, 500)}
                    />
                </FormGroup>
                <FormGroup>
                    <Label>Approving Officer 2</Label>
                    <Async
                        placeholder="Search user to add..."
                        autoload={false}
                        value={this.state.approvers[1] === undefined || this.state.approvers[1] === null ? null : this.state.approvers[2]}
                        disabled={this.state.approvers[1] === undefined || this.state.approvers[1] === null}
                        onChange={val => {
                            this.setApprover(val, 2);
                        }}
                        name="async"
                        filterOptions={this.filterOptionsApprovers}
                        loadOptions={debounce(this.getOptionsApprovers, 500)}
                    />
                </FormGroup> */}
            </div>
        );

        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        const columns = [
            reactTableFilter.generateAllColumnFilter(),
            {
                Header: 'Project Code',
                accessor: 'code',
                show: !isMobile,
            },
            {
                Header: 'Project Name',
                accessor: 'name',
                show: !isMobile,
            },
            {
                Header: '',
                accessor: 'id',
                show: !isMobile,
                Cell: props => {
                    const thisProject = this.props.projects[props.index];
                    return (
                        <div className="float-right">
                            <Button
                                color="link"
                                size="sm"
                                onClick={() => {
                                    this.setupEditModal(thisProject);
                                }}
                            >
                                <i className="align-middle material-icons">edit</i>
                            </Button>
                            <Button
                                color="link"
                                className="ml-1"
                                size="sm"
                                onClick={() => {
                                    this.setupDeleteModal(thisProject);
                                }}
                            >
                                <i className="text-danger align-middle material-icons">delete</i>
                            </Button>
                        </div>
                    );
                },
            },
            {
                Header: '',
                accessor: 'id',
                show: isMobile,
                Cell: props => {
                    const thisProject = this.props.projects[props.index];
                    return (
                        <MyCorpSettingsCard
                            onPrimaryClick={() => {
                                this.setupEditModal(thisProject);
                            }}
                            title={props.original.code}
                            remarks={props.original.name}
                            onSecondaryClick={() => {
                                this.setupDeleteModal(thisProject);
                            }}
                            secondaryIcon="delete"
                        />
                    );
                },
            },
        ];
        return (
            <div key={0}>
                <Container className="wideContainer">
                    <Row className="d-none">
                        <Col xs={12}>
                            <Form onSubmit={this.validateThenSearch}>
                                <FormGroup>
                                    <Input placeholder="Keyword" id="keyword" />
                                </FormGroup>
                                <FormGroup>
                                    <Input type="select" id="status">
                                        <option>Active</option>
                                        <option>Inactive</option>
                                    </Input>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                    <Row className="mb-2">
                        <Col className="d-flex">
                            <Input
                                className={isMobile ? 'w-100p' : 'w-300'}
                                placeholder="Search"
                                onChange={this.filterAllFunc}
                                value={this.state.filterAll}
                            />
                            {/* <LinkButton preset="addButton" onClick={() => this.handleToggleModal('add')} text="Project" /> */}
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            {this.props.loading ? (
                                <LoadingSpinner center />
                            ) : this.props.projects.length > 0 ? (
                                isMobile ? (
                                    <ReactTable
                                        filtered={this.state.filtered}
                                        onFilteredChange={this.onFilteredChange}
                                        TheadComponent={props => null}
                                        filterable={false}
                                        defaultFilterMethod={reactTableFilter.defaultFilterMethod}
                                        data={this.props.projects}
                                        columns={columns}
                                        minRows={0}
                                        showPagination={false}
                                    />
                                ) : (
                                    <ReactTable
                                        filtered={this.state.filtered}
                                        onFilteredChange={this.onFilteredChange}
                                        filterable={false}
                                        defaultFilterMethod={reactTableFilter.defaultFilterMethod}
                                        data={this.props.projects}
                                        columns={columns}
                                        minRows={0}
                                        showPagination={false}
                                    />
                                )
                            ) : this.props.errorMessage ? (
                                <Alert color="danger">{this.props.errorMessage}</Alert>
                            ) : null}
                        </Col>
                    </Row>
                </Container>
                <DialogModal
                    key="modal"
                    modal={this.props.modalOpen}
                    toggle={this.toggleAction}
                    customHeader={this.props.modalHeader}
                    customModalBody={
                        this.props.saving ? <LoadingSpinner /> : this.props.modalMessage ? this.props.modalMessage : customModalBody
                    }
                    positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                    positiveButtonText={this.calcCurrentPositiveButtonText()}
                    onlyOneButton={this.props.saved}
                    buttonDisabler={this.props.saving}
                />
                <ButtonFloatingBottom
                    onClick={e => {
                        this.handleToggleModal('add');
                    }}
                    label="+"
                />
                <MyCorpTimesheetBottomNav page="home" />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ...state.cyderProjectsSettingsSearchReducer,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        searchProjects: body => {
            dispatch(cyderProjectsSearchAction(body));
        },
        setModalProject: project => {
            dispatch(cyderProjectsSettingsEditModalSetProject(project));
        },
        toggleModal: modalAction => {
            if (modalAction === 'add') {
                // dispatch(cyderProjectsSettingsEditModalSetProject("", "", "", [{id: 'aaa'},{id: 'bbb'},{id: 'ccc'},{id: 'ddd'}]))
                const modalProject = {
                    id: '',
                    code: '',
                    name: '',
                    users: [],
                    approvers: [],
                    approvalvotescount: 1,
                    overrideexpenseapproval: false,
                };
                dispatch(cyderProjectsSettingsEditModalSetProject(modalProject));
            }
            dispatch(cyderProjectsSettingsModalToggleAction(modalAction ? modalAction : null));
        },
        changeModalInputValue: event => {
            const value = event.target.value;
            const key = event.target.id;
            dispatch(cyderProjectsSettingsEditModalChangeValue(key, value));
        },
        getOptions: input => {
            return dispatch(cyderGroupsAddAddUserSearchAction(input));
        },
        handleAdd: approvers => {
            dispatch(cyderProjectsSettingsAddProjectAction(approvers));
        },
        handleUpdate: approvers => {
            dispatch(cyderProjectsSettingsUpdateProjectAction(approvers));
        },
        handleDelete: () => dispatch(cyderProjectsSettingsDeleteProjectAction()),
        addUser: val => {
            if (val === null) return;
            const username = val.value;
            dispatch(cyderProjectsSettingsAddUserAction(username));
        },
        setApprover: (val, i) => {
            if (val === null) return;
            const username = val.value;
            dispatch(cyderProjectsSettingsAddApproverAction(username, i));
        },
        addApprover: val => {
            if (val === null) return;
            const username = val.value;
            dispatch(cyderProjectsSettingsAddApproverAction(username));
        },
        removeUser: username => {
            dispatch(cyderProjectsSettingsRemoveUserAction(username));
        },
        removeApprover: username => {
            dispatch(cyderProjectsSettingsRemoveApproverAction(username));
        },
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Settings')),
    };
};
// translate() is if we want to use HOC to perform t()
export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(StoredLayout(ProjectsSettingsPage)));
