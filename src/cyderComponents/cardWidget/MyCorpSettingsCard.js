import React from 'react';
import { Col, Row, Badge, Container, Modal, ModalBody, ModalFooter, Button, ModalHeader } from 'reactstrap';

const MyCorpSettingsCard = props => {
    const { onPrimaryClick, title, remarks, onSecondaryClick, secondaryIcon } = props;

    return (
        <div className="boxShadow">
            <Row>
                <Col
                    className="d-flex align-items-center"
                    style={{
                        justifyContent: 'space-between',
                    }}
                >
                    <div
                        onClick={onPrimaryClick}
                        style={{
                            width: '100%',
                        }}
                    >
                        <div className="d-flex justify-content-between">
                            <strong>{title}</strong>
                        </div>
                        <div>{remarks}</div>
                    </div>
                    {onSecondaryClick && (
                        <Button color="link" className="ml-1" size="lg" onClick={onSecondaryClick}>
                            <i className="text-danger align-middle material-icons">{secondaryIcon}</i>
                        </Button>
                    )}
                </Col>
            </Row>
        </div>
    );
};

export default MyCorpSettingsCard;
