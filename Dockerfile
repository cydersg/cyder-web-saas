ARG ENV=PROD
FROM node:8
LABEL version="1.0"
LABEL description="Djarvis Web App"

# Add application to container
RUN mkdir /app
RUN mkdir /app/djarvis

WORKDIR /app/djarvis
COPY . .

# RUN rm -f node_modules
# RUN rm package-lock.json
RUN npm install
RUN REACT_APP_NODE_ENV=$ENV npm run-script build

RUN yarn global add serve

# Expose port
EXPOSE 5000

# Start the app
ENTRYPOINT ["serve","-s","./build"]