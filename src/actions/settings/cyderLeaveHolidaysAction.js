import withQuery from 'with-query';
import fetchlib from 'js/fetchlib';
import { DJARVIS_COMMON_API_ROOT, CYDER_LEAVE_API_ROOT } from '../../config';

export function getAllHolidays(year) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/holiday`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getallholidays',
            year,
        });
        return fetchlib.fetch(dispatch, getState, 'GET', url);
    };
}

export function createHoliday(body) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/holiday`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'create',
        });

        // const body = {"holidaydate": "2019-12-25", "holidayname": "Christmas Eve"}'
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}

export function updateHoliday(body) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/holiday`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'update',
        });
        // const body = {"holidaydate": "2019-12-25", "holidayname": "Christmas Eve"}'
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}

export function deleteHoliday(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/holiday`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'delete',
        });
        const body = { id };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}
