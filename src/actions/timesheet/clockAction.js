import withQuery from 'with-query';
import moment from 'moment';

import {
    TIMESHEET_CLOCK_CLOCK_GET_CURRENT_ACTIVITY_REQUEST,
    TIMESHEET_CLOCK_CLOCK_GET_CURRENT_ACTIVITY_SUCCESS,
    TIMESHEET_CLOCK_CLOCK_GET_CURRENT_ACTIVITY_FAILURE,
    TIMESHEET_CLOCK_CLOCK_IN_REQUEST,
    TIMESHEET_CLOCK_CLOCK_IN_SUCCESS,
    TIMESHEET_CLOCK_CLOCK_IN_FAILURE,
    TIMESHEET_CLOCK_CLOCK_OUT_REQUEST,
    TIMESHEET_CLOCK_CLOCK_OUT_SUCCESS,
    TIMESHEET_CLOCK_CLOCK_OUT_FAILURE,
    TIMESHEET_CLOCK_FORCE_LOADING,
    TIMESHEET_MYCLOCKS_GET_PAST_ACTIVITY_REQUEST,
    TIMESHEET_MYCLOCKS_GET_PAST_ACTIVITY_SUCCESS,
    TIMESHEET_MYCLOCKS_GET_PAST_ACTIVITY_FAILURE,
    TIMESHEET_CLOCK_CLOCK_GET_ALL_FAVOURITES_SUCCESS,
    TIMESHEET_CLOCK_CLOCK_GET_ALL_FAVOURITES_FAILURE,
} from '../constants/actionTypesTimesheet';

import { CYDER_LEAVE_API_ROOT, DJARVIS_COMMON_API_ROOT } from '../../config';
import fetchlib from '../../js/fetchlib';

export function findLocation(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${DJARVIS_COMMON_API_ROOT}/location`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'findbyid',
            id,
        });
        return fetchlib.fetch(dispatch, getState, 'GET', url, null, null, null, null);
    };
}

export function getAllFavourites(owner) {
    return (dispatch, getState) => {
        const body = { owner };
        const url = withQuery(`${DJARVIS_COMMON_API_ROOT}/location`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'partialsearch',
        });
        return fetchlib.fetch(
            dispatch,
            getState,
            'POST',
            url,
            body,
            null,
            TIMESHEET_CLOCK_CLOCK_GET_ALL_FAVOURITES_SUCCESS,
            TIMESHEET_CLOCK_CLOCK_GET_ALL_FAVOURITES_FAILURE,
        );
    };
}

export function getLatestActivity() {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getlatestactivity',
        });
        return fetchlib.fetch(
            dispatch,
            getState,
            'GET',
            url,
            null,
            TIMESHEET_CLOCK_CLOCK_GET_CURRENT_ACTIVITY_REQUEST,
            TIMESHEET_CLOCK_CLOCK_GET_CURRENT_ACTIVITY_SUCCESS,
            TIMESHEET_CLOCK_CLOCK_GET_CURRENT_ACTIVITY_FAILURE,
        );
    };
}

export function getData() {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getcurrentactivity',
        });
        return fetchlib.fetch(
            dispatch,
            getState,
            'GET',
            url,
            null,
            TIMESHEET_CLOCK_CLOCK_GET_CURRENT_ACTIVITY_REQUEST,
            TIMESHEET_CLOCK_CLOCK_GET_CURRENT_ACTIVITY_SUCCESS,
            TIMESHEET_CLOCK_CLOCK_GET_CURRENT_ACTIVITY_FAILURE,
        );
    };
}
export function getPastActivityData(month, year) {
    return (dispatch, getState) => {
        const body = { month, year };
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getallactivities',
        });
        return fetchlib.fetch(
            dispatch,
            getState,
            'POST',
            url,
            body,
            TIMESHEET_MYCLOCKS_GET_PAST_ACTIVITY_REQUEST,
            TIMESHEET_MYCLOCKS_GET_PAST_ACTIVITY_SUCCESS,
            TIMESHEET_MYCLOCKS_GET_PAST_ACTIVITY_FAILURE,
        );
    };
}

export function removeLocationFromFavourite(id) {
    return (dispatch, getState) => {
        const body = { id };
        const url = withQuery(`${DJARVIS_COMMON_API_ROOT}/location`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'deletebyid',
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, body, null, null, null);
    };
}

export function addLocationToFavourite(location) {
    return (dispatch, getState) => {
        const body = {
            code: new Date().getTime() + '',
            name: location.location_name,
            address: location.location_address,
            owner: location.owner,
            lat: location.lat,
            lng: location.lng,
        };
        const url = withQuery(`${DJARVIS_COMMON_API_ROOT}/location`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'add',
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, body, null, null, null);
    };
}

export function clockIn(project, remarks, lat, lng, actualLat, actualLng, location_name, location_address) {
    return (dispatch, getState) => {
        const body = {
            project,
            purpose: remarks,
            starttime: moment().format('YYYY-MM-DD HH:mm:ss'),
            lat,
            lng,
            actual_lat: actualLat,
            actual_lng: actualLng,
            location_name,
            location_address,
        };
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'checkin',
        });
        return fetchlib.fetch(
            dispatch,
            getState,
            'POST',
            url,
            body,
            TIMESHEET_CLOCK_CLOCK_IN_REQUEST,
            TIMESHEET_CLOCK_CLOCK_IN_SUCCESS,
            TIMESHEET_CLOCK_CLOCK_IN_FAILURE,
        );
    };
}
export function clockOut(id) {
    return (dispatch, getState) => {
        const body = {
            id,
            endtime: moment().format('YYYY-MM-DD HH:mm:ss'),
        };
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'checkout',
        });
        return fetchlib.fetch(
            dispatch,
            getState,
            'POST',
            url,
            body,
            TIMESHEET_CLOCK_CLOCK_OUT_REQUEST,
            TIMESHEET_CLOCK_CLOCK_OUT_SUCCESS,
            TIMESHEET_CLOCK_CLOCK_OUT_FAILURE,
        );
    };
}

export function setLoading(loadingState) {
    return dispatch => {
        dispatch({
            type: TIMESHEET_CLOCK_FORCE_LOADING,
            loadingState,
        });
        return Promise.resolve();
    };
}
