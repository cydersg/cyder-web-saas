import {
    CYDER_GROUPS_DETAIL_REQUEST,
    CYDER_GROUPS_DETAIL_SUCCESS,
    CYDER_GROUPS_DETAIL_FAILURE,
    CYDER_GROUPS_DETAIL_REMOVE_USER_LIST_ADD,
    CYDER_GROUPS_DETAIL_REMOVE_USER_LIST_REMOVE,
    CYDER_GROUPS_DETAIL_ADD_USER_LIST_ADD,
    CYDER_GROUPS_DETAIL_ADD_USER_LIST_REMOVE,
    CYDER_GROUPS_DETAIL_CHANGE_FIELD,
    CYDER_GROUPS_DETAIL_SAVE_CHANGES_REQUEST,
    CYDER_GROUPS_DETAIL_SAVE_CHANGES_SUCCESS,
    CYDER_GROUPS_DETAIL_SAVE_CHANGES_FAILURE,
    CYDER_GROUPS_DETAIL_DELETE_REQUEST,
    CYDER_GROUPS_DETAIL_DELETE_SUCCESS,
    CYDER_GROUPS_DETAIL_DELETE_FAILURE,
    CYDER_GROUPS_DETAIL_MODAL_TOGGLE,
} from '../constants/actionTypes';
import withQuery from 'with-query';

import { CYDER_COMMON_API_ROOT } from '../../config';

/**
 * Login function
 *
 * @param {*} fields
 * @param {*} callback
 */
export function cyderRemoveUserListAdd(username) {
    return {
        type: CYDER_GROUPS_DETAIL_REMOVE_USER_LIST_ADD,
        username,
    };
}
export function cyderRemoveUserListRemove(username) {
    return {
        type: CYDER_GROUPS_DETAIL_REMOVE_USER_LIST_REMOVE,
        username,
    };
}
export function cyderAddNewUserToList(username) {
    return {
        type: CYDER_GROUPS_DETAIL_ADD_USER_LIST_ADD,
        username,
    };
}
export function cyderRemoveNewUserFromList(username) {
    return {
        type: CYDER_GROUPS_DETAIL_ADD_USER_LIST_REMOVE,
        username,
    };
}
export function cyderChangeGroupDetailFieldValueAction(key, value) {
    return {
        type: CYDER_GROUPS_DETAIL_CHANGE_FIELD,
        key,
        value,
    };
}
export function cyderGetGroupDetailsAction(groupid) {
    // Redux Thunk will inject dispatch here:
    return (dispatch, getState) => {
        // Reducers may handle this to set a flag like isFetching
        dispatch({ type: CYDER_GROUPS_DETAIL_REQUEST });

        // Perform the actual API call
        const url = withQuery(CYDER_COMMON_API_ROOT + '/groups', {
            appid: getState().cyderProfileReducer.profile.appId,
            id: groupid,
            action: 'findbyid',
        });
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    response.json().then(error => {
                        dispatch({ type: CYDER_GROUPS_DETAIL_FAILURE, error });
                        throw error;
                    });
                } else {
                    response.json().then(response => {
                        dispatch({ type: CYDER_GROUPS_DETAIL_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_GROUPS_DETAIL_FAILURE, error });
                return error;
            });
    };
}
export function cyderSaveGroupDetailChangesAction() {
    // Redux Thunk will inject dispatch here:
    return (dispatch, getState) => {
        // Reducers may handle this to set a flag like isFetching
        dispatch({ type: CYDER_GROUPS_DETAIL_SAVE_CHANGES_REQUEST });

        // Perform the actual API call
        const url = withQuery(CYDER_COMMON_API_ROOT + '/groups', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'update',
        });
        const usersList = getState().cyderGroupsDetailReducer.group.usersList;
        const removedUsers = getState().cyderGroupsDetailReducer.removedUsers;
        let newUsers = [];
        usersList.forEach(user => {
            if (!removedUsers[user.username]) {
                newUsers.push(user);
            }
        });
        const data = {
            id: getState().cyderGroupsDetailReducer.group.id,
            groupname: getState().cyderGroupsDetailReducer.group.groupname,
            groupdescription: getState().cyderGroupsDetailReducer.group.groupdescription,
            usersList: newUsers,
        };
        console.log(data);

        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(data),
            method: 'POST',
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_GROUPS_DETAIL_SAVE_CHANGES_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_GROUPS_DETAIL_SAVE_CHANGES_FAILURE, error });
                return error;
            });
    };
}
export function cyderGroupsDetailDeleteUserAction() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_GROUPS_DETAIL_DELETE_REQUEST });

        // Perform the actual API call
        const url = withQuery(CYDER_COMMON_API_ROOT + '/groups', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'delete',
        });
        const body = {
            groupname: getState().cyderGroupsDetailReducer.group.groupname,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_GROUPS_DETAIL_DELETE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_GROUPS_DETAIL_DELETE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_GROUPS_DETAIL_DELETE_FAILURE, error });
                return error;
            });
    };
}
export function cyderGroupDetailsModalToggle(modalAction) {
    return {
        type: CYDER_GROUPS_DETAIL_MODAL_TOGGLE,
        modalAction,
    };
}
