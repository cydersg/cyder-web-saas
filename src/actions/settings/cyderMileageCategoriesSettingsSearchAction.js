import {
    CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_SUCCESS,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_REQUEST,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_FAILURE,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_SET_FORCE_REFRESH_FLAG,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MODAL_SET_MILEAGE_CATEGORY,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MODAL_TOGGLE,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MODAL_CHANGE_VALUE,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_REQUEST,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_SUCCESS,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_FAILURE,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_REQUEST,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_SUCCESS,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_FAILURE,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_REQUEST,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_SUCCESS,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_FAILURE,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_ADD_APPROVER,
    CYDER_MILEAGE_CATEGORIES_SETTINGS_REMOVE_APPROVER,
} from '../constants/actionTypes';
import withQuery from 'with-query';

import { DJARVIS_COMMON_API_ROOT } from '../../config';

/**
 * Security action creators
 *
 * Author: Fernando
 */

/**
 * Login function
 *
 * @param {*} fields
 * @param {*} callback
 */
export function cyderMileageCategoriesSettingsSearchAction(body) {
    // Redux Thunk will inject dispatch here:
    return (dispatch, getState) => {
        // Reducers may handle this to set a flag like isFetching
        dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_REQUEST });
        dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_SET_FORCE_REFRESH_FLAG, val: false });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/mileagecategory', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'partialsearch',
        });
        // const bodyWithAppId = Object.assign({appId: getState().cyderProfileReducer.profile.appId}, body);
        const options = {
            body: JSON.stringify(body),
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
        };
        return fetch(url, options)
            .then(response => response.json())
            .then(
                response => {
                    const id = 1;
                    // Reducers may handle this to show the data and reset isFetching
                    dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_SUCCESS, id, response });
                    // if we return stuff here, we can use it as a promise
                    return response;
                },
                error => {
                    // TODO
                    // Reducers may handle this to reset isFetching
                    // dispatch({ type: 'GET_USER_FAILURE', id, error })
                    dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_SEARCH_FAILURE, error });
                    // Rethrow so returned Promise is rejected
                    throw error;
                },
            )
            .catch(error => {
                //TODO
                console.log('in catch block');
                console.log(error);
                throw error;
            });
    };
}
export function cyderMileageCategoriesSettingsEditModalSetMileageCategory(
    id,
    code,
    name,
    approvalvotescount,
    rate,
    currency,
    rateunit,
    approvers,
) {
    return {
        type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MODAL_SET_MILEAGE_CATEGORY,
        id,
        code,
        name,
        approvalvotescount,
        rate,
        currency,
        rateunit,
        approvers,
    };
}
export function cyderMileageCategoriesSettingsModalToggleAction(modalAction) {
    return {
        type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MODAL_TOGGLE,
        modalAction,
    };
}
export function cyderMileageCategoriesSettingsEditModalChangeValue(key, value) {
    return {
        type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MODAL_CHANGE_VALUE,
        key,
        value,
    };
}
export function cyderMileageCategoriesSettingsAddApproverAction(username) {
    return {
        type: CYDER_MILEAGE_CATEGORIES_SETTINGS_ADD_APPROVER,
        username,
    };
}
export function cyderMileageCategoriesSettingsRemoveApproverAction(username) {
    return {
        type: CYDER_MILEAGE_CATEGORIES_SETTINGS_REMOVE_APPROVER,
        username,
    };
}
export function cyderMileageCategoriesSettingsAddMileageCategoryAction() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/mileagecategory', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'add',
        });
        const body = {
            ...getState().cyderMileageCategoriesSettingsSearchReducer.modalMileageCategory,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_ADD_FAILURE, error });
                return error;
            });
    };
}
export function cyderMileageCategoriesSettingsUpdateMileageCategoryAction() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/mileagecategory', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'update',
        });
        const body = {
            ...getState().cyderMileageCategoriesSettingsSearchReducer.modalMileageCategory,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_UPDATE_FAILURE, error });
                return error;
            });
    };
}
export function cyderMileageCategoriesSettingsDeleteMileageCategoryAction() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/mileagecategory', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'delete',
        });
        const code = getState().cyderMileageCategoriesSettingsSearchReducer.modalMileageCategory.code;
        console.log(code);
        const body = {
            code,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_MILEAGE_CATEGORIES_SETTINGS_MILEAGE_CATEGORY_DELETE_FAILURE, error });
                return error;
            });
    };
}
