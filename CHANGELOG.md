# Changelog

## [1.3.24] - 2021-01-16
### Updated by Fernando Karnagi
-   Provide langtitude and long, when producing reports    

### Fixed by Fernando Karnagi
-   MyCorp Timesheet approver page show invalid owner

## [1.3.22] - 2021-01-16
### Updated by Fernando Karnagi
-   MyCorp Timesheet report page to show half size 'Submit New Report' button

## [1.3.21] - 2021-01-15
### Fixed by Fernando Karnagi
-   MyCorp Timesheet infinite loop

## [1.3.20] - 2021-01-14
### Updated
-   MyCorp Timesheet update

## [1.3.19] - 2021-01-13
### Updated
-   MyCorp Timesheet update

## [1.3.18] - 2021-01-12
### Updated
-   MyCorp Checkin, moved to center

## [1.3.17] - 2021-01-12
### Updated
-   Remove MyCorp Checkin on Select Favourite page

## [1.3.16] - 2021-01-12
### Updated
-   Disabled scan QR button
-   Updated favourites

## [1.3.15] - 2021-01-11
### Updated
-   Role propagation from MyCorp
-   Timesheet: Home button change color

## [1.3.14] - 2021-01-11
### Updated
-   Timesheet: ProjectX label
-   Timesheet: Checkin bottom nav icon

## [1.3.13] - 2021-01-11
### Updated
-   Timesheet: nav bar label
-   Timesheet: add N/A as default project
-   Timesheet: move 'Manage projects' links

## [1.3.12] - 2021-01-10
### Updated
-   Timesheet layout

## [1.3.11] - 2021-01-09
### Fixed
-   Listing to show all records in timesheet

### Added
-   Freeze report

## [1.3.10] - 2021-01-08
### Added
-   Project settings

## [1.3.9] - 2021-01-08
### Updated
-   Timesheet UI fix

## [1.3.8] - 2021-01-08
### Updated
-   Approval top header

## [1.3.6] - 2021-01-07
### Updated
-   Allow delete task by approval

## [1.3.5] - 2021-01-07
### Fixed
-   Approval

## [1.3.4] - 2021-01-06
### Added
-   Report approval
-   Delete pending report

## [1.3.3] - 2021-01-06
### Added
-   Reporting

## [1.3.2] - 2021-01-05
### Updated
-   Instatime layout
-   Disable Scan QR for now

## [1.3.1] - 2021-01-04
### Updated
-   Instatime update
-   Location setting, add location address, and shows QR code

## [1.2.9] - 2021-01-04
### Updated
-   Instatime update

## [1.2.7] - 2021-01-03
### Updated
-   Instatime update

## [1.1.0] - 2020-12-07
### Added
-   Integration with MyCorp

## [1.0.4] - 2018-09-03
### Changed
-   New Expense Receipt Page: Now sets the title before getting receipt information, which is more responsive. However, this means that we can no longer reliably determine whether a receipt is editable or not.
-   New Expense Receipt/Mileage Page: Always gets receipt info from API instead of client, which prevents lots of issues with history state.

### Added
-   Enabled new workflow
-   Implemented Recurring charges UI (actual receipt generation job not implemented yet)
-   Added TFA option. Can be enabled in settings/system. Disabled by default. Uses user's mobile number in their profile.
-   Enabled onClick handler on rows to straightaway view item details on several ReactTables: MyReceiptsPage, MyReportsPage, ReportsDetailPage, MyTasksPage
-   Added Expense Type Badge in Report Details View

### Fixed
-   Lots of cleanup under the hood. Removed all warnings - we are able to compine without CI=false now.
-   Now accepting "+" character in mobilenumber field
-   Add missing date in MyTasksPage

## [1.0.3] - 2018-08-16
### Changed
-   Updated Footer to use package.json version
-   Updated deployment script to sync package.json, CHANGELOG.md and tag version

## [1.0.2] - 2018-08-16
### Added
-   Added react-viewer for images
### Removed

-   Removed LiveAgent chatbox

### Fixed

-   Fixed loading indicator for New Receipt Page when loading images
-   Fixed Task Detail view breaking occasionally if the report only contained 1 receipt
-   Cleaned up lots of unused items

## [1.0.1] - 2018-08-15

### Added

-   Footer to show version

### Changed

-   Changed custom.css to scss
-   Updated columns of My Receipt Page

### Fixed

-   Cleaned up login page console message and unused items
-   Removed some references to userid during some API calls (userid is taken care off in the backend)

## [1.0] - 2018-08-14

### First release
