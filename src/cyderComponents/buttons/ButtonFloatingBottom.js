import React from 'react';
import { Button } from 'reactstrap';
import { layoutLib } from 'js/constlib';
const isMobile = window.innerWidth <= layoutLib.mobileWidth;

const ButtonFloatingBottom = ({ onClick, label }) => {
    return (
        <div className="mycorp-add-bottom">
            <Button className="mycorp-add-button-circle" onClick={onClick} color="success">
                {label}
            </Button>
        </div>
    );
};

export default ButtonFloatingBottom;
