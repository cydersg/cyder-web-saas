import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import StoredLayout from '../../layouts/StoredLayout';
import moment from 'moment';
import { Link } from 'react-router-dom';
import Table from 'react-table';
import { Row, Badge, Container, Button } from 'reactstrap';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import YearMonthSearchPanel from 'cyderComponents/panel/YearMonthSearchPanel';
import { TablePagination } from 'cyderComponents/pagination/';
import { setJumbotronTitle } from 'actions/pagedata';
import * as Actions from 'actions/timesheet/monthlyTimesheetAction';
import ActionExecutor from 'js/ActionExecutor';
import cyderlib from 'js/cyderlib';
import { convertDateFormat } from 'js/generalUtils';
import { expenseDeletableStatuses, urlRecurring, urlReceipt, layoutLib } from 'js/constlib';
import dateutils from 'js/dateutils';

const { getYearlyReports, deleteTimesheetReport } = Actions;

class YearlyTimesheetPage extends Component {
    state = {
        loading: false,
        reports: [],
        isModalOpen: false,
        year: '',
        modal: {
            header: 'Yearly Timesheet',
            body: '',
            action: null,
            onlyOneButton: false,
        },
    };

    componentDidMount() {
        this.props.setJumbotronTitle('Timesheet Report');
        this.getYearlyReports();
    }

    setLoading = loading => {
        this.setState({ loading });
    };

    setYear = year => {
        this.setState({ year });
    };

    getYearlyReports = year => {
        const body = {
            reportYear: year && year.label ? year.value : moment().format('YYYY'),
        };
        this.setYear(body.reportYear);
        const callback = async () => {
            const { data } = await this.props.getYearlyReports(body);
            if (!data) {
                this.showModal('Error', 'Failed to retrive yearly timesheet reports.', null, true);
                return;
            }
            this.setState({ reports: data });
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    deleteTimesheetReport = id => {
        const { year } = this.state;
        const callback = async () => {
            const { ok } = await this.props.deleteTimesheetReport(id);
            if (ok) {
                this.showModal('Confirmation', 'Successfully remove yearly timesheet report.', null, true);
                this.getYearlyReports(year);
                return;
            }
            this.showModal('Error', 'Failed to remove yearly timesheet report.', null, true);
        };
        this.showModal(
            'Confirmation',
            'Are you sure you want to delete this report?',
            () => ActionExecutor.execute(this.setLoading, callback),
            true,
        );
    };

    setIsModalOpen = (isModalOpen, id) => {
        let stateCopy = Object.assign({}, this.state);
        const key = id === 'submitModal' ? 'isSubmitModalOpen' : 'isModalOpen';
        stateCopy[key] = isModalOpen;
        this.setState(stateCopy);
    };

    showModal = (header, body, action, onlyOneButton) => {
        this.setIsModalOpen(true);
        const { modal } = this.state;
        const modalConfig = {
            ...this.state.modal,
            header: header || modal.header,
            body: body || modal.body,
            action: action || null,
            onlyOneButton: onlyOneButton || modal.onlyOneButton,
        };
        this.setState({ modal: modalConfig });
    };

    render() {
        const { year, reports, projects, loading, modal, isModalOpen } = this.state;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        return (
            <Container className="wideContainer">
                <DialogModal
                    key="confirmModal"
                    modal={isModalOpen}
                    toggle={isOpen => this.setIsModalOpen(isOpen, 'confirmModal')}
                    customHeader={modal.header}
                    customModalBody={modal.body}
                    positiveButtonAction={modal.action}
                    onlyOneButton={modal.onlyOneButton}
                />
                <Row>
                    <YearMonthSearchPanel hasYear year={year} onSearch={this.getYearlyReports} />
                </Row>
                <RowColWrapper colProps={{ className: 'pt-4 ph0' }}>
                    {loading ? (
                        <LoadingSpinner center />
                    ) : (
                        <Table
                            className="-highlight mb-2"
                            data={reports}
                            showPagination={!isMobile}
                            columns={getColumns(this.props, this.deleteTimesheetReport)}
                            pageSize={reports.length}
                            PaginationComponent={TablePagination}
                        />
                    )}
                </RowColWrapper>
            </Container>
        );
    }
}

const getColumns = (props, deleteReport) => {
    const { history } = props;
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return [
        {
            Header: '',
            accessor: 'title',
            width: 30,
            show: !isMobile,
            Cell: props => {
                return <i className="align-middle material-icons">watch_later</i>;
            },
        },
        {
            Header: 'Title',
            show: !isMobile,
            accessor: 'title',
            Cell: props => {
                return (
                    <div>
                        <b>{props.value}</b>
                        <br />
                        <span className="font14">{convertDateFormat(props.original.createddt, 'YYYY-MM-DD', 'DD-MMM-YYYY')}</span>
                    </div>
                );
            },
        },
        {
            Header: 'Month',
            width: 100,
            show: !isMobile,
            Cell: props => {
                return dateutils.monthsArray[props.value - 1];
            },
            accessor: 'report_month',
        },
        // {
        //     Header: 'Year',
        //     show: !isMobile,
        //     accessor: 'report_year',
        // },
        {
            Header: 'Owner',
            show: !isMobile,
            width: 200,
            accessor: 'owner',
        },
        // {
        //     Header: 'Submitted Date',
        //     accessor: 'createddt',
        //     width: 100,
        //     show: !isMobile,
        //     Cell: props => {
        //         return convertDateFormat(props.value, 'YYYY-MM-DD', 'DD-MMM-YYYY');
        //     },
        // },
        {
            Header: 'Status',
            show: !isMobile,
            width: 100,
            accessor: 'status',
            Cell: props => {
                return <Badge color={cyderlib.statusBadgeColor(props.value)}>{props.value}</Badge>;
            },
        },
        {
            Header: '',
            show: !isMobile,
            accessor: 'id',
            width: 100,
            Cell: props => {
                const { status } = props.original;
                return (
                    <Fragment>
                        <Button
                            size="sm"
                            color="link"
                            className="ml-1"
                            onClick={e => history.push(`../timesheet/yearlytimesheet/${props.value}`)}
                        >
                            <i className="align-middle material-icons">remove_red_eye</i>
                        </Button>
                        {status === 'Pending' ? (
                            <Button color="link" className="ml-1" size="sm" onClick={e => deleteReport(props.value)}>
                                <i className="text-danger align-middle material-icons">delete</i>
                            </Button>
                        ) : null}
                    </Fragment>
                );
            },
        },
        {
            Header: '',
            show: isMobile,
            accessor: 'id',
            Cell: props => {
                const { status } = props.original;
                return (
                    <Link
                        style={{
                            color: 'unset',
                        }}
                        to={`../timesheet/yearlytimesheet/${props.value}`}
                    >
                        <div className="boxShadow">
                            <span>
                                <strong>{props.original.title}</strong>
                            </span>
                            <br />
                            <span>{moment(props.original.createddt, 'YYYY-MM-DD HH:mm:ss').format('DD-MMM-YYYY')}</span>
                            <br />
                            <span>{props.original.originator_fullname}</span>
                            <br />
                            <Badge color={cyderlib.statusBadgeColor(props.original.status)}>{props.original.status}</Badge>
                        </div>
                    </Link>
                );
            },
        },
    ];
};

const reducer = state => {
    return {};
};

const actionList = {
    setJumbotronTitle,
    getYearlyReports,
    deleteTimesheetReport,
};

export default connect(reducer, actionList)(StoredLayout(YearlyTimesheetPage));
