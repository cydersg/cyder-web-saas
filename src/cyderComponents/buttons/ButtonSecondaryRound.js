import React from 'react';
import { Button, Col } from 'reactstrap';
import { layoutLib } from 'js/constlib';
const isMobile = window.innerWidth <= layoutLib.mobileWidth;

const ButtonSecondaryRound = ({ onClick, faIcon, disabled, style }) => {
    return (
        <Col className="mycorp-col-big-icon">
            <Button disabled={disabled} style={style} className={' mycorp-add-button-circle'} onClick={onClick} color="secondary">
                {faIcon && <i class={`fa fa-${faIcon} fa-lg mycorp-big-icon`}></i>}
            </Button>
        </Col>
    );
};

export default ButtonSecondaryRound;
