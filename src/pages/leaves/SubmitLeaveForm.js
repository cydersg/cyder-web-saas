import React, { Component } from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';
import moment from 'moment';

import { Alert, Col, FormGroup, Row } from 'reactstrap';
import { CyderFileUploader, CyderInput, CyderRadioInput, CyderSelect } from 'cyderComponents/input/index';
import CyderDatePicker from 'cyderComponents/forms/CyderDatePicker';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

import {
    generateLeaveBalance,
    getCurrentLeaveBalance,
    getStaffLeaveBalance,
    updateStaffLeaveBalance,
} from 'actions/timesheet/staffLeaveBalanceAction';
import { setJumbotronTitle } from 'actions/pagedata';
import { submitLeaveAppl } from 'actions/timesheet/userLeaveBalanceAction';
import { cyderUsersSearchAction } from 'actions/users/cyderUsersSearchAction';
import { expenseCreateReportGetApprovers } from 'actions/expenses/cyderExpenseCreateReportAction';
import { getAllHolidays } from 'actions/settings/cyderLeaveHolidaysAction';
import { layoutLib } from 'js/constlib';
import { calNumOfDays, isPlural } from 'js/generalUtils';
import ActionExecutor from 'js/ActionExecutor';

const timeSessionsData = [
    {
        value: 'AM',
        label: 'AM',
    },
    {
        value: 'PM',
        label: 'PM',
    },
];

const submitLeaveBalanceDefaultData = {
    leaveTypeCode: '',
    fromDate: moment(),
    fromSession: '',
    toDate: '',
    toSession: '',
    numOfDays: '',
    remarks: '',
    approvers: [],
};

class SubmitLeaveForm extends Component {
    state = {
        submitLeaveBalanceDataStore: submitLeaveBalanceDefaultData,
        loading: false,
        approversList: [],
        leaveBalance: [],
        deductedHolidays: '',
        deductedWeekends: '',
        isModalOpen: false,
        errorMessage: null,
        imageFile: {
            b64img: '',
            contentType: '',
            imageFile: {},
        },
        canLeaveApplSubmittedData: {
            isValid: '',
            fieldName: '',
        },
        updateLeaveBalanceDataStore: {
            id: null,
            code: '',
            balance: 0,
        },
    };

    setLeaveBalance = leaveBalance => {
        this.setState({ leaveBalance });
    };

    setApproversList = approversList => {
        this.setState({ approversList });
    };

    setLoading = loading => {
        this.setState({ loading });
    };

    setSubmitLeaveBalanceData = submitLeaveBalanceDataStore => {
        this.setState({ submitLeaveBalanceDataStore });
    };

    calNumOfDays = async (from, to) => {
        if (!from || !to) return '';
        const fromYear = from.format('YYYY');
        const toYear = to.format('YYYY');

        const fromHolidays = await this.getAllHolidays(fromYear);
        const toHolidays = fromYear !== toYear ? await this.getAllHolidays(toYear) : [];
        const [numOfDays, totalNumOfDays, deductedNumOfDays, holidays] = calNumOfDays(from, to, [...fromHolidays, ...toHolidays], true);

        this.setState({
            deductedHolidays: deductedNumOfDays.holidays,
            deductedWeekends: deductedNumOfDays.weekendDays,
        });

        return numOfDays;
    };

    componentDidMount() {
        this.getApproversList();
        this.getUserLeaveBalance();
        this.props.submitFn(this.submitUserLeaveAppl);
    }

    getUserLeaveBalance() {
        const callback = async () => {
            const { data } = await this.props.getUserLeaveBalance();
            if (!data) return;
            const newData = data.map(x => {
                if (x.balance === 0) x.disabled = true;
                return x;
            });
            this.setLeaveBalance(newData);
        };
        ActionExecutor.execute(this.setLoading, callback);
    }

    onSubmitTextChanged = e => {
        const { value, id } = e.target;
        let inputDataClone = Object.assign({}, this.state.submitLeaveBalanceDataStore);
        inputDataClone[id] = value;
        this.setSubmitLeaveBalanceData(inputDataClone);
    };

    onDateChanged = async (value, id) => {
        let inputDataClone = Object.assign({}, this.state.submitLeaveBalanceDataStore);
        inputDataClone[id] = value;

        // Auto generate number of days
        const { fromDate, toDate } = inputDataClone;
        inputDataClone.numOfDays = await this.calNumOfDays(fromDate, toDate);

        // Set state
        this.setSubmitLeaveBalanceData(inputDataClone);
    };

    onSelectChange = (value, id) => {
        let inputDataClone = Object.assign({}, this.state.submitLeaveBalanceDataStore);
        inputDataClone[id] = value;
        this.setSubmitLeaveBalanceData(inputDataClone);
    };

    onRadioGroupChange = (value, id) => {
        const inputDataClone = Object.assign({}, this.state.submitLeaveBalanceDataStore);
        inputDataClone[id] = value;
        this.setSubmitLeaveBalanceData(inputDataClone);
    };

    getApproversList = () => {
        const callback = async () => {
            const data = await this.props.getApproversList();
            this.setApproversList(data);
        };
        ActionExecutor.execute(this.setLoading, callback);
    };

    getAllHolidays = year => {
        const callback = async () => {
            const { data } = await this.props.getAllHolidays(year);
            return data;
        };
        return ActionExecutor.execute(() => {}, callback);
    };

    onUpload = image => {
        this.setState({
            imageFile: image,
        });
    };

    submitUserLeaveAppl = async () => {
        const body = this.processSubmitData();
        const { message, error } = validate(body);

        // FAIL VALIDATION HANDLING
        if (error) {
            this.setState({
                errorMessage: message,
            });
            return false;
        }

        // SUBMIT LEAVE APPL
        this.setLoading(true);
        const { errdescription, status, data } = await this.props.submitLeaveAppl(body);
        const hasError = status !== 200 ? errdescription : data.errdescription || null;

        this.setState({
            errorMessage: hasError,
            loading: false,
        });
        return hasError ? false : true;
    };

    // DETERMINE APPROVERREQUIRED
    isApproverRequired = () => {
        const { leaveTypeCode } = this.state.submitLeaveBalanceDataStore;
        const approverRequired = leaveTypeCode && leaveTypeCode.requireapproval === 'Y';
        return approverRequired;
    };

    // CONSTRUCT DATA FOR SUBMISSION
    processSubmitData = () => {
        const { submitLeaveBalanceDataStore, imageFile } = this.state;
        const { leaveTypeCode, approvers, fromDate, toDate } = submitLeaveBalanceDataStore;
        const { b64img, contentType, name } = imageFile;
        const mapDate = date => moment(date).format('YYYY-MM-DD');

        let data = {
            ...submitLeaveBalanceDataStore,
            leaveTypeCode: leaveTypeCode.code,
            fromDate: mapDate(fromDate),
            toDate: mapDate(toDate),
            attachment: b64img.split(',')[1],
            contentType,
            filename: name,
        };
        if (this.isApproverRequired()) data.approvers = approvers.email;

        return data;
    };

    render() {
        const { imageFile, errorMessage, deductedHolidays, deductedWeekends, loading, submitLeaveBalanceDataStore } = this.state;
        const { leaveTypeCode } = submitLeaveBalanceDataStore;

        const approverRequired = leaveTypeCode && leaveTypeCode.requireapproval === 'Y';

        if (loading) return <LoadingSpinner />;
        return (
            <FormGroup>
                {errorMessage ? (
                    <Alert color="danger" className="text-bold">
                        {errorMessage}
                    </Alert>
                ) : null}
                <CyderSelect
                    mandatory
                    label="Leave Type"
                    inputProps={{
                        name: 'leaveTypeCode',
                        options: this.state.leaveBalance,
                        getOptionValue: option => option.code,
                        getOptionLabel: option => option.code,
                        isOptionDisabled: option => option.disabled,
                        value: this.state.submitLeaveBalanceDataStore.leaveTypeCode,
                        onChange: value => this.onSelectChange(value, 'leaveTypeCode'),
                    }}
                />
                <Row>
                    <Col xs="12" sm="6" className="pb-2">
                        <CyderInput mandatory label="From" formGroupClassName="m-0">
                            <CyderDatePicker
                                futureDateOnly
                                timeFormat={false}
                                onChange={value => this.onDateChanged(value, 'fromDate')}
                                value={this.state.submitLeaveBalanceDataStore.fromDate}
                                placeholder="DD/MM/YYYY"
                                dateFormat="DD/MM/YYYY"
                            />
                        </CyderInput>
                        <CyderRadioInput id="fromSession" data={timeSessionsData} onChange={this.onRadioGroupChange} />
                    </Col>
                    <Col xs="12" sm="6" className="pb-2">
                        <CyderInput mandatory label="To" formGroupClassName="m-0">
                            <CyderDatePicker
                                futureDateOnly
                                timeFormat={false}
                                onChange={value => this.onDateChanged(value, 'toDate')}
                                value={this.state.submitLeaveBalanceDataStore.toDate}
                                placeholder="DD/MM/YYYY"
                                dateFormat="DD/MM/YYYY"
                            />
                        </CyderInput>
                        <CyderRadioInput id="toSession" data={timeSessionsData} onChange={this.onRadioGroupChange} />
                    </Col>
                </Row>
                <CyderInput
                    mandatory
                    label="Number of Day(s)"
                    mutetext={
                        this.state.deductedHolidays || this.state.deductedWeekends
                            ? `${deductedHolidays} ${isPlural('holiday', deductedHolidays)} and ${deductedWeekends} ${isPlural(
                                  'day',
                                  deductedWeekends,
                              )} of weekend ${isPlural('is', deductedWeekends)} excluded`
                            : null
                    }
                    inputProps={{
                        type: 'text',
                        id: 'numOfDays',
                        name: 'numOfDays',
                        // disabled: true,
                        onChange: this.onSubmitTextChanged,
                        value: this.state.submitLeaveBalanceDataStore.numOfDays,
                    }}
                />
                {approverRequired && (
                    <CyderSelect
                        mandatory
                        label="Approvers"
                        inputProps={{
                            // isMulti: true,
                            name: 'approvers',
                            options: this.state.approversList,
                            value: this.state.submitLeaveBalanceDataStore.approvers,
                            getOptionValue: option => option.id,
                            getOptionLabel: option => option.firstname + ' ' + option.lastname,
                            onChange: value => this.onSelectChange(value, 'approvers'),
                        }}
                    />
                )}
                <CyderInput
                    label="Remarks"
                    inputProps={{
                        type: 'textarea',
                        id: 'remarks',
                        name: 'remarks',
                        onChange: this.onSubmitTextChanged,
                        value: this.state.submitLeaveBalanceDataStore.remarks,
                    }}
                />
                {leaveTypeCode.code === 'MEDICAL' && (
                    <CyderFileUploader label="Attachment" imageFile={imageFile} onUpload={this.onUpload}></CyderFileUploader>
                )}
            </FormGroup>
        );
    }
}

const validate = data => {
    var message = '';
    if (data.fromDate > data.toDate) message = 'Number of days cannot be negative';
    if (!data.leaveTypeCode) message = 'Leave type is required';
    if (!data.fromDate || !moment(data.fromDate).isValid()) message = 'From Date is required';
    if (!data.toDate || !moment(data.toDate).isValid()) message = 'To Date is required';
    if (!data.toSession || !data.fromSession) message = 'Session is required';
    if (!data.numOfDays) message = 'Numbers of Days is required';
    if ((data.leaveTypeCode === 'VACATION' || data.leaveTypeCode === 'PATERNITY') && !data.approvers) message = 'Approvers is required';
    return {
        error: message,
        message,
    };
};

const mapStateToProps = state => {
    return {
        profile: state.cyderProfileReducer.profile,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getAllHolidays: year => dispatch(getAllHolidays(year)),
        setJumbotronTitle: title => dispatch(setJumbotronTitle(title)),
        getUserLeaveBalance: () => {
            return dispatch(getCurrentLeaveBalance());
        },
        getStaffLeaveBalance: owner => {
            return dispatch(getStaffLeaveBalance(owner));
        },
        getApproversList: () => {
            return dispatch(expenseCreateReportGetApprovers());
        },
        updateStaffLeaveBalance: (owner, balance, leaveTypeCode) => {
            return dispatch(updateStaffLeaveBalance(owner, balance, leaveTypeCode));
        },
        generateLeaveBalance: owner => {
            return dispatch(generateLeaveBalance(owner));
        },
        submitLeaveAppl: body => {
            return dispatch(submitLeaveAppl(body));
        },
        getUsers: () => {
            const body = {
                keyword: '',
                status: 'Active',
            };
            return dispatch(cyderUsersSearchAction(body));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(SubmitLeaveForm));
