import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import StoredLayout from 'layouts/StoredLayout';

import { Alert, Input, Button, Container, Row, Col, Form, FormGroup } from 'reactstrap';
import ReactTable from 'react-table';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

import { cyderGroupsSearchAction, cyderGroupsSearchPageResetStateAction } from 'actions/groups/cyderGroupsSearchAction';
import { setJumbotronTitle } from 'actions/pagedata';

import cyderlib from 'js/cyderlib';

class GroupsSearchPage extends React.Component {
    constructor(props) {
        super(props);
        this.validateThenSearch = this.validateThenSearch.bind(this);
    }
    componentDidMount() {
        // this.props.getAllUsers();
        this.props.resetState();
        this.props.setJumbotronTitle();
    }
    handleViewClick(user) {
        console.log(user);
    }
    validateThenSearch(e) {
        if (e) {
            e.preventDefault();
        }
        if (this.props.loading) return;
        // this.props.clearErrorMessage();

        const bothsearch = document.getElementById('bothsearch').value;
        const status = document.getElementById('status').value;

        const body = {
            bothsearch,
            status,
        };
        this.props.searchUsers(body);
    }
    render() {
        // const { t } = this.props
        const columns = [
            {
                Header: 'Group Name',
                accessor: 'groupname',
                filterMethod: cyderlib.filterIgnoreCase,
            },
            {
                Header: 'Group Description',
                accessor: 'groupdescription',
                filterMethod: cyderlib.filterIgnoreCase,
            },
            {
                Header: 'Status',
                accessor: 'status',
                filterMethod: cyderlib.filterIgnoreCase,
            },
            {
                Header: '',
                headerClassName: 'rt-no-sort-ind',
                accessor: 'id',
                sortable: false,
                filterable: false,
                width: 150,
                Cell: props => {
                    const url = '/administration/groups/details/' + props.value;
                    return (
                        <Link to={url} tabIndex="-1">
                            <i className="color-dark align-middle material-icons">edit</i>
                        </Link>
                    );
                },
            },
        ];
        return (
            <div key={0}>
                <Container>
                    <Row>
                        <Col xs={12}>
                            <Form onSubmit={this.validateThenSearch}>
                                <FormGroup>
                                    <Input placeholder="Search Name/Description" id="bothsearch" />
                                </FormGroup>
                                <FormGroup>
                                    <Input type="select" id="status">
                                        <option>Active</option>
                                        <option>Inactive</option>
                                    </Input>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                    <Row className="mb-2">
                        <Col>
                            <Button disabled={this.props.loading} onClick={() => this.validateThenSearch()} tabIndex="-1">
                                Search
                            </Button>
                            <Link to="./groups/add" className="btn btn-success" style={{ float: 'right' }} tabIndex="-1">
                                Add New Group
                            </Link>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            {this.props.loading ? (
                                <LoadingSpinner center />
                            ) : this.props.groups.length > 0 ? (
                                <ReactTable
                                    className="-highlight mb-2"
                                    data={this.props.groups}
                                    columns={columns}
                                    minRows={0}
                                    filterable={true}
                                    showPagination={false}
                                />
                            ) : this.props.errorMessage ? (
                                <Alert color="danger">{this.props.errorMessage}</Alert>
                            ) : null}
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.cyderGroupsSearchReducer.loading,
        savingSysparam: state.cyderGroupsSearchReducer.savingSysparam,
        groups: state.cyderGroupsSearchReducer.groups,
        errorMessage: state.cyderGroupsSearchReducer.errorMessage,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        searchUsers: body => {
            dispatch(cyderGroupsSearchAction(body)).then(res => {
                console.log(res);
                console.log('this is a promise');
            });
        },
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Groups')),
        resetState: () => dispatch(cyderGroupsSearchPageResetStateAction()),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(StoredLayout(GroupsSearchPage));
