import {
    CYDER_EXPENSE_NEW_RECEIPT_MODAL_TOGGLE,
    CYDER_EXPENSE_NEW_RECEIPT_SAVE_REQUEST,
    CYDER_EXPENSE_NEW_RECEIPT_SAVE_SUCCESS,
    CYDER_EXPENSE_NEW_RECEIPT_SAVE_FAILURE,
    CYDER_EXPENSE_NEW_RECEIPT_CHECK_DUPLICATION_REQUEST,
    CYDER_EXPENSE_NEW_RECEIPT_CHECK_DUPLICATION_SUCCESS,
    CYDER_EXPENSE_NEW_RECEIPT_CHECK_DUPLICATION_FAILURE,
    CYDER_EXPENSE_NEW_RECEIPT_CHANGE_FIELD,
    CYDER_EXPENSE_NEW_RECEIPT_SET_FIELDS,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_REQUEST,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_SUCCESS,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_FAILURE,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_HISTORY_REQUEST,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_HISTORY_SUCCESS,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_HISTORY_FAILURE,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_INFO_REQUEST,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_INFO_SUCCESS,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_INFO_FAILURE,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_REQUEST,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_SUCCESS,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_FAILURE,
    CYDER_EXPENSE_NEW_RECEIPT_DELETE_SAVED_RECEIPT_REQUEST,
    CYDER_EXPENSE_NEW_RECEIPT_DELETE_SAVED_RECEIPT_SUCCESS,
    CYDER_EXPENSE_NEW_RECEIPT_DELETE_SAVED_RECEIPT_FAILURE,
    CYDER_EXPENSE_NEW_RECEIPT_FORCE_LOADING_STATE,
    CYDER_EXPENSE_NEW_RECEIPT_FORCE_VALIDATE,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_GET_BLOCKCHAIN_INFO_REQUEST,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_GET_BLOCKCHAIN_INFO_SUCCESS,
    CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_GET_BLOCKCHAIN_INFO_FAILURE,
} from '../constants/actionTypes';
import withQuery from 'with-query';
import fetchlib from '../../js/fetchlib';
import cyderlib from '../../js/cyderlib';

import { CYDER_API_ROOT, CYDER_COMMON_API_ROOT, DJARVIS_EXPENSE_API_ROOT } from '../../config';

export function expenseNewReceiptClaimSaveReceipt(sdf, copyItem, recurring) {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_SAVE_REQUEST });

        const data = Object.assign({}, getState().cyderExpenseNewReceiptClaimReducer.receiptFields);
        const url = withQuery(DJARVIS_EXPENSE_API_ROOT + '/' + (recurring ? 'recurringcharges' : 'receipt'), {
            appid: getState().cyderProfileReducer.profile.appId,
            action: recurring ? (copyItem ? 'create' : data.id ? 'update' : 'create') : 'addreceipt',
        });
        // fix for select defaults
        const actualData = {
            ...sdf,
            id: data.id,
            amount: data.amount * 100,
            userid: getState().cyderProfileReducer.profile.username,
            expensedate: data.expensedate,
            receiptno: data.receiptno,
            receiptId: data.receiptId,
            remarks: data.remarks,
            reimbursable: data.reimbursable === undefined ? 'Y' : data.reimbursable === true ? 'Y' : 'N',
        };

        if (recurring) {
            actualData.frequency_type = data.frequency_type;
            if (data.frequency_type !== undefined && data.frequency_type !== 'daily') {
                actualData.frequency = data.frequency;
            }
        } else {
            delete actualData.frequency_type;
        }

        if (copyItem) {
            delete actualData.id;
        }
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(actualData),
            method: 'POST',
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_SAVE_SUCCESS, response, recurring });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_SAVE_FAILURE, error, recurring });
                return error;
            });
    };
}
export function expenseNewReceiptClaimCheckReceiptNoDuplicate(receiptNo) {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_CHECK_DUPLICATION_REQUEST });
        const url = withQuery(DJARVIS_EXPENSE_API_ROOT + '/receipt', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'checkreceiptduplication',
        });
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify({ receiptNo }),
            method: 'POST',
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_CHECK_DUPLICATION_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_CHECK_DUPLICATION_FAILURE, error });
                return error;
            });
    };
}
export function expenseNewReceiptClaimChangeField(key, value) {
    return {
        type: CYDER_EXPENSE_NEW_RECEIPT_CHANGE_FIELD,
        key,
        value,
    };
}
export function expenseNewReceiptClaimToggleModal(modalAction, recurring) {
    return {
        type: CYDER_EXPENSE_NEW_RECEIPT_MODAL_TOGGLE,
        modalAction,
        recurring,
    };
}
export function expenseNewReceiptClaimForceLoadingState(loading) {
    return {
        type: CYDER_EXPENSE_NEW_RECEIPT_FORCE_LOADING_STATE,
        loading,
    };
}
export function expenseNewReceiptClaimForceValidate() {
    return dispatch => {
        dispatch({
            type: CYDER_EXPENSE_NEW_RECEIPT_FORCE_VALIDATE,
        });
        return Promise.resolve();
    };
}
export function expenseNewReceiptClaimResetFields() {
    return dispatch => {
        dispatch({
            type: CYDER_EXPENSE_NEW_RECEIPT_SET_FIELDS,
            resetFields: true,
        });
        return Promise.resolve();
    };
}
export function expenseNewReceiptClaimGetImageInfo(receiptid) {
    return (dispatch, getState) => {
        const url = withQuery(DJARVIS_EXPENSE_API_ROOT + '/receipt', {
            action: 'getattachments',
            appid: getState().cyderProfileReducer.profile.appId,
            id: receiptid,
        });
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            method: 'POST',
        };
        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_INFO_REQUEST });
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_INFO_FAILURE, response });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_INFO_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_INFO_FAILURE, error });
                return error;
            });
    };
}
export function expenseNewReceiptClaimGetHistory(instanceid) {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_HISTORY_REQUEST });
        const url = withQuery(CYDER_COMMON_API_ROOT + '/workflow', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'gettaskhistory',
            resourceid: instanceid,
        });
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            method: 'POST',
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_HISTORY_FAILURE, response });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_HISTORY_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_HISTORY_FAILURE, error });
                return error;
            });
    };
}
export function expenseNewReceiptClaimDownloadImage(url) {
    return (dispatch, getState) => {
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            method: 'POST',
        };
        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_REQUEST });
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_FAILURE, response });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_IMAGE_FAILURE, error });
                return error;
            });
    };
}
export function expenseNewReceiptClaimSetFields(receiptid, recurring) {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_REQUEST });
        const { appId, authorizationToken } = getState().cyderProfileReducer.profile;
        const lambda = recurring ? 'recurringcharges' : 'my';
        const action = recurring ? 'findbyid' : 'findreceiptbyid';

        const url = withQuery(`${DJARVIS_EXPENSE_API_ROOT}/${lambda}`, {
            appid: appId,
            id: receiptid,
            action,
        });

        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: authorizationToken,
            },
            method: 'POST',
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        // DETERMINE RESET OR COPY
                        dispatch({
                            type: CYDER_EXPENSE_NEW_RECEIPT_SET_FIELDS,
                            receiptFields: cyderlib.normalizeReceiptFields(response),
                        });

                        // SUCCESS
                        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_FAILURE, error });
                return error;
            });
    };
}
export function expenseNewReceiptGetBlockchainInfo(receiptid) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_COMMON_API_ROOT}/blockchain/`, {
            action: 'verify',
            id: receiptid,
        });
        return fetchlib.fetch(
            dispatch,
            getState,
            'POST',
            url,
            {},
            CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_GET_BLOCKCHAIN_INFO_REQUEST,
            CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_GET_BLOCKCHAIN_INFO_SUCCESS,
            CYDER_EXPENSE_NEW_RECEIPT_GET_SAVED_RECEIPT_GET_BLOCKCHAIN_INFO_FAILURE,
            true,
        );
    };
}
export function expenseNewReceiptClaimDeleteSavedReceipt(recurring) {
    return (dispatch, getState) => {
        const lambda = recurring ? 'recurringcharges' : 'my';
        const action = recurring ? 'delete' : 'deletereceipt';
        const url = withQuery(`${DJARVIS_EXPENSE_API_ROOT}/${lambda}`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action,
        });
        const idKey = recurring ? 'id' : 'receiptId';
        const theId = getState().cyderExpenseNewReceiptClaimReducer.receiptFields[idKey];
        const bodyRest = recurring ? { id: theId } : { receiptid: theId };
        const body = {
            ...bodyRest,
            userid: getState().cyderProfileReducer.profile.username,
        };
        const options = {
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
        };

        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_DELETE_SAVED_RECEIPT_REQUEST });

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_DELETE_SAVED_RECEIPT_SUCCESS, response, recurring });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_NEW_RECEIPT_DELETE_SAVED_RECEIPT_FAILURE, error, recurring });
                return error;
            });
    };
}
