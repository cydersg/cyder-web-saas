/**
 * Travel Method Select
 * 
 * Author: Fernando
 */

import React, {Component} from 'react'
import Select from 'react-select'
import {connect} from 'react-redux'
// import history from '../../history'
import travelmethods from '../../json/travelmethods.json'
import {selectTravelMethod} from '../../actions/expenses/expenseCommonAction'

class TravelMethodSelect extends Component {
  render() {
    return ( 
      <Select
        autosize={false}
        placeholder="Select travel method"
        autofocus
        options={travelmethods}
        simpleValue
        clearable={this.props.expense.travelMethodValue.clearable}
        name="selected-project"
        disabled={this.props.expense.travelMethodValue.disabled}
        value={this.props.expense.travelMethodValue.value}
        onChange={this.props.updateValue}
        searchable={this.props.expense.travelMethodValue.searchable}
      />
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    expense: state.cyderExpenseReducer
  }
}
const mapDispatchToProps = dispatch => {
  return {
    updateValue: (e) => {
      dispatch(selectTravelMethod(e));
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TravelMethodSelect)