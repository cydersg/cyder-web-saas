import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Card, CardBody } from 'reactstrap';
import { layoutLib } from 'js/constlib';

export default function IconCardWidget(props) {
    const { xs, md, lg, sm, icon, iconClassName, title, label, color, url } = props;
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    const cardWidget = (
        <Card className={isMobile ? 'cardwidget h-100' : 'cardwidget'} style={{ backgroundColor: color }}>
            <CardBody className="cardwidget-body">
                <Row className="cardwidget-info">
                    <Col>
                        <h6 className="font-weight-bold d-none d-md-block">{label}</h6>
                        <span className="font-weight-bold fontH7 d-block d-md-none nowrap">{label}</span>
                        <h1 className="font-weight-bold d-none d-md-block">{title}</h1>
                        <span className="font-weight-bold font25 d-block d-md-none">{title}</span>
                    </Col>
                </Row>
                <Row className="cardwidget-icon">
                    <Col>
                        <i className={`material-icons material-icons-2x ${iconClassName}`}>{icon}</i>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );

    return (
        <Col xs={xs} md={md} lg={lg} sm={sm} className="cardwrapper mb10">
            {url ? <Link to={url}>{cardWidget}</Link> : cardWidget}
        </Col>
    );
}
