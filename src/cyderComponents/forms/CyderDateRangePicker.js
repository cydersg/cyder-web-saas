import React, { useState } from 'react';
import moment from 'moment';

import { Col } from 'reactstrap';
import { DateRangePicker } from 'react-dates';

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';

const formatDate = date => date && moment(date).format('YYYY-MM-DD');

export default function CyderDateRangePicker(props) {
    const [focusedInput, setFocusInput] = useState(null);
    const [issuedFrom, setIssuedFrom] = useState({
        id: 'Issued From',
        focused: false,
        date: null,
        placeholder: 'Issued From',
    });
    const [issuedTo, setIssuedTo] = useState({
        id: 'Issued To',
        focused: false,
        date: null,
        placeholder: 'Issue To',
    });

    const onDateChange = (startDate, endDate) => {
        let issuedFromState = Object.assign({}, issuedFrom);
        let issuedToState = Object.assign({}, issuedTo);
        // UPDATE
        switch (focusedInput) {
            case 'startDate':
                issuedFromState.date = startDate;
                setIssuedFrom(issuedFromState);
                break;
            case 'endDate':
                issuedToState.date = endDate;
                setIssuedTo(issuedToState);
                break;
            default:
                break;
        }

        const newStartDate = formatDate(startDate);
        const newEndDate = formatDate(endDate);
        if (props.onDateChange) props.onDateChange(newStartDate, newEndDate);
    };

    return (
        <Col className={props.className}>
            <DateRangePicker
                small
                showClearDates
                showDefaultInputIcon
                startDate={issuedFrom.date}
                startDateId={issuedFrom.id}
                endDate={issuedTo.date}
                endDateId={issuedTo.id}
                focusedInput={focusedInput}
                onDatesChange={({ startDate, endDate }) => onDateChange(startDate, endDate)}
                onFocusChange={focusedInput => setFocusInput(focusedInput)}
                isOutsideRange={day => {}}
            />
        </Col>
    );
}
