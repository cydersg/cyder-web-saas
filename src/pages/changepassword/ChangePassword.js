import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import history from 'history.js';
import sha512 from 'js-sha512';

import EmptyView2 from 'layouts/EmptyView2';
import CyderForm from 'pages/CyderForm';
import { Alert, Button, Row, Col, Card, CardText } from 'reactstrap';
import MaterialDesignInput from 'cyderComponents/forms/MaterialDesignInput';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

import {
    cyderChangePasswordPageSetMessageAction,
    cyderChangePasswordPageClearMessageAction,
    cyderChangePasswordAction,
    cyderChangePasswordNoUserAction,
    cyderGetResetPasswordInfoAction,
    cyderChangePasswordResetState,
} from 'actions/security/cyderSecurityAction';

class ChangePassword extends Component {
    state = {
        loading: false,
    };

    componentDidMount() {
        const { token } = this.props.match.params;
        this.props.changePasswordResetState();
        if (token) this.props.getResetPasswordInfo(token);
    }

    setLoading = loading => {
        this.setState({
            loading,
        });
    };

    validateThenSubmit = () => {
        const { fields } = this.props;

        this.props.clearErrorMessage();
        const password = document.getElementById(fields[0].id).value;
        const confirmpassword = document.getElementById(fields[1].id).value;

        if (!password || !confirmpassword) {
            this.props.setMessage('Please enter both fields');
            return false;
        }

        if (password !== confirmpassword) {
            this.props.setMessage('Passwords do not match');
            return false;
        }

        return password;
    };

    changePassword = async () => {
        this.setLoading(true);
        const { token } = this.props.match.params;
        const password = this.validateThenSubmit();
        if (password) await this.props.changePasswordSubmit(password, token);
        this.setLoading(false);
    };

    render() {
        const { loading } = this.state;
        const { message, success, user, profile, fields } = this.props;
        const style = message ? null : { display: 'none' };

        return (
            <CyderForm title="Change Password" description={profile.username || 'Please enter and confirm your password'}>
                {user ? (
                    <Row className="mb-2">
                        <Col sm="12">
                            <Card body>
                                <CardText className="text-center">{user.username}</CardText>
                            </Card>
                        </Col>
                    </Row>
                ) : null}
                <div className="form-group">
                    {loading ? (
                        <LoadingSpinner />
                    ) : (
                        <div>
                            <Alert className="mb-4 text-bold" style={style} color={success ? 'success' : 'danger'}>
                                <div>{message || null}</div>
                            </Alert>
                            {success ? null : (
                                <div>
                                    <MaterialDesignInput field={fields[0]} />
                                    <MaterialDesignInput field={fields[1]} />
                                </div>
                            )}
                        </div>
                    )}
                </div>
                <div className="d-flex align-items-center">
                    <Button
                        type="submit"
                        className="text-bold float-right btn btn-primary btn-rounded"
                        hidden={success}
                        disabled={loading}
                        onClick={this.changePassword}
                    >
                        Submit
                    </Button>
                    <Link className="text-bold mx-3" to="/">
                        Back to Log In
                    </Link>
                </div>
            </CyderForm>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return { ...state.cyderChangePasswordReducer, profile: state.cyderProfileReducer.profile };
};

const mapDispatchToProps = dispatch => {
    return {
        clearErrorMessage: () => {
            dispatch(cyderChangePasswordPageClearMessageAction());
        },
        setMessage: message => {
            dispatch(cyderChangePasswordPageSetMessageAction(message));
        },
        changePasswordSubmit: (password, forgotpasswordtoken) => {
            const hashedpw = sha512(password);

            var navigateHome = () => {
                var navigate = '/home';
                history.replace(navigate);
            };

            var navigateLogin = () => {
                var navigate = '/';
                history.replace(navigate);
            };

            let data = {
                password: hashedpw,
            };

            if (forgotpasswordtoken) {
                data.forgotpasswordtoken = forgotpasswordtoken;
                return dispatch(cyderChangePasswordNoUserAction(data, navigateHome, navigateLogin));
            } else {
                return dispatch(cyderChangePasswordAction(data, navigateHome, navigateLogin));
            }
        },
        getResetPasswordInfo: forgotpasswordtoken => {
            dispatch(cyderGetResetPasswordInfoAction(forgotpasswordtoken));
        },
        changePasswordResetState: () => {
            dispatch(cyderChangePasswordResetState());
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(EmptyView2(ChangePassword));
