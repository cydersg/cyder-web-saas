import React from 'react';
import LinkButton from '../../../cyderComponents/buttons/LinkButton';

const buttonStyle = {
    maxWidth: '180px',
};

const SimplyLinkButton = props => {
    const { iconName, disabled, text, to } = props;
    return (
        <LinkButton
            block
            color="success"
            altColor="light"
            className="mr-2 mb-2 text-center"
            addStyle={buttonStyle}
            to={to}
            text={text}
            disabled={disabled}
            materialIcon={iconName}
        />
    );
};

const SimplyButton = props => {
    const { iconName, disabled, text, onClick } = props;
    return (
        <LinkButton
            block
            notLink
            color="success"
            altColor="light"
            className="mr-2 mb-2 text-center"
            addStyle={buttonStyle}
            onClick={onClick}
            text={text}
            disabled={disabled}
            materialIcon={iconName}
        />
    );
};

export { SimplyLinkButton, SimplyButton };
