import React, { Fragment, Component, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import qs from 'query-string';
import Viewer from 'react-viewer';

import { Container, Row, Col, Form } from 'reactstrap';
import StoredLayout from 'layouts/StoredLayout';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import { InfoField } from 'cyderComponents/input/CyderPreview';

import {
    expenseNewReceiptClaimSetFields,
    expenseNewReceiptClaimGetImageInfo,
    expenseNewReceiptClaimDownloadImage,
    expenseNewReceiptClaimForceLoadingState,
} from 'actions/expenses/cyderExpenseNewReceiptClaimAction.js';
import {
    expenseSharedGetProjects,
    expenseSharedGetExpenseCategories,
    expenseSharedGetPaymentModes,
    expenseSharedLoadedData,
} from 'actions/expenses/cyderExpenseClaimSharedAction';
import { setJumbotronTitle } from 'actions/pagedata';

class TaskDetailReceiptComponent extends Component {
    state = {
        b64img: null,
        imagebtoa: '',
        expenseDate: null,
        dropdownOpen: false,
        selectedCurrency: 'SGD',
    };

    componentDidMount() {
        this.props.setJumbotronTitle();
        this.props.getRelevantStuff().then(res => {
            this.setState({
                b64img: res.Body,
                contentType: res.ContentType,
            });
        });
    }

    render() {
        const { loading, location } = this.props;
        const {
            amount,
            receiptno,
            updateddt,
            category,
            reimbursable,
            expensedate,
            project,
            paymentmode,
            remarks,
        } = this.props.receiptFields;

        const reportid = qs.parse(location.search).reportid;
        const realid = qs.parse(location.search).realid;

        return (
            <Container key={0}>
                <Row>
                    <Col xs={12}>
                        <Link to={`/workflow/task/${reportid}?realid=${realid}`}>
                            <i className="align-middle material-icons material-icons-3x">chevron_left</i>
                        </Link>
                    </Col>
                </Row>
                {loading ? (
                    <LoadingSpinner />
                ) : (
                    <Row className="mb-2">
                        <Col xs={12} sm={6}>
                            <Form>
                                <InfoField label="Expense Date" value={expensedate} />
                                <InfoField label="Project" value={project} />
                                <InfoField label="Payment Mode" value={paymentmode} />
                                <InfoField label="Purpose" value={remarks} />
                            </Form>
                        </Col>
                        <Col xs={12} sm={6}>
                            <Form>
                                <InfoField label="Amount" value={amount} />
                                <InfoField label="Receipt No." value={receiptno} />
                                <InfoField label="Expense Category" value={category} />
                                <InfoField label="Reimbursable" value={reimbursable} />
                                <InfoImageViewer {...this.state} {...this.props} />
                            </Form>
                        </Col>
                        {location ? <Col xs={12}>Last Saved: {moment(updateddt).format(this.props.momentDateTimeFormat)}</Col> : null}
                    </Row>
                )}
            </Container>
        );
    }
}

const InfoImageViewer = props => {
    const { visible, setVisible } = useState(false);
    const { b64img, contentType, imageFiles } = props;

    const imageStyle = useMemo(() => ({
        maxWidth: '100%',
        width: '100%',
        maxHeight: '450px',
        objectFit: 'contain',
    }));

    const imageUrl = b64img ? `data:${contentType};base64, ${b64img}` : imageFiles ? imageFiles[0].preview : null;

    return (
        <Fragment>
            <Viewer
                noNavbar
                changeable={false}
                scalable={false}
                zoomSpeed={0.25}
                zIndex={9999}
                visible={visible}
                onClose={() => setVisible(!visible)}
                images={[{ src: imageUrl, alt: '' }]}
            />
            <img alt="" style={imageStyle} onClick={() => setVisible(!visible)} src={imageUrl} />
        </Fragment>
    );
};

const mapStateToProps = state => {
    return {
        ...state.cyderExpenseNewReceiptClaimReducer,
        ...state.cyderExpenseSharedReducer,
        loading: state.cyderExpenseNewReceiptClaimReducer.loading,
        momentDateFormat: state.config.momentDateFormat,
        momentDateTimeFormat: state.config.momentDateTimeFormat,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setJumbotronTitle: () => {
            dispatch(setJumbotronTitle('Item Details'));
        },
        getRelevantStuff: () => {
            dispatch(expenseNewReceiptClaimForceLoadingState(true));
            const items = [
                dispatch(expenseSharedGetProjects()),
                dispatch(expenseSharedGetExpenseCategories()),
                dispatch(expenseSharedGetPaymentModes()),
            ];
            return Promise.all(items)
                .then(() => {
                    // will use receipt info from myReceipts list if exists on client
                    const items2 = [
                        dispatch(expenseNewReceiptClaimGetImageInfo(ownProps.match.params.itemid)),
                        dispatch(expenseNewReceiptClaimSetFields(ownProps.match.params.itemid)),
                    ];
                    return Promise.all(items2).then(res => {
                        if (res[0].length > 0) {
                            return dispatch(expenseNewReceiptClaimDownloadImage(res[0][0].url)).then(res => {
                                return res;
                            });
                        } else {
                            return Promise.resolve({ message: 'no attachment' });
                        }
                    });
                })
                .then(res => {
                    dispatch(expenseNewReceiptClaimForceLoadingState(false));
                    dispatch(expenseSharedLoadedData());
                    return res;
                });
        },
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(StoredLayout(TaskDetailReceiptComponent));
