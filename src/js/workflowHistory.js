export default {
    parseHistoryStatusColor(status) {
        switch (status) {
            case "Pending":
                return "info";
            case "Approved":
                return "success";
            default:
                return "default";
        }
    },
};
