/**
 * Common Date Picker
 */
import React from 'react';
import moment from 'moment';
import Datetime from 'react-datetime';
import '../../css/forms/react-datetime.css';
import '../../css/forms/react-datetime-new.css';

const CyderDatePicker = ({ id, onChange, value, valid, placeholder, dateFormat, timeFormat, viewMode, futureDateOnly, width }) => {
    const isValid = () => {
        if (value && value.length !== 0) {
            const isValid = value && moment(value, dateFormat || timeFormat).isValid();
            return valid || isValid ? 'is-valid' : 'is-invalid';
        }
        return '';
    };

    const allowFutureDateOnly = current => {
        var yesterday = Datetime.moment().subtract(1, 'day');
        return current.isAfter(yesterday);
    };

    const className = 'form-control ' + isValid();
    const style = width
        ? {
              width,
          }
        : {};

    return (
        <Datetime
            value={value}
            inputProps={{ id, className, placeholder, style }}
            dateFormat={dateFormat}
            timeFormat={timeFormat}
            closeOnSelect={true}
            input={true}
            viewMode={viewMode || 'days'}
            onChange={onChange}
            isValidDate={futureDateOnly ? allowFutureDateOnly : null}
        />
    );
};

export default CyderDatePicker;
