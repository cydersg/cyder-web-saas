import {
    CYDER_GROUPS_SEARCH_REQUEST,
    CYDER_GROUPS_SEARCH_SUCCESS,
    CYDER_GROUPS_SEARCH_FAILURE,
    CYDER_GROUPS_SEARCH_PAGE_RESET_STATE,
} from '../constants/actionTypes';
import withQuery from 'with-query';

import { CYDER_COMMON_API_ROOT } from '../../config';

/**
 * Security action creators
 *
 * Author: Fernando
 */

/**
 * Login function
 *
 * @param {*} fields
 * @param {*} callback
 */
export function cyderGroupsSearchPageResetStateAction() {
    return {
        type: CYDER_GROUPS_SEARCH_PAGE_RESET_STATE,
    };
}
export function cyderGroupsSearchAction(body) {
    // Redux Thunk will inject dispatch here:
    return (dispatch, getState) => {
        // Reducers may handle this to set a flag like isFetching
        dispatch({ type: CYDER_GROUPS_SEARCH_REQUEST });

        // Perform the actual API call
        const url = withQuery(CYDER_COMMON_API_ROOT + '/groups', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'partialsearch',
        });
        const options = {
            body: JSON.stringify(body),
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    response.json().then(error => {
                        dispatch({ type: CYDER_GROUPS_SEARCH_FAILURE, error });
                    });
                } else {
                    response.json().then(response => {
                        dispatch({ type: CYDER_GROUPS_SEARCH_SUCCESS, response });
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_GROUPS_SEARCH_FAILURE, error });
            });
    };
}
