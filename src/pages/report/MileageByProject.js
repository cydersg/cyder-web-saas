/**
 * Mileage by project
 * 
 * Author: Fernando Karnagi
 */
import StoredLayout from '../../layouts/StoredLayout'
import React from 'react'
import { connect } from 'react-redux'

// import history from '../../history' 
import ExpenseSummaryReportTable from '../../cyderComponents/table/ExpenseSummaryReportTable' 
import ExpenseSummaryReportSearchPanel from '../../cyderComponents/panel/ExpenseSummaryReportSearchPanel'

import { setJumbotronTitle } from '../../actions/pagedata'
import { retrieveSummaryReportAction, updateMileageByProject, expenseLoadingShow, expenseLoadingHide, expenseResetForm }
    from '../../actions/report/expenseReportAction' 

class MileageByProject extends React.Component {

    componentDidMount() { 
        this.props.load();        
    }

    render() { 
        return (
            <div>
                <ExpenseSummaryReportSearchPanel search={this.props.search}/>
                <ExpenseSummaryReportTable loading={this.props.report.loading} reports={this.props.report.mileageByProject}/>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        report: state.cyderReportExpenseReducer
    }
}
const mapDispatchToProps = (dispatch) => {
    return { 
        load: () => {
            dispatch(expenseResetForm());
            dispatch(expenseLoadingShow());
            var callback = function(error, reportData) {
                dispatch(updateMileageByProject(reportData));
                dispatch(expenseLoadingHide());
            } 
            dispatch(retrieveSummaryReportAction('mileage-by-project', callback));
        },
        search: () => {
            dispatch(expenseLoadingShow());
            var callback = function(error, reportData) {
                dispatch(updateMileageByProject(reportData));
                dispatch(expenseLoadingHide());
            } 
            dispatch(retrieveSummaryReportAction('mileage-by-project', callback));
        },
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Mileages by Project'))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(MileageByProject))