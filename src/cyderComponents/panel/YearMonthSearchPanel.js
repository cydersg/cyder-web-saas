import React, { Fragment, useState, useEffect } from 'react';
import { Container, Row, Col, Button, Label, Input } from 'reactstrap';
import { CyderSelect } from 'cyderComponents/input/index';
import { getMonthOptions, getYearOptions } from 'js/generalUtils';
import { expenseDeletableStatuses, urlRecurring, urlReceipt, layoutLib } from 'js/constlib';
import dateutils from 'js/dateutils';

const defaultOption = { value: 0, label: 'None' };

const YearMonthSearchPanel = props => {
    const { onSearch, hasMonth, hasYear, minYear, month, year } = props;
    const [monthValue, setMonthValue] = useState(defaultOption);
    const [yearValue, setYearValue] = useState(defaultOption);

    const monthOptionsTmp = React.useMemo(getMonthOptions, []);
    const monthOptions = monthOptionsTmp.map(item => {
        return {
            value: item.value,
            label: item.value ? dateutils.monthsArray[item.value - 1] : item.label,
        };
    });
    const yearOptions = React.useMemo(() => getYearOptions(minYear), [minYear]);

    const onSearchClicked = () => {
        if (!onSearch) return;
        if (hasMonth && hasYear) {
            onSearch(monthValue, yearValue);
            return;
        }
        onSearch(hasMonth ? monthValue : yearValue);
    };

    useEffect(() => {
        const defaultMonth = monthOptions.find(x => x.value === parseInt(month));
        const defaultYear = yearOptions.find(x => x.value === parseInt(year));
        setMonthValue(defaultMonth);
        setYearValue(defaultYear);
    }, [month, year]);

    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return (
        <Fragment>
            <div className="d-none d-sm-block w-100p">
                <Row>
                    {hasMonth && (
                        <Col xs="12" md="2">
                            <CyderSelect
                                label="Month"
                                formGroupClassName="m-0"
                                inputProps={{
                                    options: monthOptions,
                                    placeholder: 'Filter by month',
                                    value: monthValue,
                                    onChange: setMonthValue,
                                }}
                            />
                        </Col>
                    )}
                    {hasYear && (
                        <Col xs="12" md="2">
                            <CyderSelect
                                label="Year"
                                formGroupClassName="m-0"
                                inputProps={{
                                    options: yearOptions,
                                    placeholder: 'Filter by year',
                                    value: yearValue,
                                    onChange: setYearValue,
                                }}
                            />
                        </Col>
                    )}
                    <Col xs="12" md="1" className="mt-2 d-flex align-items-end">
                        <Button className={isMobile ? 'w-100p' : ''} onClick={onSearchClicked}>
                            Search
                        </Button>
                    </Col>
                    <Col xs="12" md="1" className="mt-2 d-flex align-items-end">
                        {props.button1}
                    </Col>
                </Row>
            </div>

            <div className="d-block d-sm-none w-100p">
                {hasMonth && (
                    <Row className="mb10">
                        <Col xs="4">
                            <Label xs="auto" className="text-bold">
                                Month
                            </Label>
                        </Col>
                        <Col xs="8">
                            <Input
                                onChange={e => {
                                    setMonthValue({
                                        value: e.target.value,
                                        label: e.target.value,
                                    });
                                }}
                                value={monthValue ? monthValue.value : null}
                                type="select"
                                id="month"
                            >
                                {getMonthOptions().map(item => (
                                    <option value={item.value} key={item.value}>
                                        {item.value ? dateutils.monthsArray[item.value - 1] : item.label}
                                    </option>
                                ))}
                            </Input>
                        </Col>
                    </Row>
                )}
                {hasYear && (
                    <Row className="mb10">
                        <Col xs="4">
                            <Label xs="auto" className="text-bold">
                                Year
                            </Label>
                        </Col>
                        <Col xs="8">
                            <Input
                                onChange={e => {
                                    setYearValue({
                                        value: e.target.value,
                                        label: e.target.value,
                                    });
                                }}
                                value={yearValue ? yearValue.value : null}
                                type="select"
                                id="year"
                            >
                                {getYearOptions().map((item, i) => (
                                    <option key={item.value}>{item.label}</option>
                                ))}
                            </Input>
                        </Col>
                    </Row>
                )}
                <Row>
                    <Col>
                        <Button className="w-100p" color="success" onClick={onSearchClicked}>
                            Search
                        </Button>
                    </Col>
                </Row>
                <Row>
                    <Col>{props.button1}</Col>
                </Row>
            </div>
        </Fragment>
    );
};

export default YearMonthSearchPanel;
