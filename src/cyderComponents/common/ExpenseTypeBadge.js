import React from "react";

const ExpenseTypeBadge = ({ type }) => {
    if (type === "Mileage") {
        return (
            <div>
                <i className="align-middle material-icons">directions_car</i>
            </div>
        );
    } else if (type === "Receipt") {
        return (
            <div>
                <i className="align-middle material-icons">receipt</i>
            </div>
        );
    } else {
        return type;
    }
};
export default ExpenseTypeBadge;
