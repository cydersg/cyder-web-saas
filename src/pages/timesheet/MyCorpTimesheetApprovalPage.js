import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import withQuery from 'with-query';
import history from 'history.js';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import Table from 'react-table';
import { Badge, Container, Row, Col } from 'reactstrap';
import StoredLayout from 'layouts/StoredLayout';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import { TablePagination } from 'cyderComponents/pagination/';
import { CyderSelect, CyderRadioInput } from 'cyderComponents/input/index';
import MyCorpTimesheetBottomNav from './MyCorpTimesheetBottomNav';
import { setJumbotronTitle } from 'actions/pagedata';
import * as Actions from 'actions/workflow/myTasksAction';
import ActionExecutor from 'js/ActionExecutor';
import cyderlib from 'js/cyderlib';
import { layoutLib } from 'js/constlib';
import { inboxOutboxOptions, navigateTo, TooglePanel } from './MyCorpTimesheetReport';
const { myTasksGetTasks } = Actions;

class MyCorpTimesheetApprovalPage extends Component {
    state = {
        loading: false,
        reports: [],
    };

    componentDidMount() {
        this.props.setJumbotronTitle('Approval');
        this.getTimesheetReport();
    }

    onInboxOutboxRadioChange = (value, id) => {
        navigateTo(this.props.history, value);
    };

    showLoading = loading => {
        this.setState({ loading });
    };

    getTimesheetReport = () => {
        const callback = async () => {
            const { data } = await this.props.myTasksGetTasks('Timesheet Report');
            this.setState({ reports: data && data.reverse() });
        };
        ActionExecutor.execute(this.showLoading, callback);
    };

    getTdProps = (state, rowInfo) => {
        return {
            onClick: (e, handleOriginal) => {
                if (!rowInfo) return;
                const { instance_id, resource_id, id } = rowInfo.original;
                const url = withQuery(`../timesheet/approval/${resource_id}/${id}`, {
                    realid: id,
                });
                history.push(url);
                if (handleOriginal) handleOriginal();
            },
        };
    };

    render() {
        const { loading, reports } = this.state;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        if (loading) return <LoadingSpinner center />;
        return (
            <Container className="wideContainer">
                <Row className="justify-content-center">
                    <Col
                        className="ph0"
                        style={{
                            textAlign: 'center',
                        }}
                    >
                        {/* <b>MyCorp Check In</b> */}
                        <hr className="mycorp-hr-small" />
                    </Col>
                </Row>
                <TooglePanel module="approval" history={this.props.history} />
                <RowColWrapper colProps={{ className: ' mycorp-list' }}>
                    <Table
                        className="-highlight mb-2"
                        minRows={1}
                        TheadComponent={props => null}
                        showPagination={!isMobile}
                        data={reports}
                        columns={getColumns()}
                        getTdProps={this.getTdProps}
                        PaginationComponent={TablePagination}
                    />
                </RowColWrapper>
                <MyCorpTimesheetBottomNav page="report" />
            </Container>
        );
    }
}

const getColumns = () => {
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return [
        {
            Header: '',
            headerClassName: 'rt-no-sort-ind',
            accessor: 'instance_id',
            Cell: props => {
                return (
                    <Link
                        style={{
                            color: 'unset',
                        }}
                    >
                        <div className="boxShadow">
                            <span>
                                <strong>{props.original.title}</strong>
                            </span>
                            <br />
                            <span
                                style={{
                                    fontSize: 12,
                                }}
                            >
                                {moment(props.original.createddt, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY')},{' '}
                                {props.original.originator_fullname}
                            </span>
                            <br />
                            <Badge color={cyderlib.statusBadgeColor(props.original.status)}>{props.original.status}</Badge>
                        </div>
                    </Link>
                );
            },
        },
    ];
};

const reducer = state => {
    return {};
};

const actionList = {
    setJumbotronTitle,
    myTasksGetTasks,
};

export default connect(reducer, actionList)(StoredLayout(MyCorpTimesheetApprovalPage));
