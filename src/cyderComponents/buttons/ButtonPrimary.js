import React from 'react';
import { Button } from 'reactstrap';
import { layoutLib } from 'js/constlib';
const isMobile = window.innerWidth <= layoutLib.mobileWidth;

const ButtonPrimary = ({ onClick, faIcon, label, disabled, style, normalized }) => {
    return (
        <Button disabled={disabled} style={style} className={isMobile && !normalized ? 'w-100p' : ''} onClick={onClick} color="primary">
            {faIcon && <i class={`fa fa-${faIcon} fa-lg`}></i>}
            {label}
        </Button>
    );
};

export default ButtonPrimary;
