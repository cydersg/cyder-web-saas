import {
    DELEGATIONS_SEARCH_SUCCESS,
    DELEGATIONS_SEARCH_REQUEST,
    DELEGATIONS_SEARCH_FAILURE,
    DELEGATIONS_SEARCH_SET_FORCE_REFRESH_FLAG,
    DELEGATIONS_MODAL_SET_DELEGATION,
    DELEGATIONS_MODAL_TOGGLE,
    DELEGATIONS_MODAL_CHANGE_VALUE,
    DELEGATIONS_DELEGATION_ADD_REQUEST,
    DELEGATIONS_DELEGATION_ADD_SUCCESS,
    DELEGATIONS_DELEGATION_ADD_FAILURE,
    DELEGATIONS_DELEGATION_UPDATE_REQUEST,
    DELEGATIONS_DELEGATION_UPDATE_SUCCESS,
    DELEGATIONS_DELEGATION_UPDATE_FAILURE,
    DELEGATIONS_DELEGATION_DELETE_REQUEST,
    DELEGATIONS_DELEGATION_DELETE_SUCCESS,
    DELEGATIONS_DELEGATION_DELETE_FAILURE,
} from '../constants/actionTypesDelegations';
import withQuery from 'with-query';

import { CYDER_COMMON_API_ROOT } from '../../config';

export function delegationsSearchAction() {
    return (dispatch, getState) => {
        dispatch({ type: DELEGATIONS_SEARCH_REQUEST });
        dispatch({ type: DELEGATIONS_SEARCH_SET_FORCE_REFRESH_FLAG, val: false });

        const url = withQuery(CYDER_COMMON_API_ROOT + '/delegation', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'findall',
        });
        const options = {
            // body: {},
            method: 'GET',
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    response.json().then(error => {
                        dispatch({ type: DELEGATIONS_SEARCH_FAILURE, error });
                        return error;
                    });
                } else {
                    response.json().then(response => {
                        dispatch({ type: DELEGATIONS_SEARCH_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: DELEGATIONS_SEARCH_FAILURE, error });
                return error;
            });
    };
}
export function delegationsEditModalSetDelegation(modalDelegation) {
    return {
        modalDelegation,
        type: DELEGATIONS_MODAL_SET_DELEGATION,
    };
}
export function delegationsModalToggleAction(modalAction) {
    return {
        type: DELEGATIONS_MODAL_TOGGLE,
        modalAction,
    };
}
export function delegationsEditModalChangeValue(key, value) {
    return {
        type: DELEGATIONS_MODAL_CHANGE_VALUE,
        key,
        value,
    };
}
export function delegationsAddDelegationAction(delegateUsers) {
    return (dispatch, getState) => {
        dispatch({ type: DELEGATIONS_DELEGATION_ADD_REQUEST });

        const url = withQuery(CYDER_COMMON_API_ROOT + '/delegation', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'create',
        });
        const modalDelegation = getState().delegationsReducer.modalDelegation;
        const body = {
            delegator: delegateUsers[1].value,
            delegatee: delegateUsers[2].value,
            start_date: modalDelegation.start_date.format ? modalDelegation.start_date.format('DD/MM/YYYY') : modalDelegation.start_date,
            end_date: modalDelegation.end_date.format ? modalDelegation.end_date.format('DD/MM/YYYY') : modalDelegation.end_date,
            id: modalDelegation.id,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: DELEGATIONS_DELEGATION_ADD_FAILURE, error });
                        return error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: DELEGATIONS_DELEGATION_ADD_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: DELEGATIONS_DELEGATION_ADD_FAILURE, error });
                return error;
            });
    };
}
export function delegationsUpdateDelegationAction(delegateUsers) {
    return (dispatch, getState) => {
        dispatch({ type: DELEGATIONS_DELEGATION_UPDATE_REQUEST });

        const url = withQuery(CYDER_COMMON_API_ROOT + '/delegation', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'update',
        });
        const modalDelegation = getState().delegationsReducer.modalDelegation;
        const body = {
            delegator: delegateUsers[1].value,
            delegatee: delegateUsers[2].value,
            start_date: modalDelegation.start_date.format ? modalDelegation.start_date.format('DD/MM/YYYY') : modalDelegation.start_date,
            end_date: modalDelegation.end_date.format ? modalDelegation.end_date.format('DD/MM/YYYY') : modalDelegation.end_date,
            id: modalDelegation.id,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: DELEGATIONS_DELEGATION_UPDATE_FAILURE, error });
                        return error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: DELEGATIONS_DELEGATION_UPDATE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: DELEGATIONS_DELEGATION_UPDATE_FAILURE, error });
                return error;
            });
    };
}
export function delegationsDeleteDelegationAction() {
    return (dispatch, getState) => {
        dispatch({ type: DELEGATIONS_DELEGATION_DELETE_REQUEST });

        // Perform the actual API call
        const url = withQuery(CYDER_COMMON_API_ROOT + '/delegation', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'delete',
        });
        const id = getState().delegationsReducer.modalDelegation.id;
        console.log(id);
        const body = {
            id,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: DELEGATIONS_DELEGATION_DELETE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: DELEGATIONS_DELEGATION_DELETE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: DELEGATIONS_DELEGATION_DELETE_FAILURE, error });
                return error;
            });
    };
}
