import StoredLayout from '../../layouts/StoredLayout';
import Profile from './Profile';

export default StoredLayout(Profile);
