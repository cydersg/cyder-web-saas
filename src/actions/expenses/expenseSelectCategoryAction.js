/**
 * Expenses Select Category Action
 * 
 * Author: Fernando
 */

import { CYDER_EXPENSE_SELECT_CATEGORY } from '../constants/actionTypes'

/**
 * Select Expense Category action
 *
 * NOTE: this is NOT currently being used
 * 
 * @param {*} fields 
 * @param {*} callback 
 */
export function expenseSelectCategoryAction(category, callback) {
    return function(dispatch, getState) {

        console.log("Change expense category selection, new category: ", category);
        // var state = getState();

        // dispatch(updateSelectedCategoryAction(category));

        if (callback) {
            callback();
        }

    }
}

/**
 * Update selected category
 * 
 * @param {*} newCategory 
 */
export function updateSelectedCategoryAction(newCategory) {
    return {
        type: CYDER_EXPENSE_SELECT_CATEGORY,
        newCategory
    }
}