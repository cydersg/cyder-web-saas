import React from 'react';

import ReactTable from 'react-table';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import { TablePagination } from 'cyderComponents/pagination/';

const ConciergeHomeTable = props => {
    const { title, columns, data, defaultPageSize } = props;

    return (
        <RowColWrapper rowProps={{ className: 'mt-3' }} colProps={{ xs: 12 }}>
            <ReactTable
                className="-highlight mb-2"
                columns={columns}
                data={data}
                minRows={1}
                defaultPageSize={defaultPageSize}
                filterable={false}
                showPagination={false}
                PaginationComponent={TablePagination}
            />
        </RowColWrapper>
    );
};

export default ConciergeHomeTable;
