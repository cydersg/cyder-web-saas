import withQuery from 'with-query';
import fetchlib from 'js/fetchlib';

import {
    PAYMENTS_LIST_TOGGLE_CHECKBOX,
    PAYMENTS_LIST_TOGGLE_CHECKBOX_ALL,
    PAYMENTS_LIST_GET_EXPENSES_REQUEST,
    PAYMENTS_LIST_GET_EXPENSES_SUCCESS,
    PAYMENTS_LIST_GET_EXPENSES_FAILURE,
    PAYMENTS_LIST_GET_REPORTS_REQUEST,
    PAYMENTS_LIST_GET_REPORTS_SUCCESS,
    PAYMENTS_LIST_GET_REPORTS_FAILURE,
    PAYMENTS_LIST_DELETE_RECEIPT_REQUEST,
    PAYMENTS_LIST_DELETE_RECEIPT_SUCCESS,
    PAYMENTS_LIST_DELETE_RECEIPT_FAILURE,
    PAYMENTS_LIST_MODAL_TOGGLE,
    PAYMENTS_LIST_COPY_RECEIPT,
} from '../constants/actionTypesPayments';

import { DJARVIS_EXPENSE_API_ROOT } from '../../config';

export function paymentsListGetExpenses(status) {
    return (dispatch, getState) => {
        const url = withQuery(DJARVIS_EXPENSE_API_ROOT + '/my', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getreceiptsbystatus',
            status,
        });
        const options = {
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: {},
        };
        dispatch({ type: PAYMENTS_LIST_GET_EXPENSES_REQUEST });

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(response => {
                        dispatch({ type: PAYMENTS_LIST_GET_EXPENSES_FAILURE, response });
                    });
                } else {
                    return response.json().then(response => {
                        return response;
                        // use myReceiptsRenderReceipts after pulling shared category data
                        // dispatch({type: PAYMENTS_LIST_GET_EXPENSES_SUCCESS, response});
                    });
                }
            })
            .catch(error => {
                dispatch({ type: PAYMENTS_LIST_GET_EXPENSES_FAILURE, error });
            });
    };
}

export function paymentsListGetReports(status, from, to) {
    return (dispatch, getState) => {
        const url = withQuery(`${DJARVIS_EXPENSE_API_ROOT}/report`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getreportsbystatus',
        });
        const body = {
            status,
            from,
            to,
        };

        return fetchlib.fetch(
            dispatch,
            getState,
            'POST',
            url,
            body,
            PAYMENTS_LIST_GET_REPORTS_REQUEST,
            null,
            PAYMENTS_LIST_GET_REPORTS_FAILURE,
        );
    };
}
export function paymentsListRenderReceipts(response, type) {
    switch (type) {
        case 'reports':
            return { type: PAYMENTS_LIST_GET_REPORTS_SUCCESS, response };
        case 'expenses':
            return { type: PAYMENTS_LIST_GET_EXPENSES_SUCCESS, response };
        default:
            return { type: PAYMENTS_LIST_GET_EXPENSES_SUCCESS, response };
    }
}
export function paymentsListDeleteReceipt() {
    return (dispatch, getState) => {
        const url = withQuery(DJARVIS_EXPENSE_API_ROOT + '/my', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'deletereceipt',
        });
        const body = {
            receiptid: getState().cyderExpenseMyReceiptsReducer.receiptIdForDeletion,
            userid: getState().cyderProfileReducer.profile.username,
        };
        const options = {
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
        };

        dispatch({ type: PAYMENTS_LIST_DELETE_RECEIPT_REQUEST });

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: PAYMENTS_LIST_DELETE_RECEIPT_FAILURE, error });
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: PAYMENTS_LIST_DELETE_RECEIPT_SUCCESS, response });
                    });
                }
            })
            .catch(error => {
                dispatch({ type: PAYMENTS_LIST_DELETE_RECEIPT_FAILURE, error });
            });
    };
}
export function paymentsListCopyReceipt(index) {
    return {
        type: PAYMENTS_LIST_COPY_RECEIPT,
        index,
    };
}
export function toggleCheckboxes(id) {
    return {
        type: PAYMENTS_LIST_TOGGLE_CHECKBOX,
        id,
    };
}
export function toggleCheckboxesAll() {
    return {
        type: PAYMENTS_LIST_TOGGLE_CHECKBOX_ALL,
    };
}
export function expenseMyReceiptsToggleModal(modalAction, data) {
    return {
        type: PAYMENTS_LIST_MODAL_TOGGLE,
        modalAction,
        data,
    };
}
