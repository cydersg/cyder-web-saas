import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';

import ReactTable from 'react-table';
import { Container, Row, Col, Button, Label, Input } from 'reactstrap';
import StoredLayout from 'layouts/StoredLayout';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import { TablePagination } from 'cyderComponents/pagination/';

import { getPastActivityData, setLoading } from 'actions/timesheet/clockAction';
import { expenseSharedGetProjects } from 'actions/expenses/cyderExpenseClaimSharedAction';
import { setJumbotronTitle } from 'actions/pagedata';

import { getMonthOptions, getYearOptions } from 'js/generalUtils';
import dateutils from 'js/dateutils';
import { expenseDeletableStatuses, urlRecurring, urlReceipt, layoutLib } from 'js/constlib';

class MyClocksPage extends Component {
    state = {
        month: moment().format('M'),
        year: moment().format('YYYY'),
    };

    componentDidMount() {
        this.props.setJumbotronTitle('History');
        this.getPastActivityData('yes');
    }

    onChangeSelect = e => {
        let newState = {};
        newState[e.target.id] = e.target.value;
        this.setState(newState);
    };

    getPastActivityData = getProjects => {
        const { month, year } = this.state;
        this.props.getPastActivityData(month, year, getProjects);
    };

    render() {
        const { forceLoading, pastActivity, projectsMap } = this.props;
        const { month, year } = this.state;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        if (forceLoading) return <LoadingSpinner />;
        return (
            <Container className="wideContainer">
                <Row>
                    <Col xs={12} className="pb-4">
                        <Link to="./clock">
                            <i className="align-middle material-icons material-icons-3x">chevron_left</i>
                        </Link>
                    </Col>
                </Row>

                <div className="d-none d-sm-block">
                    <Row className="mb-4">
                        <Col xs="2" className="p-0">
                            <Label xs="auto" className="text-bold">
                                Month
                            </Label>
                            <Col>
                                <Input onChange={this.onChangeSelect} value={month} type="select" id="month">
                                    {getMonthOptions().map(item => (
                                        <option value={item.value} key={item.value}>
                                            {item.value ? dateutils.monthsArray[item.value - 1] : item.label}
                                        </option>
                                    ))}
                                </Input>
                            </Col>
                        </Col>
                        <Col xs="2" className="p-0">
                            <Label xs="auto" className="text-bold">
                                Year
                            </Label>
                            <Col>
                                <Input onChange={this.onChangeSelect} value={year} type="select" id="year">
                                    {getYearOptions().map((item, i) => (
                                        <option key={item.value}>{item.label}</option>
                                    ))}
                                </Input>
                            </Col>
                        </Col>
                        <Col className="pb-1 d-flex align-items-end">
                            <Button color="success" onClick={() => this.getPastActivityData(true)}>
                                Search
                            </Button>
                        </Col>
                    </Row>
                </div>

                <div className="d-block d-sm-none">
                    <Row className="mb-4">
                        <Col xs="12" className="p-0">
                            <Row>
                                <Col xs="4">
                                    <Label xs="auto" className="text-bold">
                                        Month
                                    </Label>
                                </Col>
                                <Col xs="8" className="mb10">
                                    <Input onChange={this.onChangeSelect} value={month} type="select" id="month">
                                        {getMonthOptions().map(item => (
                                            <option value={item.value} key={item.value}>
                                                {item.value ? dateutils.monthsArray[item.value - 1] : item.label}
                                            </option>
                                        ))}
                                    </Input>
                                </Col>
                            </Row>
                        </Col>

                        <Col xs="12" className="p-0">
                            <Row>
                                <Col xs="4">
                                    <Label xs="auto" className="text-bold">
                                        Year
                                    </Label>
                                </Col>
                                <Col xs="8" className="mb10">
                                    <Input onChange={this.onChangeSelect} value={year} type="select" id="year">
                                        {getYearOptions().map((item, i) => (
                                            <option key={item.value}>{item.label}</option>
                                        ))}
                                    </Input>
                                </Col>
                            </Row>
                        </Col>

                        <Col xs="12" className="wideContainer">
                            <Button className="w-100p" color="success" onClick={() => this.getPastActivityData(true)}>
                                Search
                            </Button>
                        </Col>
                    </Row>
                </div>

                <Row className="wideContainer">
                    <Col className="wideContainer">
                        <ReactTable
                            className="-highlight mb-2"
                            data={pastActivity}
                            showPagination={!isMobile}
                            columns={getColumn(projectsMap)}
                            pageSize={pastActivity.length > 10 ? 10 : pastActivity.length}
                            PaginationComponent={TablePagination}
                        />
                    </Col>
                </Row>
            </Container>
        );
    }
}

function getColumn(projectsMap) {
    const formatData = date => moment(date, 'YYYY-MM-DD hh:mm:ss').format('DD/MM/YYYY hh:mm a');
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return [
        {
            Header: 'Start Time',
            accessor: 'starttime',
            show: !isMobile,
            Cell: props => {
                return (
                    <>
                        <span className="font-weight-bold">{moment(props.value).format('DD-MMM-YYYY')}</span> <br />
                        <span className="font-weight-bold text-muted">{moment(props.value).format('h:mm a')}</span>
                    </>
                );
            },
        },
        {
            Header: 'End Time',
            accessor: 'endtime',
            show: !isMobile,
            Cell: props => {
                return (
                    <>
                        <span className="font-weight-bold">{moment(props.value).format('DD-MMM-YYYY')}</span> <br />
                        <span className="font-weight-bold text-muted">{moment(props.value).format('h:mm a')}</span>
                    </>
                );
            },
        },
        {
            Header: 'GPS Location',
            accessor: 'id',
            show: !isMobile,
            Cell: props => {
                return (
                    <>
                        {props.original.lat && props.original.lng ? (
                            <span>
                                Lat {props.original.lat}
                                <br />
                                Lng {props.original.lng}
                            </span>
                        ) : (
                            '-'
                        )}
                    </>
                );
            },
        },
        {
            Header: 'Project',
            accessor: 'project',
            show: !isMobile,
            Cell: props => projectsMap[props.value] || props.value,
        },
        {
            Header: 'Purpose',
            show: !isMobile,
            accessor: 'purpose',
        },
        {
            Header: '',
            headerClassName: 'rt-no-sort-ind',
            accessor: 'id',
            sortable: false,
            show: isMobile,
            filterable: false,
            Cell: props => {
                return (
                    <div className="boxShadow">
                        <span>
                            <strong>{formatData(props.original.starttime)}</strong>
                        </span>
                        <br />
                        <span>
                            <strong>{formatData(props.original.endtime)}</strong>
                        </span>
                        <br />
                        {props.original.lat && props.original.lng && (
                            <>
                                <span>
                                    Lat {props.original.lat}
                                    <br />
                                    Lng {props.original.lng}
                                </span>
                                <br />
                            </>
                        )}
                        <span>{projectsMap[props.original.project] || props.original.project}</span>
                        <br />
                        <span>{props.original.purpose}</span>
                    </div>
                );
            },
        },
    ];
}

const mapStateToProps = state => {
    return {
        ...state.clockReducer,
        projects: state.cyderExpenseSharedReducer.projects,
        projectsMap: state.cyderExpenseSharedReducer.projectsMap,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setJumbotronTitle: title => {
            dispatch(setJumbotronTitle(title));
        },
        getPastActivityData: (month, year, getProjects) => {
            const array = [dispatch(getPastActivityData(month, year)), getProjects === 'yes' && dispatch(expenseSharedGetProjects())];
            dispatch(setLoading(true)).then(() => Promise.all(array).then(() => dispatch(setLoading(false))));
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(MyClocksPage));
