import React, { Component } from 'react';
import { connect } from 'react-redux';
import sha512 from 'js-sha512';
import history from 'history.js';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

import {
    cyderLoginTokenAction,
    cyderLoginSetErrorMessageAction,
    cyderLoginClearErrorMessageAction,
} from 'actions/security/cyderSecurityAction';

class Token extends Component {
    componentDidMount() {
        const token = this.props.match && this.props.match.params && this.props.match.params.token;
        const module = this.props.match && this.props.match.params && this.props.match.params.module;
        this.props.loginSubmit(token, module);
    }

    render() {
        return (
            <div className="d-flex row justify-content-between" style={{ width: '100%' }}>
                <LoadingSpinner />
            </div>
        );
    }
}

const parseAuthResponse = (res, dispatch) => {
    const module = res.module;

    var navigateHome = () => {
        let navigate = '/home';
        if (module === 'instatime') {
            navigate = '/mycorp/timesheet/home';
            dispatch({
                type: 'MYCORP_SET_MODULE',
                module,
            });
        } else if (module === 'instaxpense') {
            navigate = '/mycorp/expense/home';
            dispatch({
                type: 'MYCORP_SET_MODULE',
                module,
            });
        } else if (module === 'instaleave') {
            navigate = '/mycorp/leave/home';
            dispatch({
                type: 'MYCORP_SET_MODULE',
                module,
            });
        }
        history.replace(navigate);
    };
    if (!res.authorizationToken) return;
    navigateHome();
};

const mapStateToProps = state => {
    return {
        fields: state.cyderLoginReducer.fields,
        loginError: state.cyderLoginReducer.loginError,
        errorMessage: state.cyderLoginReducer.errorMessage,
        requesting: state.cyderLoginReducer.requesting,
        djarvisWebsite: state.config.djarvisWebsite,
        tfatoken: state.cyderLoginReducer.tfatoken,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        clearErrorMessage: () => {
            dispatch(cyderLoginClearErrorMessageAction());
        },
        setErroMessage: errorMessage => {
            dispatch(cyderLoginSetErrorMessageAction(errorMessage));
        },
        loginSubmit: (token, module) => {
            const data = {
                token,
                module,
                system: 'djarvis',
            };
            dispatch(cyderLoginTokenAction(data)).then(res => parseAuthResponse(res, dispatch));
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Token);
