import {
    CYDER_PROJECTS_SETTINGS_SEARCH_SUCCESS,
    CYDER_PROJECTS_SETTINGS_SEARCH_REQUEST,
    CYDER_PROJECTS_SETTINGS_SEARCH_FAILURE,
    CYDER_PROJECTS_SETTINGS_SEARCH_SET_FORCE_REFRESH_FLAG,
    CYDER_PROJECTS_SETTINGS_MODAL_SET_PROJECT,
    CYDER_PROJECTS_SETTINGS_MODAL_TOGGLE,
    CYDER_PROJECTS_SETTINGS_MODAL_CHANGE_VALUE,
    CYDER_PROJECTS_SETTINGS_PROJECT_ADD_REQUEST,
    CYDER_PROJECTS_SETTINGS_PROJECT_ADD_SUCCESS,
    CYDER_PROJECTS_SETTINGS_PROJECT_ADD_FAILURE,
    CYDER_PROJECTS_SETTINGS_PROJECT_UPDATE_REQUEST,
    CYDER_PROJECTS_SETTINGS_PROJECT_UPDATE_SUCCESS,
    CYDER_PROJECTS_SETTINGS_PROJECT_UPDATE_FAILURE,
    CYDER_PROJECTS_SETTINGS_PROJECT_DELETE_REQUEST,
    CYDER_PROJECTS_SETTINGS_PROJECT_DELETE_SUCCESS,
    CYDER_PROJECTS_SETTINGS_PROJECT_DELETE_FAILURE,
    CYDER_PROJECTS_SETTINGS_ADD_USER,
    CYDER_PROJECTS_SETTINGS_REMOVE_USER,
    CYDER_PROJECTS_SETTINGS_ADD_APPROVER,
    CYDER_PROJECTS_SETTINGS_REMOVE_APPROVER,
} from '../constants/actionTypes';
import withQuery from 'with-query';

import { DJARVIS_COMMON_API_ROOT } from '../../config';

export function cyderProjectsSettingsAddUserAction(username) {
    return {
        type: CYDER_PROJECTS_SETTINGS_ADD_USER,
        username,
    };
}
export function cyderProjectsSettingsRemoveUserAction(username) {
    return {
        type: CYDER_PROJECTS_SETTINGS_REMOVE_USER,
        username,
    };
}
export function cyderProjectsSettingsAddApproverAction(username) {
    return {
        type: CYDER_PROJECTS_SETTINGS_ADD_APPROVER,
        username,
    };
}
export function cyderProjectsSettingsRemoveApproverAction(username) {
    return {
        type: CYDER_PROJECTS_SETTINGS_REMOVE_APPROVER,
        username,
    };
}
export function cyderProjectsSearchAction(body) {
    // Redux Thunk will inject dispatch here:
    return (dispatch, getState) => {
        // Reducers may handle this to set a flag like isFetching
        dispatch({ type: CYDER_PROJECTS_SETTINGS_SEARCH_REQUEST });
        dispatch({ type: CYDER_PROJECTS_SETTINGS_SEARCH_SET_FORCE_REFRESH_FLAG, val: false });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/project', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'partialsearch',
        });
        // const bodyWithAppId = Object.assign({appId: getState().cyderProfileReducer.profile.appId}, body);
        const options = {
            body: JSON.stringify(body),
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
        };
        return fetch(url, options)
            .then(response => response.json())
            .then(
                response => {
                    dispatch({ type: CYDER_PROJECTS_SETTINGS_SEARCH_SUCCESS, response });
                    return response;
                },
                error => {
                    // TODO
                    // Reducers may handle this to reset isFetching
                    // dispatch({ type: 'GET_USER_FAILURE', id, error })
                    dispatch({ type: CYDER_PROJECTS_SETTINGS_SEARCH_FAILURE, error });
                    // Rethrow so returned Promise is rejected
                    throw error;
                },
            )
            .catch(error => {
                //TODO
                console.log('in catch block');
                console.log(error);
                throw error;
            });
    };
}
export function cyderProjectsSettingsEditModalSetProject(modalProject) {
    return {
        ...modalProject,
        type: CYDER_PROJECTS_SETTINGS_MODAL_SET_PROJECT,
    };
}
export function cyderProjectsSettingsModalToggleAction(modalAction) {
    return {
        type: CYDER_PROJECTS_SETTINGS_MODAL_TOGGLE,
        modalAction,
    };
}
export function cyderProjectsSettingsEditModalChangeValue(key, value) {
    return {
        type: CYDER_PROJECTS_SETTINGS_MODAL_CHANGE_VALUE,
        key,
        value,
    };
}
export function cyderProjectsSettingsAddProjectAction(approvers) {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_PROJECTS_SETTINGS_PROJECT_ADD_REQUEST });

        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/project', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'add',
        });
        let approverList = Object.values(approvers);
        let approversParsed = '';
        let approversArray = [];
        if (approverList.length === 0 || approverList.indexOf(null) !== -1) {
            approversParsed = '';
        } else {
            approverList.forEach((approver, i) => {
                approversArray.push(approver.value);
                approversParsed = approversParsed + approver.value;
                if (i < approverList.length - 1) {
                    approversParsed = approversParsed + ',';
                }
            });
        }
        const body = Object.assign(
            {
                ...getState().cyderProjectsSettingsSearchReducer.modalProject,
            },
            {
                approvers: approversArray,
            },
        );
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_PROJECTS_SETTINGS_PROJECT_ADD_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_PROJECTS_SETTINGS_PROJECT_ADD_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_PROJECTS_SETTINGS_PROJECT_ADD_FAILURE, error });
                return error;
            });
    };
}
export function cyderProjectsSettingsUpdateProjectAction(approvers) {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_PROJECTS_SETTINGS_PROJECT_UPDATE_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/project', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'update',
        });
        const modalProject = getState().cyderProjectsSettingsSearchReducer.modalProject;
        let approverList = Object.values(approvers);
        let approversArray = [];
        if (approverList.length === 0 || approverList.indexOf(null) !== -1) {
            approversArray = [];
        } else {
            approverList.forEach(approver => {
                approversArray.push(approver.value);
            });
        }
        const body = {
            code: modalProject.code,
            name: modalProject.name,
            users: modalProject.users,
            approvers: approversArray,
            overrideexpenseapproval: modalProject.overrideexpenseapproval,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_PROJECTS_SETTINGS_PROJECT_UPDATE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_PROJECTS_SETTINGS_PROJECT_UPDATE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_PROJECTS_SETTINGS_PROJECT_UPDATE_FAILURE, error });
                return error;
            });
    };
}
export function cyderProjectsSettingsDeleteProjectAction() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_PROJECTS_SETTINGS_PROJECT_DELETE_REQUEST });

        // Perform the actual API call
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/project', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'delete',
        });
        const code = getState().cyderProjectsSettingsSearchReducer.modalProject.code;
        console.log(code);
        const body = {
            code,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_PROJECTS_SETTINGS_PROJECT_DELETE_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_PROJECTS_SETTINGS_PROJECT_DELETE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_PROJECTS_SETTINGS_PROJECT_DELETE_FAILURE, error });
                return error;
            });
    };
}
