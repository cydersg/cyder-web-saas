import React from 'react';
import { Row, Col } from 'reactstrap';

const RowColWrapper = ({ children, rowProps, colProps }) => (
    <Row {...rowProps}>
        <Col {...colProps}>{children}</Col>
    </Row>
);

export default RowColWrapper;
