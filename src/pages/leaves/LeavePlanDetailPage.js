import React, { Component, useState, useEffect, useCallback } from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import StoredLayout from 'layouts/StoredLayout';
import { Container, Row, Col, Badge, CardBody } from 'reactstrap';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import { InfoField } from 'cyderComponents/input/CyderPreview';
import { CyderFileUploader } from 'cyderComponents/input';

import { setJumbotronTitle } from 'actions/pagedata';
import * as Actions from 'actions/timesheet/monthlyTimesheetAction';
import { cyderUsersSearchAction } from 'actions/users/cyderUsersSearchAction';
import { getAllHolidays } from 'actions/settings/cyderLeaveHolidaysAction';

import ActionExecutor from 'js/ActionExecutor';
import { calNumOfDays } from 'js/generalUtils';
import cyderlib from 'js/cyderlib';
import { layoutLib } from 'js/constlib';

const { findLeaveById } = Actions;

class LeaveTaskDetailPage extends Component {
    state = {
        loading: false,
        taskDetail: {},
        users: [],
    };

    componentDidMount() {
        const { id } = this.props.match.params;
        if (id) this.getTaskDetails(id);
        this.props.setJumbotronTitle('Leave Plan');
        this.getUsers();
    }

    showLoading = loading => {
        this.setState({ loading });
    };

    getTaskDetails = id => {
        const callback = async () => {
            const { data } = await this.props.findLeaveById(id);
            this.setState({ taskDetail: data });
        };
        ActionExecutor.execute(this.showLoading, callback);
    };

    getUsers = () => {
        const callback = async () => {
            const data = await this.props.cyderUsersSearchAction({ keyword: '', status: 'Active' });
            this.setState({ users: data });
        };
        ActionExecutor.execute(this.showLoading, callback);
    };

    render() {
        const { taskDetail, users } = this.state;
        const userInfo = users.find(x => x.email === taskDetail.owner) || {};

        return (
            <div>
                <Container className="wideContainer">
                    <RowColWrapper rowProps={{ className: 'mb-2 mb-4' }}>
                        <Link to="../../leaveplan" tabIndex="-1">
                            <i className="align-middle material-icons material-icons-3x">chevron_left</i>
                        </Link>
                    </RowColWrapper>
                    <DetailPanel {...this.props} taskDetail={taskDetail} userInfo={userInfo} />
                </Container>
            </div>
        );
    }
}

const DetailPanel = props => {
    const { taskDetail, userInfo } = props;
    const [holidays, setHolidays] = useState({});

    const formatDate = date => {
        if (!date) return 'Not date found';
        return moment(date, 'YYYY-MM-DD').format('DD MMM YYYY');
    };

    const getAllHolidays = async year => {
        const callback = async () => {
            const { data } = await props.getAllHolidays(year);
            return data;
        };
        return ActionExecutor.execute(() => {}, callback);
    };

    const getHolidays = async (from, to) => {
        if (!from || !to) return;
        const fromYear = from.format('YYYY');
        const toYear = to.format('YYYY');
        const fromHolidays = await getAllHolidays(fromYear);
        const toHolidays = fromYear !== toYear ? await getAllHolidays(toYear) : [];
        const [numOfDays, totalNumOfDays, deductedNumOfDays, deductedList] = calNumOfDays(from, to, [...fromHolidays, ...toHolidays]);

        setHolidays({
            numOfDays,
            totalNumOfDays,
            deductedNumOfDays,
            holidays: deductedList.holidaysList,
            weekends: deductedList.weekendsList,
        });
    };

    useEffect(() => {
        const { fromdate, todate } = taskDetail;
        const from = fromdate && moment(fromdate, 'YYYY-MM-DD');
        const to = todate && moment(todate, 'YYYY-MM-DD');
        getHolidays(from, to);
    }, [taskDetail]);

    return (
        <div className="wideContainer">
            <Row>
                <Col>
                    <h2 className="font-weight-bold d-none d-md-block">{taskDetail.leaveTypeCode}</h2>
                    <h4 className="font-weight-bold d-block d-md-none">{taskDetail.leaveTypeCode}</h4>
                    <Badge color={cyderlib.statusBadgeColor(taskDetail.status)}>{taskDetail.status}</Badge>
                </Col>
            </Row>
            <Row>
                <Col xs="12">
                    <Row>
                        <Col xs="12" sm="4">
                            <InfoField label="From" value={formatDate(taskDetail.fromdate) + ' - ' + taskDetail.fromsession} />
                        </Col>
                        <Col xs="12" sm="4">
                            <InfoField label="To" value={formatDate(taskDetail.todate) + ' - ' + taskDetail.tosession} />
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row>
                <Col xs="12" sm="4">
                    <InfoField label="Owner" value={userInfo.firstname + ' ' + userInfo.lastname} />
                </Col>
                <Col xs="12" sm="4">
                    <InfoField label="Number of Days" value={taskDetail.numOfDays} />
                </Col>
            </Row>
            <Row>
                <Col xs="12" sm="4">
                    <InfoField label="Email" value={taskDetail.owner} />
                </Col>
                <Col xs="12" sm="4">
                    <InfoField
                        label="Holidays in between"
                        value={
                            holidays.holidays && holidays.holidays.length > 0
                                ? holidays.holidays.map((x, i) => <div key={i}>{x.holidayname}</div>)
                                : '-'
                        }
                    />
                </Col>
            </Row>
            <Row>
                <Col xs="6">
                    <InfoField label="Remarks" value={taskDetail.remarks} />
                </Col>
            </Row>
            {taskDetail.leaveTypeCode === 'MEDICAL' ? (
                <Row>
                    <Col xs="6">
                        <InfoField label="Attachment">
                            <h6 className="font-weight-bold">
                                {taskDetail.Body ? (
                                    <CyderFileUploader
                                        disabled
                                        id={taskDetail.filename}
                                        imageFile={{
                                            b64img: taskDetail.Body,
                                        }}
                                    ></CyderFileUploader>
                                ) : (
                                    'No attachment file'
                                )}
                            </h6>
                        </InfoField>
                    </Col>
                </Row>
            ) : null}
        </div>
    );
};

const mapStateToProps = state => {
    return {};
};

const actionlist = {
    getAllHolidays,
    setJumbotronTitle,
    cyderUsersSearchAction,
    findLeaveById,
};

export default withTranslation()(connect(mapStateToProps, actionlist)(StoredLayout(LeaveTaskDetailPage)));
