import React, { Fragment, Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import history from 'history.js';
import qs from 'query-string';
import withQuery from 'with-query';
import { withTranslation } from 'react-i18next';

import Switch from 'react-switch';
import StoredLayout from 'layouts/StoredLayout';
import GoogleMapReact from 'google-map-react';
import SearchBox from 'cyderComponents/map/SearchBox';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import { Input, Button, Container, Row, Col, Form, FormGroup, Label, InputGroup, InputGroupAddon } from 'reactstrap';
import { CyderInput, CyderSelect } from 'cyderComponents/input';
import { InfoField } from 'cyderComponents/input/CyderPreview';

import {
    expenseNewMileageClaimChangeField,
    expenseNewMileageClaimSaveMileage,
    expenseNewMileageClaimSetFields,
    expenseNewMileageClaimForceLoadingState,
    expenseNewMileageClaimToggleModal,
    expenseNewMileageClaimDeleteSavedReceipt,
    expenseNewMileageClaimResetFields,
} from 'actions/expenses/cyderExpenseNewMileageClaimAction';
import {
    expenseSharedGetMileageCategories,
    expenseSharedGetProjects,
    expenseSharedGetLocations,
    expenseSharedLoadedData,
} from 'actions/expenses/cyderExpenseClaimSharedAction';
import { getSysparamByCode } from 'actions/settings/cyderSysparamAction';
import CyderDatePicker from 'cyderComponents/forms/CyderDatePicker';
import { setJumbotronTitle } from 'actions/pagedata';
import { layoutLib } from 'js/constlib';

import controlledInputLib from 'js/controlledInputLib';

class ExpenseMileage extends Component {
    state = {
        map: '',
        distance: null,
        theqs: null,
        recalculateLocation: true,
        paymentMode: null,
        rate: 0,
        rateunit: 'km',
        travelMode: 'car',
        roundTrip: false,
        showLocations: true,
        locMarkers: [],
        startLocText: '',
        destinationLocText: '',
        pinningStartLoc: false,
        pinningDestinationLoc: false,
        pinnedstartLoc: false,
        pinnedDestinationLoc: false,
        travelLocStart: null,
        travelLocStartMarker: null,
        travelLocDestination: null,
        travelLocDestinationMarker: null,
        lastStartMarkerIsSavedLocation: false,
        lastDestinationMarkerIsSavedLocation: false,
    };

    componentDidMount() {
        const { location, resetState, getRelevantStuff, setJumbotronTitle } = this.props;
        const theqs = location ? qs.parse(location.search) : {};
        this.setState({
            theqs,
        });

        resetState().then(() => {
            getRelevantStuff(theqs.receiptid).then(mileageInfo => {
                setJumbotronTitle(this.generateJumbotronTitle());

                console.log('mileageInfo: ', mileageInfo);
                if (mileageInfo) {
                    const { mileageCategories } = this.props;
                    const { rate, rateunit } = mileageCategories.find(item => item.code === mileageInfo.category) || {};

                    this.setState({
                        roundTrip: mileageInfo.roundTrip === 'Y',
                        destinationLocText: mileageInfo.to,
                        startLocText: mileageInfo.from,
                        distance: mileageInfo.distance,
                        rateUnit: rateunit,
                        rate: rate ? parseFloat(rate) : 0,
                    });
                }

                if (this.state.map && this.state.maps) this.registerMarkers();
            });
        });
    }

    componentDidUpdate(prevProps) {
        // needed because My Expenses and Recurring Charges uses the same Component class
        if (prevProps.location.pathname !== this.props.location.pathname) {
            this.props.setJumbotronTitle(this.generateJumbotronTitle());
        }
    }

    generateJumbotronTitle = () => {
        const { mileageFields, location } = this.props;
        const copyItem =
            location &&
            location.search &&
            location.pathname === `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts/new/mileage`;

        if (copyItem) return 'New Mileage (copying from existing)';
        if (mileageFields.status === 'Ready') return 'Edit Mileage';
        if (mileageFields.status) return 'View Mileage';
        return 'New Mileage';
    };

    calcDistance = () => {
        let {
            map,
            maps,
            travelLocStart,
            travelLocDestination,
            travelLocStartMarker,
            travelLocDestinationMarker,
            directionsDisplay,
            pinnedStartLoc,
            pinnedDestinationLoc,
        } = this.state;
        if (!travelLocStart || !travelLocDestination) return;
        this.handleSwitchChange(false); // TURN OFF SHOWING ALL LOCATIONS

        // REMOVE PREVIOUS RESULT IF EXISTS
        if (directionsDisplay) {
            directionsDisplay.setMap(null);
            directionsDisplay.setPanel(null);
        }

        directionsDisplay = new maps.DirectionsRenderer({ draggable: false });

        // START
        const startLat =
            pinnedStartLoc && travelLocStartMarker ? travelLocStartMarker.position.lat() : travelLocStart.geometry.location.lat();
        const startLng =
            pinnedStartLoc && travelLocStartMarker ? travelLocStartMarker.position.lng() : travelLocStart.geometry.location.lng();

        // DESTINATION
        const destLat =
            pinnedDestinationLoc && travelLocDestinationMarker
                ? travelLocDestinationMarker.position.lat()
                : travelLocDestination.geometry.location.lat();
        const destLng =
            pinnedDestinationLoc && travelLocDestinationMarker
                ? travelLocDestinationMarker.position.lng()
                : travelLocDestination.geometry.location.lng();

        const request = {
            origin: new maps.LatLng(startLat, startLng),
            destination: new maps.LatLng(destLat, destLng),
            travelMode: 'DRIVING',
        };

        this.setState({
            directionsDisplay,
        });

        const setStateAdapter = this.setStateAdapter;
        new maps.DirectionsService().route(request, (result, status) => {
            if (!result) return;
            let distance;

            if (result.routes.length === 0) {
                distance = 0;
            } else {
                result.routes[0].legs.forEach(leg => {
                    distance = +leg.distance.value;
                });
            }

            let newStateObj = { distance };
            if (status === 'OK') {
                directionsDisplay.setDirections(result);
                directionsDisplay.setMap(map);
                newStateObj.directionsResult = result;
            }
            setStateAdapter(newStateObj);
        });
    };

    setStateAdapter = obj => {
        this.setState(obj);
    };

    setMap() {
        this.state.directionsDisplay.setMap(null);
    }

    setStartLocMarker = loc => {
        const { lastStartMarkerIsSavedLocation, travelLocStartMarker, maps, map } = this.state;
        if (!lastStartMarkerIsSavedLocation && travelLocStartMarker) {
            // remove previous marker if the last saved marker is a searched loc
            travelLocStartMarker.setMap();
        }

        const marker = new maps.Marker({
            map,
            title: loc.name,
            position: {
                lat: loc.geometry.location.lat(),
                lng: loc.geometry.location.lng(),
            },
        }).setMap(map);

        this.setState({
            travelLocStart: loc,
            travelLocStartMarker: marker,
            startLocText: loc.formatted_address,
            recalculateLocation: true,
            lastStartMarkerIsSavedLocation: false,
        });

        this.calcDistance();
    };

    setDestinationLocMarker = loc => {
        const { lastStartMarkerIsSavedLocation, travelLocDestinationMarker, maps, map } = this.state;
        if (!lastStartMarkerIsSavedLocation && travelLocDestinationMarker) {
            // remove previous marker if the last saved marker is a searched loc
            travelLocDestinationMarker.setMap();
        }

        const marker = new maps.Marker({
            map,
            title: loc.name,
            position: {
                lat: loc.geometry.location.lat(),
                lng: loc.geometry.location.lng(),
            },
        }).setMap(map);

        this.setState({
            travelLocDestination: loc,
            travelLocDestinationMarker: marker,
            destinationLocText: loc.formatted_address,
            recalculateLocation: true,
            lastDestinationMarkerIsSavedLocation: false,
        });

        this.calcDistance();
    };

    initDefaultRoute = markers => {
        const { from, to } = this.props.mileageFields;
        if (from && to) {
            this.setState(
                {
                    pinningStartLoc: true,
                },
                () => markers[from](),
            );
            this.setState(
                {
                    pinningDestinationLoc: true,
                },
                () => markers[to](),
            );
        }
    };

    initGoogleMaps = (map, maps) => {
        this.setState({
            map,
            maps,
        });
        if (this.props.locations.length > 0) this.registerMarkers();
    };

    pinStartLoc = () => {
        if (this.state.pinningDestinationLoc) {
            this.setState({ pinningDestinationLoc: false });
        }
        this.setState({ pinningStartLoc: !this.state.pinningStartLoc });
    };

    pinDestinationLoc = () => {
        if (this.state.pinningStartLoc) {
            this.setState({ pinningStartLoc: false });
        }
        this.setState({ pinningDestinationLoc: !this.state.pinningDestinationLoc });
    };

    registerMarkers = () => {
        let markers = {};

        this.props.locations.map(loc => {
            const marker = new this.state.maps.Marker({
                map: this.state.map,
                title: loc.name,
                position: {
                    lat: loc.lat,
                    lng: loc.lng,
                },
            });

            const callback = () => this.onMarkerClick(marker);
            markers[loc.name] = callback;
            marker.setMap(this.state.map);

            // ON MARKER CLICK LISTENER
            marker.addListener('click', callback);
            this.state.locMarkers.push(marker);
        });

        // INITIALISE DEFAULT MARKER
        setTimeout(() => {
            // this.initDefaultRoute(markers);
        }, 1000);
    };

    onMarkerClick = marker => {
        const {
            pinningStartLoc,
            lastStartMarkerIsSavedLocation,
            travelLocStartMarker,
            pinningDestinationLoc,
            lastDestinationMarkerIsSavedLocation,
            travelLocDestinationMarker,
        } = this.state;

        if (pinningStartLoc) {
            if (!lastStartMarkerIsSavedLocation && travelLocStartMarker) travelLocStartMarker.setMap();
            this.setState(
                {
                    startLocText: marker.title,
                    pinningStartLoc: false,
                    pinnedStartLoc: true,
                    travelLocStart: {},
                    travelLocStartMarker: marker,
                    lastStartMarkerIsSavedLocation: true,
                },
                this.calcDistance,
            );
        }

        if (pinningDestinationLoc) {
            if (!lastDestinationMarkerIsSavedLocation && travelLocDestinationMarker) travelLocDestinationMarker.setMap();
            this.setState(
                {
                    destinationLocText: marker.title,
                    pinningDestinationLoc: false,
                    pinnedDestinationLoc: true,
                    travelLocDestination: {},
                    travelLocDestinationMarker: marker,
                    lastDestinationMarkerIsSavedLocation: true,
                },
                this.calcDistance,
            );
        }

        marker.setMap(this.state.map);
    };

    handleMileageCategoryChange = event => {
        const { mileageCategories } = this.props;
        const { value, id } = event.target;
        this.props.handleChange(event);

        const { rate, rateunit } = mileageCategories.find(x => x.code === value) || {};
        if (!rate) return;

        this.setState({
            rate: parseFloat(rate),
            rateUnit: rateunit,
        });
    };

    handleCheckbox = () => {
        this.setState({
            roundTrip: !this.state.roundTrip,
        });
    };

    handleRecalculateLocationSwitchChange = checked => {
        this.setState({ recalculateLocation: checked });
    };

    handleSwitchChange = checked => {
        const { locMarkers, map } = this.state;
        locMarkers.forEach(loc => {
            loc.setMap(checked ? map : null);
        });

        this.setState({ showLocations: checked });
    };

    handleDestinationLocTextChange = event => {
        this.setState({
            destinationLocText: event.target.value,
            recalculateLocation: true,
        });
    };

    handleStartLocTextChange = event => {
        this.setState({
            startLocText: event.target.value,
            recalculateLocation: true,
        });
    };

    handleDateChangeWithEvent = (momentOrStringVal, id, isTime) => {
        const momentFormat = isTime ? 'HH:mm' : 'DD/MM/YYYY';
        const synthEvent = {
            target: {
                id,
                value: typeof momentOrStringVal === 'string' ? momentOrStringVal : momentOrStringVal.format(momentFormat),
            },
        };
        this.props.handleChange(synthEvent);
    };

    handleSave = () => {
        const { mileageFields, location } = this.props;
        const {
            travelLocStartMarker,
            travelLocStart,
            travelLocDestinationMarker,
            travelLocDestination,
            pinnedStartLoc,
            pinnedDestinationLoc,
            destinationLocText,
            startLocText,
            distance,
            roundTrip,
        } = this.state;

        // MAP LOCATION
        let supplementaryFields = {};
        if (mileageFields.fromlat) {
            /**
             * if there is fromlat, there will be fromlng, tolat, tolng as well
             * this means that data was previously set by a saved mileage expense item
             */
            supplementaryFields = {
                ...mileageFields,
            };
        } else {
            supplementaryFields = {
                fromlat: pinnedStartLoc ? travelLocStartMarker.position.lat() : travelLocStart.geometry.location.lat(),
                fromlng: pinnedStartLoc ? travelLocStartMarker.position.lng() : travelLocStart.geometry.location.lng(),
                tolat: pinnedDestinationLoc ? travelLocDestinationMarker.position.lat() : travelLocDestination.geometry.location.lat(),
                tolng: pinnedDestinationLoc ? travelLocDestinationMarker.position.lng() : travelLocDestination.geometry.location.lng(),
            };
        }

        const data = {
            ...supplementaryFields,
            ...mileageFields,
            traveltimestart: document.getElementById('traveltimestart').value,
            traveltimeend: document.getElementById('traveltimeend').value,
            roundTrip,
            distance,
            amount: this.getClaimableAmount() * 100,
            from: startLocText || travelLocStartMarker.title,
            to: destinationLocText || travelLocDestinationMarker.title,
        };

        const copyItem =
            location &&
            location.pathname === `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts/new/mileage`;
        this.props.handleSave(data, copyItem);
    };

    isSavable = isNew => {
        const { project, status } = this.props.mileageFields;
        const { distance, rate } = this.state;

        if (isNew) return true;
        if (!status || status === 'Ready') return true;
        if (rate === 0) return true;

        return false;
    };

    isDeletable = () => {
        const { allLoaded } = this.props;
        const { status } = this.props.mileageFields;

        if (status !== 'Ready' && status !== 'Attention' && status !== 'Active') return false;
        if (!allLoaded) return false;

        return true;
    };

    toggleAction = () => {
        this.handleToggleModal();
        if (this.props.modalAction === 'redirect')
            history.push(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts`);
    };

    calcCurrentPositiveButtonText() {
        const { modalAction } = this.props;
        if (modalAction === 'save') return 'Save';
        if (modalAction === 'delete') return 'Delete';
        return 'Ok';
    }

    calcCurrentPositiveButtonAction() {
        const { modalAction, refreshList } = this.props;
        if (modalAction === 'save') return this.handleSave;
        if (modalAction === 'delete') return this.props.handleDelete;
        if (modalAction === 'redirect') {
            return () => {
                history.push(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts`);
                if (refreshList) refreshList();
                this.handleToggleModal();
            };
        }
        return this.handleToggleModal;
    }

    handleToggleModal = modalAction => {
        this.props.toggleModal(modalAction);
    };

    getClaimableAmount = () => {
        const { distance, roundTrip, rate } = this.state;
        let claimableAmount = (distance / 1000) * rate;
        if (roundTrip) claimableAmount = claimableAmount * 2;
        return claimableAmount;
    };

    render() {
        const { theqs } = this.state;
        const { location, loading, mileageFields } = this.props;

        const hasQs = theqs && Object.keys(theqs).length !== 0;
        const isNew =
            location &&
            location.pathname === `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts/new/mileage`;

        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        return (
            <div>
                {loading ? (
                    <LoadingSpinner center />
                ) : (
                    <Container key={0} className="mb10 wideContainer">
                        <Row className="mb-3">
                            <Col xs={12}>
                                <Link to={`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts`}>
                                    <i className="align-middle material-icons material-icons-3x">chevron_left</i>
                                </Link>
                            </Col>
                        </Row>
                        <Row>
                            <Fragment>
                                <Col xs="12" md="6" className="my-4">
                                    {this.isSavable(isNew) ? (
                                        <EditForm
                                            {...this.props}
                                            {...this.state}
                                            hasQs={hasQs}
                                            handleDateChangeWithEvent={this.handleDateChangeWithEvent}
                                            handleMileageCategoryChange={this.handleMileageCategoryChange}
                                            handleCheckbox={this.handleCheckbox}
                                            handleRecalculateLocationSwitchChange={this.handleRecalculateLocationSwitchChange}
                                            handleChange={this.props.handleChange}
                                            getClaimableAmount={this.getClaimableAmount}
                                            setStartLocMarker={loc => this.setStartLocMarker(loc)}
                                            onStartLocChange={this.handleStartLocTextChange}
                                            onStartLocClick={this.pinStartLoc}
                                            setDestinationLocMarker={loc => this.setDestinationLocMarker(loc)}
                                            onDestinationLocTextChange={this.handleDestinationLocTextChange}
                                            onDestinationLocClick={this.pinDestinationLoc}
                                        />
                                    ) : (
                                        <ViewForm {...this.props} {...this.state} getClaimableAmount={this.getClaimableAmount} />
                                    )}
                                </Col>
                                <Col xs="12" md="6">
                                    <Map
                                        {...this.props}
                                        {...this.state}
                                        hasQs={hasQs}
                                        initGoogleMaps={this.initGoogleMaps}
                                        handleSwitchChange={this.handleSwitchChange}
                                    />
                                </Col>
                            </Fragment>
                        </Row>

                        <br />
                        <br />
                        <br />
                        <Row>
                            <Col xs="12" sm="6" className="wideContainer">
                                <Row>
                                    {this.isSavable(isNew) ? (
                                        <Col xs="12" sm="6">
                                            <Button
                                                className={isMobile ? '  w-100p mb10' : 'w-100p  mb10 '}
                                                onClick={() => this.handleToggleModal('save')}
                                            >
                                                Save
                                            </Button>
                                        </Col>
                                    ) : null}
                                    {this.isDeletable(isNew) &&
                                    hasQs &&
                                    location.pathname !==
                                        `/${
                                            window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'
                                        }/myreceipts/new/mileage` ? (
                                        <Col xs="12" sm="6">
                                            <Button
                                                className={isMobile ? '  w-100p mb10' : ' w-100p mb10 '}
                                                color="danger"
                                                onClick={() => this.handleToggleModal('delete')}
                                            >
                                                Delete
                                            </Button>
                                        </Col>
                                    ) : null}
                                    {/* {!isNew && mileageFields.id ? (
                                    <Col xs="12" md="6">
                                        <Link
                                            to={withQuery(`/${window.location.pathname.indexOf("mycorp") >= 0 ? 'mycorp' : 'expense'}/myreceipts/new/mileage`, {
                                                receiptid: theqs && theqs.receiptid,
                                                index: theqs && theqs.index,
                                            })}
                                        >
                                            <Button className={isMobile ? "mr-2 w-100p mb10" : "mr-2"} color="info">
                                                Copy Item
                                        </Button>
                                        </Link>
                                    </Col>
                                ) : null} */}
                                </Row>
                            </Col>
                        </Row>

                        <DialogModal
                            key="modal"
                            modal={this.props.modalOpen}
                            toggle={this.toggleAction}
                            onlyOneButton={this.props.saved}
                            buttonDisabler={this.props.saving}
                            customHeader={this.props.modalHeader || 'Confirmation'}
                            customModalBody={this.props.saving ? <LoadingSpinner /> : this.props.modalMessage}
                            positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                            positiveButtonText={this.calcCurrentPositiveButtonText()}
                        />
                    </Container>
                )}
            </div>
        );
    }
}

const Map = props => {
    const { hasQs, handleSwitchChange, showLocations, recalculateLocation, initGoogleMaps } = props;

    return (
        <div
            className={!hasQs || recalculateLocation ? null : 'd-none'}
            style={{
                height: '100%',
                width: '100%',
                minHeight: '400px',
            }}
        >
            <GoogleMapReact
                defaultZoom={11}
                resetBoundsOnResize
                yesIWantToUseGoogleMapApiInternals
                center={{ lat: 1.3521, lng: 103.8198 }}
                onGoogleApiLoaded={({ map, maps }) => initGoogleMaps(map, maps)}
                bootstrapURLKeys={{
                    key: 'AIzaSyBr4vviN07QGsh6OtH0mvRd9RRWL9YtSWo',
                }}
            />
            <CyderInput label="Show Locations" formGroupClassName="m-0">
                <Switch
                    height={24}
                    id="showLocations"
                    className="react-switch"
                    uncheckedIcon={null}
                    onChange={handleSwitchChange}
                    checked={showLocations}
                />
            </CyderInput>
        </div>
    );
};

const EditForm = props => {
    const {
        hasQs,
        handleDateChangeWithEvent,
        handleMileageCategoryChange,
        handleRecalculateLocationSwitchChange,
        handleCheckbox,
        getClaimableAmount,
        recalculateLocation,
        roundTrip,
        handleChange,
        mileageFields,
        projects,
        mileageCategories,
        featureRT,
    } = props;
    const { setStartLocMarker, onStartLocChange, onStartLocClick, startLocText, pinningStartLoc } = props;
    const { setDestinationLocMarker, onDestinationLocTextChange, onDestinationLocClick, destinationLocText, pinningDestinationLoc } = props;

    return (
        <Form>
            <Row>
                <Col xs="12" sm="6">
                    <CyderInput mandatory label="Travel Date " formGroupClassName="m-0">
                        <CyderDatePicker
                            value={mileageFields.expensedate}
                            placeholder="DD/MM/YYYY"
                            dateFormat="DD/MM/YYYY"
                            onChange={event => handleDateChangeWithEvent(event, 'expensedate')}
                        />
                    </CyderInput>
                </Col>
                <Col xs="12" sm="6">
                    <CyderInput label="Start & End Time" formGroupClassName="m-0">
                        <Row>
                            <Col xs="6">
                                <CyderDatePicker
                                    id="traveltimestart"
                                    placeholder="HH:mm"
                                    timeFormat="HH:mm"
                                    dateFormat={false}
                                    viewMode="time"
                                    value={mileageFields.traveltimestart}
                                    onChange={event => handleDateChangeWithEvent(event, 'traveltimestart', true)}
                                />
                            </Col>
                            <Col xs="6">
                                <CyderDatePicker
                                    id="traveltimeend"
                                    placeholder="HH:mm"
                                    timeFormat="HH:mm"
                                    dateFormat={false}
                                    viewMode="time"
                                    value={mileageFields.traveltimeend}
                                    onChange={event => handleDateChangeWithEvent(event, 'traveltimeend', true)}
                                />
                            </Col>
                        </Row>
                    </CyderInput>
                </Col>
            </Row>
            <LocationInput
                label="From"
                setLoc={setStartLocMarker}
                onChange={onStartLocChange}
                onClick={onStartLocClick}
                value={startLocText}
                color={pinningStartLoc ? 'primary' : 'secondary'}
            />
            <LocationInput
                label="To"
                setLoc={setDestinationLocMarker}
                onChange={onDestinationLocTextChange}
                onClick={onDestinationLocClick}
                value={destinationLocText}
                color={pinningDestinationLoc ? 'primary' : 'secondary'}
            />
            <Row>
                <Col xs="12" sm="6">
                    <CyderSelect
                        mandatory
                        label="Project"
                        inputProps={{
                            name: 'project',
                            options: projects,
                            value: projects.find(x => x.code === mileageFields.project),
                            getOptionValue: option => option.code,
                            getOptionLabel: option => option.name,
                            onChange: value => {
                                handleChange({
                                    target: {
                                        id: 'project',
                                        value: value.code,
                                    },
                                });
                            },
                        }}
                    />
                </Col>
                <Col xs="12" sm="6">
                    <CyderSelect
                        mandatory
                        label="Travel Mode "
                        inputProps={{
                            name: 'category',
                            options: mileageCategories,
                            value: mileageCategories.find(x => x.code === mileageFields.category),
                            getOptionValue: option => option.code,
                            getOptionLabel: option => option.name,
                            onChange: value => {
                                handleMileageCategoryChange({
                                    target: {
                                        id: 'category',
                                        value: value.code,
                                    },
                                });
                            },
                        }}
                    />
                </Col>
            </Row>
            <CyderInput
                label="Remarks"
                inputProps={{
                    type: 'textarea',
                    id: 'remarks',
                    name: 'remarks',
                    onChange: handleChange,
                    value: mileageFields.remarks,
                }}
            />
            {featureRT ? (
                <FormGroup check>
                    <Label className="text-bold">
                        <Input id="roundTrip" type="checkbox" checked={roundTrip} onChange={handleCheckbox} /> Round Trip
                    </Label>
                </FormGroup>
            ) : null}
            {hasQs ? (
                <CyderInput label="Recalculate Location " formGroupClassName="m-0">
                    <Switch
                        height={24}
                        id="recalculateLocation"
                        className="react-switch"
                        uncheckedIcon={null}
                        checked={recalculateLocation}
                        onChange={handleRecalculateLocationSwitchChange}
                    />
                </CyderInput>
            ) : null}
            <CyderInput label="Reimbursable" formGroupClassName="m-0">
                <Switch
                    height={24}
                    id="reimbursable"
                    className="react-switch"
                    uncheckedIcon={null}
                    checked={mileageFields.reimbursable}
                    onChange={checked => handleChange(controlledInputLib.generateSwitchSynthEvent(checked, 'reimbursable'))}
                />
            </CyderInput>
            <CalculateDetails {...props} claimableAmount={getClaimableAmount()}></CalculateDetails>
        </Form>
    );
};

const ViewForm = props => {
    const { getClaimableAmount, roundTrip, mileageFields, projects, featureRT } = props;
    const { startLocText } = props;
    const { destinationLocText } = props;

    const project = projects.find(x => x.code === mileageFields.project) || {};

    return (
        <Form>
            <Row>
                <Col xs="12" sm="6">
                    <InfoField label="Travel Date" value={mileageFields.expensedate || '-'} />
                </Col>
                <Col xs="12" sm="6">
                    <InfoField
                        label="Start & End Time"
                        value={`${mileageFields.traveltimestart || '-'} to ${mileageFields.traveltimeend || '-'}`}
                    />
                </Col>
            </Row>
            <InfoField label="From" value={startLocText || '-'} />
            <InfoField label="To" value={destinationLocText || '-'} />
            <Row>
                <Col xs="12" sm="6">
                    <InfoField
                        label="Project"
                        value={(project.name && `${project.name} ${mileageFields.project}`) || mileageFields.project || '-'}
                    />
                </Col>
                <Col xs="12" sm="6">
                    <InfoField label="Travel Mode" value={mileageFields.category || '-'} />
                </Col>
            </Row>
            <InfoField label="Remarks" value={mileageFields.remarks || '-'} />
            {featureRT ? <InfoField label="Round Trip" value={roundTrip ? 'Yes' : 'No'} /> : null}
            <InfoField label="Reimbursable" value={mileageFields.reimbursable ? 'Yes' : 'No'} />
            <CalculateDetails {...props} claimableAmount={getClaimableAmount()}></CalculateDetails>
        </Form>
    );
};

const CalculateDetails = props => {
    const { claimableAmount, rate, rateunit, distance, roundTrip } = props;
    console.log('props: ', props);

    const distanceText = `${distance / 1000} km ${roundTrip ? 'one way' : ''}`;
    const claimableAmountText = rate === 0 ? 'Please select Travel Mode' : `S$${claimableAmount.toFixed(2)}`;

    return (
        <Fragment>
            <div className="py-1">
                <div className="font-weight-bold text-muted my-1">Current Rate:</div>
                <h5 className="font-weight-bold">
                    S${rate.toFixed(2)} / {rateunit}
                </h5>
            </div>
            {distance ? (
                <Row>
                    <Col xs="12" sm="6">
                        <div className="py-1">
                            <div className="font-weight-bold text-muted my-1">Distance: </div>
                            <h5>
                                <b>{distanceText}</b>
                            </h5>
                        </div>
                    </Col>
                    <Col xs="12" sm="6">
                        <div className="py-1">
                            <div className="font-weight-bold text-muted my-1">Claimable Amount: </div>
                            <h5>
                                <b>{claimableAmountText}</b>
                            </h5>
                        </div>
                    </Col>
                </Row>
            ) : null}
        </Fragment>
    );
};

const LocationInput = props => {
    const { label, setLoc, onChange, value, onClick, color } = props;
    return (
        <FormGroup>
            <Label className="text-bold">
                {label} <span className="text-danger">*</span>
            </Label>
            <InputGroup>
                <SearchBox
                    setLoc={setLoc}
                    inputProps={{
                        className: 'form-control',
                        placeholder: 'Starting Location',
                        onChange,
                        value,
                    }}
                />
                <InputGroupAddon addonType="append">
                    <Button onClick={onClick} color={color}>
                        <i className="align-middle material-icons m-0">my_location</i>
                    </Button>
                </InputGroupAddon>
            </InputGroup>
        </FormGroup>
    );
};

const mapStateToProps = state => {
    return {
        ...state.cyderExpenseNewMileageClaimReducer,
        ...state.cyderExpenseSharedReducer,
        loading: state.cyderExpenseNewMileageClaimReducer.loading,
        featureRT: state.cyderSysparamReducer.featureRT,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        toggleModal: modalAction => {
            dispatch(expenseNewMileageClaimToggleModal(modalAction || null));
        },
        handleChange: event => {
            const { id, value } = event.target;
            dispatch(expenseNewMileageClaimChangeField(id, value));
        },
        handleDelete: () => {
            dispatch(expenseNewMileageClaimDeleteSavedReceipt());
        },
        resetState: () => {
            return dispatch(expenseNewMileageClaimResetFields());
        },
        getSysparamByCode: code => {
            return dispatch(getSysparamByCode(code));
        },
        handleSave: (data, copyItem) => dispatch(expenseNewMileageClaimSaveMileage(data, copyItem)),
        setJumbotronTitle: title => dispatch(setJumbotronTitle(title || 'New Mileage Claim')),
        getRelevantStuff: receiptid => {
            dispatch(expenseNewMileageClaimForceLoadingState(true));
            return Promise.all([
                dispatch(expenseSharedGetMileageCategories()),
                dispatch(expenseSharedGetLocations()),
                dispatch(expenseSharedGetProjects()),
                dispatch(getSysparamByCode('RT')),
            ])
                .then(() => {
                    // will use receipt info from myReceipts list if exists on client
                    if (receiptid) return dispatch(expenseNewMileageClaimSetFields(receiptid));
                })
                .then(res => {
                    dispatch(expenseNewMileageClaimForceLoadingState(false));
                    dispatch(expenseSharedLoadedData());
                    return res;
                });
        },
    };
};

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(StoredLayout(ExpenseMileage)));
