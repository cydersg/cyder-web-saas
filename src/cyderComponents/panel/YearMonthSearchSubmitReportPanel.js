import React, { Fragment, useState, useEffect } from 'react';
import { Container, Row, Col, Button, Label, Input } from 'reactstrap';
import { CyderSelect } from 'cyderComponents/input/index';
import { getMonthOptions, getYearOptions } from 'js/generalUtils';
import dateutils from 'js/dateutils';
import ButtonPrimary from 'cyderComponents/buttons/ButtonPrimary';
import ButtonSecondary from 'cyderComponents/buttons/ButtonSecondary';
import { layoutLib } from 'js/constlib';

const defaultOption = { value: 0, label: 'None' };

const YearMonthSearchSubmitReportPanel = props => {
    const { onSearch, minYear, year, onSubmit } = props;
    const [yearValue, setYearValue] = useState(defaultOption);
    const yearOptions = React.useMemo(() => getYearOptions(minYear), [minYear]);

    // const onSearchClicked = () => {
    //     if (!onSearch) return;
    //     onSearch(yearValue);
    // };

    useEffect(() => {
        const defaultYear = yearOptions.find(x => x.value === parseInt(year));
        setYearValue(defaultYear);
    }, [year]);

    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return (
        <Fragment>
            <div className="w-100p">
                {isMobile ? (
                    <Row>
                        <Col xs="12" className="mb10">
                            <ButtonPrimary normalized onClick={onSubmit} faIcon="upload" label="New Report" />
                        </Col>
                        <Col xs="12" className="mb10 ph0">
                            <CyderSelect
                                // label="Year"
                                formGroupClassName="m-0"
                                inputProps={{
                                    options: yearOptions,
                                    placeholder: 'Filter by year',
                                    value: yearValue,
                                    onChange: e => {
                                        setYearValue(e);
                                        onSearch(e);
                                    },
                                }}
                            />
                        </Col>
                    </Row>
                ) : (
                    <div
                        style={{
                            display: 'flex',
                        }}
                    >
                        <div
                            style={{
                                width: 150,
                                marginRight: 10,
                            }}
                        >
                            <CyderSelect
                                // label="Year"
                                formGroupClassName="m-0"
                                inputProps={{
                                    options: yearOptions,
                                    placeholder: 'Filter by year',
                                    value: yearValue,
                                    onChange: e => {
                                        setYearValue(e);
                                        onSearch(e);
                                    },
                                }}
                            />
                        </div>
                        <div>
                            <ButtonPrimary onClick={onSubmit} faIcon="upload" label="Submit New Report" />
                        </div>
                    </div>
                )}
            </div>
        </Fragment>
    );
};

export default YearMonthSearchSubmitReportPanel;
