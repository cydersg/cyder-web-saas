import React from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';

import ExpenseSummaryReportTable from 'cyderComponents/table/ExpenseSummaryReportTable';
import ExpenseSummaryReportSearchPanel from 'cyderComponents/panel/ExpenseSummaryReportSearchPanel';
import { setJumbotronTitle } from 'actions/pagedata';
import {
    retrieveSummaryReportAction,
    updateReceiptByProject,
    expenseLoadingShow,
    expenseLoadingHide,
    expenseResetForm,
} from 'actions/report/expenseReportAction';

class ReceiptByProject extends React.Component {
    componentDidMount() {
        this.props.load();
    }
    render() {
        return (
            <div className="wideContainer">
                <ExpenseSummaryReportSearchPanel search={this.props.search} />
                <ExpenseSummaryReportTable loading={this.props.report.loading} reports={this.props.report.receiptByProject} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        report: state.cyderReportExpenseReducer,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        load: () => {
            dispatch(expenseResetForm());
            dispatch(expenseLoadingShow());
            var callback = function(error, reportData) {
                dispatch(updateReceiptByProject(reportData));
                dispatch(expenseLoadingHide());
            };
            dispatch(retrieveSummaryReportAction('receipt-by-project', callback));
        },
        search: () => {
            dispatch(expenseLoadingShow());
            var callback = function(error, reportData) {
                dispatch(updateReceiptByProject(reportData));
                dispatch(expenseLoadingHide());
            };
            dispatch(retrieveSummaryReportAction('receipt-by-project', callback));
        },
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Receipts by Project')),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(ReceiptByProject));
