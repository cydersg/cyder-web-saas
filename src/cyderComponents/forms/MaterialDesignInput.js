/**
 * Cyder Input Group
 *
 * Author: Fernando
 */

import React from 'react';
import { connect } from 'react-redux';

const MaterialDesignInput = ({ field, onKeyPress }) => {
    return (
        <div className="mat-input-group">
            <input id={field.id} className="mat-input-input" type={field.type} name={field.name} required onKeyPress={onKeyPress} />
            <span className="highlight" />
            <span className="bar" />
            <label className="mat-input-label font-weight-bold">{field.placeholder || ''}</label>
        </div>
    );
};

const mapStateToProps = (state, ownProps) => {
    return {};
};
const mapDispatchToProps = dispatch => {
    return {};
};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(MaterialDesignInput);
