import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import history from 'history.js';

import { Container, FormGroup, Input, Form, Badge, Row, Col } from 'reactstrap';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import StoredLayout from 'layouts/StoredLayout';
import ReactTable from 'react-table';
import CyderDateRangePicker from 'cyderComponents/forms/CyderDateRangePicker';
import { TablePagination } from 'cyderComponents/pagination/';

import {
    paymentsListGetReports,
    paymentsListRenderReceipts,
    paymentsListDeleteReceipt,
    expenseMyReceiptsToggleModal,
    paymentsListCopyReceipt,
} from 'actions/payments/paymentsListAction.js';
import { expenseSharedGetMileageCategories, expenseSharedGetExpenseCategories } from 'actions/expenses/cyderExpenseClaimSharedAction';
import { setJumbotronTitle } from 'actions/pagedata';

import cyderlib from 'js/cyderlib.js';
import reactTableFilter from 'js/reactTableFilter';

import '../../css/forms/default-forms.css';

class PaymentsListPage extends React.Component {
    constructor(props) {
        super(props);
        this.onFilteredChange = this.onFilteredChange.bind(this);
        this.onRangeDateChange = this.onRangeDateChange.bind(this);
        this.filterAllFunc = this.filterAllFunc.bind(this);
        this.state = {
            filterAll: '',
            startDate: null,
            endDate: null,
        };
    }
    componentDidMount() {
        const { startDate, endDate } = this.state;
        this.props.getReceipts(startDate, endDate);
    }
    tabNav(index, url) {
        history.replace(url);
    }
    onFilteredChange(filtered) {
        this.setState(reactTableFilter.onFilteredChange(filtered));
    }
    filterAllFunc(e) {
        this.setState(reactTableFilter.filterAllFunc(e));
    }
    onRangeDateChange(startDate, endDate) {
        this.setState(
            {
                startDate,
                endDate,
            },
            () => this.props.getReceipts(startDate, endDate),
        );
    }
    render() {
        return (
            <Container>
                <Container className="mt-2">
                    <Row key={0} className="mb-4 pb-4">
                        <Col className="d-flex">
                            <div className="ml-auto">
                                <Form inline>
                                    <FormGroup>
                                        <Input type="select" id="status" onChange={this.props.getReceipts}>
                                            <option>Approved</option>
                                            <option value="PendingPayment">Pending Payment</option>
                                            <option>Paid</option>
                                        </Input>
                                        <CyderDateRangePicker onDateChange={this.onRangeDateChange} />
                                        <Input
                                            className="w-300"
                                            placeholder="Search"
                                            onChange={this.filterAllFunc}
                                            value={this.state.filterAll}
                                        />
                                    </FormGroup>
                                </Form>
                            </div>
                        </Col>
                    </Row>
                    <PaymentsListComponent
                        startDate={this.state.startDate}
                        endDate={this.state.endDate}
                        filtered={this.state.filtered}
                        onFilteredChange={this.onFilteredChange}
                        key={1}
                        getReceipts={this.props.getReceipts}
                        modalAction={this.props.modalAction}
                        modalOpen={this.props.modalOpen}
                        modalHeader={this.props.modalHeader}
                        modalMessage={this.props.modalMessage}
                        saved={this.props.saved}
                        saving={this.props.saving}
                        toggleModal={this.props.toggleModal}
                        handleDelete={this.props.handleDelete}
                        selectedItemsCount={this.props.selectedItemsCount}
                        loading={this.props.loading}
                        reports={this.props.reports}
                        selectedItems={this.props.selectedItems}
                        expenseCategoriesMap={this.props.expenseCategoriesMap}
                        mileageCategoriesMap={this.props.mileageCategoriesMap}
                    />
                </Container>
            </Container>
        );
    }
}

class PaymentsListComponent extends React.Component {
    constructor(props) {
        super(props);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.toggleAction = this.toggleAction.bind(this);
    }
    toggleAction() {
        const { modalAction, startDate, endDate } = this.props;
        this.handleToggleModal();
        if (modalAction === 'refresh') this.props.getReceipts(startDate, endDate);
        if (modalAction === 'redirect') history.push('/expense/myreceipts');
    }
    calcCurrentPositiveButtonText() {
        const { modalAction } = this.props;
        switch (modalAction) {
            case 'save':
                return 'Save';
            case 'delete':
                return 'Delete';
            default:
                return 'Ok';
        }
    }
    calcCurrentPositiveButtonAction() {
        const { modalAction, handleSave, handleDelete, getReceipts, startDate, endDate } = this.props;
        switch (modalAction) {
            case 'delete':
                return handleDelete;
            case 'close':
                return this.handleToggleModal;
            case 'refresh':
                return () => {
                    getReceipts(startDate, endDate);
                    this.handleToggleModal();
                };
            case 'redirect':
                return () => {
                    history.push('/expense/myreceipts');
                    this.handleToggleModal();
                };
            case 'save':
                return () => {
                    let selectedDefaultFixes = {
                        category: document.getElementById('category').value,
                        paymentmode: document.getElementById('paymentmode').value,
                        project: document.getElementById('project').value,
                        currency: this.state.selectedCurrency,
                    };
                    handleSave(selectedDefaultFixes);
                };
            default:
                return this.handleToggleModal;
        }
    }
    handleToggleModal(modalAction, data) {
        this.props.toggleModal(modalAction, data);
    }
    render() {
        const columns = [
            {
                Header: '',
                accessor: 'type',
                width: 50,
                sortable: true,
                filterable: false,
                Cell: ({ value }) => {
                    if (value) return '';
                    const isMileage = value === 'Mileage';
                    return (
                        <div>
                            <i className="align-middle material-icons">{isMileage ? 'directions_car' : 'receipt'}</i>
                        </div>
                    );
                },
            },
            {
                Header: 'Date',
                accessor: 'createddt',
                width: 100,
                filterable: false,
                sortMethod: (a, b, desc) => cyderlib.tableSortDate(a, b, desc, 'DD-MMM-YYYY'),
                Cell: props => moment(props.value).format('DD-MMM-YYYY'),
            },
            {
                Header: 'Report ID',
                accessor: 'reportId',
            },
            {
                accessor: 'remarks',
                Header: 'Particulars',
                Cell: props => props.value,
            },
            {
                className: 'text-right',
                headerClassName: 'text-right',
                Header: 'Total Amount',
                width: 150,
                filterMethod: cyderlib.filterCurrencyCent,
                accessor: 'totalamount',
                Cell: props => 'S$' + (props.value / 100).toFixed(2),
            },
            {
                Header: 'Status',
                minWidth: 80,
                filterable: false,
                filterMethod: cyderlib.filterIgnoreCase,
                accessor: 'status',
                Cell: props => {
                    return <Badge color={cyderlib.statusBadgeColor(props.value)}>{props.value}</Badge>;
                },
            },
            {
                Header: '',
                headerClassName: 'rt-no-sort-ind',
                accessor: 'id',
                sortable: false,
                filterable: false,
                width: 150,
                Cell: props => {
                    const url = `/payments/${props.value}`;
                    return (
                        <Link className="text-bold" to={url}>
                            View
                        </Link>
                    );
                },
            },
            reactTableFilter.generateAllColumnFilter(['remarks', 'category', 'amount', 'status', 'receiptId', 'expensedate'], {
                Mileage: this.props.mileageCategoriesMap,
                Receipt: this.props.expenseCategoriesMap,
            }),
        ];

        const CustomNoData = () => <div style={{ display: 'none' }} />;
        return (
            <Fragment>
                {this.props.loading ? (
                    <LoadingSpinner center />
                ) : (
                    <ReactTable
                        filtered={this.props.filtered}
                        onFilteredChange={this.onFilteredChange}
                        filterable={false}
                        defaultFilterMethod={reactTableFilter.defaultFilterMethod}
                        className="-highlight mb-2"
                        data={this.props.reports}
                        columns={columns}
                        minRows={1}
                        NoDataComponent={CustomNoData}
                        defaultPageSize={10}
                        showPagination={true}
                        PaginationComponent={TablePagination}
                    />
                )}
                <DialogModal
                    key="modal"
                    modal={this.props.modalOpen}
                    toggle={this.toggleAction}
                    customHeader={this.props.modalHeader || 'Confirmation'}
                    customModalBody={this.props.saving ? <LoadingSpinner /> : this.props.modalMessage}
                    positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                    positiveButtonText={this.calcCurrentPositiveButtonText()}
                    onlyOneButton={this.props.saved}
                    buttonDisabler={this.props.saving}
                />
            </Fragment>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        ...state.paymentsListReducer,
        expenseCategoriesMap: state.cyderExpenseSharedReducer.expenseCategoriesMap,
        mileageCategoriesMap: state.cyderExpenseSharedReducer.mileageCategoriesMap,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        toggleModal: (modalAction, data) => {
            dispatch(expenseMyReceiptsToggleModal(modalAction ? modalAction : null, data));
        },
        handleDelete: () => {
            dispatch(paymentsListDeleteReceipt());
        },
        copyItem: index => dispatch(paymentsListCopyReceipt(index)),
        setJumbotronTitle: title => dispatch(setJumbotronTitle(title)),
        getReceipts: (startDate, endDate) => {
            const status = document.getElementById('status').value || 'Completed';
            const tasks = [
                dispatch(expenseSharedGetExpenseCategories()),
                dispatch(expenseSharedGetMileageCategories()),
                dispatch(paymentsListGetReports(status, startDate, endDate)),
            ];
            Promise.all(tasks).then(res => {
                dispatch(paymentsListRenderReceipts(res[2].data, 'reports'));
            });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(PaymentsListPage));
