/**
 * Mileage by Category
 * 
 * Author: Fernando Karnagi
 */
import StoredLayout from '../../layouts/StoredLayout'
import React from 'react'
import { connect } from 'react-redux'

// import history from '../../history' 
import ExpenseSummaryReportTable from '../../cyderComponents/table/ExpenseSummaryReportTable' 
import ExpenseSummaryReportSearchPanel from '../../cyderComponents/panel/ExpenseSummaryReportSearchPanel'
  
import { setJumbotronTitle } from '../../actions/pagedata'
import { retrieveSummaryReportAction, updateMileageByCategory, expenseLoadingShow, expenseLoadingHide, expenseResetForm }
    from '../../actions/report/expenseReportAction' 

class MileageByCategory extends React.Component {

    componentDidMount() {
        this.props.load();
    }

    render() {
        return (
            <div>
                <ExpenseSummaryReportSearchPanel search={this.props.search}/>
                <ExpenseSummaryReportTable loading={this.props.report.loading} reports={this.props.report.mileageByCategory}/>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        report: state.cyderReportExpenseReducer
    }
}
const mapDispatchToProps = (dispatch) => {
    return { 
        load: () => {
            dispatch(expenseLoadingShow());
            dispatch(expenseResetForm());
            var callback = function(error, reportData) {
                dispatch(updateMileageByCategory(reportData));
                dispatch(expenseLoadingHide());
            }
 
            dispatch(retrieveSummaryReportAction('mileage-by-category', callback));
        },
        search: () => {
            dispatch(expenseLoadingShow());
            var callback = function(error, reportData) {
                dispatch(updateMileageByCategory(reportData));
                dispatch(expenseLoadingHide());
            } 
            dispatch(retrieveSummaryReportAction('mileage-by-category', callback));
        },
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Mileages by Category'))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(MileageByCategory))