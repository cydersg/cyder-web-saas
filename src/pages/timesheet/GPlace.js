import React, { useEffect, useRef, useState } from 'react';
import { Input } from 'reactstrap';

const GPlace = ({ currentPlace, setChosenLocation }) => {
    const placeInputRef = useRef(null);
    const [place, setPlace] = useState(null);

    useEffect(() => {
        initPlaceAPI();
    }, []);

    useEffect(() => {
        setPlace(currentPlace);
    }, [currentPlace]);

    useEffect(() => {
        if (place && place.lat && place.lng) {
            setChosenLocation(place);
        }
    }, [place]);

    // initialize the google place autocomplete
    const initPlaceAPI = () => {
        var options = {
            componentRestrictions: { country: 'sg' },
        };
        let autocomplete = new window.google.maps.places.Autocomplete(placeInputRef.current, options);
        new window.google.maps.event.addListener(autocomplete, 'place_changed', function() {
            let place = autocomplete.getPlace();
            setPlace({
                address: place.formatted_address,
                name: place.name,
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng(),
            });
        });
    };

    return (
        <>
            <input
                className="form-control"
                value={place && place.name}
                onChange={e => {
                    setPlace(e.target.value);
                }}
                type="text"
                ref={placeInputRef}
            />
            {place && (
                <div style={{ marginTop: 5, lineHeight: '20px', fontSize: 12 }}>
                    {place.name && (
                        <div>
                            <b>{place.name}</b>
                        </div>
                    )}
                    <div>{place.address}</div>
                </div>
            )}
        </>
    );
};

export default GPlace;
