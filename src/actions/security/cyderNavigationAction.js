/**
 * Navigation action creators
 *
 * Author: Fernando
 */

import { SET_NAVIGATION } from '../constants/actionTypes';

export function cyderNavigationAction(url) {
    return {
        type: SET_NAVIGATION,
        url,
    };
}
