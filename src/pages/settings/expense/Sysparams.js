import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import i18next from 'i18next';
import { withTranslation, Trans } from 'react-i18next';

import StoredLayout from 'layouts/StoredLayout';
import { Input, Table, Button, Container, Row, Col, Form, FormGroup, Label } from 'reactstrap';

import {
    cyderSysparamSaveAction,
    cyderSysparamGetAction,
    cyderSysparamEditModalChangeValueAction,
    cyderSysparamEditModalToggle,
    cyderSysparamEditModalSetSysparam,
} from 'actions/settings/cyderSysparamAction';
import DialogModal from 'cyderComponents/modals/DialogModal';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

class SysparamsPage extends React.Component {
    constructor(props) {
        super(props);
        this.hugeAmountCheckboxOnChange = this.hugeAmountCheckboxOnChange.bind(this);
        this.state = {
            hugeAmountMax: false,
        };
    }
    componentDidMount() {
        this.props.getSysparams(null);
    }
    setupEditModal(sysparam) {
        this.props.setModalSysparam(sysparam.code, sysparam.label, sysparam.val, sysparam.type);
        this.props.toggleModal();
        if (sysparam.code === 'EXPENSE_HUGE_AMOUNT') {
            this.setState({ hugeAmountMax: sysparam.val === '9999999' });
        }
    }
    hugeAmountCheckboxOnChange() {
        this.setState({ hugeAmountMax: !this.state.hugeAmountMax });
        this.props.simpleChangeModalTextAreaValue('9999999');
    }
    render() {
        const { t } = this.props;
        const customHeader = 'Update System Setting';
        const customModalBody = (
            <Form>
                <FormGroup>
                    <Label>{this.props.editModalSysparamLabel}</Label>
                    {this.props.editModalSysparamType === 'boolean' ? (
                        <Input
                            onChange={event => this.props.changeModalTextAreaValue(event)}
                            type="select"
                            value={this.props.editModalSysparamVal}
                        >
                            <option>Enabled</option>
                            <option>Disabled</option>
                        </Input>
                    ) : (
                        <Input
                            disabled={this.props.editModalSysparamCode === 'EXPENSE_HUGE_AMOUNT' && this.state.hugeAmountMax}
                            onChange={event => this.props.changeModalTextAreaValue(event)}
                            type="textarea"
                            maxLength={500}
                            value={this.props.editModalSysparamVal}
                        />
                    )}
                </FormGroup>
                {this.props.editModalSysparamCode === 'EXPENSE_HUGE_AMOUNT' ? (
                    <FormGroup check>
                        <Label check>
                            <Input type="checkbox" checked={this.state.hugeAmountMax} onChange={this.hugeAmountCheckboxOnChange} />
                            Disable this feature
                        </Label>
                    </FormGroup>
                ) : null}
            </Form>
        );
        const trMap = this.props.sysparams.map((item, i) => {
            // TODO: might want validation later
            // const valid = cyderlib.validate(this.props.sysparams[i].val, 'alphanumeric');
            return (
                <tr key={i}>
                    {/* <td>{item.section}</td> */}
                    <td>{item.label}</td>
                    <td>{this.props.sysparams[i].val}</td>
                    <td>
                        <Button
                            tabIndex="-1"
                            className="mx-1"
                            onClick={() => {
                                this.setupEditModal(this.props.sysparams[i]);
                            }}
                        >
                            edit
                        </Button>
                    </td>
                </tr>
            );
        });

        if (this.props.loading) return <LoadingSpinner center />;
        return (
            <Fragment>
                <div key={0}>
                    <Container>
                        <Row>
                            <Col xs={12}>
                                <Table responsive hover>
                                    <thead>
                                        <tr>
                                            {/* <th><Trans>Section</Trans></th> */}
                                            <th>
                                                <Trans>Label</Trans>
                                            </th>
                                            <th>{t('Value')}</th>
                                            <th>
                                                <Trans>Action</Trans>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>{trMap}</tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <DialogModal
                    key="modal"
                    modal={this.props.editModalOpen}
                    toggle={this.props.toggleModal}
                    customHeader={customHeader}
                    customModalBody={customModalBody}
                    positiveButtonAction={() => this.props.save(this.props.editModalSysparamCode, this.props.editModalSysparamVal)}
                    positiveButtonText="Save"
                    buttonDisabler={this.props.savingSysparam}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        editModalOpen: state.cyderSysparamReducer.editModalOpen,
        editModalSysparamVal: state.cyderSysparamReducer.editModalSysparamVal,
        editModalSysparamCode: state.cyderSysparamReducer.editModalSysparamCode,
        editModalSysparamLabel: state.cyderSysparamReducer.editModalSysparamLabel,
        editModalSysparamType: state.cyderSysparamReducer.editModalSysparamType,
        loading: state.cyderSysparamReducer.loading,
        savingSysparam: state.cyderSysparamReducer.savingSysparam,
        sysparams: state.cyderSysparamReducer.sysparams,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        setModalSysparam: (code, label, val, type) => {
            dispatch(cyderSysparamEditModalSetSysparam(code, label, val, type));
        },
        toggleModal: () => {
            dispatch(cyderSysparamEditModalToggle());
        },
        save: (code, val) => {
            const body = {
                code,
                val: val.trim(),
            };
            const cb = function() {
                dispatch(cyderSysparamEditModalToggle());
                dispatch(cyderSysparamGetAction(null));
            };
            dispatch(cyderSysparamSaveAction(body, cb));
        },
        simpleChangeModalTextAreaValue: newValue => {
            dispatch(cyderSysparamEditModalChangeValueAction(newValue));
        },
        changeModalTextAreaValue: event => {
            const newValue = event.target.value;
            dispatch(cyderSysparamEditModalChangeValueAction(newValue));
        },
        change: (someparam, index) => {
            if (i18next.language === 'en') {
                i18next.changeLanguage('ch');
            } else {
                i18next.changeLanguage('en');
            }
            let action = {
                type: 'TEST',
                someparam: someparam,
                index: index,
            };
            dispatch(action);
        },
        getSysparams: (someparam = null) => {
            dispatch(cyderSysparamGetAction(someparam));
        },
    };
};
// withTranslation() is if we want to use HOC to perform t()
export default withTranslation()(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(StoredLayout(SysparamsPage)),
);
