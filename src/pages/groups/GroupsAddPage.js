import StoredLayout from '../../layouts/StoredLayout';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Async } from 'react-select';
// import history from '../../history'
// import cyderlib from '../../js/cyderlib'
import debounce from 'es6-promise-debounce';
import {
    Input,
    // Table,
    Button,
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Label,
    FormFeedback,
    ListGroup,
    ListGroupItem,
} from 'reactstrap';
import {
    withTranslation,
    // Trans
} from 'react-i18next';

import {
    changeFieldValueAction,
    cyderGroupsAddForceValidate,
    cyderGroupsAddModalToggleAction,
    cyderGroupsAddClearFields,
    cyderGroupsAddAddUser,
    cyderGroupsAddRemoveUser,
    cyderGroupsAddAddGroup,
    cyderGroupsAddAddUserSearchAction,
} from '../../actions/groups/cyderGroupsAddAction';
import { setJumbotronTitle } from '../../actions/pagedata';
import LoadingSpinner from '../../cyderComponents/loadingSpinner/LoadingSpinner';
// import CyderDatePicker from '../../cyderComponents/forms/CyderDatePicker';
import DialogModal from '../../cyderComponents/modals/DialogModal';

// import cyderlib from '../../js/cyderlib';

class GroupsAdd extends React.Component {
    constructor(props) {
        super(props);
        this.getOptions = this.getOptions.bind(this);
        this.filterOptions = this.filterOptions.bind(this);
    }

    saveOnDialogButtonClick() {
        let data = Object.assign({}, this.props.fields);
        const users = this.props.users.concat();
        const usersList = [];
        users.forEach(user => {
            const userObj = { username: user };
            usersList.push(userObj);
        });
        data.usersList = usersList.concat();
        this.props.addGroup(data);
    }
    handleSave() {
        // put all of these into a function, to be executed after forcing validation
        const save = () => {
            for (let key in this.props.validation) {
                if (this.props.validation[key].approved === false) {
                    return false;
                }
            }
            this.props.toggleModalAndReset();
            return;
        };
        this.props.forceValidate(save);
    }
    componentDidMount() {
        this.props.setJumbotronTitle();
    }
    handleDOBChangeWithEvent(momentOrStringVal) {
        const synthEvent = {
            target: {
                id: 'dateofbirth',
                value: momentOrStringVal.format ? momentOrStringVal.format('DD/MM/YYYY') : momentOrStringVal,
            },
        };
        this.props.handleChange(synthEvent);
    }
    getOptions(input) {
        // const username = document.getElementById('searchUsername').value;
        return this.props.getOptions(input);
    }
    filterOptions(options) {
        const results = options.filter(item => !this.props.usersReference[item.value]);
        return results;
    }
    render() {
        // TODO: transltae
        // for translate later
        // const { t } = this.props
        return [
            <Container key={0}>
                <Row className="mb-2">
                    <Col>
                        <Link to="/administration/groups" tabIndex="-1">
                            Back To Search
                        </Link>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <Form onSubmit={e => e.preventDefault()}>
                            <FormGroup row>
                                <Label sm={2}>Group Name</Label>
                                <Col sm={10}>
                                    <Input
                                        value={this.props.fields['groupname'] ? this.props.fields['groupname'] : ''}
                                        id="groupname"
                                        valid={
                                            this.props.saveButtonPressed
                                                ? this.props.validation['groupname'].approved
                                                : !this.props.fields['groupname']
                                                ? null
                                                : this.props.validation['groupname']
                                                ? this.props.validation['groupname'].approved
                                                : false
                                        }
                                        onChange={event => this.props.handleChange(event)}
                                    />
                                    <FormFeedback>
                                        {this.props.saveButtonPressed ||
                                        (this.props.fields['groupname'] && this.props.validation['groupname'])
                                            ? this.props.validation['groupname'].errors[0]
                                            : null}
                                    </FormFeedback>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={2}>Group Description</Label>
                                <Col sm={10}>
                                    <Input
                                        value={this.props.fields['groupdescription'] ? this.props.fields['groupdescription'] : ''}
                                        id="groupdescription"
                                        valid={
                                            this.props.saveButtonPressed
                                                ? this.props.validation['groupdescription'].approved
                                                : !this.props.fields['groupdescription']
                                                ? null
                                                : this.props.validation['groupdescription']
                                                ? this.props.validation['groupdescription'].approved
                                                : false
                                        }
                                        onChange={event => this.props.handleChange(event)}
                                    />
                                    <FormFeedback>
                                        {this.props.saveButtonPressed ||
                                        (this.props.fields['groupdescription'] && this.props.validation['groupdescription'])
                                            ? this.props.validation['groupdescription'].errors[0]
                                            : null}
                                    </FormFeedback>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Label>
                                    <h6>{this.props.users ? this.props.users.length : 0} Users</h6>
                                </Label>
                                <Async
                                    placeholder="Search user to add..."
                                    autoload={false}
                                    onChange={val => {
                                        if (val.length !== 0) this.props.addUser(val);
                                    }}
                                    name="async"
                                    filterOptions={this.filterOptions}
                                    loadOptions={debounce(this.getOptions, 500)}
                                />
                            </FormGroup>
                        </Form>
                        <ListGroup>
                            {this.props.users.map((user, i) => {
                                return (
                                    <ListGroupItem className="justify-content-between" key={i}>
                                        <span className="lh-2-1-rem">{user}&nbsp;</span>
                                        <Button
                                            size="sm"
                                            onClick={() => this.props.removeUser(user)}
                                            style={{ padding: '5px', float: 'right' }}
                                            color="danger"
                                        >
                                            Remove
                                        </Button>
                                    </ListGroupItem>
                                );
                            })}
                        </ListGroup>
                    </Col>
                </Row>
                <Row className="my-2">
                    <Col>
                        <Button
                            className="mr-2"
                            color="success"
                            disabled={this.props.loading}
                            onClick={() => this.handleSave()}
                            tabIndex="-1"
                        >
                            Save
                        </Button>
                    </Col>
                </Row>
            </Container>,
            <DialogModal
                key="modal"
                modal={this.props.modalOpen}
                toggle={() => this.props.toggleModal(this.props.saved, this.props.match.params.groupid)}
                customHeader={this.props.messageModalHeader}
                customModalBody={this.props.saving ? <LoadingSpinner /> : this.props.messageModalMessage}
                positiveButtonAction={!this.props.saved ? () => this.saveOnDialogButtonClick() : () => this.props.toggleModal()}
                positiveButtonText={this.props.saved ? 'Ok' : 'Save'}
                onlyOneButton={this.props.saved}
                buttonDisabler={this.props.saving}
            />,
        ];
    }
}

const mapStateToProps = state => {
    return {
        ...state.cyderGroupsAddReducer,
        fakeUserData: state.cyderFakeUserReducer.userData,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        handleChange: event => {
            const key = event.target.id;
            const value = event.target.value;
            dispatch(changeFieldValueAction(key, value));
        },
        forceValidate: cb => {
            dispatch(cyderGroupsAddForceValidate()).then(() => {
                // execute callback after async action is completed
                cb();
            });
        },
        clearFields: () => {
            dispatch(cyderGroupsAddClearFields());
        },
        addUserOld: e => {
            e.preventDefault();
            const username = document.getElementById('searchUsername').value;
            // TODO: validation, etc. here
            document.getElementById('searchUsername').value = '';
            dispatch(cyderGroupsAddAddUser(username));
        },
        addUser: val => {
            if (val === null) return;
            const username = val.value;
            dispatch(cyderGroupsAddAddUser(username));
        },
        removeUser: username => {
            dispatch(cyderGroupsAddRemoveUser(username));
        },
        addGroup: group => {
            dispatch(cyderGroupsAddAddGroup(group));
        },
        toggleModalAndReset: () => {
            dispatch(cyderGroupsAddModalToggleAction(true));
        },
        toggleModal: () => {
            dispatch(cyderGroupsAddModalToggleAction());
        },
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Add New Group')),
        getOptions: input => {
            // const username = document.getElementById('searchUsername').value;
            return dispatch(cyderGroupsAddAddUserSearchAction(input));
        },
    };
};
// withTranslation() is if we want to use HOC to perform t()
export default withTranslation()(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(StoredLayout(GroupsAdd)),
);
