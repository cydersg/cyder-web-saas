/**
 * Report Table.
 * Common table used for reporting
 *
 * Author: Fenando Karnagi
 */
import React from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import '../../css/forms/default-forms.css';
import { expenseDeletableStatuses, urlRecurring, urlReceipt, layoutLib } from 'js/constlib';
var numeral = require('numeral');
class ExpenseSummaryReportTable extends React.Component {
    render() {
        const data = this.props.reports;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        const columns = [
            {
                Header: 'Category',
                accessor: 'category',
                show: !isMobile,
                filterMethod: (filter, row) => {
                    return row[filter.id].toLowerCase().indexOf(filter.value.toLowerCase()) !== -1;
                },
            },
            {
                Header: 'Currency',
                width: 100,
                show: !isMobile,
                accessor: 'currency',
                filterMethod: (filter, row) => {
                    return row[filter.id].toLowerCase().indexOf(filter.value.toLowerCase()) !== -1;
                },
            },
            {
                className: 'text-right',
                Header: 'Total Amount',
                show: !isMobile,
                width: 200,
                filterMethod: (filter, row) => {
                    const moddedAmount = 'S$' + (row.totalAmount / 100).toFixed(2);
                    return moddedAmount.indexOf(filter.value) !== -1;
                },
                accessor: 'totalAmount',
                Cell: props => 'S$' + numeral((props.value / 100).toFixed(2)).format('0,0.00'),
            },
            {
                Header: '',
                accessor: 'id',
                filterable: false,
                sortable: false,
                show: isMobile,
                Cell: props => {
                    return (
                        <div className="boxShadow">
                            <div>
                                <strong>{props.original.category}</strong>
                            </div>
                            <br />
                            <span className="text-right">
                                {'S$' + numeral((props.original.totalAmount / 100).toFixed(2)).format('0,0.00')}
                            </span>
                        </div>
                    );
                },
            },
        ];

        const CustomNoData = () => <div style={{ display: 'none' }} />;
        if (this.props.loading) return <LoadingSpinner center />;
        return (
            <div key="rt-table">
                <ReactTable
                    className="-highlight mb-2"
                    data={data}
                    columns={columns}
                    minRows={1}
                    NoDataComponent={CustomNoData}
                    filterable={data && data.length > 0}
                    showPagination={false}
                />
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        ownProps,
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ExpenseSummaryReportTable);
