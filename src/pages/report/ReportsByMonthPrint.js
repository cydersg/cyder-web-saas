import React, { Component } from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';
import ReportsByMonthPanel from '../expenses/reports/ReportsByMonthPanel';
import { setConfig } from 'actions/config';

class ReportsByMonthPrint extends Component {
    componentWillMount() {
        this.props.showPwaView(true);
    }

    componentWillUnmount() {
        this.props.showPwaView(false);
    }

    render() {
        return <ReportsByMonthPanel mode="admin" {...this.props} />;
    }
}

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {
        showPwaView: flag => dispatch(setConfig('pwaView', flag)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(ReportsByMonthPrint));
