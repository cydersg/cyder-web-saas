import React, { Fragment, Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import classnames from 'classnames';

import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import StoredLayout from 'layouts/StoredLayout';
import { Row, Col, Container, Button, FormGroup, Label, Input, Card, CardBody, Badge } from 'reactstrap';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';

import { getData, clockIn, clockOut, setLoading } from 'actions/timesheet/clockAction';
import { expenseSharedGetProjects } from 'actions/expenses/cyderExpenseClaimSharedAction';
import { setJumbotronTitle } from 'actions/pagedata';

import cyderlib from 'js/cyderlib';
import { layoutLib } from 'js/constlib';

class ClockPageV2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lat: null,
            lng: null,
            gpsMessage: 'Getting current position ...',
        };
    }

    componentDidMount() {
        this.props.setJumbotronTitle('Check In/Out');
        this.props.getData();

        let myThis = this;

        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                myThis.setState({
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                    gpsMessage: '',
                });
                console.log('Latitude is :', position.coords.latitude);
                console.log('Longitude is :', position.coords.longitude);
            });
        } else {
            console.log('Not Available');
            myThis.setState({
                lat: null,
                lng: null,
                gpsMessage: 'GPS is not available',
            });
        }
    }

    onClockIn = () => {
        const project = document.getElementById('project').value;
        const remarks = document.getElementById('remarks').value;

        if (this.state.lat == null || this.state.lng == null) {
            if (!window.confirm('GPS Location is unavailable, proceed?')) {
                return;
            }
        }
        this.props.clockIn(project, remarks, this.state.lat, this.state.lng);
    };

    render() {
        const { clockOut, projectsMap, activity, forceLoading, projects } = this.props;
        const usedProject = activity.map(x => x.project);
        const filteredProjects = projects.filter(x => !usedProject.includes(x.code));

        if (forceLoading) return <LoadingSpinner center />;
        return (
            <Fragment>
                <Row
                    style={{
                        marginTop: '10px',
                    }}
                >
                    <Col>
                        <Row className="justify-content-center">
                            <Col className="mb-1 pb-1 ph0">
                                <InputPanel onClockIn={this.onClockIn} {...this.state} filteredProjects={filteredProjects} />
                            </Col>
                        </Row>
                        <div className="ml-1 mr-1">
                            {/* <Row className={classnames('clock-indicator-wrapper', { ' justify-content-center': activity.length <= 3 })}> */}
                            <Row className={classnames('clock-indicator-wrapper')}>
                                {activity.map((data, i) => {
                                    const { project } = data;
                                    data.project = projectsMap[project] || project;
                                    return (
                                        <Col key={i} xs="12" md="6" lg="3" className="ph0">
                                            <ClockCard data={data} onClockOut={clockOut} latest={i === 0} />
                                        </Col>
                                    );
                                })}
                                {activity.length === 0 ? <Col className="text-center text-muted">No activities found</Col> : null}
                            </Row>
                        </div>
                    </Col>
                </Row>
            </Fragment>
        );
    }
}

const InputPanel = props => {
    const { onClockIn, filteredProjects, lat, lng, gpsMessage } = props;
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return (
        filteredProjects &&
        filteredProjects.length > 0 && (
            <Fragment>
                <FormGroup>
                    <Label className="text-bold mandatory">
                        Project
                        <span> *</span>
                    </Label>
                    <Input type="select" id="project">
                        {filteredProjects.map((item, i) => (
                            <option key={i} value={item.code}>
                                {item.name}
                            </option>
                        ))}
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label className="text-bold">Remarks</Label>
                    <Input type="textarea" id="remarks" />
                </FormGroup>
                <FormGroup>
                    <Label className="text-bold">GPS Location</Label>
                    <br />
                    {gpsMessage && (
                        <>
                            <Label>{gpsMessage}</Label>
                            <br />
                        </>
                    )}
                    {lat && (
                        <>
                            <Label>Lat: {lat.toFixed(4)}</Label>
                            <br />
                        </>
                    )}
                    {lng && (
                        <>
                            <Label>Lng: {lng.toFixed(4)}</Label>
                            <br />
                        </>
                    )}
                </FormGroup>
                <div>
                    <Button onClick={onClockIn} className={isMobile ? 'mb10 w-100p' : 'mr-2'} color="success">
                        Check-in
                    </Button>
                    <Link to="myclocks">
                        <Button className={isMobile ? 'mb10 w-100p' : 'mr-2'} color="primary">
                            History
                        </Button>
                    </Link>
                </div>
            </Fragment>
        )
    );
};

const ClockCard = props => {
    const { onClockOut, latest } = props;
    const { starttime, status, project, purpose, id, lat, lng } = props.data;

    const momentStartTime = moment(starttime, 'YYYY-MM-DD hh:mm:ss');
    const startTime = momentStartTime.format('h:mm a');

    // Auto clock out after 1 day
    // const diff = momentStartTime.diff(moment(), 'days');
    // if (diff <= -1) onClockOut(id);

    return (
        <Card className="clock-indicator">
            <CardBody>
                <Row>
                    <Col className="d-flex align-items-center ">
                        <Badge color={cyderlib.statusBadgeColor(status)} className="mr-2">
                            {status}
                        </Badge>
                        {latest ? <Badge color="warning">Latest</Badge> : null}
                    </Col>
                    <Col className="text-right clock-indicator-status">
                        <Button color="link" onClick={() => onClockOut(id)}>
                            <i className="material-icons float-right">save_alt</i>
                        </Button>
                    </Col>
                </Row>
                <RowColWrapper>
                    <h6 className="font-weight-bold mt-2">{momentStartTime.format('DD MMM YYYY')}</h6>
                    <p className="font-weight-bold text-muted">
                        Clocked in {momentStartTime.fromNow()}, at {startTime}
                    </p>
                </RowColWrapper>

                {lat && lng && (
                    <RowColWrapper>
                        <h6 className="font-weight-bold mt-2">GPS Location</h6>
                        <p className="font-weight-bold text-muted">
                            Lat {lat}, Lng {lng}
                        </p>
                    </RowColWrapper>
                )}

                <RowColWrapper colProps={{ className: 'my-2' }}>
                    <span className="text-muted font-weight-bold">Project</span>
                    <h5 className="font-weight-bold">{project}</h5>
                </RowColWrapper>
                <RowColWrapper>{purpose}</RowColWrapper>
            </CardBody>
        </Card>
    );
};

const mapStateToProps = state => {
    return {
        ...state.clockReducer,
        projects: state.cyderExpenseSharedReducer.projects,
        projectsMap: state.cyderExpenseSharedReducer.projectsMap,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setJumbotronTitle: title => dispatch(setJumbotronTitle(title)),
        getData: () => {
            const array = [dispatch(getData()), dispatch(expenseSharedGetProjects())];
            dispatch(setLoading(true)).then(() => Promise.all(array).then(() => dispatch(setLoading(false))));
        },
        clockIn: (project, remarks, lat, lng) => {
            dispatch(setLoading(true)).then(() =>
                dispatch(clockIn(project, remarks, lat, lng)).then(() => dispatch(getData()).then(() => dispatch(setLoading(false)))),
            );
        },
        clockOut: id => {
            dispatch(setLoading(true)).then(() =>
                dispatch(clockOut(id)).then(() => dispatch(getData()).then(() => dispatch(setLoading(false)))),
            );
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(ClockPageV2));
