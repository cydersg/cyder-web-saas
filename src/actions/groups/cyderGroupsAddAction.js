import {
    CYDER_GROUPS_ADD_CHANGE_FIELD,
    CYDER_GROUPS_ADD_ADD_GROUP_REQUEST,
    CYDER_GROUPS_ADD_ADD_GROUP_SUCCESS,
    CYDER_GROUPS_ADD_ADD_GROUP_FAILURE,
    CYDER_GROUPS_ADD_ADD_GROUP_FORCE_VALIDATE,
    CYDER_GROUPS_ADD_MODAL_TOGGLE,
    CYDER_GROUPS_ADD_CLEAR_FIELDS,
    CYDER_GROUPS_ADD_ADD_USER,
    CYDER_GROUPS_ADD_REMOVE_USER,
    CYDER_GROUPS_ADD_ADD_USER_SEARCH_REQUEST,
    CYDER_GROUPS_ADD_ADD_USER_SEARCH_SUCCESS,
    CYDER_GROUPS_ADD_ADD_USER_SEARCH_FAILURE,
} from '../constants/actionTypes';
import withQuery from 'with-query';

import { CYDER_COMMON_API_ROOT } from '../../config';

export function cyderGroupsAddForceValidate() {
    return dispatch => {
        dispatch({ type: CYDER_GROUPS_ADD_ADD_GROUP_FORCE_VALIDATE });
        // return this for async actions
        return Promise.resolve();
    };
}
export function changeFieldValueAction(key, value) {
    return {
        type: CYDER_GROUPS_ADD_CHANGE_FIELD,
        key,
        value,
    };
}
export function cyderGroupsAddAddUserSearchAction(input, isAdmin) {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_GROUPS_ADD_ADD_USER_SEARCH_REQUEST });
        const appId = getState().cyderProfileReducer.profile.appId;
        const url = withQuery(CYDER_COMMON_API_ROOT + '/users', {
            appid: appId,
            action: 'searchbynameforgroup',
        });
        if (input === '') return Promise.resolve([]);
        const data = {
            username: input,
            firstname: input,
            lastname: input,
            status: 'Active',
            isAdmin: isAdmin,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'content-type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(data),
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(res => {
                        // error
                        dispatch({ type: CYDER_GROUPS_ADD_ADD_USER_SEARCH_FAILURE, response: res });
                    });
                } else {
                    return response.json().then(res => {
                        dispatch({ type: CYDER_GROUPS_ADD_ADD_USER_SEARCH_SUCCESS, response: res });
                        let newRes = [];
                        res.forEach(each => {
                            newRes.push({ value: each.username, label: each.username });
                        });
                        return newRes;
                    });
                }
            })
            .catch(error => {
                // error
                dispatch({ type: CYDER_GROUPS_ADD_ADD_GROUP_FAILURE, response: error });
            });
    };
}
export function cyderGroupsAddAddGroup(data, callback) {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_GROUPS_ADD_ADD_GROUP_REQUEST });
        const appId = getState().cyderProfileReducer.profile.appId;
        const url = withQuery(CYDER_COMMON_API_ROOT + '/groups', {
            appid: appId,
            action: 'add',
        });
        const dataWithAppId = Object.assign({ appId }, data);
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'content-type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(dataWithAppId),
            method: 'POST',
        };
        console.log(data);
        fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    response.json().then(res => {
                        // error
                        dispatch({ type: CYDER_GROUPS_ADD_ADD_GROUP_FAILURE, response: res });
                        console.log('error');
                        console.log(res.message);
                    });
                } else {
                    response.json().then(res => {
                        dispatch({ type: CYDER_GROUPS_ADD_ADD_GROUP_SUCCESS, response: res });
                        if (callback) callback();
                        console.log(res);
                        console.log('success');
                    });
                }
            })
            .catch(error => {
                // error
                dispatch({ type: CYDER_GROUPS_ADD_ADD_GROUP_FAILURE, response: error });
            });
    };
}
export function cyderGroupsAddModalToggleAction(reset) {
    return {
        type: CYDER_GROUPS_ADD_MODAL_TOGGLE,
        reset,
    };
}
export function cyderGroupsAddClearFields() {
    return {
        type: CYDER_GROUPS_ADD_CLEAR_FIELDS,
    };
}
export function cyderGroupsAddAddUser(username) {
    return {
        type: CYDER_GROUPS_ADD_ADD_USER,
        username,
    };
}
export function cyderGroupsAddRemoveUser(username) {
    return {
        type: CYDER_GROUPS_ADD_REMOVE_USER,
        username,
    };
}
