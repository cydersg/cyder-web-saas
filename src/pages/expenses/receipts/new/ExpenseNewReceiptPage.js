import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import withQuery from 'with-query';
import history from 'history.js';
import qs from 'query-string';
import moment from 'moment';
import debounce from 'es6-promise-debounce';
import { withTranslation } from 'react-i18next';
import { CyderFileUploader, CyderInput, CyderSelect } from 'cyderComponents/input';
import { InfoField } from 'cyderComponents/input/CyderPreview';

import {
    Button,
    Col,
    Container,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Form,
    FormFeedback,
    FormGroup,
    FormText,
    Input,
    InputGroupButtonDropdown,
    Label,
    Row,
} from 'reactstrap';
import Switch from 'react-switch';
import StoredLayout from 'layouts/StoredLayout';
import CyderDatePicker from 'cyderComponents/forms/CyderDatePicker';
import WorkflowHistoryTable from 'cyderComponents/common/WorkflowHistoryTable';
import { InputGroupComponent } from 'reactstrap-input';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import { ConfirmModalManager } from 'cyderComponents/ConfirmModal';

import {
    expenseNewReceiptClaimToggleModal,
    expenseNewReceiptClaimChangeField,
    expenseNewReceiptClaimSaveReceipt,
    expenseNewReceiptClaimSetFields,
    expenseNewReceiptClaimGetImageInfo,
    expenseNewReceiptClaimDownloadImage,
    expenseNewReceiptClaimForceLoadingState,
    expenseNewReceiptClaimDeleteSavedReceipt,
    expenseNewReceiptClaimResetFields,
    expenseNewReceiptClaimGetHistory,
    expenseNewReceiptClaimForceValidate,
    expenseNewReceiptClaimCheckReceiptNoDuplicate,
    expenseNewReceiptGetBlockchainInfo,
} from 'actions/expenses/cyderExpenseNewReceiptClaimAction';
import {
    expenseSharedGetProjects,
    expenseSharedGetExpenseCategories,
    expenseSharedGetPaymentModes,
    expenseSharedLoadedData,
} from 'actions/expenses/cyderExpenseClaimSharedAction';
import { setJumbotronTitle } from 'actions/pagedata';

import dateutils from 'js/dateutils';
import { expenseDeletableStatuses } from 'js/constlib';
import controlledInputLib from 'js/controlledInputLib';

const urlRecurring = `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/recurring`;
const urlReceipt = `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts`;

class ExpenseNewReceiptPage extends Component {
    state = {
        imageFile: {},
        b64img: null,
        imagebtoa: '',
        expenseDate: null,
        dropdownOpen: false,
        selectedCurrency: 'SGD',
        debouncedCheckReceiptNo: debounce(this.checkReceiptNo, 500),
    };

    generateJumbotronTitle = () => {
        const { receiptFields, location, recurring, resetFields } = this.props;
        const { search, pathname } = location;
        const { status } = receiptFields;

        if (search) {
            // NEW RECEIPT
            if (pathname === `${urlReceipt}/new/receipt`) {
                return 'New Receipt (copying from existing)';
            }

            // VIEW RECEIPT
            if (pathname === `${urlReceipt}/edit/receipt`) {
                if (status === 'Ready') return 'Edit Receipt';
                return 'View Receipt';
            }

            // NEW RECURRING (COPY)
            if (pathname === `${urlRecurring}/new`) {
                return 'New Recurring Charge (copying from existing)';
            }

            // EDIT / VIEW RECURRING
            if (pathname === `${urlRecurring}/edit`) {
                if (status === 'Active') return 'Edit Recurring Charge';
                return 'View Recurring Charge';
            }
        }

        // NEW RECURRING
        if (recurring) {
            resetFields();
            return 'New Recurring Charge';
        }

        resetFields();
        return null;
    };

    componentDidMount() {
        const { location, setJumbotronTitle, getRelevantStuff, getInputOptions, recurring } = this.props;
        const theqs = location ? qs.parse(location.search) : {};
        setJumbotronTitle(this.generateJumbotronTitle());

        getInputOptions();
        getRelevantStuff(theqs, recurring).then(res => {
            const { ContentType, Body } = res;

            this.setState({
                theqs,
                b64img: Body,
                contentType: ContentType,
                imageFile: {
                    b64img: Body, // Decode encoded base64
                    type: ContentType,
                },
            });
        });
    }

    componentDidUpdate(prevProps) {
        // needed because My Expenses and Recurring Charges uses the same Component class
        if (prevProps.location.pathname !== this.props.location.pathname) {
            this.props.setJumbotronTitle(this.generateJumbotronTitle());
        }
    }

    handleExpenseDateChange = momentOrStringVal => {
        const synthEvent = {
            target: {
                id: 'expensedate',
                value: momentOrStringVal.format ? momentOrStringVal.format('DD/MM/YYYY') : momentOrStringVal,
            },
        };
        this.props.handleChange(synthEvent);
    };

    saveOnDialogButtonClick = () => {
        const { users, appId, addGroup } = this.props;
        let data = Object.assign({}, this.props.fields);
        data.users = users.concat();
        data.appId = appId;
        addGroup(data);
    };

    onUpload = image => {
        this.setState({
            ...this.state,
            ...image,
            imageFile: image,
        });
    };

    toggleDropDown = () => {
        this.setState({ dropdownOpen: !this.state.dropdownOpen });
    };

    handleCurrencyDropdown = event => {
        this.setState({ selectedCurrency: event.target.value });
    };

    getBackUrl = () => {
        const { recurring, location } = this.props;
        if (recurring) return urlRecurring;
        const theqs = location ? qs.parse(location.search) : {};
        if (theqs.reportid)
            return `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreports/details/${theqs.reportid}`;
        return urlReceipt;
    };

    toggleAction = () => {
        const { receiptid } = this.props.match.params;
        const { modalAction, getGroupDetails } = this.props;
        this.handleToggleModal();
        if (modalAction === 'refresh') getGroupDetails(receiptid);
        if (modalAction === 'redirect') history.push(this.getBackUrl());
    };

    calcCurrentPositiveButtonText = () => {
        switch (this.props.modalAction) {
            case 'save':
                return 'Save';
            case 'delete':
                return 'Delete';
            default:
                return 'OK';
        }
    };

    calcCurrentPositiveButtonAction = recurring => {
        const { modalAction, handleSave, refreshList, receiptFields } = this.props;
        const { search, pathname } = this.props.location;
        const { imageFile, b64img } = this.state;

        switch (modalAction) {
            case 'delete':
                return this.handleDelete;
            case 'redirect':
                return () => {
                    history.push(this.getBackUrl());
                    if (refreshList) refreshList();
                    this.handleToggleModal();
                };
            case 'save':
                return () => {
                    // Fixed by Fernando on 1 August 2019, to support the real base64
                    const fileInfo = {
                        attachment: b64img && b64img.split(',')[1], // NOTE: Remove header
                        contentType: imageFile.contentType,
                        filename: imageFile.name,
                    };

                    let selectedDefaultFixes = {
                        category: receiptFields.category,
                        paymentmode: receiptFields.paymentmode,
                        project: receiptFields.project,
                        currency: this.state.selectedCurrency,
                        ...fileInfo,
                    };

                    if (recurring) {
                        selectedDefaultFixes = {
                            ...selectedDefaultFixes,
                            title: 'Recurring Charge',
                            type: 'Receipt',
                        };
                    }

                    if (!selectedDefaultFixes.project) delete selectedDefaultFixes.project;

                    const copyItem = search && (pathname === `${urlReceipt}/new/receipt` || pathname === `${urlRecurring}/new`);
                    handleSave(selectedDefaultFixes, copyItem, recurring);
                };
            default:
                return this.handleToggleModal;
        }
    };

    handleToggleModal = modalAction => {
        const { toggleModal, recurring } = this.props;
        toggleModal(modalAction, recurring);
    };

    removeZero = e => {
        const { value, id } = e.target;
        if (value === '0.00') {
            const event = {
                target: {
                    id,
                    value: '',
                },
            };
            this.props.handleChange(event);
        }
    };

    validateThenSave = () => {
        // Solution to calling function on loop
        const shouldValidate = (x, key) => x === key;

        this.props.forceValidate().then(() => {
            const { validation } = this.props;
            for (var key in validation) {
                const validateInputs = ['amount', 'category', 'expensedate', 'paymentmode', 'project', 'receiptno'];
                if (validateInputs.find(shouldValidate.bind(this, key)) && !validation[key].approved) {
                    ConfirmModalManager.show('Warning', validation[key].errors[0], () => ConfirmModalManager.toggle(), true);
                    return;
                }
            }
            this.handleToggleModal('save');
        });
    };

    handleReceiptNoChange = e => {
        // needed to persist non-synthetic event, for the debounced function
        e.persist();
        this.state.debouncedCheckReceiptNo(e);
        this.props.handleChange(e);
    };

    checkReceiptNo = e => {
        const { value } = e.target;
        if (value) this.props.expenseNewReceiptClaimCheckReceiptNoDuplicate(value);
    };

    handleDelete = () => {
        const { handleDelete, recurring } = this.props;
        handleDelete(recurring);
    };

    isDeletable = isEditable => {
        const { allLoaded, recurring, receiptFields } = this.props;

        // If not all loaded
        if (!allLoaded) return false;

        // If not editable
        if (isEditable) return true;

        // If this item is a recurring item
        if (recurring) return true;

        // If this item's status is not ready
        if (expenseDeletableStatuses[receiptFields.status]) return true;
        return false;
    };

    isSavable = isEditable => {
        const { allLoaded, receiptFields } = this.props;
        const { status } = receiptFields;

        if (!isEditable) return false;
        if (status === 'Submitted' || status === 'Approved' || status === 'Returned') return false;
        if (!allLoaded) return false;

        return true;
    };

    getCustomModalBodyContent = () => {
        const { saving, saved, validReceiptNo, receiptFields, modalMessage } = this.props;
        if (saving) return <LoadingSpinner />;
        if (saved) return this.props.modalMessage;
        if (!validReceiptNo && receiptFields.receiptno) return <DuplicateReceiptNoModalBody />;
        return modalMessage;
    };

    render() {
        const { theqs } = this.state;
        const { location, loading } = this.props;
        const { pathname } = location;

        const isNew = pathname === `${urlReceipt}/new/receipt` || pathname === `${urlRecurring}/new`;
        const isEdit = pathname === `${urlReceipt}/edit/receipt`;
        const isEditable = isNew || isEdit || (theqs && theqs.copy);
        const disabled = !this.isSavable(isEditable);

        return (
            <Container key={0} className="mb10 wideContainer">
                {loading ? (
                    <LoadingSpinner center />
                ) : (
                    <Row className="mb-2">
                        <Col xs={12} className="p-0 mb-4">
                            <Link to={this.getBackUrl()}>
                                <i className="align-middle material-icons material-icons-3x">chevron_left</i>
                            </Link>
                        </Col>
                        {disabled ? (
                            <ViewForm
                                {...this.props}
                                {...this.state}
                                isNew={isNew}
                                isEditable={isEditable}
                                disabled={disabled}
                                validateThenSave={this.validateThenSave}
                                handleToggleModal={this.handleToggleModal}
                                isDeletable={() => this.isDeletable(isEditable)}
                                isSavable={() => this.isSavable(isEditable)}
                            />
                        ) : (
                            <EditForm
                                {...this.props}
                                {...this.state}
                                isNew={isNew}
                                isEditable={isEditable}
                                disabled={disabled}
                                handleChange={this.props.handleChange}
                                handleExpenseDateChange={this.handleExpenseDateChange}
                                removeZero={this.removeZero}
                                toggleDropDown={this.toggleDropDown}
                                handleCurrencyDropdown={this.handleCurrencyDropdown}
                                validateThenSave={this.validateThenSave}
                                handleToggleModal={this.handleToggleModal}
                                isDeletable={() => this.isDeletable(isEditable)}
                                isSavable={() => this.isSavable(isEditable)}
                                handleReceiptNoChange={this.handleReceiptNoChange}
                                onUpload={this.onUpload}
                            />
                        )}
                    </Row>
                )}
                <div className="d-sm-none d-block">
                    <SaveBarComponent
                        {...this.props}
                        isNew={isNew}
                        isEditable={isEditable}
                        validateThenSave={this.validateThenSave}
                        handleToggleModal={this.handleToggleModal}
                        isDeletable={() => this.isDeletable(isEditable)}
                        isSavable={() => this.isSavable(isEditable)}
                    />
                </div>
                {!this.props.loading && this.props.workflowHistory && this.props.workflowHistory.length > 0 ? (
                    <WorkflowHistoryTable workflowHistory={this.props.workflowHistory} />
                ) : null}
                <DialogModal
                    key="modal"
                    modal={this.props.modalOpen}
                    toggle={this.toggleAction}
                    customHeader={this.props.modalHeader || 'Confirmation'}
                    customModalBody={this.getCustomModalBodyContent()}
                    positiveButtonAction={this.calcCurrentPositiveButtonAction(this.props.recurring)}
                    positiveButtonText={this.calcCurrentPositiveButtonText()}
                    onlyOneButton={this.props.saved}
                    buttonDisabler={this.props.saving}
                />
            </Container>
        );
    }
}

const ViewForm = props => {
    const {
        loading,
        expenseCategories = [],
        receiptFields,
        recurring,
        isNew,
        isEditable,
        validateThenSave,
        handleToggleModal,
        isDeletable,
        isSavable,
        imageFile,
    } = props;

    const expenseCategory = expenseCategories.find(x => x.code === receiptFields.category) || {};

    return (
        <Fragment>
            <Col xs={12} sm={6}>
                <Form>
                    <Row>
                        {recurring ? (
                            <Col sm={6}>
                                <InfoField
                                    label="Frequency"
                                    value={`${receiptFields.frequency || '-'} of every ${receiptFields.frequency_type || '-'}`}
                                />
                            </Col>
                        ) : (
                            <Col sm={6}>
                                <InfoField label="Expense Date" value={receiptFields.expensedate || '-'} />
                            </Col>
                        )}
                        <Col sm={6}>
                            <InfoField label="Amount" value={`SGD ${receiptFields.amount || '0'}`} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <InfoField label="Project" value={receiptFields.project || '-'} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <InfoField label="Payment Mode" value={receiptFields.paymentmode || '-'} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <InfoField label="Remarks" value={receiptFields.remarks || '-'} />
                        </Col>
                    </Row>
                </Form>
                <div className="d-none d-sm-block">
                    <SaveBarComponent
                        {...props}
                        isNew={isNew}
                        isEditable={isEditable}
                        validateThenSave={validateThenSave}
                        handleToggleModal={handleToggleModal}
                        isDeletable={isDeletable}
                        isSavable={isSavable}
                    />
                </div>
            </Col>
            <Col xs={12} sm={6}>
                <Form>
                    <Row>
                        <Col sm="12">
                            <InfoField label="Receipt No." value={receiptFields.receiptno || '-'} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <InfoField label="Expense Category" value={`${expenseCategory.name} (${receiptFields.category})`} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <InfoField label="Reimbursable" value={receiptFields.reimbursable ? 'Yes' : 'No'} />
                        </Col>
                    </Row>
                    {!recurring ? (
                        <InfoField label="Attachment">
                            <div className="font-weight-bold">
                                {!loading && imageFile.b64img ? (
                                    <CyderFileUploader disabled id="attachment" imageFile={imageFile}></CyderFileUploader>
                                ) : (
                                    '-'
                                )}
                            </div>
                        </InfoField>
                    ) : null}
                </Form>
            </Col>
        </Fragment>
    );
};

const EditForm = props => {
    const {
        receiptFields,
        disabled,
        validation,
        recurring,
        dropdownOpen,
        validReceiptNo,
        checkingReceiptNo,
        selectedCurrency,
        handleCurrencyDropdown,
        toggleDropDown,
        handleChange,
        handleExpenseDateChange,
        removeZero,
        validateThenSave,
        handleToggleModal,
        isDeletable,
        isSavable,
        handleReceiptNoChange,
        onUpload,
        projects,
        paymentModes,
        expenseCategories,
        imageFile,
        isEditable,
        isNew,
    } = props;

    return (
        <Fragment>
            <Col xs={12} sm={6}>
                <Form>
                    <Row>
                        {recurring ? (
                            <Col sm={6}>
                                <FormGroup>
                                    <Label className="text-bold">Frequency</Label>
                                    {receiptFields.frequency_type === 'month' ? (
                                        <div>
                                            <Input
                                                type="select"
                                                id="frequency"
                                                disabled={disabled}
                                                value={receiptFields.frequency}
                                                onChange={handleChange}
                                            >
                                                <MonthOptions />
                                            </Input>
                                            <div className="my-1 text-center">of every</div>
                                        </div>
                                    ) : null}
                                    {receiptFields.frequency_type === 'week' ? (
                                        <div>
                                            <Input
                                                type="select"
                                                id="frequency"
                                                disabled={disabled}
                                                value={receiptFields.frequency}
                                                onChange={handleChange}
                                            >
                                                <WeekOptions />
                                            </Input>
                                            <div className="my-1 text-center">of every</div>
                                        </div>
                                    ) : null}
                                    <Input
                                        type="select"
                                        id="frequency_type"
                                        disabled={disabled}
                                        value={receiptFields.frequency_type}
                                        onChange={handleChange}
                                    >
                                        <option value="month">Month</option>
                                        <option value="week">Week</option>
                                        <option value="day">Daily</option>
                                    </Input>
                                </FormGroup>
                            </Col>
                        ) : (
                            <Col sm={6}>
                                <CyderInput mandatory label="Expense Date " formGroupClassName="m-0">
                                    <CyderDatePicker
                                        disabled={disabled}
                                        onChange={event => handleExpenseDateChange(event)}
                                        value={receiptFields.expensedate}
                                        placeholder="DD/MM/YYYY"
                                        dateFormat="DD/MM/YYYY"
                                    />
                                </CyderInput>
                            </Col>
                        )}
                        <Col sm={6}>
                            <FormGroup>
                                <InputGroupComponent
                                    type="text"
                                    valueKey="amount"
                                    placeholder="Amount"
                                    saveButtonPressed
                                    disabled={disabled}
                                    onFocus={removeZero}
                                    handleChange={handleChange}
                                    validationObj={validation}
                                    value={receiptFields.amount || ''}
                                    label={
                                        <div className="text-bold">
                                            Amount <span className="text-danger">*</span>
                                        </div>
                                    }
                                >
                                    <InputGroupButtonDropdown addonType="append" isOpen={dropdownOpen} toggle={toggleDropDown}>
                                        <DropdownToggle caret>{selectedCurrency || ''}</DropdownToggle>
                                        <DropdownMenu onClick={handleCurrencyDropdown}>
                                            <DropdownItem value="SGD">SGD</DropdownItem>
                                        </DropdownMenu>
                                    </InputGroupButtonDropdown>
                                </InputGroupComponent>
                            </FormGroup>
                        </Col>
                    </Row>
                    <CyderSelect
                        mandatory
                        label="Project"
                        inputProps={{
                            name: 'project',
                            options: projects,
                            disabled,
                            value: projects.find(x => x.code === receiptFields.project),
                            getOptionValue: option => option.code,
                            getOptionLabel: option => option.name,
                            onChange: value =>
                                handleChange({
                                    target: {
                                        id: 'project',
                                        value: value.code,
                                    },
                                }),
                        }}
                    />
                    <CyderSelect
                        mandatory
                        label="Payment Mode"
                        inputProps={{
                            name: 'paymentmode',
                            options: paymentModes,
                            disabled,
                            value: paymentModes.find(x => x.code === receiptFields.paymentmode),
                            getOptionValue: option => option.code,
                            getOptionLabel: option => option.name,
                            onChange: value =>
                                handleChange({
                                    target: {
                                        id: 'paymentmode',
                                        value: value.code,
                                    },
                                }),
                        }}
                    />
                    <CyderInput
                        label="Remarks"
                        inputProps={{
                            type: 'textarea',
                            id: 'remarks',
                            name: 'remarks',
                            disabled,
                            onChange: handleChange,
                            value: receiptFields.remarks,
                        }}
                    />
                </Form>
                <div className="d-none d-sm-block">
                    <SaveBarComponent
                        {...props}
                        isNew={isNew}
                        isEditable={isEditable}
                        validateThenSave={validateThenSave}
                        handleToggleModal={handleToggleModal}
                        isDeletable={isDeletable}
                        isSavable={isSavable}
                    />
                </div>
            </Col>
            <Col xs={12} sm={6}>
                <Form>
                    <FormGroup>
                        <CyderInput
                            label="Receipt No."
                            inputProps={{
                                type: 'text',
                                id: 'receiptno',
                                name: 'receiptno',
                                placeholder: 'Receipt No.',
                                disabled,
                                onChange: handleReceiptNoChange,
                                value: receiptFields.receiptno || '',
                            }}
                        />
                        {checkingReceiptNo ? (
                            <FormText>Checking receipt number...</FormText>
                        ) : !receiptFields.receiptno ? null : (
                            <FormFeedback valid={validReceiptNo}>
                                {validReceiptNo ? (
                                    <div>
                                        Non-duplicate receipt number <i className="align-middle material-icons">check</i>
                                    </div>
                                ) : (
                                    'This receipt number is a duplicate'
                                )}
                            </FormFeedback>
                        )}
                    </FormGroup>
                    <CyderSelect
                        mandatory
                        label="Expense Category "
                        inputProps={{
                            name: 'category',
                            options: expenseCategories,
                            value: expenseCategories.find(x => x.code === receiptFields.category),
                            getOptionValue: option => option.code,
                            getOptionLabel: option => option.name,
                            disabled,
                            onChange: value =>
                                handleChange({
                                    target: {
                                        id: 'category',
                                        value: value.code,
                                    },
                                }),
                        }}
                    />
                    <FormGroup className="d-flex" row>
                        <Label className="p-2 align-middle text-bold">
                            Reimbursable
                            <Switch
                                height={24}
                                id="reimbursable"
                                className="react-switch"
                                uncheckedIcon={null}
                                checked={receiptFields.reimbursable}
                                onChange={checked => handleChange(controlledInputLib.generateSwitchSynthEvent(checked, 'reimbursable'))}
                            />
                        </Label>
                    </FormGroup>
                    {!recurring ? <CyderFileUploader imageFile={imageFile} onUpload={onUpload} /> : null}
                </Form>
            </Col>
        </Fragment>
    );
};

const SaveBarComponent = props => {
    const {
        isNew,
        isEditable,
        location,
        blockchainVerified,
        isSavable,
        recurring,
        receiptFields,
        isDeletable,
        handleToggleModal,
        validateThenSave,
    } = props;
    const { updateddt, id } = receiptFields;
    const { search } = location || {};
    const theqs = search ? qs.parse(search) : {};

    const copyLink = withQuery(`${recurring ? `${urlRecurring}/new` : `${urlReceipt}/new/receipt`}`, {
        receiptid: theqs ? theqs.receiptid : null,
        copy: true,
    });
    return (
        <Fragment>
            <hr />
            {location
                ? (
                      <Row className="pb-4">
                          {blockchainVerified && (
                              <Col xs={12} className="mb-2 text-bold text-success">
                                  <i className="align-middle material-icons">check</i> Blockchain Verified
                              </Col>
                          )}
                          <Col xs={12}>
                              <span className="text-bold">Last Saved:</span>{' '}
                              {updateddt ? moment(updateddt).format('DD/MM/YYYY HH:mm') : 'N/A'}
                          </Col>
                      </Row>
                  ) || recurring
                : null}
            <Row>
                {isSavable() ? (
                    <Col xs={12} md={4}>
                        <Button className="mr-2 w-100p mb10" onClick={validateThenSave}>
                            Save
                        </Button>
                    </Col>
                ) : null}
                {(!isNew && search && isDeletable(isEditable)) || (!isEditable && id) ? (
                    <Col xs={12} md={4}>
                        <Link to={copyLink}>
                            <Button color="info" className="mr-2 w-100p mb10">
                                Clone
                            </Button>
                        </Link>
                    </Col>
                ) : null}
                {!isNew && search && isDeletable(isEditable) ? (
                    <Col xs={12} md={4}>
                        <Button color="danger" className="mr-2 w-100p mb10" onClick={() => handleToggleModal('delete')}>
                            Delete
                        </Button>
                    </Col>
                ) : null}

                {/* {!isEditable && id ? (
                        <Link to={copyLink}>
                            <Button color="info">Copy Item</Button>
                        </Link>
                    ) : null} */}
            </Row>
        </Fragment>
    );
};

const MonthOptions = () => {
    let arr = [];
    for (let x = 0; x < 31; x++) arr[x] = x;
    return dateutils.daysOfMonthArray.map((dayInMonth, i) => (
        <option key={i + 1} value={i + 1}>
            {dayInMonth}
        </option>
    ));
};

const WeekOptions = () => {
    return dateutils.daysOfWeekArray.map(dayInWeek => {
        return (
            <option key={dayInWeek} value={dayInWeek.substring(0, 3)}>
                {dayInWeek}
            </option>
        );
    });
};

const DuplicateReceiptNoModalBody = () => (
    <div>
        Are you sure you want to save this receipt?
        <div className="text-danger">
            <strong>The Receipt No. is a duplicate.</strong>
        </div>
    </div>
);

const mapStateToProps = state => {
    return {
        loading: state.cyderExpenseNewReceiptClaimReducer.loading,
        receipts: state.cyderExpenseMyReceiptsReducer.receipts,
        ...state.cyderExpenseNewReceiptClaimReducer,
        ...state.cyderExpenseSharedReducer,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getInputOptions: () => {
            return Promise.all([
                dispatch(expenseSharedGetProjects()),
                dispatch(expenseSharedGetExpenseCategories()),
                dispatch(expenseSharedGetPaymentModes()),
            ]);
        },
        getRelevantStuff: (queryStrings, recurring) => {
            const { receiptid, copy } = queryStrings;

            // CHECK FOR INVALID ARGUMENTS
            if (!receiptid && !recurring) {
                dispatch(expenseSharedLoadedData());
                return Promise.resolve({});
            }

            // GET RELEVANT STUFFS
            return Promise.all([
                dispatch(expenseNewReceiptClaimForceLoadingState(true)),
                dispatch(expenseNewReceiptGetBlockchainInfo(receiptid)),
                dispatch(expenseNewReceiptClaimGetImageInfo(receiptid)),
                dispatch(expenseNewReceiptClaimSetFields(receiptid, recurring, copy)),
                dispatch(expenseNewReceiptClaimGetHistory(receiptid)),
                dispatch(expenseSharedLoadedData()),
            ])
                .then(res => {
                    const attachments = res[2];
                    if (attachments.length === 0) return { message: 'new receipt' };
                    return dispatch(expenseNewReceiptClaimDownloadImage(attachments[0].url)).then(res => res);
                })
                .then(res => {
                    dispatch(expenseNewReceiptClaimForceLoadingState(false));
                    return res;
                });
        },
        toggleModal: (modalAction, recurring) => {
            dispatch(expenseNewReceiptClaimToggleModal(modalAction || null, recurring));
        },
        handleChange: event => {
            const { id, value } = event.target;
            dispatch(expenseNewReceiptClaimChangeField(id, value));
        },
        handleSave: (selectedDefaultFixes, copyItem, recurring) => {
            dispatch(expenseNewReceiptClaimSaveReceipt(selectedDefaultFixes, copyItem, recurring));
        },
        handleDelete: recurring => {
            dispatch(expenseNewReceiptClaimDeleteSavedReceipt(recurring));
        },
        resetFields: () => {
            return dispatch(expenseNewReceiptClaimResetFields());
        },
        setJumbotronTitle: title => {
            dispatch(setJumbotronTitle(title || 'Receipt'));
        },
        forceValidate: () => {
            return dispatch(expenseNewReceiptClaimForceValidate());
        },
        expenseNewReceiptClaimCheckReceiptNoDuplicate: receiptNo => {
            return dispatch(expenseNewReceiptClaimCheckReceiptNoDuplicate(receiptNo));
        },
        expenseNewReceiptGetBlockchainInfo: receiptId => dispatch(expenseNewReceiptGetBlockchainInfo(receiptId)),
    };
};

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(StoredLayout(ExpenseNewReceiptPage)));
