import {
    CYDER_EXPENSE_REPORT_DETAILS_MODAL_TOGGLE,
    CYDER_EXPENSE_REPORT_DETAILS_SET_FIELDS,
    CYDER_EXPENSE_REPORT_DETAILS_GET_SAVED_REPORT_REQUEST,
    CYDER_EXPENSE_REPORT_DETAILS_GET_SAVED_REPORT_SUCCESS,
    CYDER_EXPENSE_REPORT_DETAILS_GET_SAVED_REPORT_FAILURE,
    CYDER_EXPENSE_REPORT_DETAILS_SET_STATUS_REQUEST,
    CYDER_EXPENSE_REPORT_DETAILS_SET_STATUS_SUCCESS,
    CYDER_EXPENSE_REPORT_DETAILS_SET_STATUS_FAILURE,
    CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_REPORT_REQUEST,
    CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_REPORT_SUCCESS,
    CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_REPORT_FAILURE,
    CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_ATTACHMENT_REQUEST,
    CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_ATTACHMENT_SUCCESS,
    CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_ATTACHMENT_FAILURE,
    CYDER_EXPENSE_REPORT_DETAILS_TOGGLE_CHECKBOX,
    CYDER_EXPENSE_REPORT_DETAILS_TOGGLE_CHECKBOX_ALL,
} from '../../constants/actionTypesReports';
import withQuery from 'with-query';
import cyderlib from '../../../js/cyderlib';
import fetchlib from 'js/fetchlib';

import { DJARVIS_EXPENSE_API_ROOT, DJARVIS_REPORT_API_ROOT, CYDER_COMMON_API_ROOT } from '../../../config';

export function expenseReportsDetailToggleModal(modalAction) {
    return {
        type: CYDER_EXPENSE_REPORT_DETAILS_MODAL_TOGGLE,
        modalAction,
    };
}

export function expenseReportsDetailSetFields(reportOrTaskId, index, isTask) {
    return (dispatch, getState) => {
        const { reports } = getState().cyderExpenseMyReportsReducer;
        const { appId } = getState().cyderProfileReducer.profile;

        // Not Task
        if (!isTask && reports.length !== 0 && reports[index]) {
            const report = reports[index];
            dispatch({ type: CYDER_EXPENSE_REPORT_DETAILS_SET_FIELDS, report: cyderlib.normalizeReceiptFields(report) });
            return Promise.resolve(report);
        }

        // DETERMINE URL
        let url = '';
        if (isTask) {
            url = withQuery(`${CYDER_COMMON_API_ROOT}/workflow`, {
                appid: appId,
                action: 'gettaskdetails',
                instanceid: reportOrTaskId,
            });
        } else {
            url = withQuery(`${DJARVIS_EXPENSE_API_ROOT}/report`, {
                appid: appId,
                id: reportOrTaskId,
                action: 'findbyid',
            });
        }

        // Fetch
        return fetchlib.fetch(
            dispatch,
            getState,
            'POST',
            url,
            null,
            CYDER_EXPENSE_REPORT_DETAILS_GET_SAVED_REPORT_REQUEST,
            null,
            CYDER_EXPENSE_REPORT_DETAILS_GET_SAVED_REPORT_FAILURE,
            isTask,
            null,
            // CUSTOM ACTION SUCCESS
            response => {
                const { data } = response;
                const isTaskData = cyderlib.normalizeReceiptFields(data);
                dispatch({
                    type: CYDER_EXPENSE_REPORT_DETAILS_SET_FIELDS,
                    report: isTask ? isTaskData : data,
                });
                dispatch({ type: CYDER_EXPENSE_REPORT_DETAILS_GET_SAVED_REPORT_SUCCESS, data });
            },
        );
    };
}

export function expenseReportsDetailSetStatus(newStatus, itemid) {
    return (dispatch, getState) => {
        const url = withQuery(`${DJARVIS_EXPENSE_API_ROOT}/report`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'updatereportandreceiptsstatus',
        });
        const body = {
            status: newStatus,
            reportId: itemid,
        };
        return fetchlib.fetch(
            dispatch,
            getState,
            'POST',
            url,
            body,
            CYDER_EXPENSE_REPORT_DETAILS_SET_STATUS_REQUEST,
            CYDER_EXPENSE_REPORT_DETAILS_SET_STATUS_SUCCESS,
            CYDER_EXPENSE_REPORT_DETAILS_SET_STATUS_FAILURE,
        );
    };
}

export function getFullReports(startdate, enddate, status) {
    return (dispatch, getState) => {
        const url = withQuery(`${DJARVIS_EXPENSE_API_ROOT}/report`, {
            action: 'get-full-report',
            appid: getState().cyderProfileReducer.profile.appId,
            startdate,
            enddate,
            status,
        });

        const opt = {
            method: 'GET',
            headers: {
                Authorization: 'allow',
                'x-api-key': getState().config.apiKey,
                'content-type': 'application/json',
            },
        };
        return fetchlib.fetch(
            dispatch,
            getState,
            'GET',
            url,
            null,
            CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_REPORT_REQUEST,
            CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_REPORT_SUCCESS,
            CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_REPORT_FAILURE,
            null,
            opt,
        );
    };
}
export function expenseFullReportDownloadPDF(startdate, enddate, status) {
    return (dispatch, getState) => {
        const url = withQuery(`${DJARVIS_REPORT_API_ROOT}`, {
            action: 'print-full-report',
            appid: getState().cyderProfileReducer.profile.appId,
            startdate,
            enddate,
            status,
        });

        const opt = {
            method: 'GET',
            headers: {
                Authorization: 'allow',
                'x-api-key': getState().config.apiKey,
                'content-type': 'application/json',
            },
        };
        return fetchlib.fetch(
            dispatch,
            getState,
            'GET',
            url,
            null,
            CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_REPORT_REQUEST,
            CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_REPORT_SUCCESS,
            CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_REPORT_FAILURE,
            null,
            opt,
        );
    };
}
export function expenseReportDownloadPDF(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${DJARVIS_REPORT_API_ROOT}`, {
            action: 'print-expense-report',
            appid: getState().cyderProfileReducer.profile.appId,
            id,
        });

        const opt = {
            method: 'GET',
            headers: {
                Authorization: 'allow',
                'x-api-key': getState().config.apiKey,
                'content-type': 'application/json',
            },
        };
        return fetchlib.fetch(
            dispatch,
            getState,
            'GET',
            url,
            null,
            CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_REPORT_REQUEST,
            CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_REPORT_SUCCESS,
            CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_REPORT_FAILURE,
            null,
            opt,
        );
    };
}
export function downloadAllAttachmentsZIP(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${DJARVIS_REPORT_API_ROOT}`, {
            action: 'download-expense-attachment',
            appid: getState().cyderProfileReducer.profile.appId,
            id,
        });

        const opt = {
            method: 'GET',
            headers: {
                Authorization: 'allow',
                'x-api-key': getState().config.apiKey,
                'content-type': 'application/json',
            },
        };
        return fetchlib.fetch(
            dispatch,
            getState,
            'GET',
            url,
            null,
            CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_ATTACHMENT_REQUEST,
            CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_ATTACHMENT_SUCCESS,
            CYDER_EXPENSE_REPORT_DETAILS_DOWNLOAD_ATTACHMENT_FAILURE,
            null,
            opt,
        );
    };
}
export function toggleCheckboxes(id) {
    return {
        type: CYDER_EXPENSE_REPORT_DETAILS_TOGGLE_CHECKBOX,
        id,
    };
}
export function toggleCheckboxesAll() {
    return {
        type: CYDER_EXPENSE_REPORT_DETAILS_TOGGLE_CHECKBOX_ALL,
    };
}
