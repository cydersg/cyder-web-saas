import {
    CYDER_WORKFLOW_TASKS_DETAIL_MODAL_TOGGLE,
    CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_REQUEST,
    CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_SUCCESS,
    CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_FAILURE,
    CYDER_WORKFLOW_TASKS_DETAIL_APPROVE_REJECT_TASK_REQUEST,
    CYDER_WORKFLOW_TASKS_DETAIL_APPROVE_REJECT_TASK_SUCCESS,
    CYDER_WORKFLOW_TASKS_DETAIL_APPROVE_REJECT_TASK_FAILURE,
    CYDER_WORKFLOW_TASKS_DETAIL_PAYMENT_SET_STATUS_REQUEST,
    CYDER_WORKFLOW_TASKS_DETAIL_PAYMENT_SET_STATUS_SUCCESS,
    CYDER_WORKFLOW_TASKS_DETAIL_PAYMENT_SET_STATUS_FAILURE,
    CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_IMAGE_REQUEST,
    CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_IMAGE_SUCCESS,
    CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_IMAGE_FAILURE,
} from 'actions/constants/actionTypesWorkflow';

import withQuery from 'with-query';
import { DJARVIS_EXPENSE_API_ROOT, CYDER_COMMON_API_ROOT } from 'config.js';

export function expenseNewReceiptClaimGetImage(receiptid) {
    return (dispatch, getState) => {
        const url = withQuery(DJARVIS_EXPENSE_API_ROOT + '/receipt', {
            action: 'getattachments',
            appid: getState().cyderProfileReducer.profile.appId,
            id: receiptid,
        });
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            method: 'POST',
        };
        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_IMAGE_REQUEST });
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_IMAGE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_IMAGE_FAILURE, error });
                return error;
            });
    };
}
export function expenseNewReceiptClaimDownloadImage(url) {
    return (dispatch, getState) => {
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            method: 'POST',
        };
        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_IMAGE_REQUEST });
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_IMAGE_FAILURE, response });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_IMAGE_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_IMAGE_FAILURE, error });
                return error;
            });
    };
}
export function tasksDetailGetTaskDetail(instanceid, isPayment) {
    return (dispatch, getState) => {
        const { appId, authorizationToken } = getState().cyderProfileReducer.profile;
        const { apiKey, apiKeyCyder } = getState().config;

        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_REQUEST });

        // URL
        const url = isPayment
            ? withQuery(`${DJARVIS_EXPENSE_API_ROOT}/my`, {
                  appid: getState().cyderProfileReducer.profile.appId,
                  action: 'findreceiptbyid',
                  id: instanceid,
              })
            : withQuery(`${CYDER_COMMON_API_ROOT}/workflow`, {
                  appid: appId,
                  action: 'gettaskdetails',
                  instanceid,
              });

        // Options
        const options = {
            headers: {
                'x-api-key': isPayment ? apiKey : apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: authorizationToken,
            },
            method: 'POST',
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_FAILURE, response });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_GET_TASK_FAILURE, error });
                return error;
            });
    };
}

export function tasksDetailApproveReturn(action, realid, remarks) {
    return (dispatch, getState) => {
        const { appId } = getState().cyderProfileReducer.profile;
        const { selectedItems } = getState().expenseReportsDetailReducer;

        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_APPROVE_REJECT_TASK_REQUEST });

        // URL
        const url = withQuery(`${CYDER_COMMON_API_ROOT}/workflow`, {
            appid: appId,
            action,
        });

        // BODY
        const body = {
            id: realid,
            remarks,
        };

        if (action === 'reject') {
            let data = [];
            for (var key in selectedItems) {
                if (selectedItems[key] === true) data.push(key);
            }
            // Body data contain id receipt id of selected receipt
            body.data = data;
        }

        // OPTIONS
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_APPROVE_REJECT_TASK_SUCCESS, response, action });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_APPROVE_REJECT_TASK_FAILURE, error, action });
                return error;
            });
    };
}
export function tasksDetailSetStatus(newStatus, itemid, remarks) {
    return (dispatch, getState) => {
        const { authorizationToken, appId } = getState().cyderProfileReducer.profile;
        const { apiKey } = getState().config;

        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_PAYMENT_SET_STATUS_REQUEST });
        const url = withQuery(`${DJARVIS_EXPENSE_API_ROOT}/receipt`, {
            action: remarks ? 'updatepaid' : 'updatestatus',
            appid: appId,
        });

        // BODY
        const body = remarks
            ? {
                  id: itemid,
                  paidremarks: remarks,
                  paidamount: null,
              }
            : {
                  id: itemid,
                  status: newStatus,
              };

        // OPTIONS
        const options = {
            headers: {
                'x-api-key': apiKey,
                'Content-Type': 'application/json',
                Authorization: authorizationToken,
            },
            body: JSON.stringify(body),
            method: 'POST',
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_PAYMENT_SET_STATUS_FAILURE, error, newStatus });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_PAYMENT_SET_STATUS_SUCCESS, response, newStatus });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_WORKFLOW_TASKS_DETAIL_PAYMENT_SET_STATUS_FAILURE, error, newStatus });
                return error;
            });
    };
}

export function taskDetailToggleModal(modalAction) {
    return {
        type: CYDER_WORKFLOW_TASKS_DETAIL_MODAL_TOGGLE,
        modalAction,
    };
}
