import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

function MyCorpTimesheetBottomNav(props) {
    return (
        <Navbar
            bg="light"
            fixed="bottom"
            style={{
                top: 'auto',
                position: 'fixed',
                padding: 0,
            }}
        >
            <Nav className="mycorp-bottomnav">
                <Nav.Link href="/mycorp/timesheet/home" className="mycorp-bottomnav-link">
                    <span
                        className={`mycorp-bottomnav ${
                            props.page === 'home' ? 'mycorp-bottomnav-selected' : 'mycorp-bottomnav-unselected'
                        }`}
                    >
                        <i className="material-icons mycorp-buttomnav-icon">home</i>
                    </span>
                    <span
                        className={`mycorp-buttomnav-text ${
                            props.page === 'home' ? 'mycorp-bottomnav-selected mycorp-text-underlined' : 'mycorp-bottomnav-unselected'
                        }`}
                    >
                        Home
                    </span>
                </Nav.Link>
                <Nav.Link href="/mycorp/timesheet/history">
                    <span
                        className={`mycorp-bottomnav ${
                            props.page === 'history' ? 'mycorp-bottomnav-selected' : 'mycorp-bottomnav-unselected'
                        }`}
                    >
                        <i className="material-icons mycorp-buttomnav-icon">history</i>
                    </span>
                    <span
                        className={`mycorp-buttomnav-text ${
                            props.page === 'history' ? 'mycorp-bottomnav-selected mycorp-text-underlined' : 'mycorp-bottomnav-unselected'
                        }`}
                    >
                        History
                    </span>
                </Nav.Link>
                <Nav.Link href="/mycorp/timesheet/report">
                    <span
                        className={`mycorp-bottomnav ${
                            props.page === 'report' ? 'mycorp-bottomnav-selected' : 'mycorp-bottomnav-unselected'
                        }`}
                    >
                        <i className="material-icons mycorp-buttomnav-icon">folder</i>
                    </span>
                    <span
                        className={`mycorp-buttomnav-text ${
                            props.page === 'report' ? 'mycorp-bottomnav-selected mycorp-text-underlined' : 'mycorp-bottomnav-unselected'
                        }`}
                    >
                        Report
                    </span>
                </Nav.Link>
            </Nav>
        </Navbar>
    );
}

export default MyCorpTimesheetBottomNav;
