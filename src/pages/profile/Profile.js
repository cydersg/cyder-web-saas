import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Dropzone from 'react-dropzone';
import InputField from '../users/UsersInputField';
import { CyderSelect } from 'cyderComponents/input/index';
import { Input, Button, Container, Row, Col, Form, FormGroup, Label } from 'reactstrap';
import LoadingSpinner from '../../cyderComponents/loadingSpinner/LoadingSpinner';
import {
    changeFieldValueAction,
    profileSaveAction,
    cyderProfileModalToggle,
    cyderDevRecheckProfile,
    forceValidate,
    profilePicSaveAction,
} from '../../actions/profile/cyderProfileAction';
import { cyderUsersDetailGetUserDetailsAction } from '../../actions/users/cyderUsersDetailAction.js';
import { departmentsSettingsSearchAction } from '../../actions/settings/departmentsSettingsAction';
import { expenseDeletableStatuses, urlRecurring, urlReceipt, layoutLib } from 'js/constlib';

import DialogModal from '../../cyderComponents/modals/DialogModal';
import { setJumbotronTitle } from '../../actions/pagedata';
import StoredLayout from '../../layouts/StoredLayout';

const imageStyle = {
    maxWidth: '100%',
    width: '100%',
    maxHeight: '450px',
    objectFit: 'contain',
};

const dropzoneStyle = {
    width: '100%',
    height: '100%',
    maxHeight: '500px',
    border: '2px dotted gray',
};

class ProfilePage extends Component {
    state = {
        b64img: null,
        imagebtoa: '',
    };

    componentDidMount() {
        this.props.refreshProfile();
        this.props.setJumbotronTitle();
        this.props.departmentsSettingsSearchAction();
    }

    handleDOBChangeWithEvent = momentOrStringVal => {
        const synthEvent = {
            target: {
                id: 'dateofbirth',
                value: momentOrStringVal.format ? momentOrStringVal.format('DD/MM/YYYY') : momentOrStringVal,
            },
        };
        this.props.handleChange(synthEvent);
    };

    validateBeforeToggleModal = () => {
        this.props.forceValidate(() => {
            const required = {
                email: true,
                firstname: true,
                lastname: true,
                mobilenumber: true,
                idno: true,
            };
            for (let key in this.props.validation) {
                if (required[key] === true && this.props.validation[key].approved === false) {
                    return false;
                }
            }
            this.props.toggleModal('save');
        });
    };

    onDrop = imageFiles => {
        imageFiles.forEach(file => {
            const reader = new FileReader();
            reader.onload = () => {
                const fileAsBinaryString = reader.result;
                this.setState({
                    b64img: btoa(fileAsBinaryString),
                    imagebtoa: btoa(fileAsBinaryString),
                    imageFiles: imageFiles,
                });
                // do whatever you want with the file content
            };

            reader.readAsBinaryString(file);
        });
    };

    handleProfilePicSave = () => {
        this.props.profilePicSaveAction(this.state.imagebtoa).then(res => {
            this.props.refreshUserProfile();
        });
    };

    validInput = key => {
        return this.props.profile[key] ? true : false;
    };

    formFeedbackValid = key => {
        return this.props.profile[key] ? true : false;
    };

    formFeedbackText = key => {
        if (this.props.profile[key]) return '';
        switch (key) {
            case 'idno':
                return 'Identification No is required';
            case 'officenumber':
                return 'Office Number is required';
            case 'firstname':
                return 'First Name is required';
            case 'lastname':
                return 'Last Name is required';
            case 'mobilenumber':
                return 'Mobile Number is required';
            case 'officenumber':
                return 'Office Number is required';
            case 'companyaddress':
                return 'Company Address is required';
        }
    };

    render() {
        const additionalProps = {
            validInput: this.validInput,
            handleChange: this.props.handleChange,
            formFeedbackText: this.formFeedbackText,
            formFeedbackValid: this.formFeedbackValid,
        };

        const imageUrl = this.state.b64img
            ? 'data:' + this.state.contentType + ';base64, ' + this.state.b64img
            : this.props.profile.profile_picture
            ? 'data:image/png;base64, ' + this.props.profile.profile_picture
            : '/assets/images/profile_picture_default.jpg';

        const deptFromCode = this.props.departments.find(item => {
            return item.code === this.props.profile.department_code;
        });

        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        return [
            <Container key={0}>
                <Row className="mb-2">
                    <Col xs={12} sm={12} md={6} lg={3} xl={3} className="my-4">
                        <Dropzone style={dropzoneStyle} onDrop={this.onDrop}>
                            {({ getRootProps, getInputProps }) => {
                                return (
                                    <div {...getRootProps()}>
                                        <Row>
                                            <Col>
                                                <input {...getInputProps()} />
                                                <img
                                                    alt=""
                                                    style={imageStyle}
                                                    src={imageUrl}
                                                    onError={e => (e.target.src = '/assets/images/profile_picture_default.jpg')}
                                                />
                                                <Button onClick={e => e.preventDefault()} color="primary" block>
                                                    Upload or drop an image
                                                </Button>
                                                <Button
                                                    onClick={this.handleProfilePicSave}
                                                    disabled={!this.state.imageFiles || this.props.savingProfilePic}
                                                    color="success"
                                                    block
                                                >
                                                    {this.props.savingProfilePic ? 'Saving' : 'Save'}
                                                </Button>
                                            </Col>
                                        </Row>
                                    </div>
                                );
                            }}
                        </Dropzone>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={7}>
                        {this.props.loading ? (
                            <LoadingSpinner key={0} />
                        ) : (
                            <Form>
                                <FormGroup row>
                                    <Label sm={3} className="text-bold">
                                        Email
                                    </Label>
                                    <Col sm={6}>
                                        <Input readOnly disabled value={this.props.profile.username} type="text" id="username" />
                                    </Col>
                                    <Col className={isMobile ? 'w-100p mt10 d-flex' : 'd-flex'} sm={3}>
                                        <Link to="/changepassword">
                                            <button
                                                style={{
                                                    paddingLeft: '20px',
                                                    paddingRight: '20px',
                                                    borderRadius: '30px',
                                                }}
                                                className="ml-auto btn btn-outline-success color-success color-hover-light"
                                            >
                                                Change password
                                            </button>
                                        </Link>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={3} className="text-bold">
                                        First Name <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        sm={9}
                                        value={this.props.profile['firstname'] ? this.props.profile['firstname'] : ''}
                                        type="text"
                                        valueKey="firstname"
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={3} className="text-bold">
                                        Last Name <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        sm={9}
                                        value={this.props.profile['lastname'] ? this.props.profile['lastname'] : ''}
                                        type="text"
                                        valueKey="lastname"
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={3} className="text-bold">
                                        Mobile Number <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        sm={9}
                                        value={this.props.profile['mobilenumber'] ? this.props.profile['mobilenumber'] : ''}
                                        type="text"
                                        valueKey="mobilenumber"
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={3} className="text-bold">
                                        Office Number
                                    </Label>
                                    <InputField
                                        sm={9}
                                        value={this.props.profile['officenumber'] ? this.props.profile['officenumber'] : ''}
                                        type="text"
                                        valueKey="officenumber"
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={3} className="text-bold">
                                        Company Name <span className="text-danger">*</span>
                                    </Label>
                                    <Col sm={9}>
                                        <Input
                                            value={this.props.profile['companyname']}
                                            type="select"
                                            id="companyname"
                                            onChange={event => this.props.handleChange(event)}
                                        >
                                            <option value="NRIC">Cyder SG</option>
                                        </Input>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={3} className="text-bold">
                                        Company Address
                                    </Label>
                                    <InputField
                                        sm={9}
                                        value={this.props.profile['companyaddress'] ? this.props.profile['companyaddress'] : ''}
                                        type="text"
                                        valueKey="companyaddress"
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={3} className="text-bold">
                                        Identification Type <span className="text-danger">*</span>
                                    </Label>
                                    <Col sm={9}>
                                        <Input
                                            value={this.props.profile.idtype || 'NRIC'}
                                            type="select"
                                            id="idtype"
                                            onChange={event => this.props.handleChange(event)}
                                        >
                                            <option value="NRIC">NRIC</option>
                                            <option value="FIN">FIN</option>
                                            <option value="Passport No.">Passport No.</option>
                                            <option value="Staff ID">Staff ID</option>
                                        </Input>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={3} className="text-bold">
                                        Identification No. <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        sm={9}
                                        value={this.props.profile['idno'] ? this.props.profile['idno'] : ''}
                                        type="text"
                                        valueKey="idno"
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={3} className="text-bold">
                                        Department
                                    </Label>
                                    <Col>
                                        <CyderSelect
                                            mandatory
                                            inputProps={{
                                                name: 'department_code',
                                                options: this.props.departments,
                                                value: deptFromCode,
                                                getOptionValue: option => option.id,
                                                getOptionLabel: option => option.name,
                                                onChange: value => {
                                                    this.props.handleChange({
                                                        target: {
                                                            id: 'department_code',
                                                            value: value.code,
                                                        },
                                                    });
                                                },
                                            }}
                                        />
                                    </Col>
                                </FormGroup>
                            </Form>
                        )}
                        <Row className="mb-2">
                            <Col>
                                <Button
                                    className={isMobile ? 'w-100p mb10' : 'mr-2'}
                                    color="success"
                                    onClick={this.validateBeforeToggleModal}
                                    tabIndex="-1"
                                >
                                    Update
                                </Button>
                                <Link to="/home" className={isMobile ? 'w-100p mb10 btn btn-danger' : 'mr-2 btn btn-danger'} tabIndex="-1">
                                    Cancel
                                </Link>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>,
            <DialogModal
                key="modal"
                modal={this.props.modalOpen}
                toggle={() => this.props.toggleModal(this.props.saved)}
                customHeader="Confirmation"
                customModalBody={this.props.saving ? <LoadingSpinner /> : this.props.modalMessage}
                positiveButtonAction={!this.props.saved ? () => this.props.handleSave() : () => this.props.toggleModal(this.props.saved)}
                positiveButtonText={this.props.saved ? 'Ok' : 'Save'}
                onlyOneButton={this.props.saved}
                buttonDisabler={this.props.savingSysparam}
            />,
        ];
    }
}

const mapStateToProps = state => {
    return {
        ...state.cyderProfileReducer,
        departments: state.departmentSettingsReducer.departments,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        handleChange: event => {
            const key = event.target.id;
            const value = event.target.value;

            console.log('key: ', key);
            console.log('value: ', value);
            dispatch(changeFieldValueAction(key, value));
        },
        handleSave: () => {
            dispatch(profileSaveAction({}));
        },
        forceValidate: cb => {
            dispatch(forceValidate()).then(() => {
                // execute callback after async action is completed
                cb();
            });
        },
        toggleModal: save => {
            dispatch(cyderProfileModalToggle(save));
        },
        refreshProfile: () => {
            dispatch(cyderDevRecheckProfile());
        },
        profilePicSaveAction: b64img => {
            return dispatch(profilePicSaveAction(b64img));
        },
        refreshUserProfile: () => {
            return dispatch(cyderUsersDetailGetUserDetailsAction(null, true));
        },
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Profile')),
        departmentsSettingsSearchAction: () => dispatch(departmentsSettingsSearchAction()),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(ProfilePage));
