/**
 * Common action creator for expense
 * 
 * Author: Fernando
 */

import {
    CYDER_EXPENSE_START,
    CYDER_EXPENSE_CANCEL,
    CYDER_EXPENSE_SELECT_PROJECT,
    CYDER_EXPENSE_SELECT_EXPENSE_DATE,
    CYDER_EXPENSE_SELECT_TRAVEL_METHOD
} from '../constants/actionTypes'

/**
 * Start expense workflow
 * 
 */
export function startExpense() {
    return {
        type: CYDER_EXPENSE_START
    }
}

/**
 * Cancel expense workflow
 * 
 */
export function cancelExpense() {
    return {
        type: CYDER_EXPENSE_CANCEL
    }
}

/**
 * Select project
 * 
 */
export function selectProject(newProject) {
    return {
        type: CYDER_EXPENSE_SELECT_PROJECT,
        newProject
    }
}

/**
 * Select travel method
 * 
 */
export function selectTravelMethod(newTravelMethod) {
    return {
        type: CYDER_EXPENSE_SELECT_TRAVEL_METHOD,
        newTravelMethod
    }
}

/**
 * Select date
 * 
 */
export function selectExpenseDate(expenseDate) {
    return {
        type: CYDER_EXPENSE_SELECT_EXPENSE_DATE,
        expenseDate
    }
}