import withQuery from 'with-query';
import {
    CYDER_EXPENSE_REPORTS_CREATE_MODAL_TOGGLE,
    CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_REQUEST,
    CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_SUCCESS,
    CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_FAILURE,
    CYDER_EXPENSE_REPORTS_CREATE_RESET_STATE,
    CYDER_EXPENSE_REPORTS_GET_APPROVERS_REQUEST,
    CYDER_EXPENSE_REPORTS_GET_APPROVERS_SUCCESS,
    CYDER_EXPENSE_REPORTS_GET_APPROVERS_FAILURE,
} from '../constants/actionTypesReports';
import { CYDER_COMMON_API_ROOT, DJARVIS_EXPENSE_API_ROOT } from '../../config';
import { CYDER_EXPENSE_MYRECEIPTS_TOGGLE_CHECKBOX } from '../constants/actionTypes';

export function expenseCreateReportResetState() {
    return {
        type: CYDER_EXPENSE_REPORTS_CREATE_RESET_STATE,
    };
}

export function expenseCreateReportSubmitReport(receiptData, remarks, approvers) {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_REQUEST });

        const url = withQuery(DJARVIS_EXPENSE_API_ROOT + '/report', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'submitexpensereport',
        });
        let receiptIds = '';
        receiptData.forEach((each, i) => {
            if (each.id) {
                if (i > 0) {
                    receiptIds = receiptIds + ',';
                }
                receiptIds = receiptIds + each.id;
            }
        });
        // fix for select defaults
        let parsedApproverList = [];
        approvers.forEach(aprv => {
            parsedApproverList.push(aprv.value);
        });
        const actualData = {
            receipts: receiptIds,
            remarks,
            approvers: parsedApproverList,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(actualData),
            method: 'POST',
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_FAILURE, error });
                return error;
            });
    };
}
export function expenseCreateReportGetApprovers() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_EXPENSE_REPORTS_GET_APPROVERS_REQUEST });
        const url = withQuery(CYDER_COMMON_API_ROOT + '/users', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getapprovers',
        });
        const options = {
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            method: 'POST',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_EXPENSE_REPORTS_GET_APPROVERS_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_REPORTS_GET_APPROVERS_FAILURE, error });
                return error;
            });
    };
}
export function expenseCreateReportToggleModal(modalAction, data) {
    return {
        type: CYDER_EXPENSE_REPORTS_CREATE_MODAL_TOGGLE,
        modalAction,
        data,
    };
}
export function expenseCreateReportRemoveItemFromList() {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_EXPENSE_REPORTS_CREATE_MODAL_TOGGLE, modalAction: 'close' });
        dispatch({
            type: CYDER_EXPENSE_MYRECEIPTS_TOGGLE_CHECKBOX,
            id: getState().cyderExpenseReportsCreateReducer.receiptIdForRemoval,
        });
    };
}
