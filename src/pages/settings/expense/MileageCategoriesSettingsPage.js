import React from 'react';
import { connect } from 'react-redux';
import StoredLayout from 'layouts/StoredLayout';
import { withTranslation } from 'react-i18next';

import ReactTable from 'react-table';
import LinkButton from 'cyderComponents/buttons/LinkButton';
import { Alert, Input, Label, Button, Container, Row, Col, Form, FormGroup } from 'reactstrap';
import DialogModal from 'cyderComponents/modals/DialogModal';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import { setJumbotronTitle } from 'actions/pagedata';

import {
    cyderMileageCategoriesSettingsSearchAction,
    cyderMileageCategoriesSettingsEditModalSetMileageCategory,
    cyderMileageCategoriesSettingsModalToggleAction,
    cyderMileageCategoriesSettingsEditModalChangeValue,
    cyderMileageCategoriesSettingsAddMileageCategoryAction,
    cyderMileageCategoriesSettingsUpdateMileageCategoryAction,
    cyderMileageCategoriesSettingsDeleteMileageCategoryAction,
    cyderMileageCategoriesSettingsAddApproverAction,
    cyderMileageCategoriesSettingsRemoveApproverAction,
} from 'actions/settings/cyderMileageCategoriesSettingsSearchAction';
import { cyderGroupsAddAddUserSearchAction } from 'actions/groups/cyderGroupsAddAction';

import reactTableFilter from 'js/reactTableFilter';
import cyderlib from 'js/cyderlib.js';

class MileageCategoriesSettingsPage extends React.Component {
    constructor(props) {
        super(props);
        this.onFilteredChange = this.onFilteredChange.bind(this);
        this.filterAllFunc = this.filterAllFunc.bind(this);
        this.state = {
            filterAll: '',
        };
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.toggleAction = this.toggleAction.bind(this);
        this.validateThenSearch = this.validateThenSearch.bind(this);
        this.filterOptionsApprovers = this.filterOptionsApprovers.bind(this);
    }
    componentDidMount() {
        if (this.props.forceRefresh) {
            this.validateThenSearch();
        }
        this.validateThenSearch();
        this.props.setJumbotronTitle();
        // this.props.getAllMileageCategories();
    }
    validateThenSearch(e) {
        if (e) {
            e.preventDefault();
        }
        // this.props.clearErrorMessage();

        const keyword = document.getElementById('keyword').value;
        const status = document.getElementById('status').value;

        const body = {
            keyword,
            status,
        };
        this.props.searchMileageCategories(body);
    }
    setupEditModal(mileageCategory) {
        this.props.setModalMileageCategory(mileageCategory);
        this.props.toggleModal('update');
    }
    setupDeleteModal(mileageCategory) {
        this.props.setModalMileageCategory(mileageCategory);
        this.props.toggleModal('delete');
    }
    toggleAction() {
        this.handleToggleModal();
        if (this.props.modalAction === 'refresh') {
            this.validateThenSearch();
        }
    }
    calcCurrentPositiveButtonText() {
        if (this.props.modalAction === 'update') {
            return 'Update';
        } else if (this.props.modalAction === 'add') {
            return 'Add';
        } else if (this.props.modalAction === 'delete') {
            return 'Delete';
        } else if (this.props.modalAction === 'close') {
            return 'Ok';
        } else {
            return 'Ok';
        }
    }
    calcCurrentPositiveButtonAction() {
        if (this.props.modalAction === 'update') {
            return this.props.handleUpdate;
        } else if (this.props.modalAction === 'refresh') {
            return () => {
                this.handleToggleModal();
                this.validateThenSearch();
            };
        } else if (this.props.modalAction === 'add') {
            return this.props.handleAdd;
        } else if (this.props.modalAction === 'delete') {
            return this.props.handleDelete;
        } else if (this.props.modalAction === 'close') {
            return this.handleToggleModal;
        } else {
            return this.handleToggleModal;
        }
    }
    handleToggleModal(modalAction) {
        this.props.toggleModal(modalAction);
    }
    filterOptionsApprovers(options) {
        const results = options.filter(item => !this.props.approversReference[item.value]);
        return results;
    }
    onFilteredChange(filtered) {
        this.setState(reactTableFilter.onFilteredChange(filtered));
    }
    filterAllFunc(e) {
        this.setState(reactTableFilter.filterAllFunc(e));
    }
    render() {
        const customModalBody = (
            <div>
                <FormGroup>
                    <Label for="exampleText">Mileage Category Code</Label>
                    <Input
                        onChange={event => this.props.changeModalTextAreaValue(event)}
                        disabled={this.props.modalAction === 'update'}
                        type="text"
                        maxLength={10}
                        id="code"
                        value={this.props.modalMileageCategory.code}
                        onKeyPress={e => {
                            cyderlib.handleOnKeyPress(
                                e,
                                [
                                    this.props.modalMileageCategory.code,
                                    this.props.modalMileageCategory.name,
                                    this.props.modalMileageCategory.rate,
                                ],
                                this.calcCurrentPositiveButtonAction(),
                            );
                        }}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleText">Mileage Category Name</Label>
                    <Input
                        onChange={event => this.props.changeModalTextAreaValue(event)}
                        type="text"
                        maxLength={100}
                        id="name"
                        value={this.props.modalMileageCategory.name}
                        onKeyPress={e => {
                            cyderlib.handleOnKeyPress(
                                e,
                                [
                                    this.props.modalMileageCategory.code,
                                    this.props.modalMileageCategory.name,
                                    this.props.modalMileageCategory.rate,
                                ],
                                this.calcCurrentPositiveButtonAction(),
                            );
                        }}
                    />
                </FormGroup>
                {/*
                <FormGroup>
                    <Label for="exampleText">Number of Required Approval Level</Label>
                    <Input
                        onChange={(event) => this.props.changeModalTextAreaValue(event)}
                        type="select"
                        id="approvalvotescount"
                        value={this.props.modalMileageCategory.approvalvotescount}
                    >
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </Input>
                </FormGroup>
                */}
                <Label>Rate</Label>
                <FormGroup row>
                    <Col sm={4}>
                        <Input
                            onChange={event => this.props.changeModalTextAreaValue(event)}
                            type="text"
                            placeholder="Amount"
                            maxLength={100}
                            id="rate"
                            value={this.props.modalMileageCategory.rate}
                            onKeyPress={e => {
                                cyderlib.handleOnKeyPress(
                                    e,
                                    [
                                        this.props.modalMileageCategory.code,
                                        this.props.modalMileageCategory.name,
                                        this.props.modalMileageCategory.rate,
                                    ],
                                    this.calcCurrentPositiveButtonAction(),
                                );
                            }}
                        />
                    </Col>
                    <Col sm={4}>
                        <Input
                            onChange={event => this.props.changeModalTextAreaValue(event)}
                            type="select"
                            id="currency"
                            value={this.props.modalMileageCategory.currency}
                        >
                            <option value="SGD">SGD</option>
                            <option disabled value="USD">
                                USD
                            </option>
                            <option disabled value="MYR">
                                MYR
                            </option>
                        </Input>
                    </Col>
                    <Col sm={4}>
                        <Input
                            onChange={event => this.props.changeModalTextAreaValue(event)}
                            type="select"
                            id="rateunit"
                            value={this.props.modalMileageCategory.rateunit}
                        >
                            <option value="km">per km</option>
                            <option disabled value="mi">
                                per mile
                            </option>
                        </Input>
                    </Col>
                </FormGroup>
            </div>
        );
        const columns = [
            reactTableFilter.generateAllColumnFilter(),
            {
                Header: 'Category Code',
                accessor: 'code',
            },
            {
                Header: 'Category Name',
                accessor: 'name',
            },
            {
                Header: 'Rate',
                accesor: 'rate',
                Cell: props => {
                    return props.original.rate + ' ' + props.original.currency + ' per ' + props.original.rateunit;
                },
            },
            {
                Header: '',
                accessor: 'id',
                Cell: props => {
                    const thisMileageCategory = this.props.mileagecategories[props.index];
                    return (
                        <div className="float-right">
                            <Button
                                color="link"
                                size="sm"
                                onClick={() => {
                                    this.setupEditModal(thisMileageCategory);
                                }}
                            >
                                <i className="align-middle material-icons">edit</i>
                            </Button>
                            <Button
                                color="link"
                                className="ml-1"
                                size="sm"
                                onClick={() => {
                                    this.setupDeleteModal(thisMileageCategory);
                                }}
                            >
                                <i className="text-danger align-middle material-icons">delete</i>
                            </Button>
                        </div>
                    );
                },
            },
        ];
        return (
            <div key={0}>
                <Container>
                    <Form className="d-none" onSubmit={this.validateThenSearch}>
                        <FormGroup>
                            <Input placeholder="Keyword" id="keyword" />
                        </FormGroup>
                        <FormGroup>
                            <Input type="select" id="status">
                                <option>Active</option>
                                <option>Inactive</option>
                            </Input>
                        </FormGroup>
                    </Form>
                    <Row className="mb-2">
                        <Col className="d-flex">
                            <Input className="w-300" placeholder="Search" onChange={this.filterAllFunc} value={this.state.filterAll} />
                            <LinkButton preset="addButton" onClick={() => this.handleToggleModal('add')} text="Mileage Category" />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            {this.props.loading ? (
                                <LoadingSpinner center />
                            ) : this.props.mileagecategories.length > 0 ? (
                                <ReactTable
                                    filtered={this.state.filtered}
                                    onFilteredChange={this.onFilteredChange}
                                    filterable={false}
                                    defaultFilterMethod={reactTableFilter.defaultFilterMethod}
                                    className="-highlight mb-2"
                                    data={this.props.mileagecategories}
                                    columns={columns}
                                    minRows={0}
                                    showPagination={false}
                                />
                            ) : this.props.errorMessage ? (
                                <Alert color="danger">{this.props.errorMessage}</Alert>
                            ) : null}
                        </Col>
                    </Row>
                </Container>
                <DialogModal
                    key="modal"
                    modal={this.props.modalOpen}
                    toggle={this.toggleAction}
                    customHeader={this.props.modalHeader}
                    customModalBody={
                        this.props.saving ? <LoadingSpinner /> : this.props.modalMessage ? this.props.modalMessage : customModalBody
                    }
                    positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                    positiveButtonText={this.calcCurrentPositiveButtonText()}
                    onlyOneButton={this.props.saved}
                    buttonDisabler={this.props.saving}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ...state.cyderMileageCategoriesSettingsSearchReducer,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        searchMileageCategories: body => {
            dispatch(cyderMileageCategoriesSettingsSearchAction(body));
        },
        setModalMileageCategory: mileageCategory => {
            dispatch(
                cyderMileageCategoriesSettingsEditModalSetMileageCategory(
                    mileageCategory.id,
                    mileageCategory.code,
                    mileageCategory.name,
                    mileageCategory.approvalvotescount,
                    mileageCategory.rate,
                    mileageCategory.currency,
                    mileageCategory.rateunit,
                ),
            );
        },
        toggleModal: modalAction => {
            if (modalAction === 'add') {
                dispatch(cyderMileageCategoriesSettingsEditModalSetMileageCategory('', '', '', '', '', 'SGD', 'km', []));
            }
            dispatch(cyderMileageCategoriesSettingsModalToggleAction(modalAction ? modalAction : null));
        },
        changeModalTextAreaValue: event => {
            const value = event.target.value;
            const key = event.target.id;
            dispatch(cyderMileageCategoriesSettingsEditModalChangeValue(key, value));
        },
        getOptions: input => {
            // const username = document.getElementById('searchUsername').value;
            return dispatch(cyderGroupsAddAddUserSearchAction(input)).then(response => {
                return { options: response };
            });
        },
        addApprover: val => {
            if (val === null) return;
            const username = val.value;
            dispatch(cyderMileageCategoriesSettingsAddApproverAction(username));
        },
        removeApprover: username => {
            dispatch(cyderMileageCategoriesSettingsRemoveApproverAction(username));
        },
        handleAdd: () => dispatch(cyderMileageCategoriesSettingsAddMileageCategoryAction()),
        handleUpdate: () => dispatch(cyderMileageCategoriesSettingsUpdateMileageCategoryAction()),
        handleDelete: () => dispatch(cyderMileageCategoriesSettingsDeleteMileageCategoryAction()),
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Settings')),
    };
};
// translate() is if we want to use HOC to perform t()
export default withTranslation()(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(StoredLayout(MileageCategoriesSettingsPage)),
);
