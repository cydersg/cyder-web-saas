import {
    CYDER_EXPENSE_MYREPORTS_GET_REPORTS_REQUEST,
    CYDER_EXPENSE_MYREPORTS_GET_REPORTS_SUCCESS,
    CYDER_EXPENSE_MYREPORTS_GET_REPORTS_FAILURE,
} from '../constants/actionTypes';
import withQuery from 'with-query';

import { DJARVIS_EXPENSE_API_ROOT } from '../../config';

export function myReportsGetReports(startdate, enddate, status = 'all', mode = 'self') {
    return (dispatch, getState) => {
        const url = withQuery(DJARVIS_EXPENSE_API_ROOT + '/report', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: mode === 'self' ? 'getmyreports' : 'getallreports',
        });

        const body = {
            startdate: startdate,
            enddate: enddate,
            status,
        };

        const options = {
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
        };

        dispatch({ type: CYDER_EXPENSE_MYREPORTS_GET_REPORTS_REQUEST });
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_EXPENSE_MYREPORTS_GET_REPORTS_FAILURE, error });
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_EXPENSE_MYREPORTS_GET_REPORTS_SUCCESS, response });
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_MYREPORTS_GET_REPORTS_FAILURE, error });
            });
    };
}
