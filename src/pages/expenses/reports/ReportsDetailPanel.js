import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import history from 'history.js';
import qs from 'query-string';
import withQuery from 'with-query';

import { Row, Col, Container, Button, FormGroup, Label, Input } from 'reactstrap';
import ReactTable from 'react-table';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import StatusBadge from 'cyderComponents/common/StatusBadge';
import ExpenseTypeBadge from 'cyderComponents/common/ExpenseTypeBadge';

import {
    expenseReportsDetailSetFields,
    expenseReportsDetailToggleModal,
    expenseReportsDetailSetStatus,
    toggleCheckboxes,
    toggleCheckboxesAll,
    expenseReportDownloadPDF,
    downloadAllAttachmentsZIP,
} from 'actions/expenses/reports/reportsDetailAction.js';
import { expenseNewReceiptClaimGetHistory } from 'actions/expenses/cyderExpenseNewReceiptClaimAction';
import { setJumbotronTitle } from 'actions/pagedata';

import ActionExecutor from 'js/ActionExecutor';
import cyderlib from 'js/cyderlib.js';
import fileDownloadUtils from 'js/fileDownloadUtils';
import { layoutLib } from 'js/constlib';

import '../../../css/forms/default-forms.css';

class ReportDetailPage extends Component {
    state = {
        returnedRemarks: null,
    };

    componentDidMount() {
        const { reportid, taskid } = this.props.match.params;
        this.props.setJumbotronTitle('Report Details');
        this.getRelevantStuffs(reportid, taskid);
        this.getReportHistory(reportid);
    }

    getRelevantStuffs = async (reportid, taskid) => {
        // REPORT
        if (reportid) {
            this.props.getReport(reportid);
            return;
        }

        // TASK
        this.props.getReport(taskid, 0, true);
    };

    getReportHistory = reportid => {
        if (!reportid) return;
        const callback = async () => {
            const data = await this.props.getReportHistory(reportid);
            if (!data) return;
            const returnedInfo = data.find(x => x.status === 'Returned') || {};
            this.setState({ returnedRemarks: returnedInfo.remarks });
        };
        ActionExecutor.execute(() => {}, callback);
    };

    downloadExpenseReport = () => {
        const { report, downloadExpenseReport } = this.props;
        const callback = async () => {
            const { data } = await downloadExpenseReport(report.id);
            if (!data) return;
            fileDownloadUtils.arrayBufferToBlob(data.Body.data);
        };
        ActionExecutor.execute(() => {}, callback);
    };

    downloadAllAttachments = () => {
        const { report, downloadAllAttachments } = this.props;
        const callback = async () => {
            const { data } = await downloadAllAttachments(report.id);
            if (!data) return;
            window.open(`https://s3-ap-southeast-1.amazonaws.com/${data.bucket}/${data.key}`);
            // fileDownloadUtils.arrayBufferToZip(data.Body.data);
        };
        ActionExecutor.execute(() => {}, callback);
    };

    toggleAction = () => {
        const { modalAction, getGroupDetails } = this.props;
        const { reportid } = this.props.match.params;

        this.handleToggleModal();
        if (modalAction === 'refresh') getGroupDetails(reportid);
        if (modalAction === 'redirect') history.push(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'workflow'}/mytasks`);
        if (modalAction === 'redirectPayment') history.push(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp/' : ''}payments`);
    };

    calcCurrentPositiveButtonText = () => {
        switch (this.props.modalAction) {
            case 'approve':
                return 'Approve';
            case 'reject':
                return 'Reject';
            case 'validating':
                return 'Mark as Validating';
            default:
                return 'Ok';
        }
    };

    calcCurrentPositiveButtonAction = () => {
        const { refreshList, modalAction, handleMarkValidating, handleMarkPaid } = this.props;
        const { reportid } = this.props.match.params;

        if (modalAction === 'validating') return () => handleMarkValidating(reportid);
        if (modalAction === 'paid') return () => handleMarkPaid(reportid);
        if (modalAction === 'close') return this.handleToggleModal;
        if (modalAction === 'redirect') {
            return () => {
                history.push(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'workflow'}/mytasks`);
                if (refreshList) refreshList();
                this.handleToggleModal();
            };
        }
        if (modalAction === 'redirectPayment') {
            return () => {
                history.push(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp/' : ''}payments`);
                if (refreshList) refreshList();
                this.handleToggleModal();
            };
        }
        return this.handleToggleModal;
    };

    handleToggleModal = modalAction => {
        this.props.toggleModal(modalAction);
    };

    getTdProps = (state, rowInfo) => {
        return {
            onClick: (e, handleOriginal) => {
                const { mode, location } = this.props;
                const { taskid, reportid } = this.props.match.params;
                const { type, id } = rowInfo.original;

                if (!rowInfo) return;
                // if (mode && mode === 'admin') return;
                const expenseType = type.toLowerCase();

                let url = '';
                if (taskid) {
                    url = withQuery(
                        `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'workflow'}/task/taskview/${expenseType}/${id}`,
                        {
                            realid: qs.parse(location.search.substring(1)).realid,
                            reportid: taskid,
                        },
                    );
                } else {
                    url = withQuery(
                        `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts/view/${expenseType}`,
                        {
                            receiptid: id,
                            reportid: reportid,
                        },
                    );
                }
                history.push(url);
                if (handleOriginal) handleOriginal();
            },
        };
    };

    render() {
        const { mode, report, loading, modalOpen, modalHeader, modalMessage, modalAction, saving, saved } = this.props;
        const { taskid } = this.props.match.params;
        const { path } = this.props.match;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        const payments = path === `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp/' : ''}payments/:reportid`;
        const urlBack = payments
            ? `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp/' : ''}payments`
            : taskid
            ? `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'workflow'}/mytasks`
            : mode === 'self'
            ? `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreports`
            : `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp/' : ''}report/expense/reportsbymonth`;

        if (loading) return <LoadingSpinner center />;
        return (
            <Container className="wideContainer">
                <RowColWrapper rowClassName="mb-2 mb-4">
                    <Link to={urlBack} tabIndex="-1">
                        <i className="align-middle material-icons material-icons-3x">chevron_left</i>
                    </Link>
                </RowColWrapper>
                <Fragment>
                    <PreviewPanel payments={payments} {...this.props} {...this.state} />
                    <RowColWrapper xs={12} colClassName="text-right mt-3">
                        <Button className={isMobile ? 'mx-1 w-100p mb10' : 'mx-1'} color="success" onClick={this.downloadExpenseReport}>
                            Download Expense Report
                        </Button>
                        <Button className={isMobile ? 'mx-1 w-100p mb10' : 'mx-1'} color="success" onClick={this.downloadAllAttachments}>
                            Download All Attachment
                        </Button>
                        {payments && (
                            <>
                                <Button
                                    className={isMobile ? 'mx-1 w-100p mb10' : 'mx-1'}
                                    color="info"
                                    onClick={() => this.handleToggleModal('validating')}
                                >
                                    Mark as Validating
                                </Button>
                                <Button
                                    className={isMobile ? 'mx-1 w-100p mb10' : 'mx-1'}
                                    color="info"
                                    onClick={() => this.handleToggleModal('paid')}
                                >
                                    Mark as Paid
                                </Button>
                            </>
                        )}
                    </RowColWrapper>
                </Fragment>
                <RowColWrapper xs={12} rowClassName="mt-4">
                    <ReactTable
                        className="-highlight"
                        data={report.receipts}
                        columns={getColumns(this.props)}
                        minRows={0}
                        showPagination={false}
                        getTdProps={this.getTdProps}
                    />
                </RowColWrapper>
                <DialogModal
                    key="modal"
                    modal={modalOpen}
                    toggle={this.toggleAction}
                    customHeader={modalHeader || 'Confirmation'}
                    customModalBody={saving ? <LoadingSpinner /> : modalMessage}
                    positiveButtonAction={this.calcCurrentPositiveButtonAction(modalAction)}
                    positiveButtonText={this.calcCurrentPositiveButtonText(modalAction)}
                    onlyOneButton={saved}
                    buttonDisabler={saving}
                />
            </Container>
        );
    }
}

const PreviewPanel = props => {
    const { returnedRemarks, payments, report } = props;
    const { taskid } = props.match.params;
    const { status, createdby, remarks } = report;

    return (
        <Row>
            <Col xs={12} sm={6}>
                {!payments && !taskid ? (
                    <div className="d-flex pb-3">
                        <StatusBadge status={status} />
                    </div>
                ) : null}
                {payments && <span>Submitted By: {createdby}</span>}
                {remarks && (
                    <div>
                        <span className="text-muted font-weight-bold m-0">Report Remarks</span>
                        <h6>{remarks}</h6>
                    </div>
                )}
                {returnedRemarks && (
                    <div>
                        <span className="text-muted font-weight-bold m-0">Returned Remarks</span>
                        <h6>{returnedRemarks}</h6>
                    </div>
                )}
            </Col>
            {/* FOR BIGGER SCREEN WIDTH */}
            <Col sm={6} className="d-none d-sm-block text-right">
                {report && <ColContent report={report} />}
            </Col>
            {/* FOR SMALLER SCREEN WIDTH */}
            <Col xs={12} className="d-block d-sm-none pt-4 pb-4">
                {report && <ColContent report={report} />}
            </Col>
        </Row>
    );
};

const getReduceAmount = receipts => {
    const reducer = (total, receipt) => (receipt.reimbursable !== 'Y' ? (total += receipt.amount) : total);
    return receipts.reduce(reducer, 0);
};

const ColContent = props => {
    const { status, receipts, createddt, totalamount } = props.report;
    const reduceAmount = getReduceAmount(receipts || []);
    const submittedDate = moment(createddt).format('DD/MM/YYYY');
    const submittedTime = moment(createddt).format('hh:mm');
    const amount = '$' + (totalamount / 100).toFixed(2);
    const totalReimbursableAmt = '$' + ((totalamount - reduceAmount) / 100).toFixed(2);

    return (
        <div>
            <h6>
                <b className="text-muted">
                    Submitted on {submittedDate} {submittedTime}
                </b>
            </h6>
            <hr />
            {status !== 'submitted' ? (
                <div>
                    <b>
                        <span>Total Reimbursable Amount</span>
                        <h4>{totalReimbursableAmt}</h4>
                    </b>
                </div>
            ) : null}
            <div className="pt-3">
                <b>
                    <span className="pb-4">Total Amount</span>
                    <h4>{amount}</h4>
                </b>
            </div>
        </div>
    );
};

const RowColWrapper = props => {
    const { children, rowClassName, colClassName, xs } = props;
    return (
        <Row className={rowClassName}>
            <Col xs={xs} className={colClassName}>
                {children}
            </Col>
        </Row>
    );
};

const getColumns = props => {
    const { selectedAll, selectedItems, handleCheckbox, handleCheckboxAll, mode, match } = props;
    const { taskid } = props.match.params;
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    let column = [
        {
            accessor: 'id',
            show: false, // What is this column for ????, for now, it is not shown
            Header: () => {
                return (
                    <FormGroup check>
                        <Label>
                            <Input checked={selectedAll} type="checkbox" onChange={handleCheckboxAll} />
                            <div className=""> Flag</div>
                        </Label>
                    </FormGroup>
                );
            },
            Cell: props => {
                return (
                    <FormGroup check>
                        <Label>
                            <Input
                                type="checkbox"
                                checked={selectedItems[props.value] || false}
                                onClick={e => e.stopPropagation()}
                                onChange={() => handleCheckbox(props.value)}
                            />
                            <div className="invisible"> Selector</div>
                        </Label>
                    </FormGroup>
                );
            },
        },
        {
            Header: 'Receipt No.',
            width: 140,
            show: !isMobile,
            accessor: 'receiptno',
        },
        {
            Header: 'Expense Date',
            show: !isMobile,
            accessor: 'expensedate',
            width: 140,
            sortMethod: cyderlib.tableSortDate,
            filterMethod: (filter, row) => cyderlib.filterDateText(filter, row, 'DD/MM/YYYY'),
            Cell: props => moment(props.value).format('DD/MM/YYYY'),
        },
        {
            Header: 'Amount',
            show: !isMobile,
            accessor: 'amount',
            width: 100,
            filterMethod: cyderlib.filterCurrencyCent,
            Cell: props => 'S$' + (props.value / 100).toFixed(2),
        },
        {
            Header: 'Remarks',
            accessor: 'remarks',
            show: !isMobile,
            filterMethod: cyderlib.filterIgnoreCase,
            Cell: props => {
                return (
                    <div
                        style={{
                            width: '100%',
                        }}
                    >
                        {props.value}
                    </div>
                );
            },
        },
        {
            Header: '',
            show: !isMobile,
            width: 100,
            accessor: 'id',
            Cell: props => {
                const { id, type } = props.original;
                const { taskid } = match.params;

                // if (mode && mode === 'admin') return '';
                const expenseType = type.toLowerCase();

                const copyUrl = withQuery(
                    `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts/new/${expenseType}`,
                    {
                        receiptid: id,
                    },
                );

                return (
                    <div className="float-right">
                        <Link onClick={e => e.stopPropagation()}>
                            <Button color="link" size="sm">
                                <i className="align-middle material-icons">info_outline</i>
                            </Button>
                        </Link>
                        {/* {!taskid ? (
                            <Link to={copyUrl} onClick={e => e.stopPropagation()}>
                                <Button color="link" size="sm">
                                    <i className="align-middle material-icons">content_copy</i>
                                </Button>
                            </Link>
                        ) : null} */}
                    </div>
                );
            },
        },
        {
            Header: '',
            headerClassName: 'rt-no-sort-ind',
            accessor: 'id',
            sortable: false,
            show: isMobile,
            filterable: false,
            Cell: props => {
                const { id, type } = props.original;
                const { taskid } = match.params;

                const expenseType = type.toLowerCase();

                const copyUrl = withQuery(
                    `${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts/new/${expenseType}`,
                    {
                        receiptid: id,
                    },
                );

                return (
                    <div className="boxShadow">
                        <div className="d-flex justify-content-between">
                            <strong>{moment(props.original.expensedate).format('DD MMM YYYY')}</strong>
                            <span>{cyderlib.formatCurrencyCent(props.original.amount)}</span>
                        </div>
                        {/* <span>
                                    {props.original.type === 'Mileage'
                                        ? mileageCategoriesMap[props.original.category]
                                        : expenseCategoriesMap[props.original.category]}
                                </span> */}
                        <br />
                        {props.original.remarks}
                    </div>
                );
            },
        },
    ];

    if (!taskid) column.slice(1, column.length);
    return column;
};

const mapStateToProps = (state, ownProps) => {
    return {
        ...state.expenseReportsDetailReducer,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        handleMarkValidating: itemid => {
            dispatch(expenseReportsDetailSetStatus('PendingPayment', itemid));
        },
        handleMarkPaid: itemid => {
            dispatch(expenseReportsDetailSetStatus('Paid', itemid));
        },
        toggleModal: modalAction => {
            dispatch(expenseReportsDetailToggleModal(modalAction ? modalAction : null));
        },
        setJumbotronTitle: title => {
            dispatch(setJumbotronTitle(title));
        },
        getReport: (reportid, index, isTask) => {
            return dispatch(expenseReportsDetailSetFields(reportid, index, isTask)).then(res => res.data);
        },
        handleCheckbox: id => {
            dispatch(toggleCheckboxes(id));
        },
        handleCheckboxAll: () => {
            dispatch(toggleCheckboxesAll());
        },
        downloadExpenseReport: id => {
            return dispatch(expenseReportDownloadPDF(id));
        },
        downloadAllAttachments: id => {
            return dispatch(downloadAllAttachmentsZIP(id));
        },
        getReportHistory: id => {
            return dispatch(expenseNewReceiptClaimGetHistory(id));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReportDetailPage);
