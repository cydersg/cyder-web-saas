import React, { Component } from 'react';
import qs from 'query-string';
import history from 'history.js';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';

import StoredLayout from 'layouts/StoredLayout';
import { Input, Button, Container, Row, Col, Label } from 'reactstrap';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import ReportDetailPage from 'pages/expenses/reports/ReportsDetailPage.js';
import WorkflowHistoryTable from 'cyderComponents/common/WorkflowHistoryTable';

import {
    taskDetailToggleModal,
    tasksDetailGetTaskDetail,
    tasksDetailApproveReturn,
    tasksDetailSetStatus,
} from 'actions/workflow/tasksDetailAction';
import { expenseNewReceiptClaimGetHistory } from 'actions/expenses/cyderExpenseNewReceiptClaimAction';
import { setJumbotronTitle } from 'actions/pagedata';
import { expenseDeletableStatuses, urlRecurring, urlReceipt, layoutLib } from 'js/constlib';

class TasksDetailPage extends Component {
    state = {
        b64img: null,
        imagebtoa: '',
        expenseDate: null,
        dropdownOpen: false,
        selectedCurrency: 'SGD',
    };
    componentDidMount() {
        const { getTaskDetail, setJumbotronTitle } = this.props;
        const { taskid, itemid } = this.props.match.params;

        if (taskid) {
            getTaskDetail(taskid);
            setJumbotronTitle(false);
            return;
        }

        if (itemid) {
            getTaskDetail(itemid, true);
            setJumbotronTitle(true);
        }
    }
    handleToggleModal = modalAction => {
        this.props.toggleModal(modalAction);
    };
    onModalToggle = () => {
        const { receiptid } = this.props.match.params;
        const { modalAction, getGroupDetails } = this.props;
        switch (modalAction) {
            case 'refresh':
                getGroupDetails(receiptid);
                break;
            case 'redirect':
                history.push(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'workflow'}/mytasks`);
                break;
            case 'redirectPayment':
                history.push(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp/' : ''}payments`);
                break;
            default:
                break;
        }
        this.handleToggleModal();
    };
    getModalOKText = () => {
        const { modalAction } = this.props;
        switch (modalAction) {
            case 'approve':
                return 'Approve';
            case 'reject':
                return 'Return';
            case 'validating':
                return 'Mark as Validating';
            default:
                return 'Ok';
        }
    };
    getModalOkAction = () => {
        const { location, modalAction, handleApprove, handleReturn, handleMarkValidating, handleMarkPaid, refreshList } = this.props;
        const { itemid } = this.props.match.params;

        switch (modalAction) {
            case 'approve':
                return () => {
                    const realid = qs.parse(location.search.substring(1)).realid;
                    handleApprove(realid);
                };
            case 'reject':
                return () => {
                    const realid = qs.parse(location.search.substring(1)).realid;
                    handleReturn(realid);
                };
            case 'redirect':
                return () => {
                    history.push('/workflow/mytasks');
                    if (refreshList) refreshList();
                    this.handleToggleModal();
                };
            case 'redirectPayment':
                return () => {
                    history.push('/payments');
                    if (refreshList) refreshList();
                    this.handleToggleModal();
                };
            case 'validating':
                return () => handleMarkValidating(itemid);
            case 'paid':
                return () => handleMarkPaid(itemid);
            default:
                return this.handleToggleModal;
        }
    };
    render() {
        const { loading, selectedItems, workflowHistory } = this.props;
        const { taskid } = this.props.match.params;

        return (
            <div>
                <ReportDetailPage {...this.props} />
                <Container className="wideContainer">
                    {loading ? null : (
                        <Row className="my-4">
                            <Col xs={12}>
                                <Label className="font-weight-bold">Remarks: </Label>
                                <Input type="textarea" row="4" id="remarks" />
                            </Col>
                        </Row>
                    )}
                    <ApproveReturn
                        selectedItems={selectedItems}
                        handleToggleModal={this.handleToggleModal}
                        isPayment={taskid ? false : true}
                    />
                    {!loading && workflowHistory && workflowHistory.length > 0 ? (
                        <div className="mt-4">
                            <WorkflowHistoryTable workflowHistory={workflowHistory} />
                        </div>
                    ) : null}
                </Container>
                <DialogModal
                    key="modal"
                    modal={this.props.modalOpen}
                    toggle={this.onModalToggle}
                    customHeader={this.props.modalHeader || 'Confirmation'}
                    customModalBody={this.props.saving ? <LoadingSpinner /> : this.props.modalMessage}
                    positiveButtonAction={this.getModalOkAction()}
                    positiveButtonText={this.getModalOKText()}
                    onlyOneButton={this.props.saved}
                    buttonDisabler={this.props.saving}
                />
            </div>
        );
    }
}

const ApproveReturn = props => {
    const { selectedItems, isPayment, handleToggleModal } = props;

    const shouldReturnDisabled = () => {
        for (let key in selectedItems) {
            if (selectedItems[key] === true) return false;
        }
        return true;
    };

    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return (
        <Row>
            <Col xs={12}>
                {isPayment ? (
                    <div>
                        <Button className={isMobile ? 'w-100p mb10' : null} color="info" onClick={() => handleToggleModal('validating')}>
                            Mark as Validating
                        </Button>
                        <Button className={isMobile ? 'w-100p' : 'ml-2'} color="success" onClick={() => handleToggleModal('paid')}>
                            Mark as Paid
                        </Button>
                    </div>
                ) : (
                    <div>
                        <Button className={isMobile ? 'w-100p mb10' : null} color="success" onClick={() => handleToggleModal('approve')}>
                            Approve
                        </Button>
                        <Button
                            color="danger"
                            className={isMobile ? 'w-100p' : 'ml-2'}
                            // disabled={shouldReturnDisabled()}
                            onClick={() => handleToggleModal('reject')}
                        >
                            Return
                        </Button>
                    </div>
                )}
            </Col>
        </Row>
    );
};

const mapStateToProps = state => {
    const { momentDateFormat, momentDateTimeFormat } = state.config;
    return {
        ...state.tasksDetailReducer,
        momentDateFormat,
        momentDateTimeFormat,
        workflowHistory: state.cyderExpenseNewReceiptClaimReducer.workflowHistory,
        selectedItems: state.expenseReportsDetailReducer.selectedItems,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        getTaskDetail: (taskid, isPayment) => {
            dispatch(tasksDetailGetTaskDetail(taskid, isPayment)).then(res => {
                dispatch(expenseNewReceiptClaimGetHistory(res.id));
            });
        },
        handleApprove: realid => {
            const remarks = document.getElementById('remarks').value;
            dispatch(tasksDetailApproveReturn('approve', realid, remarks));
        },
        handleReturn: realid => {
            const remarks = document.getElementById('remarks').value;
            dispatch(tasksDetailApproveReturn('reject', realid, remarks));
        },
        handleMarkValidating: itemid => {
            dispatch(tasksDetailSetStatus('Validating', itemid));
        },
        handleMarkPaid: itemid => {
            const remarks = document.getElementById('remarks').value;
            dispatch(tasksDetailSetStatus('Paid', itemid, remarks));
        },
        toggleModal: modalAction => {
            dispatch(taskDetailToggleModal(modalAction || null));
        },
        setJumbotronTitle: isPayment => {
            dispatch(setJumbotronTitle(isPayment ? 'Report Payment Details' : 'Report Details'));
        },
    };
};

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(StoredLayout(TasksDetailPage)));
