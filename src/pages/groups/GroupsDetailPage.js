import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Async } from 'react-select';
import debounce from 'es6-promise-debounce';
import { Container, Row, Col, Button, ListGroup, ListGroupItem, Form, FormGroup, Label, Input, FormFeedback } from 'reactstrap';
import {
    withTranslation,
    // Trans,
} from 'react-i18next';

import {
    cyderGetGroupDetailsAction,
    cyderRemoveUserListAdd,
    cyderRemoveUserListRemove,
    cyderAddNewUserToList,
    cyderRemoveNewUserFromList,
    cyderChangeGroupDetailFieldValueAction,
    cyderSaveGroupDetailChangesAction,
    cyderGroupDetailsModalToggle,
    cyderGroupsDetailDeleteUserAction,
} from '../../actions/groups/cyderGroupsDetailAction';
import { cyderGroupsAddAddUserSearchAction } from '../../actions/groups/cyderGroupsAddAction';
import { setJumbotronTitle } from '../../actions/pagedata';
import LoadingSpinner from '../../cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from '../../cyderComponents/modals/DialogModal';
import StoredLayout from '../../layouts/StoredLayout';

import history from '../../history';
// import cyderlib from '../../js/cyderlib';

class GroupsDetailPage extends React.Component {
    constructor(props) {
        super(props);
        this.getOptions = this.getOptions.bind(this);
        this.filterOptions = this.filterOptions.bind(this);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.toggleAction = this.toggleAction.bind(this);
    }
    componentDidMount() {
        this.props.setJumbotronTitle();
        this.props.getGroupDetails(this.props.match.params.groupid);
    }
    validateThenToggleModal() {
        //TODO: validation
        /*
        for (let key in this.props.validation) {
            if (this.props.validation[key].approved === false) {
                console.log("at least one field is not valid");
                return false;
            }
        }
        */
        this.handleToggleModal('update');
    }
    toggleUserRemove(user) {
        if (this.props.removedUsers[user.username]) {
            this.props.removeUserFromRemoveList(user.username);
        } else {
            this.props.addUserToRemoveList(user.username);
        }
    }
    toggleAction() {
        this.handleToggleModal();
        if (this.props.modalAction === 'refresh') {
            this.props.getGroupDetails(this.props.match.params.groupid);
        } else if (this.props.modalAction === 'redirect') {
            history.push('/administration/groups/');
        }
    }
    calcCurrentPositiveButtonText() {
        if (this.props.modalAction === 'update') {
            return 'Update';
        } else if (this.props.modalAction === 'delete') {
            return 'Delete';
        } else if (this.props.modalAction === 'close') {
            return 'Ok';
        } else {
            return 'Ok';
        }
    }
    calcCurrentPositiveButtonAction() {
        if (this.props.modalAction === 'update') {
            return this.props.handleSave;
        } else if (this.props.modalAction === 'delete') {
            return this.props.handleDelete;
        } else if (this.props.modalAction === 'close') {
            return this.handleToggleModal;
        } else if (this.props.modalAction === 'refresh') {
            return () => {
                this.handleToggleModal();
                this.props.getGroupDetails(this.props.match.params.groupid);
            };
        } else if (this.props.modalAction === 'redirect') {
            return () => {
                this.handleToggleModal();
                history.push('/administration/groups/');
            };
        } else {
            return this.handleToggleModal;
        }
    }
    handleToggleModal(modalAction) {
        const callback = () => {
            this.props.getGroupDetails(this.props.match.params.groupid);
        };
        this.props.toggleModal(modalAction, callback);
    }
    getOptions(input) {
        // const username = document.getElementById('searchUsername').value;
        return this.props.getOptions(input, this.props.usersReference);
    }
    filterOptions(options) {
        const results = options.filter(item => {
            return !this.props.usersReference[item.value];
        });
        return results;
    }
    render() {
        // const { t } = this.props
        return (
            <div key={0}>
                <Container>
                    <Row className="mb-2">
                        <Col>
                            <Link to="/administration/groups" tabIndex="-1">
                                Back To Search
                            </Link>
                        </Col>
                    </Row>
                    {this.props.loading ? (
                        <LoadingSpinner />
                    ) : (
                        [
                            <Row key={0}>
                                <Col xs={12}>
                                    <h5>Status: {this.props.group.status}</h5>
                                    <Form>
                                        <FormGroup row>
                                            <Label sm={2}>Group Name</Label>
                                            <Col sm={10}>
                                                <Input
                                                    style={{ backgroundColor: 'lightgray' }}
                                                    disabled={true}
                                                    value={this.props.group['groupname'] ? this.props.group['groupname'] : ''}
                                                    id="groupname"
                                                    valid={
                                                        this.props.changesMade
                                                            ? this.props.saveButtonPressed
                                                                ? this.props.validation['groupname'].approved
                                                                : !this.props.group['groupname']
                                                                ? null
                                                                : this.props.validation['groupname']
                                                                ? this.props.validation['groupname'].approved
                                                                : false
                                                            : null
                                                    }
                                                    onFocus={event => this.props.handleChange(event)}
                                                    onChange={event => this.props.handleChange(event)}
                                                />
                                                <FormFeedback>
                                                    {this.props.saveButtonPressed ||
                                                    (this.props.group['groupname'] && this.props.validation['groupname'])
                                                        ? this.props.validation['groupname'].errors[0]
                                                        : null}
                                                </FormFeedback>
                                            </Col>
                                        </FormGroup>
                                        <FormGroup row>
                                            <Label sm={2}>Group Description</Label>
                                            <Col sm={10}>
                                                <Input
                                                    value={this.props.group['groupdescription'] ? this.props.group['groupdescription'] : ''}
                                                    id="groupdescription"
                                                    valid={
                                                        this.props.changesMade
                                                            ? this.props.saveButtonPressed
                                                                ? this.props.validation['groupdescription'].approved
                                                                : !this.props.group['groupdescription']
                                                                ? null
                                                                : this.props.validation['groupdescription']
                                                                ? this.props.validation['groupdescription'].approved
                                                                : false
                                                            : null
                                                    }
                                                    onFocus={event => this.props.handleChange(event)}
                                                    onChange={event => this.props.handleChange(event)}
                                                />
                                                <FormFeedback>
                                                    {this.props.saveButtonPressed ||
                                                    (this.props.group['groupdescription'] && this.props.validation['groupdescription'])
                                                        ? this.props.validation['groupdescription'].errors[0]
                                                        : null}
                                                </FormFeedback>
                                            </Col>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>
                                                <h6>{this.props.group.usersList ? this.props.group.usersList.length : 0} Users</h6>
                                            </Label>
                                            <Async
                                                placeholder="Search user to add..."
                                                autoload={false}
                                                onChange={val => {
                                                    if (val.length !== 0) this.props.addUser(val);
                                                }}
                                                name="async"
                                                filterOptions={this.filterOptions}
                                                loadOptions={debounce(this.getOptions, 500)}
                                            />
                                        </FormGroup>
                                    </Form>
                                    <ListGroup>
                                        {this.props.group.usersList
                                            ? this.props.group.usersList.map((user, i) => {
                                                  return (
                                                      <ListGroupItem className="justify-content-between" key={i}>
                                                          <span
                                                              className="lh-2-1-rem"
                                                              style={
                                                                  this.props.removedUsers[user.username]
                                                                      ? { textDecoration: 'line-through' }
                                                                      : null
                                                              }
                                                          >
                                                              {user.username}&nbsp;
                                                          </span>
                                                          <Button
                                                              size="sm"
                                                              onClick={() => this.toggleUserRemove(user)}
                                                              style={{ padding: '5px', float: 'right' }}
                                                              color={this.props.removedUsers[user.username] ? 'default' : 'danger'}
                                                          >
                                                              {this.props.removedUsers[user.username] ? 'Undo Remove' : 'Remove'}
                                                          </Button>
                                                      </ListGroupItem>
                                                  );
                                              })
                                            : null}
                                    </ListGroup>
                                </Col>
                            </Row>,
                            <Row key={1} className="mt-2">
                                <Col>
                                    <Button color="success" onClick={() => this.validateThenToggleModal()}>
                                        Update
                                    </Button>
                                    <Button
                                        className="float-right"
                                        color="danger"
                                        onClick={() => this.handleToggleModal('delete')}
                                        tabIndex="-1"
                                    >
                                        Delete
                                    </Button>
                                </Col>
                            </Row>,
                        ]
                    )}
                </Container>
                <DialogModal
                    key="modal"
                    modal={this.props.modalOpen}
                    toggle={this.toggleAction}
                    customHeader="Confirmation"
                    customModalBody={this.props.saving ? <LoadingSpinner /> : this.props.modalMessage}
                    positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                    positiveButtonText={this.calcCurrentPositiveButtonText()}
                    onlyOneButton={this.props.saved}
                    buttonDisabler={this.props.saving}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ...state.cyderGroupsDetailReducer,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        getGroupDetails: groupid => {
            dispatch(cyderGetGroupDetailsAction(groupid));
        },
        addUserToRemoveList: username => {
            dispatch(cyderRemoveUserListAdd(username));
        },
        removeUserFromRemoveList: username => {
            dispatch(cyderRemoveUserListRemove(username));
        },
        addUser: val => {
            if (val === null) return;
            const username = val.value;
            dispatch(cyderAddNewUserToList(username));
        },
        getOptions: (input, usersReference) => {
            // const username = document.getElementById('searchUsername').value;
            return dispatch(cyderGroupsAddAddUserSearchAction(input));
        },
        removeNewUserFromList: username => {
            // TODO: not used atm. currently, users added to the list
            // will be treated the same way as the intial users obtained
            // through the API. These newly added users can be removed
            // with the same method as the initial users.
            dispatch(cyderRemoveNewUserFromList(username));
        },
        handleChange: event => {
            const key = event.target.id;
            const value = event.target.value;
            dispatch(cyderChangeGroupDetailFieldValueAction(key, value));
        },
        handleSave: () => {
            dispatch(cyderSaveGroupDetailChangesAction());
        },
        toggleModal: (modalAction, callback) => {
            dispatch(cyderGroupDetailsModalToggle(modalAction ? modalAction : null, callback));
        },
        handleDelete: () => dispatch(cyderGroupsDetailDeleteUserAction()),
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Group Detail')),
    };
};
// translate() is if we want to use HOC to perform t()
export default withTranslation()(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(StoredLayout(GroupsDetailPage)),
);
