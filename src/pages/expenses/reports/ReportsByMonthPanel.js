import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { Container, Button, Row, Col } from 'reactstrap';
import ReactTable from 'react-table';
import { getFullReports } from 'actions/expenses/reports/reportsDetailAction.js';
import ActionExecutor from 'js/ActionExecutor';
const formatCurrency = require('format-currency');
const currencyFormatterOptions = { format: '%c%s %v', code: 'SGD', symbol: '$' };

class ReportsByMonthPanel extends Component {
    constructor(props) {
        super(props);
        // this.onFilteredChange = this.onFilteredChange.bind(this);
        this.expandAll = this.expandAll.bind(this);
        this.collapseAll = this.collapseAll.bind(this);
        this.state = {
            reports: [],
            expanded: false,
            startDate: '-',
            endDate: '-',
            status: '-',
            totalEmployees: 0,
            totalExpenses: 0,
            expandedRows: [],
        };
    }

    expandAll() {
        let state = this.state;
        const defaultExpandedRows = this.state.reports.map((element, index) => {
            return { index: true };
        });
        state.expandedRows = defaultExpandedRows;
        state.expanded = true;
        this.setState(state);
    }

    collapseAll() {
        let state = this.state;
        const defaultExpandedRows = this.state.reports.map((element, index) => {
            return { index: false };
        });
        state.expandedRows = defaultExpandedRows;
        state.expanded = false;
        this.setState(state);
    }

    componentDidMount = () => {
        const { getFullReportsFn } = this.props;
        const atob = require('atob');
        const reportParams = JSON.parse(atob(this.props.match.params.reportparam));

        const startDateParam =
            reportParams.startdate != '' ? moment(reportParams.startdate, 'DD/MM/YYYY HH:mm A').format('YYYY-MM-DD HH:mm') : '2000-01-01';
        const endDateParam =
            reportParams.enddate != '' ? moment(reportParams.enddate, 'DD/MM/YYYY HH:mm A').format('YYYY-MM-DD HH:mm') : '2099-01-01';
        const statusParam = reportParams.status;

        const callback = async () => {
            const { data } = await getFullReportsFn(startDateParam, endDateParam, statusParam);
            var emplCount = 0;
            var expenseSum = 0;
            var prevEmpl = '';
            data.forEach(e => {
                expenseSum += e.amount;
                if (e.staff_email !== prevEmpl) {
                    emplCount++;
                    prevEmpl = e.staff_email;
                }
            });
            this.setState({
                reports: data,
                startDate: startDateParam,
                endDate: endDateParam,
                status: statusParam,
                reportParams,
                totalEmployees: emplCount,
                totalExpenses: expenseSum,
            });
        };
        ActionExecutor.execute(() => {}, callback);
    };

    render() {
        return (
            <Container className="wideContainer">
                {this.state.reportParams && (
                    <Row>
                        <Col xs={10}>
                            <br />
                            <h4>
                                Report Period: {this.state.reportParams.startdate ? this.state.reportParams.startdate : 'N/A'} until{' '}
                                {this.state.reportParams.enddate ? this.state.reportParams.enddate : 'N/A'}
                            </h4>
                            <h5>Status: {this.state.reportParams.status}</h5>
                            <h6>Total Expenses: {formatCurrency(this.state.totalExpenses, currencyFormatterOptions)}</h6>
                            <h6>Total Employees: {this.state.totalEmployees}</h6>
                            <br />
                        </Col>
                    </Row>
                )}
                <Row>
                    <Col xs={12}>
                        <ReactTable
                            minRows={1}
                            showPagination={false}
                            data={this.state.reports}
                            defaultPageSize={10000}
                            className="-highlight mb-2"
                            sortable={false}
                            columns={getColumns()}
                            // expanded={this.state.expandedRows}
                            // onExpandedChange={(newExpanded, index) => {
                            //     if (newExpanded[index[0]] === false) {
                            //         newExpanded = {};
                            //     } else {
                            //         Object.keys(newExpanded).map((k) => {
                            //             newExpanded[k] = {};
                            //         });
                            //     }
                            //     this.setState({
                            //         ...this.state,
                            //         expandedRows: newExpanded,
                            //     });
                            // }}
                            NoDataComponent={() => <div className="no-rtData">No record found</div>}
                            SubComponent={v => {
                                return (
                                    <div style={{ padding: '10px' }}>
                                        <img
                                            height="350px"
                                            src={`https://common.api.prod.djarvis.cyder.com.sg/attachment-download?action=download-attachment&appId=123&id=${v.row._original.attachment_id}&folder=receipt`}
                                        />
                                    </div>
                                );
                            }}
                        />
                    </Col>
                </Row>
            </Container>
        );
    }
}

const getColumns = () => {
    let columns = [
        {
            Header: 'Staff Email',
            filterable: false,
            accessor: 'staff_email',
        },
        {
            Header: 'Expense Date',
            filterable: false,
            accessor: 'expense_date',
        },
        {
            Header: 'Category',
            filterable: false,
            accessor: 'category',
        },
        {
            Header: 'Amount',
            filterable: false,
            accessor: 'amount',
            Cell: props => formatCurrency(props.value, currencyFormatterOptions),
        },
    ];

    return columns;
};

const mapStateToProps = state => {
    return {
        ...state.cyderExpenseMyReportsReducer,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getFullReportsFn: (startdate, enddate, status) => {
            return dispatch(getFullReports(startdate, enddate, status));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReportsByMonthPanel);
