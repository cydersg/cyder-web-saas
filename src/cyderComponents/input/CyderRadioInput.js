import React, { useState } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import { Row, Col } from 'reactstrap';
import { RadioGroup, Radio } from 'react-radio-group';

function CyderRadioInput(props) {
    const { id, data, label, defaultVal, onChange = () => {}, row, col, customLabelComponent, labelClassName, labelStyle } = props;
    const [selectedVal, setSelectedVal] = useState(defaultVal);
    const isRow = row || !col;

    // LOCAL ONCHANGE SUPPORT
    const onRadioButtonChange = value => {
        setSelectedVal(value);
        if (onChange) onChange(value, id);
    };

    // GENERATE RADIO OPTIONS
    const getRadioComponents = () => {
        return data.map((x, i) => {
            const { value, label } = x;
            if (!value || !label) return null;

            return (
                <Col key={i} className={classnames('pt-0 pb-0', isRow && 'text-center')}>
                    <>
                        <Radio value={value} />
                        {' ' + label}
                    </>
                </Col>
            );
        });
    };

    // GET INPUT LABEL
    const getLabelComponent = () => {
        if (customLabelComponent) return customLabelComponent;
        return (
            label && (
                <Col className={classnames(labelClassName, col && 'pl-0 pr-0 pt-0')} style={labelStyle}>
                    <b>{label + ': '}</b>
                </Col>
            )
        );
    };

    // DISPLAY RADIO OPTIONS IN ROW OR COLUMN
    const name = id || 'radioGroup';
    return isRow ? (
        <RadioGroup name={name} selectedValue={selectedVal} onChange={onRadioButtonChange}>
            <Row>
                {getLabelComponent()}
                {getRadioComponents()}
            </Row>
        </RadioGroup>
    ) : (
        <RadioGroup name={name} selectedValue={selectedVal} onChange={onRadioButtonChange}>
            <Col>{getLabelComponent()}</Col>
            {getRadioComponents()}
        </RadioGroup>
    );
}

CyderRadioInput.propTypes = {
    row: PropTypes.bool,
    col: PropTypes.bool,
    label: PropTypes.string,
    labelClassName: PropTypes.string,
    labelStyle: PropTypes.object,
    data: PropTypes.array.isRequired,
    onChange: PropTypes.func,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default CyderRadioInput;
