import React, { Component, Fragment } from 'react';

import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

let pageCount = 3;
let start = 0;
let end = pageCount;

export default class TablePagination extends Component {
    constructor(props) {
        super();
        this.state = {
            page: props.page,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ page: nextProps.page });
    }

    getSafePage = page => {
        if (Number.isNaN(page)) page = this.props.page;
        return Math.min(Math.max(page, 0), this.props.pages - 1);
    };

    changePage = page => {
        page = this.getSafePage(page);
        this.setState({ page });
        if (this.props.page !== page) this.props.onPageChange(page);
    };

    applyPage = e => {
        if (e) e.preventDefault();
        const page = this.state.page;
        this.changePage(page === '' ? this.props.page : page);
    };

    pageClick = pageIndex => {
        this.changePage(pageIndex);
    };

    calculateDistance = () => {
        const { page } = this.state;
        const { pages } = this.props;

        var from = start;
        var to = end;

        if (page + 1 > end) {
            from = start += pageCount;
            to = end += pageCount;
            return { from, to };
        }

        if (page < start) {
            from = start -= pageCount;
            to = end -= pageCount;
            return { from, to };
        }

        return { from, to };
    };

    renderPages = () => {
        const { page } = this.state;
        const { pages } = this.props;

        let pageButtons = [];
        const { from, to } = this.calculateDistance();

        for (let i = from < 0 ? 0 : from; i < (to > pages ? pages : to); i++) {
            let active = page === i ? true : false;
            pageButtons.push(
                <PaginationItem key={i} active={active} className="vsdv">
                    <PaginationLink onClick={() => this.pageClick(i)}>{i + 1}</PaginationLink>
                </PaginationItem>,
            );
        }
        return pageButtons;
    };

    render() {
        const { page, pages, canPrevious, canNext, data } = this.props;

        return (
            <Pagination size="sm" aria-label="Page navigation example" listClassName="justify-content-center">
                {data.length > 0 ? (
                    <Fragment>
                        <Paging
                            icon="first_page"
                            className="prev"
                            disabled={!canPrevious}
                            onClick={() => {
                                if (!canPrevious) return;
                                start = 0;
                                end = pageCount;
                                this.changePage(0);
                            }}
                        />
                        <Paging
                            icon="chevron_left"
                            className="prev"
                            disabled={!canPrevious}
                            onClick={() => {
                                if (!canPrevious) return;
                                this.changePage(page - 1);
                            }}
                        />

                        {this.renderPages()}

                        <Paging
                            icon="chevron_right"
                            className="next"
                            disabled={!canNext}
                            onClick={() => {
                                if (!canNext) return;
                                this.changePage(page + 1);
                            }}
                        />
                        <Paging
                            icon="last_page"
                            className="next"
                            disabled={!canNext}
                            onClick={() => {
                                if (!canNext) return;
                                end = pages;
                                start = pages - pageCount;
                                this.changePage(pages);
                            }}
                        />
                    </Fragment>
                ) : null}
            </Pagination>
        );
    }
}

const Paging = props => {
    const { disabled, className, onClick, icon } = props;
    return (
        <PaginationItem className={`${disabled && 'disabled'}`}>
            <PaginationLink className={className} disabled={disabled} onClick={onClick}>
                <i className="material-icons">{icon}</i>
            </PaginationLink>
        </PaginationItem>
    );
};
