import {
    CYDER_EXPENSE_MYRECEIPTS_TOGGLE_CHECKBOX,
    CYDER_EXPENSE_MYRECEIPTS_TOGGLE_CHECKBOX_ALL,
    CYDER_EXPENSE_MYRECEIPTS_GET_RECEIPTS_REQUEST,
    CYDER_EXPENSE_MYRECEIPTS_GET_RECEIPTS_SUCCESS,
    CYDER_EXPENSE_MYRECEIPTS_GET_RECEIPTS_FAILURE,
    CYDER_EXPENSE_MYRECEIPTS_DELETE_RECEIPT_REQUEST,
    CYDER_EXPENSE_MYRECEIPTS_DELETE_RECEIPT_SUCCESS,
    CYDER_EXPENSE_MYRECEIPTS_DELETE_RECEIPT_FAILURE,
    CYDER_EXPENSE_MYRECEIPTS_MODAL_TOGGLE,
} from '../constants/actionTypes';
import withQuery from 'with-query';
import { DJARVIS_EXPENSE_API_ROOT } from '../../config';

export function myReceiptsGetReceipts(status) {
    return (dispatch, getState) => {
        let action = '';
        switch (status) {
            case 'recurring':
                action = 'getbyloginuser';
                break;
            case 'Attention':
                action = 'getdraftandreturnedreceipts';
                break;
            default:
                action = 'getpendingsubmissionreceipts';
        }
        const lambda = status === 'recurring' ? 'recurringcharges' : 'my';
        const url = withQuery(DJARVIS_EXPENSE_API_ROOT + '/' + lambda, {
            appid: getState().cyderProfileReducer.profile.appId,
            action,
        });
        const body = {
            status,
            // userid: "fkarnagi@gmail.com",
        };
        const options = {
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
        };
        dispatch({ type: CYDER_EXPENSE_MYRECEIPTS_GET_RECEIPTS_REQUEST });

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_EXPENSE_MYRECEIPTS_GET_RECEIPTS_FAILURE, response });
                    });
                } else {
                    return response.json().then(response => response);
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_MYRECEIPTS_GET_RECEIPTS_FAILURE, error });
            });
    };
}

export function myReceiptsRenderReceipts(response) {
    return { type: CYDER_EXPENSE_MYRECEIPTS_GET_RECEIPTS_SUCCESS, response };
}

export function myReceiptsDeleteReceipt(recurring) {
    return (dispatch, getState) => {
        const lambda = recurring ? 'recurringcharges' : 'my';
        const action = recurring ? 'delete' : 'deletereceipt';
        const url = withQuery(`${DJARVIS_EXPENSE_API_ROOT}/${lambda}`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action,
        });
        const theId = getState().cyderExpenseMyReceiptsReducer.receiptIdForDeletion;
        const bodyRest = recurring ? { id: theId } : { receiptid: theId };
        const body = {
            ...bodyRest,
            userid: getState().cyderProfileReducer.profile.username,
        };
        const options = {
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(body),
        };

        dispatch({ type: CYDER_EXPENSE_MYRECEIPTS_DELETE_RECEIPT_REQUEST });

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_EXPENSE_MYRECEIPTS_DELETE_RECEIPT_SUCCESS, response, recurring });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_MYRECEIPTS_DELETE_RECEIPT_FAILURE, error, recurring });
                return error;
            });
    };
}
export function toggleCheckboxes(id) {
    return {
        type: CYDER_EXPENSE_MYRECEIPTS_TOGGLE_CHECKBOX,
        id,
    };
}
export function toggleCheckboxesAll() {
    return {
        type: CYDER_EXPENSE_MYRECEIPTS_TOGGLE_CHECKBOX_ALL,
    };
}
export function expenseMyReceiptsToggleModal(modalAction, data, recurring) {
    return {
        type: CYDER_EXPENSE_MYRECEIPTS_MODAL_TOGGLE,
        modalAction,
        data,
        recurring,
    };
}
