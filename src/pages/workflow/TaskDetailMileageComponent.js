import StoredLayout from '../../layouts/StoredLayout'
import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import qs from 'query-string';
import {
    // Table,
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Label,
    // FormFeedback,
} from "reactstrap";

import {
    expenseNewMileageClaimSetFields,
    expenseNewMileageClaimForceLoadingState,
    expenseNewMileageClaimResetFields,
} from '../../actions/expenses/cyderExpenseNewMileageClaimAction.js'
import {
    expenseSharedGetMileageCategories,
    expenseSharedGetProjects,
    expenseSharedGetLocations,
    expenseSharedLoadedData,
} from '../../actions/expenses/cyderExpenseClaimSharedAction'
import {
    getSysparamByCode,
} from '../../actions/settings/cyderSysparamAction'
import LoadingSpinner from '../../cyderComponents/loadingSpinner/LoadingSpinner';
import { setJumbotronTitle } from '../../actions/pagedata'

import cyderlib from '../../js/cyderlib';

class TaskDetailReceiptComponent extends React.Component {
    componentDidMount() {
        this.props.setJumbotronTitle();
        this.props.resetStateStuff().then(() => {
            this.props.getRelevantStuff();
        });
    }
    render() {
        const reportid = qs.parse(this.props.location.search).reportid;
        const realid = qs.parse(this.props.location.search).realid;
        return (
            <Container key={0}>
                <Row className="mb-3">
                    <Col xs={12}>
                        <Link to={"/workflow/task/" + reportid + "?realid=" + realid}>
                            <i className="align-middle material-icons material-icons-3x">chevron_left</i>
                        </Link>
                    </Col>
                </Row>
                {this.props.loading ? <LoadingSpinner/> : (
                    <Row className="mb-2">
                        <Col xs={12} sm={6}>
                            <Form>
                                <FormGroup>
                                    <Label for="expenseDate">Travel Date</Label>
                                    <h5>
                                        {this.props.mileageFields.expensedate}
                                    </h5>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="exampleSelect">Travel Time</Label>
                                    <h5>
                                        {this.props.mileageFields.traveltimestart || ""} - {this.props.mileageFields.traveltimeend || ""}
                                    </h5>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="exampleSelect">Project</Label>
                                    <h5>
                                        {this.props.projectsMap[this.props.mileageFields.project] || ""}
                                    </h5>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="exampleSelect">Travel Mode</Label>
                                    <h5>
                                        {this.props.mileageCategoriesMap[this.props.mileageFields.category]}
                                    </h5>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Reimbursable</Label>
                                    <h5>
                                        {cyderlib.parseReimbursable(this.props.mileageFields.reimbursable)}
                                    </h5>
                                </FormGroup>
                            </Form>
                        </Col>
                        <Col xs={12} sm={6}>
                            <Form>
                                <FormGroup>
                                    <Label for="expenseAmount">Amount</Label>
                                    <h5>
                                        {"S$" + this.props.mileageFields.amount}
                                    </h5>
                                </FormGroup>
                                <FormGroup>
                                    <Label>From</Label>
                                    <h5>
                                        {this.props.mileageFields.from}
                                    </h5>
                                </FormGroup>
                                <FormGroup>
                                    <Label>To</Label>
                                    <h5>
                                        {this.props.mileageFields.to}
                                    </h5>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="remarks">Distance</Label>
                                    <h5>
                                        {cyderlib.formatDistanceMeters(this.props.mileageFields.distance)}
                                    </h5>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="remarks">Purpose</Label>
                                    <h5>
                                        {this.props.mileageFields.remarks}
                                    </h5>
                                </FormGroup>
                            </Form>
                        </Col>
                        {this.props.location ? (
                            <Col xs={12}>
                                Last Saved: {this.props.mileageFields.updateddt}
                            </Col>
                        ) : null}
                    </Row>
                )}
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return Object.assign({
        ...state.cyderExpenseNewMileageClaimReducer,
        ...state.cyderExpenseSharedReducer,
    }, {
        loading: state.cyderExpenseNewMileageClaimReducer.loading,
        featureRT: state.cyderSysparamReducer.featureRT,
    });
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setJumbotronTitle: () => {
            dispatch(setJumbotronTitle('Item Details'));
        },
        getRelevantStuff: () => {
            dispatch(expenseNewMileageClaimForceLoadingState(true));
            const items = [
                dispatch(expenseSharedGetMileageCategories()),
                dispatch(expenseSharedGetLocations()),
                dispatch(expenseSharedGetProjects()),
                dispatch(getSysparamByCode("RT")),
                // dispatch(expenseSharedGetPaymentModes()),
                // dispatch(expenseSharedGetExpenseCategories()),
            ]
            return Promise.all(items).then(() => {
                return dispatch(expenseNewMileageClaimSetFields(ownProps.match.params.itemid));
            }).then((res) => {
                dispatch(expenseNewMileageClaimForceLoadingState(false));
                dispatch(expenseSharedLoadedData());
                return res;
            });
        },
        resetStateStuff: () => {
            return dispatch(expenseNewMileageClaimResetFields());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(TaskDetailReceiptComponent))
