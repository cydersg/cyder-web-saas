import {
    CYDER_SYSPARAMS_GET,
    CYDER_SYSPARAMS_GET_SUCCESS,
    CYDER_SYSPARAMS_SAVE_VALUE,
    CYDER_SYSPARAMS_SAVE_VALUE_SUCCESS,
    CYDER_SYSPARAMS_PAGE_MODAL_OPEN,
    CYDER_SYSPARAMS_PAGE_MODAL_CLOSE,
    CYDER_SYSPARAMS_PAGE_LOADING,
    CYDER_SYSPARAMS_PAGE_MODAL_TOGGLE,
    CYDER_SYSPARAMS_PAGE_MODAL_SET_SYSPARAM,
    CYDER_SYSPARAMS_PAGE_MODAL_CHANGE_VALUE,
    CYDER_SYSPARAMS_SAVE_VALUE_SAVING,
    CYDER_SYSPARAMS_GET_BY_CODE_REQUEST,
    CYDER_SYSPARAMS_GET_BY_CODE_SUCCESS,
    CYDER_SYSPARAMS_GET_BY_CODE_FAILURE,
} from '../../actions/constants/actionTypes'

// defaults
var cyderSysparams = {
    editModalOpen: false,
    editModalSysparamVal: null,
    editModalSysparamCode: null,
    editModalSysparamLabel: null,
    editModalSysparamType: 'textarea',
    savingSysparam: false,
    loading: false,
    sysparams: [],
    featureRT: true,
}

export function cyderSysparamReducer(state = cyderSysparams, action) {
    let newState = Object.assign({}, state);
    // need to clone every layer. for now, only sysparams array will
    // have issues. if sysparams array is modified and we want to
    // see dynamic changes, we have to clone
    // the sysparam array itself with:
    // newState.sysparams = state.sysparams.concat();
    switch (action.type) {
        case CYDER_SYSPARAMS_PAGE_MODAL_SET_SYSPARAM:
            newState.editModalSysparamVal = action.newValue;
            newState.editModalSysparamCode = action.code;
            newState.editModalSysparamLabel = action.label;
            newState.editModalSysparamType = action.optionType;
            return newState;
        case CYDER_SYSPARAMS_PAGE_MODAL_TOGGLE:
            newState.editModalOpen = !state.editModalOpen;
            return newState;
        case CYDER_SYSPARAMS_PAGE_MODAL_OPEN:
            newState.editModalOpen = true;
            return newState;
        case CYDER_SYSPARAMS_PAGE_MODAL_CLOSE:
            newState.editModalOpen = false;
            return newState;
        case CYDER_SYSPARAMS_PAGE_LOADING:
            newState.loading = true;
            return newState;
        case CYDER_SYSPARAMS_SAVE_VALUE:
            // TODO
            return state;
        case CYDER_SYSPARAMS_SAVE_VALUE_SUCCESS:
            // TODO
            return state;
        case CYDER_SYSPARAMS_PAGE_MODAL_CHANGE_VALUE:
            newState.editModalSysparamVal = action.newValue
            return newState;
        case CYDER_SYSPARAMS_GET:
            newState.loading = true;
            return newState;
        case CYDER_SYSPARAMS_GET_SUCCESS:
            newState.savingSysparam = false;
            newState.loading = false;
            newState.sysparams = action.response;
            return newState;
        case CYDER_SYSPARAMS_SAVE_VALUE_SAVING:
            newState.savingSysparam = true;
            return newState;
        case CYDER_SYSPARAMS_GET_BY_CODE_REQUEST:
            return state;
        case CYDER_SYSPARAMS_GET_BY_CODE_SUCCESS:
            newState.featureRT = (action.response.val.toLowerCase() === "Enabled");
            return newState;
        case CYDER_SYSPARAMS_GET_BY_CODE_FAILURE:
            // TODO: might need error catching
            return state;
        default:
            return state;
    }
}
