import React from 'react';

import '../scss/pages/form.scss';

const style1 = {
    width: '75%',
    paddingTop: '20px',
    paddingBottom: '10px',
};

const style2 = {
    display: 'block',
    width: '100%',
    height: 'auto',
};

const CyderForm = ({ title, description, children, className, style, logoPath }) => (
    <div style={style} className={'cyder-form' + (className ? ' ' + className : '')}>
        {logoPath ? (
            <div className="d-flex justify-content-center">
                <div style={style1}>
                    <img alt="" style={style2} src="/assets/images/header_blacktext.png" />
                </div>
            </div>
        ) : (
            <div className="p-3 pb-4 text-center text-uppercase text-inception-less">
                <h6>
                    <strong>{title}</strong>
                </h6>
            </div>
        )}
        <form onSubmit={e => e.preventDefault()}>
            <div className="description mb-4">{description}</div>
            {children}
        </form>
    </div>
);

export default CyderForm;
