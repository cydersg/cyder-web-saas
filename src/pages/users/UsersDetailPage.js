import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import InputField from './UsersInputField';
import { Async } from 'react-select';
import debounce from 'es6-promise-debounce';
import {
    Container,
    Row,
    Col,
    Button,
    // ListGroup,
    // ListGroupItem,
    Form,
    FormGroup,
    Label,
    Input,
    FormFeedback,
} from 'reactstrap';
import {
    withTranslation,
    // Trans,
} from 'react-i18next';
import Switch from 'react-switch';

import {
    cyderUsersDetailGetUserDetailsAction,
    cyderUsersDetailChangeFieldAction,
    cyderUsersDetailSaveChangesAction,
    cyderUsersDetailModalToggleAction,
    cyderUsersDetailDeleteUserAction,
    forceValidate,
} from '../../actions/users/cyderUsersDetailAction';
import { departmentsSettingsSearchAction } from '../../actions/settings/departmentsSettingsAction';
import { setJumbotronTitle } from '../../actions/pagedata';
import LoadingSpinner from '../../cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from '../../cyderComponents/modals/DialogModal';
import StoredLayout from '../../layouts/StoredLayout';

import history from '../../history';
// import cyderlib from '../../js/cyderlib';
import modal from '../../js/modal';

class UsersDetailPage extends React.Component {
    constructor(props) {
        super(props);
        this.setDepartment = this.setDepartment.bind(this);
        this.state = {
            department: null,
        };
        this.validateBeforeToggleModal = this.validateBeforeToggleModal.bind(this);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.toggleAction = this.toggleAction.bind(this);
        this.handleSwitchChange = this.handleSwitchChange.bind(this);
    }
    handleSwitchChange(checked) {
        const synthEvent = {
            target: {
                id: 'admin',
                value: checked,
            },
        };
        this.props.handleChange(synthEvent);
    }
    componentDidMount() {
        this.props.setJumbotronTitle();
        this.props.getUserDetails(this.props.match.params.userid);
    }
    handleDOBChangeWithEvent(momentOrStringVal) {
        const synthEvent = {
            target: {
                id: 'dateofbirth',
                value: momentOrStringVal.format ? momentOrStringVal.format('DD/MM/YYYY') : momentOrStringVal,
            },
        };
        this.props.handleChange(synthEvent);
    }
    validateBeforeToggleModal() {
        this.props.forceValidate(() => {
            const required = {
                email: true,
                firstname: true,
                lastname: true,
                department_code: true,
            };
            for (let key in this.props.validation) {
                if (
                    (required[key] === true && this.props.validation[key].approved === false) ||
                    document.getElementsByClassName('invalid-feedback').length > 0
                ) {
                    console.log('at least one field is not valid');
                    return false;
                }
            }
            console.log('all fields validated');
            this.props.toggleModal('save');
        });
    }
    setDepartment(val) {
        const synthEvent = {
            target: {
                id: 'department_code',
                value: val,
            },
        };
        this.props.handleChange(synthEvent);
    }
    render() {
        // const { t } = this.props
        const validInput = key => {
            if (this.props.saveButtonPressed) {
                if (this.props.validation[key] !== undefined) {
                    const valid = this.props.validation[key].approved;
                    return valid;
                } else {
                    return false;
                }
            } else {
                return null;
            }
        };
        const formFeedbackText = key => {
            if (this.props.validation[key]) {
                return this.props.validation[key].errors[0];
            } else {
                return null;
            }
        };
        const formFeedbackValid = key => {
            if (this.props.validation[key] !== undefined) {
                const valid = this.props.validation[key].approved;
                return valid;
            } else {
                return false;
            }
        };

        const actionMap = {
            refresh: () => {
                this.handleToggleModal();
                this.props.getUserDetails(this.props.match.params.userid);
            },
            save: this.props.handleSave,
            delete: this.props.handleDelete,
            close: this.handleToggleModal,
            default: this.handleToggleModal,
        };
        const additionalProps = {
            validInput: validInput,
            formFeedbackText: formFeedbackText,
            formFeedbackValid: formFeedbackValid,
            handleChange: this.props.handleChange,
        };
        return [
            <Container key={0}>
                <Row className="mb-2">
                    <Col>
                        <Link to="/administration/users" tabIndex="-1">
                            Back To Search
                        </Link>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={12}>
                        {this.props.loading ? (
                            <LoadingSpinner key={0} />
                        ) : (
                            <Form>
                                <FormGroup row>
                                    <Label sm={2}>
                                        Email <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        inputProps={{ disabled: true }}
                                        value={this.props.user['email'] ? this.props.user['email'] : ''}
                                        type="email"
                                        valueKey="email"
                                        noFeedback
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>
                                        First Name <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        value={this.props.user['firstname'] ? this.props.user['firstname'] : ''}
                                        type="text"
                                        valueKey="firstname"
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>
                                        Last Name <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        value={this.props.user['lastname'] ? this.props.user['lastname'] : ''}
                                        type="text"
                                        valueKey="lastname"
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>
                                        Mobile Number <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        value={this.props.user['mobilenumber'] ? this.props.user['mobilenumber'] : ''}
                                        type="text"
                                        valueKey="mobilenumber"
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>Office Number</Label>
                                    <InputField
                                        value={this.props.user['officenumber'] ? this.props.user['officenumber'] : ''}
                                        type="text"
                                        valueKey="officenumber"
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>Company Name</Label>
                                    <Col sm={10}>
                                        <Input
                                            value={this.props.user['companyname']}
                                            type="select"
                                            id="companyname"
                                            onChange={event => this.props.handleChange(event)}
                                        >
                                            <option value="NRIC">{this.props.account_companyname}</option>
                                        </Input>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>Company Address</Label>
                                    <InputField
                                        value={this.props.user['companyaddress'] ? this.props.user['companyaddress'] : ''}
                                        type="text"
                                        valueKey="companyaddress"
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>
                                        Identification Type <span className="text-danger">*</span>
                                    </Label>
                                    <Col sm={10}>
                                        <Input
                                            value={this.props.user['idtype']}
                                            type="select"
                                            id="idtype"
                                            onChange={event => this.props.handleChange(event)}
                                        >
                                            <option value="NRIC">NRIC</option>
                                            <option value="FIN">FIN</option>
                                            <option value="Passport No.">Passport No.</option>
                                            <option value="staffid">Staff ID</option>
                                        </Input>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>Identification No.</Label>
                                    <InputField
                                        value={this.props.user['idno'] ? this.props.user['idno'] : ''}
                                        type="text"
                                        valueKey="idno"
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>
                                        Department <span className="text-danger">*</span>
                                    </Label>
                                    <Col sm={10}>
                                        <Async
                                            placeholder="Search user to add..."
                                            autoload={true}
                                            value={this.props.user.department_code}
                                            onChange={val => {
                                                this.setDepartment(val);
                                            }}
                                            name="async"
                                            loadOptions={debounce(this.props.getDepartments, 500)}
                                        />
                                        <FormFeedback
                                            style={formFeedbackValid ? { display: 'block' } : null}
                                            valid={formFeedbackValid('department_code')}
                                        >
                                            {formFeedbackText('department_code')}
                                        </FormFeedback>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>Admin</Label>
                                    <Col sm={{ size: 10 }}>
                                        <FormGroup>
                                            <Label>
                                                <Switch
                                                    disabled={this.props.user.role === 2}
                                                    onChange={this.handleSwitchChange}
                                                    checked={
                                                        this.props.user.admin !== undefined
                                                            ? this.props.user.admin
                                                            : this.props.user.role > 0
                                                    }
                                                    uncheckedIcon={null}
                                                    id="admin"
                                                />
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                </FormGroup>
                            </Form>
                        )}
                        <Row className="mb-2">
                            <Col>
                                <Button
                                    className="mr-2"
                                    color="success"
                                    disabled={this.props.loading}
                                    onClick={this.validateBeforeToggleModal}
                                    tabIndex="-1"
                                >
                                    Save
                                </Button>
                                <Button
                                    className="float-right"
                                    color="danger"
                                    onClick={() => this.handleToggleModal('delete')}
                                    tabIndex="-1"
                                >
                                    Delete
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>,
            <DialogModal
                key="modal"
                modal={this.props.modalOpen}
                toggle={this.toggleAction}
                customHeader="Confirmation"
                customModalBody={this.props.saving ? <LoadingSpinner /> : this.props.modalMessage}
                positiveButtonAction={actionMap[this.props.modalAction]}
                positiveButtonText={modal.calcCurrentPositiveButtonText(this.props.modalAction)}
                onlyOneButton={this.props.saved}
                buttonDisabler={this.props.saving}
            />,
        ];
    }
    toggleAction() {
        this.handleToggleModal();
        if (this.props.modalAction === 'refresh') {
            this.props.getUserDetails(this.props.match.params.userid);
        } else if (this.props.modalAction === 'redirect') {
            history.push('/administration/users/');
        }
    }
    handleToggleModal(modalAction) {
        const callback = () => {
            this.props.getUserDetails(this.props.match.params.userid);
        };
        this.props.toggleModal(modalAction, callback);
    }
}

const mapStateToProps = state => {
    return {
        ...state.cyderUsersDetailReducer,
        departments: state.departmentSettingsReducer.departments,
        account_companyname: state.cyderProfileReducer.profile.companyname,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        getUserDetails: userid => {
            dispatch(cyderUsersDetailGetUserDetailsAction(userid)).then(res => {
                console.log(res);
            });
        },
        handleChange: event => {
            const key = event.target.id;
            const value = event.target.value;
            dispatch(cyderUsersDetailChangeFieldAction(key, value));
        },
        handleSave: () => {
            dispatch(cyderUsersDetailSaveChangesAction());
        },
        forceValidate: cb => {
            dispatch(forceValidate()).then(() => {
                // execute callback after async action is completed
                cb();
            });
        },
        toggleModal: modalAction => {
            dispatch(cyderUsersDetailModalToggleAction(modalAction ? modalAction : null));
        },
        setJumbotronTitle: () => dispatch(setJumbotronTitle('User Detail')),
        handleDelete: () => dispatch(cyderUsersDetailDeleteUserAction()),
        getDepartments: input => {
            // const username = document.getElementById('searchUsername').value;
            const body = { keyword: input, status: 'Active' };
            return dispatch(departmentsSettingsSearchAction(body, true));
        },
    };
};
// withTranslation() is if we want to use HOC to perform t()
export default withTranslation()(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(StoredLayout(UsersDetailPage)),
);
