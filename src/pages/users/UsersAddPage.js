import React, { Component } from 'react';
import sha512 from 'js-sha512';
import Switch from 'react-switch';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import debounce from 'es6-promise-debounce';
import { withTranslation } from 'react-i18next';
import AsyncSelect from 'react-select/lib/Async';

import history from '../../history';
import InputField from './UsersInputField';
import StoredLayout from '../../layouts/StoredLayout';
import { Input, Button, Container, Row, Col, Form, FormGroup, Label, FormFeedback } from 'reactstrap';
import LoadingSpinner from '../../cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from '../../cyderComponents/modals/DialogModal';

import {
    clearFields,
    addUserAction,
    forceValidate,
    changeFieldValueAction,
    cyderUsersAddMessageModalToggleAction,
} from '../../actions/users/cyderUsersAddAction';
import { cyderGroupsAddAddUserSearchAction } from '../../actions/groups/cyderGroupsAddAction';
import {
    departmentsSettingsAddDepartmentAction,
    departmentsSettingsEditModalChangeValue,
    departmentsSettingsEditModalSetDepartment,
} from '../../actions/settings/departmentsSettingsAction';
import { departmentsSettingsSearchAction } from '../../actions/settings/departmentsSettingsAction';

import modal from '../../js/modal';

class UsersAdd extends Component {
    state = {
        filterAll: '',
        approvers: {},
    };

    componentDidMount() {
        this.props.clearFields();
    }

    handleSwitchChange = checked => {
        const synthEvent = {
            target: {
                id: 'admin',
                value: checked,
            },
        };
        this.props.handleChange(synthEvent);
    };

    handleSave = () => {
        // put all of these into a function, to be executed after forcing validation
        const save = () => {
            let data = Object.assign({}, this.props.fields);
            let pw = this.props.fields.password;
            data.password = sha512(pw);
            delete data['confirmpassword'];
            this.props.handleAdd(data);
        };
        save();
    };

    handleDOBChangeWithEvent(momentOrStringVal) {
        const synthEvent = {
            target: {
                id: 'dateofbirth',
                value: momentOrStringVal.format ? momentOrStringVal.format('DD/MM/YYYY') : momentOrStringVal,
            },
        };
        this.props.handleChange(synthEvent);
    }

    toggleAction = () => {
        this.handleToggleModal();
        if (this.props.modalAction === 'refresh') {
            // this.validateThenSearch();
        } else if (this.props.modalAction === 'redirect') {
            history.push('/administration/users');
        }
    };

    handleToggleModal = modalAction => {
        this.props.toggleModal(modalAction);
    };

    validateBeforeToggleModal = () => {
        this.props.forceValidate(() => {
            const required = {
                email: true,
                firstname: true,
                lastname: true,
                password: true,
                confirmpassword: true,
                mobilenumber: true,
                department_code: true,
            };
            for (let key in this.props.validation) {
                if (required[key] === true && this.props.validation[key].approved === false) {
                    return false;
                }
            }
            this.props.toggleModal('save');
        });
    };

    setDepartment = val => {
        const synthEvent = {
            target: {
                id: 'department_code',
                value: val,
            },
        };
        this.props.handleChange(synthEvent);
    };

    filterOptionsApprovers = options => {
        const results = options.filter(item => {
            let approvers = Object.values(this.state.approvers);
            const hasOption = approvers.findIndex(approver => {
                return approver !== null && approver.value === item.value;
            });
            return hasOption === -1;
        });
        return results;
    };

    getOptionsApprovers = input => {
        return this.props.getOptions(input);
    };

    setApprover = (val, i) => {
        if (i === 1 && val === null) {
            // clear both fields when 1st officer is cleared.
            // previously wouldn't clear 2nd officer
            this.setState({
                approvers: {},
            });
            return;
        }
        let newApprovers = {};
        newApprovers[i] = val;
        this.setState({
            approvers: Object.assign(this.state.approvers, newApprovers),
        });
    };
    render() {
        const customModalBody = (
            <div>
                <FormGroup>
                    <Label for="exampleText">Department Code</Label>
                    <Input
                        id="code"
                        type="text"
                        maxLength={10}
                        disabled={this.props.modalAction === 'update'}
                        value={this.props.modalDepartment.code}
                        onChange={this.props.changeModalInputValue}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleText">Department Name</Label>
                    <Input
                        id="name"
                        type="text"
                        maxLength={100}
                        value={this.props.modalDepartment.name}
                        onChange={this.props.changeModalInputValue}
                    />
                </FormGroup>
                <FormGroup>
                    <Label>Approving Officer 1</Label>
                    <AsyncSelect
                        name="async"
                        autoload={false}
                        placeholder="Search user to add..."
                        value={this.state.approvers[1]}
                        onChange={val => this.setApprover(val, 1)}
                        filterOptions={this.filterOptionsApprovers}
                        loadOptions={debounce(this.getOptionsApprovers, 500)}
                    />
                </FormGroup>
                <FormGroup>
                    <Label>Approving Officer 2</Label>
                    <AsyncSelect
                        name="async"
                        autoload={false}
                        placeholder="Search user to add..."
                        value={this.state.approvers[1] === undefined || this.state.approvers[1] === null ? null : this.state.approvers[2]}
                        disabled={this.state.approvers[1] === undefined || this.state.approvers[1] === null}
                        onChange={val => this.setApprover(val, 2)}
                        filterOptions={this.filterOptionsApprovers}
                        loadOptions={debounce(this.getOptionsApprovers, 500)}
                    />
                </FormGroup>
            </div>
        );
        const validInput = key => {
            if (this.props.saveButtonPressed) {
                if (this.props.validation[key]) return this.props.validation[key].approved;
                return false;
            }
            return null;
        };
        const formFeedbackText = key => {
            if (this.props.validation[key]) return this.props.validation[key].errors[0];
            return null;
        };
        const formFeedbackValid = key => {
            if (this.props.validation[key]) {
                const valid = this.props.validation[key].approved;
                return valid;
            }
            return false;
        };

        const actionMap = {
            save: this.handleSave,
            close: this.handleToggleModal,
            default: this.handleToggleModal,
            update: this.props.handleUpdate,
            delete: this.props.handleDelete,
            refresh: () => this.handleToggleModal(),
            adddepartment: () => this.props.handleAddDepartment(this.state.approvers),
        };

        const additionalProps = {
            validInput: validInput,
            formFeedbackText: formFeedbackText,
            formFeedbackValid: formFeedbackValid,
            handleChange: this.props.handleChange,
        };
        const fieldsStillEmpty = this.props.modalDepartment.code.length === 0 || this.props.modalDepartment.name.length === 0;
        return [
            <Container key={0}>
                <Row className="mb-2">
                    <Col>
                        <Link to="/administration/users" tabIndex="-1">
                            Back To Search
                        </Link>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        {this.props.loading ? (
                            <LoadingSpinner key={0} />
                        ) : (
                            <Form>
                                <FormGroup row>
                                    <Label sm={2}>
                                        Email <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        type="email"
                                        valueKey="email"
                                        value={this.props.fields['email'] || ''}
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>
                                        First Name <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        type="text"
                                        valueKey="firstname"
                                        value={this.props.fields['firstname'] || ''}
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>
                                        Last Name <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        type="text"
                                        valueKey="lastname"
                                        value={this.props.fields['lastname'] || ''}
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>
                                        Password <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        type="password"
                                        valueKey="password"
                                        value={this.props.fields['password'] || ''}
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>
                                        Confirm Password <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        type="password"
                                        valueKey="confirmpassword"
                                        value={this.props.fields['confirmpassword'] || ''}
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>
                                        Mobile Number <span className="text-danger">*</span>
                                    </Label>
                                    <InputField
                                        type="text"
                                        valueKey="mobilenumber"
                                        value={this.props.fields['mobilenumber'] || ''}
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>Office Number</Label>
                                    <InputField
                                        type="text"
                                        valueKey="officenumber"
                                        value={this.props.fields['officenumber'] || ''}
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>Company Name</Label>
                                    <Col sm={10}>
                                        <Input
                                            type="select"
                                            id="companyname"
                                            value={this.props.fields['companyname']}
                                            onChange={event => this.props.handleChange(event)}
                                        >
                                            <option value="NRIC">{this.props.account_companyname}</option>
                                        </Input>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>Company Address</Label>
                                    <InputField
                                        type="text"
                                        valueKey="companyaddress"
                                        value={this.props.fields['companyaddress'] || ''}
                                        {...additionalProps}
                                    />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>Identification Type</Label>
                                    <Col sm={10}>
                                        <Input
                                            id="idtype"
                                            type="select"
                                            value={this.props.fields['idtype']}
                                            onChange={event => this.props.handleChange(event)}
                                        >
                                            <option value="NRIC">NRIC</option>
                                            <option value="FIN">FIN</option>
                                            <option value="Passport No.">Passport No.</option>
                                            <option value="staffid">Staff ID</option>
                                        </Input>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>
                                        Identification No. <span className="text-danger">*</span>
                                    </Label>
                                    <InputField type="text" valueKey="idno" value={this.props.fields['idno'] || ''} {...additionalProps} />
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>
                                        Department <span className="text-danger">*</span>
                                    </Label>
                                    <Col sm={10}>
                                        <div className="d-flex">
                                            <div style={{ width: '100%' }}>
                                                <AsyncSelect
                                                    name="async"
                                                    placeholder="Search department"
                                                    value={this.props.fields.department_code}
                                                    onChange={val => this.setDepartment(val)}
                                                    loadOptions={debounce(this.props.getDepartments, 500)}
                                                />
                                            </div>
                                            <Button
                                                color="success"
                                                className="ml-auto"
                                                onClick={() => this.props.toggleModal('adddepartment')}
                                            >
                                                Add Department
                                            </Button>
                                        </div>
                                        <FormFeedback
                                            style={formFeedbackValid ? { display: 'block' } : null}
                                            valid={formFeedbackValid('department_code')}
                                        >
                                            {formFeedbackText('department_code')}
                                        </FormFeedback>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={2}>Admin</Label>
                                    <Col sm={{ size: 10 }}>
                                        <FormGroup>
                                            <Label>
                                                <Switch
                                                    id="admin"
                                                    uncheckedIcon={null}
                                                    onChange={this.handleSwitchChange}
                                                    checked={this.props.fields.admin || false}
                                                />
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                </FormGroup>
                            </Form>
                        )}
                    </Col>
                </Row>
                <Row className="mb-2">
                    <Col>
                        <Button
                            tabIndex="-1"
                            className="mr-2"
                            color="success"
                            disabled={this.props.loading}
                            onClick={this.validateBeforeToggleModal}
                        >
                            Save
                        </Button>
                    </Col>
                </Row>
            </Container>,
            <DialogModal
                key="modal"
                modal={this.props.modalOpen}
                toggle={this.toggleAction}
                customHeader={this.props.modalHeader || 'Confirmation'}
                customModalBody={this.props.saving ? <LoadingSpinner /> : this.props.modalMessage || customModalBody}
                positiveButtonAction={actionMap[this.props.modalAction]}
                positiveButtonText={modal.calcCurrentPositiveButtonText(this.props.modalAction)}
                onlyOneButton={this.props.saved}
                buttonDisabler={this.props.saving}
                buttonDisablerPositive={!this.props.saved && this.props.modalAction !== 'save' && fieldsStillEmpty}
            />,
        ];
    }
}

const mapStateToProps = state => {
    return {
        ...state.cyderUsersAddReducer,
        departments: state.departmentSettingsReducer.departments,
        account_companyname: state.cyderProfileReducer.profile.companyname,
        modalDepartment: state.departmentSettingsReducer.modalDepartment,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        handleChange: event => {
            const key = event.target.id;
            const value = event.target.value;
            dispatch(changeFieldValueAction(key, value));
        },
        handleAdd: data => {
            const department_code = data.department_code.value;
            data.department_code = department_code;
            dispatch(addUserAction(data, null));
        },
        forceValidate: callback => {
            dispatch(forceValidate()).then(callback);
        },
        toggleModal: modalAction => {
            if (modalAction === 'adddepartment') {
                const modalDepartment = {
                    id: '',
                    code: '',
                    name: '',
                    users: [],
                    approvers: [],
                    approvalvotescount: 1,
                    overrideexpenseapproval: false,
                };
                dispatch(departmentsSettingsEditModalSetDepartment(modalDepartment));
            }
            dispatch(cyderUsersAddMessageModalToggleAction(modalAction || null));
        },
        clearFields: () => {
            dispatch(clearFields());
        },
        getDepartments: input => {
            console.log('hello');
            const body = { keyword: input || '', status: 'Active' };
            return dispatch(departmentsSettingsSearchAction(body, true));
        },
        handleAddDepartment: approvers => {
            dispatch(departmentsSettingsAddDepartmentAction(approvers));
        },
        changeModalInputValue: event => {
            const { value, id } = event.target;
            dispatch(departmentsSettingsEditModalChangeValue(id, value));
        },
        getOptions: input => {
            return dispatch(cyderGroupsAddAddUserSearchAction(input));
        },
    };
};

export default withTranslation()(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(StoredLayout(UsersAdd)),
);
