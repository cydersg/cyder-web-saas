import withQuery from 'with-query';

import fetchlib from 'js/fetchlib';
import { CYDER_COMMON_API_ROOT, DJARVIS_COMMON_API_ROOT, CYDER_LEAVE_API_ROOT } from 'config.js';

export function getYearlyReports(body) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getallreports',
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, body, null);
    };
}

export function getYearlyReportsByDateRange(body) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getallreportsbydaterange',
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, body, null);
    };
}

export function findMonthlyActivity(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'findreportbyid',
            id,
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url);
    };
}

export function findMonthlyActivityByDateRange(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'findreportbyiddaterange',
            id,
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url);
    };
}

export function findMonthlyActivityByReportId(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'findreportactivitiesbyid',
            id,
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url);
    };
}

export function getMonthlyActivities(body) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getallactivities',
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, body, null);
    };
}

export function getDateRangeActivities(body) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getallactivitiesbydaterange',
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, body, null);
    };
}

export function getMonthlyLeaveSummary(body) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/leave`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getallleaves',
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}

export function getAllLeaveSummary(body) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/leave`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'geteveryoneleaves',
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}

export function getApprovers() {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_COMMON_API_ROOT}/users`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'getapprovers',
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, null, null, null, null, true);
    };
}

export function submitTimesheetReport(body) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'submitreport',
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, body, null);
    };
}

export function submitTimesheetReportByDateRange(body) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'submitreportbydaterange',
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url, body, null);
    };
}

export function getProjects() {
    return (dispatch, getState) => {
        const url = withQuery(DJARVIS_COMMON_API_ROOT + '/project', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'findall',
        });
        return fetchlib.fetch(dispatch, getState, 'GET', url);
    };
}

export function deleteTimesheetActivity(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'deleteactivity',
            id,
        });
        return fetchlib.fetch(dispatch, getState, 'GET', url);
    };
}

export function deleteTimesheetReport(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'deletereport',
            id,
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url);
    };
}

export function deleteLeave(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/leave`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'delete',
            id,
        });
        return fetchlib.fetch(dispatch, getState, 'POST', url);
    };
}

export function findLeaveById(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/leave`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'findleavebyid',
        });
        const body = {
            id,
        };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}
