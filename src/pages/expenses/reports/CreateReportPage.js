import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import history from 'history.js';
import withQuery from 'with-query';

import ReactTable from 'react-table';
import Select from 'react-select';
import StoredLayout from 'layouts/StoredLayout';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import { Row, Col, Container, Button, Label, Input, FormText } from 'reactstrap';

import {
    expenseCreateReportToggleModal,
    expenseCreateReportRemoveItemFromList,
    expenseCreateReportSubmitReport,
    expenseCreateReportResetState,
    expenseCreateReportGetApprovers,
} from 'actions/expenses/cyderExpenseCreateReportAction.js';
import { setJumbotronTitle } from 'actions/pagedata';

import cyderlib from 'js/cyderlib.js';
import modal from 'js/modal.js';
import 'css/forms/default-forms.css';
import { expenseDeletableStatuses, urlRecurring, urlReceipt, layoutLib } from 'js/constlib';

class CreateReportPage extends React.Component {
    constructor(props) {
        super(props);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.toggleAction = this.toggleAction.bind(this);
        this.state = {
            selectedApprovers: [],
        };
    }
    componentDidMount() {
        this.props.setJumbotronTitle('Submit Expense Report');
        this.props.expenseCreateReportGetApprovers();
        this.props.expenseCreateReportResetState();
    }
    handleToggleModal(modalAction, data) {
        this.props.toggleModal(modalAction, data);
    }
    toggleAction() {
        const { modalAction } = this.props;
        this.handleToggleModal();
        if (modalAction === 'redirect') history.push('/expense/myreports');
    }
    calcCurrentPositiveButtonAction() {
        const { modalAction, handleSave, handleRemove, receipts, selectedItems } = this.props;
        switch (modalAction) {
            case 'save':
                const remarks = document.getElementById('remarks').value;
                const receiptsData = this.getSelectedReceiptData(receipts, selectedItems);
                return () => handleSave(receiptsData, remarks, this.state.selectedApprovers);
            case 'redirect':
                return () => {
                    history.push('/expense/myreports');
                    this.handleToggleModal();
                };
            case 'remove':
                return handleRemove;
            default:
                return this.handleToggleModal;
        }
    }
    getSelectedReceiptData(receipts, selectedItems) {
        let data = receipts.map(item => (selectedItems[item.receiptId] ? item : null)).filter(x => x);
        data.push({ amount: 0 }); // For last row that display total amount
        return data;
    }
    handleSelectChange(value) {
        this.setState({ selectedApprovers: value });
    }
    render() {
        const { receipts, approverList, selectedItems, modalOpen, modalHeader, modalMessage, modalAction, saving, saved } = this.props;
        const { selectedApprovers } = this.state;
        const data = this.getSelectedReceiptData(receipts, selectedItems);
        return (
            <Container className="wideContainer">
                <ReactTable className="-highlight mb-2" showPagination={false} data={data} columns={getColumn(data)} minRows={0} />

                <Container className="wideContainer">
                    <Row className="mt-4 mb-2">
                        <Label sm={2} className="font-weight-bold">
                            Approvers
                        </Label>
                        <Col sm={10}>
                            <Select
                                closeMenuOnSelect={false}
                                isMulti
                                onChange={this.handleSelectChange}
                                placeholder="Select approvers"
                                removeSelected={true}
                                options={selectedApprovers.length < 4 ? approverList : selectedApprovers}
                                value={selectedApprovers}
                            />
                            <FormText>Select up to 4 approvers.</FormText>
                        </Col>
                    </Row>
                    <Row className="mb-2">
                        <Label sm={2} className="font-weight-bold">
                            Remarks
                        </Label>
                        <Col sm={10}>
                            <Input type="textarea" id="remarks" />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button
                                color="success"
                                className="mr-2"
                                disabled={selectedApprovers.length === 0 || Object.keys(selectedItems).length === 0}
                                onClick={() => this.handleToggleModal('save')}
                            >
                                Submit
                            </Button>
                            <Link to="/expense/myreceipts">
                                <Button color="danger">Cancel</Button>
                            </Link>
                        </Col>
                    </Row>
                </Container>
                <DialogModal
                    key="modal"
                    modal={modalOpen}
                    toggle={this.toggleAction}
                    customHeader={modalHeader || 'Confirmation'}
                    customModalBody={saving ? <LoadingSpinner /> : modalMessage}
                    positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                    positiveButtonText={modal.calcCurrentPositiveButtonText(modalAction)}
                    onlyOneButton={saved}
                    buttonDisabler={saving}
                />
            </Container>
        );
    }
}

function getColumn(data) {
    // const formatCurrency = centValue => (centValue / 100).toFixed(2) + ' SGD';
    const getTotalAmount = data => data.reduce((accumulated, x) => x.amount + accumulated, 0);
    const totalAmount = getTotalAmount(data);

    const formatCurrency = amount => {
        if (amount == null) return 'Amount Not Found';
        return (
            'S$ ' +
            Number(amount / 100).toLocaleString('en-EN', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
            })
        );
    };

    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return [
        {
            Header: '',
            accessor: 'type',
            width: 50,
            sortable: false,
            filterable: false,
            show: false,
            Cell: props => {
                const { value } = props;
                if (props.index === data.length - 1) return null;
                if (value === 'Mileage') return <i className="align-middle material-icons">directions_car</i>;
                if (value === 'Receipt') return <i className="align-middle material-icons">receipt</i>;
                return value;
            },
        },
        {
            Header: 'Date',
            accessor: 'expensedate',
            width: 100,
            show: !isMobile,
            sortable: false,
            filterable: false,
            sortMethod: (a, b, desc) => cyderlib.tableSortDate(a, b, desc, 'DD-MMM-YYYY'),
            Cell: props => {
                if (props.index === data.length - 1) return null;
                return moment(props.value, 'YYYY-MM-DD').format('DD/MM/YYYY');
            },
        },
        {
            Header: 'Category',
            accessor: 'category',
            sortable: false,
            filterable: false,
            show: !isMobile,
            width: 125,
            filterMethod: (filter, row) => row[filter.id].toLowerCase().indexOf(filter.value.toLowerCase()) !== -1,
            Cell: props => {
                if (props.index === data.length - 1) return null;
                return props.value;
            },
        },
        {
            Header: 'Remarks',
            accessor: 'remarks',
            sortable: false,
            show: !isMobile,
            filterable: false,
            filterMethod: (filter, row) => row[filter.id].toLowerCase().indexOf(filter.value.toLowerCase()) !== -1,
            Cell: props => {
                if (props.index === data.length - 1) return null;
                return props.value;
            },
        },
        {
            Header: <div className="float-right">Amount</div>,
            accessor: 'amount',
            sortable: false,
            show: !isMobile,
            width: 100,
            filterable: false,
            style: { overflow: 'inherit' },
            Cell: props => {
                if (props.index === data.length - 1) {
                    return (
                        <div className="text-nowrap">
                            <Row>
                                <Col className="text-right text-muted font-weight-bold">Total Amount</Col>
                            </Row>
                            <Row>
                                <Col className="pt-2">
                                    <h4 className="float-right font-weight-bold">{formatCurrency(totalAmount)}</h4>
                                </Col>
                            </Row>
                        </div>
                    );
                }
                return <div className="float-right font-weight-bold">{formatCurrency(props.value)}</div>;
            },
        },
        {
            Header: '',
            accessor: 'id',
            sortable: false,
            show: isMobile,
            filterable: false,
            Cell: props => {
                if (props.index === data.length - 1) {
                    return null;
                } else {
                    return (
                        <div className="boxShadow">
                            <div className="d-flex justify-content-between">
                                <strong>{moment(props.original.expensedate).format('DD MMM YYYY')}</strong>
                                <span>{cyderlib.formatCurrencyCent(props.original.amount)}</span>
                            </div>
                            <span>{props.original.category}</span>
                            <br />
                            {props.original.remarks}
                        </div>
                    );
                }
            },
        },
    ];
}

const mapStateToProps = (state, ownProps) => {
    return {
        receipts: state.cyderExpenseMyReceiptsReducer.receipts,
        selectedItems: state.cyderExpenseMyReceiptsReducer.selectedItems,
        ...state.cyderExpenseReportsCreateReducer,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        toggleModal: (modalAction, data) => {
            dispatch(expenseCreateReportToggleModal(modalAction ? modalAction : null, data));
        },
        handleSave: (data, remarks, approvers) => dispatch(expenseCreateReportSubmitReport(data, remarks, approvers)),
        handleRemove: () => {
            dispatch(expenseCreateReportRemoveItemFromList());
        },
        setJumbotronTitle: title => dispatch(setJumbotronTitle(title)),
        expenseCreateReportResetState: () => dispatch(expenseCreateReportResetState()),
        expenseCreateReportGetApprovers: () => dispatch(expenseCreateReportGetApprovers()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(CreateReportPage));
