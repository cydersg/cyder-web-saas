import React, { useRef, useState, useEffect, useCallback, Fragment } from 'react';
import Dropzone from 'react-dropzone';
import classnames from 'classnames';
import Viewer from 'react-viewer';

import { Button, Col, FormGroup, Label, Row } from 'reactstrap';

import { openFile } from 'js/generalUtils';

const CyderFileUploader = props => {
    const { disabled = false, id, mandatory, label, onUpload, imageFile } = props;
    const dropzone = useRef();

    const [visible, setVisible] = useState(false);
    const [file, setFile] = useState({
        b64img: '',
        imagebtoa: '',
        imageFile: {},
        contentType: '',
    });

    const onDrop = useCallback(
        imageFiles => {
            imageFiles.forEach(file => {
                const reader = new FileReader();
                reader.onload = () => {
                    const data = {
                        b64img: reader.result,
                        imagebtoa: btoa(reader.result),
                        imageFile: file,
                        contentType: file.type,
                        name: file.name,
                    };
                    setFile(data);
                    onUpload && onUpload(data);
                };
                reader.readAsDataURL(file);
            });
        },
        [onUpload],
    );

    const downloadImage = useCallback(url => {
        const parsedUrl = url.replace(/^data:image\/[^;]+/, 'data:application/octet-stream');
        openFile(parsedUrl, file.imageFile.name);
    }, []);

    const toggleVisible = useCallback(() => {
        setVisible(!visible);
    });

    useEffect(() => {
        if (imageFile) {
            setFile({
                ...imageFile,
            });
        }
    }, [imageFile]);

    const imageUrl = file.b64img && file.b64img.indexOf('base64') < 0 ? `data:image/png;base64, ${file.b64img}` : file.b64img;

    return (
        <FormGroup>
            <Label
                for={id}
                className={classnames('text-bold', {
                    mandatory: mandatory ? true : false,
                })}
            >
                {label} {mandatory && <span>*</span>}
            </Label>
            <Viewer
                noNavbar
                zIndex={9999}
                zoomSpeed={0.1}
                scalable={false}
                changeable={false}
                downloadable={true}
                visible={visible}
                onClose={toggleVisible}
                images={[{ src: imageUrl, alt: '' }]}
            />
            <div>
                <img alt={id} className="preview-image" onClick={toggleVisible} src={imageUrl} />
                {!disabled ? (
                    <Fragment>
                        <div className="dropzone" style={{ display: imageUrl && 'none' }}>
                            <Dropzone onDrop={onDrop} ref={dropzone}>
                                {({ getRootProps, getInputProps }) => {
                                    return (
                                        <Col className="p-0">
                                            <section>
                                                <div {...getRootProps()}>
                                                    <input {...getInputProps()} />
                                                    <div className="dropzone-content">
                                                        <i className="material-icons">cloud_upload</i>
                                                        <p>Drag 'n' drop image here or click to select file</p>
                                                    </div>
                                                </div>
                                            </section>
                                        </Col>
                                    );
                                }}
                            </Dropzone>
                        </div>
                        <Row>
                            <Col xs={imageUrl ? '6' : '12'}>
                                <Button
                                    block
                                    color="primary"
                                    className="mt-3"
                                    onClick={() => {
                                        if (!dropzone) return;
                                        dropzone.current.open();
                                    }}
                                >
                                    Upload
                                </Button>
                            </Col>
                            {imageUrl && (
                                <Col xs="6">
                                    <Button block color="danger" className="mt-3" onClick={() => setFile({})}>
                                        Remove
                                    </Button>
                                </Col>
                            )}
                        </Row>
                    </Fragment>
                ) : null}
                {file.b64img ? (
                    <Row className="mt-2">
                        <Col>
                            <Button block color="default" className="ml-auto" onClick={() => downloadImage(imageUrl)}>
                                <i className="align-middle material-icons">file_download</i> Download Image
                            </Button>
                        </Col>
                    </Row>
                ) : null}
            </div>
        </FormGroup>
    );
};

export default CyderFileUploader;
