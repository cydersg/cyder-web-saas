import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import withQuery from 'with-query';
import history from 'history.js';

import Table from 'react-table';
import { Badge, Container } from 'reactstrap';
import StoredLayout from 'layouts/StoredLayout';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import { TablePagination } from 'cyderComponents/pagination/';

import { setJumbotronTitle } from 'actions/pagedata';
import * as Actions from 'actions/workflow/myTasksAction';
import ActionExecutor from 'js/ActionExecutor';
import cyderlib from 'js/cyderlib';
import { layoutLib } from 'js/constlib';

const { myTasksGetTasks } = Actions;

class LeaveTasksPage extends Component {
    state = {
        loading: false,
        reports: [],
    };

    componentDidMount() {
        this.props.setJumbotronTitle('Leave Approval');
        this.getLeaveTasks();
    }

    showLoading = loading => {
        this.setState({ loading });
    };

    getLeaveTasks = () => {
        const callback = async () => {
            const { data } = await this.props.myTasksGetTasks('Leave Request');
            this.setState({ reports: data && data.reverse() });
        };
        ActionExecutor.execute(this.showLoading, callback);
    };

    getTdProps(state, rowInfo) {
        return {
            onClick: (e, handleOriginal) => {
                if (!rowInfo) return;
                const { instance_id, id } = rowInfo.original;
                const url = withQuery(
                    `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'leavetype'}/leaves/leavetask/detail/${instance_id}`,
                    {
                        realid: id,
                    },
                );
                history.push(url);
                if (handleOriginal) handleOriginal();
            },
        };
    }

    render() {
        const { loading, reports } = this.state;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        if (loading) return <LoadingSpinner center />;
        return (
            <Container className="wideContainer">
                <div key="rt-table">
                    <Table
                        showPagination={!isMobile}
                        className="-highlight mb-2"
                        minRows={1}
                        data={reports}
                        columns={getColumns()}
                        // NoDataComponent={() => <div style={{ display: 'none' }} />}
                        getTdProps={this.getTdProps}
                        PaginationComponent={TablePagination}
                    />
                </div>
            </Container>
        );
    }
}

const getColumns = () => {
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return [
        {
            Header: 'Created Date',
            accessor: 'createddt',
            show: !isMobile,
            width: 120,
            Cell: props => {
                return moment(props.value, 'YYYY-MM-DD HH:mm:ss').format('DD-MMM-YYYY');
            },
        },
        {
            Header: 'Type',
            width: 200,
            show: !isMobile,
            accessor: 'resource_type',
        },
        {
            Header: 'Originator',
            show: !isMobile,
            accessor: 'originator_fullname',
        },
        {
            Header: 'Status',
            width: 80,
            show: !isMobile,
            accessor: 'status',
            Cell: props => (
                <Badge className="badge-info badge-sm" color={cyderlib.statusBadgeColor(props.value)}>
                    {props.value}
                </Badge>
            ),
        },
        {
            Header: '',
            headerClassName: 'rt-no-sort-ind',
            accessor: 'instance_id',
            width: 50,
            show: !isMobile,
            Cell: props => {
                return <Link className="font-weight-bold">View</Link>;
            },
        },
        {
            Header: '',
            show: isMobile,
            accessor: 'id',
            Cell: props => {
                const { fromsession, tosession } = props.original;
                return (
                    <Link
                        style={{
                            color: 'unset',
                        }}
                        to={`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'leavetype'}/leaves/leaveplan/detail/${
                            props.value
                        }`}
                    >
                        <div className="boxShadow">
                            <span>
                                <div className="font-weight-bold">
                                    {moment(props.original.createddt, 'YYYY-MM-DD HH:mm:ss').format('DD-MMM-YYYY')}
                                </div>
                            </span>
                            <span>
                                <strong>{props.original.resource_type}</strong>
                            </span>
                            <br />
                            <span>{props.original.originator_fullname}</span>
                            <br />
                            <Badge className="badge-info badge-sm" color={cyderlib.statusBadgeColor(props.original.status)}>
                                {props.original.status}
                            </Badge>
                        </div>
                    </Link>
                );
            },
        },
    ];
};

const reducer = state => {
    return {};
};

const actionList = {
    setJumbotronTitle,
    myTasksGetTasks,
};

export default connect(reducer, actionList)(StoredLayout(LeaveTasksPage));
