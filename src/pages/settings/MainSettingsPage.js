import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch } from 'react-router';
import { Link } from 'react-router-dom';
import history from 'history.js';

import { Nav, NavItem, Container } from 'reactstrap';
import PrivateRoute from 'cyderComponents/common/PrivateRoute';

// EXPENSE SETTINGS
import ExpenseCategoriesSettingsPage from 'pages/settings/expense/ExpenseCategoriesSettingsPage';
import MileageCategoriesSettingsPage from 'pages/settings/expense/MileageCategoriesSettingsPage';
import PaymentModesSettingsPage from 'pages/settings/expense/PaymentModesSettingsPage';
import DepartmentsSettingsPage from 'pages/settings/expense/DepartmentsSettingsPage';
import ProjectsSettingsPage from 'pages/settings/expense/ProjectsSettingsPage';
import LocationsSettingsPage from 'pages/settings/expense/LocationsSettingsPage';
import SysparamsPage from 'pages/settings/expense/Sysparams';
import DelegationsPage from 'pages/workflow/DelegationsPage';

// LEAVE SETTINGS
import LeaveTypesSettingsPage from 'pages/settings/leave/LeaveTypesSettingsPage';
import LeaveBalanceUserSettingsPage from 'pages/settings/leave/LeaveBalanceUserSettingsPage';
import LeaveHolidaySettingsPage from 'pages/settings/leave/LeaveHolidaySettingsPage';

import {
    expenseSharedGetProjects,
    expenseSharedGetLocations,
    expenseSharedGetPaymentModes,
    expenseSharedGetExpenseCategories,
    expenseSharedGetMileageCategories,
    expenseSharedLoadedData,
} from 'actions/expenses/cyderExpenseClaimSharedAction';
import { forceLoading } from 'actions/common/homeAction';
import { setJumbotronTitle } from 'actions/pagedata';

const expenseSettingsUrls = {
    expensecategories: {
        url: '/settings/expense/expensecategories',
        text: 'Expense Categories',
        reactElement: ExpenseCategoriesSettingsPage,
        accessLevel: 1,
    },
    mileagecategories: {
        url: '/settings/expense/mileagecategories',
        text: 'Mileage Categories',
        reactElement: MileageCategoriesSettingsPage,
        accessLevel: 1,
    },
    departments: {
        url: '/settings/expense/departments',
        text: 'Departments',
        reactElement: DepartmentsSettingsPage,
        accessLevel: 1,
    },
    projects: {
        url: '/settings/expense/projects',
        text: 'Projects',
        reactElement: ProjectsSettingsPage,
        accessLevel: 1,
    },
    locations: {
        url: '/settings/expense/locations',
        text: 'Locations',
        reactElement: LocationsSettingsPage,
        accessLevel: 1,
    },
    sysparams: {
        url: '/settings/expense/sysparams',
        text: 'System',
        reactElement: SysparamsPage,
        accessLevel: 1,
    },
    paymentmodes: {
        url: '/settings/expense/paymentmodes',
        text: 'Payment Modes',
        reactElement: PaymentModesSettingsPage,
        accessLevel: 1,
    },
    delegations: {
        url: '/settings/expense/delegations',
        text: 'Delegations',
        reactElement: DelegationsPage,
        accessLevel: 0,
    },
};

const leaveTypeSettingsUrls = {
    leavetype: {
        url: '/settings/leave/leavetype',
        text: 'Leave Type',
        reactElement: LeaveTypesSettingsPage,
        accessLevel: 1,
    },
    leavebalance: {
        url: '/settings/leave/leavebalance',
        text: 'Leave Balance',
        reactElement: LeaveBalanceUserSettingsPage,
        accessLevel: 1,
    },
    leaveholiday: {
        url: '/settings/leave/holidays',
        text: 'Holidays',
        reactElement: LeaveHolidaySettingsPage,
        accessLevel: 1,
    },
};

class MainSettingsPage extends Component {
    state = {
        dropdownOpen: false,
    };

    componentDidMount() {
        this.props.setJumbotronTitle();
        this.setDefaultSettings();
    }

    componentDidUpdate(prevProps) {
        const { pathname } = this.props.location;
        if (pathname === '/settings/expense/' || pathname === '/settings/leave/') {
            this.setDefaultSettings();
        }
    }

    setDefaultSettings = () => {
        const { profile, history, location } = this.props;
        const replacement = location.pathname.includes('expense') ? `expense/expensecategories` : 'leave/leavetype';
        if (profile.role > 0) history.replace(`/settings/${replacement}`);
    };

    toggle = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen,
        });
    };

    render() {
        const { config, profile, location } = this.props;
        const isExpenseNavigation = config.navigationMenu === 'expenseNavigation';
        const settingsUrls = isExpenseNavigation ? expenseSettingsUrls : leaveTypeSettingsUrls;

        return (
            <div>
                <Container>
                    <Nav tabs className="mb-3 d-flex">
                        {Object.keys(settingsUrls).map((key, i) => {
                            const isDefault = profile.role > 0 ? i === 0 : null;
                            if (profile.role <= 0) return null;
                            if (profile)
                                return (
                                    <SettingsNavLink
                                        key={i}
                                        linkTo={settingsUrls[key]}
                                        defaultNav={isDefault}
                                        pathname={location.pathname}
                                    />
                                );
                        })}
                    </Nav>
                </Container>
                {profile.role <= 0 && <Container className="text-center">No settings available</Container>}
                <Switch>
                    {Object.keys(settingsUrls).map((item, i) => {
                        return (
                            <PrivateRoute
                                key={i}
                                exact
                                accessLevel={settingsUrls[item].accessLevel}
                                path={settingsUrls[item].url}
                                component={settingsUrls[item].reactElement}
                            />
                        );
                    })}
                </Switch>
            </div>
        );
    }
}

const SettingsNavLink = ({ linkTo, defaultNav, pathname }) => {
    const isOnDefaultUrl = defaultNav && pathname === '/settings';
    return (
        <NavItem>
            <Link
                to="#"
                onClick={() => history.replace(linkTo.url)}
                className={'nav-link ' + (pathname === linkTo.url || isOnDefaultUrl ? 'active' : null)}
            >
                <b>{linkTo.text}</b>
            </Link>
        </NavItem>
    );
};

const mapStateToProps = state => {
    return {
        profile: state.cyderProfileReducer.profile,
        loading: state.homeReducer.loading,
        config: state.config,
        ...state.cyderExpenseSharedReducer,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setJumbotronTitle: () => dispatch(setJumbotronTitle('Settings')),
        getRelevantStuff: () => {
            dispatch(forceLoading(true));
            const items = [
                dispatch(expenseSharedGetProjects()),
                dispatch(expenseSharedGetLocations()),
                dispatch(expenseSharedGetExpenseCategories()),
                dispatch(expenseSharedGetPaymentModes()),
                dispatch(expenseSharedGetMileageCategories()),
                dispatch(expenseSharedLoadedData()),
            ];
            return Promise.all(items).then(() => dispatch(forceLoading(false)));
        },
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(MainSettingsPage);
