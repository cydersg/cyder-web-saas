import {
    CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_REQUEST,
    CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_SUCCESS,
    CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_FAILURE,
} from '../constants/actionTypes';
import withQuery from 'with-query';
import { DJARVIS_EXPENSE_API_ROOT } from '../../config';

export function expenseNewReceiptClaimSaveReceipt(sdf) {
    // Redux Thunk will inject dispatch here:
    return (dispatch, getState) => {
        dispatch({ type: CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_REQUEST });
        const url = withQuery(DJARVIS_EXPENSE_API_ROOT + '/receipt', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'addreceipt',
        });
        const data = Object.assign({}, getState().cyderExpenseNewReceiptClaimReducer.receiptFields);
        // fix for select defaults
        const actualData = {
            ...sdf,
            amount: data.amount * 100,
            userid: getState().cyderProfileReducer.profile.username,
            expensedate: data.expensedate,
            receiptno: data.receiptno,
            receiptId: data.receiptId,
            remarks: data.remarks,
        };
        const options = {
            headers: {
                'x-api-key': getState().config.apiKey,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
            body: JSON.stringify(actualData),
            method: 'POST',
        };

        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        dispatch({ type: CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_FAILURE, error });
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        dispatch({ type: CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_SUCCESS, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_EXPENSE_REPORTS_CREATE_SUBMIT_EXPENSE_REPORT_FAILURE, error });
                return error;
            });
    };
}
