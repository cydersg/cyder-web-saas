import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import withQuery from 'with-query';
import moment from 'moment';
import qs from 'query-string';
import history from '../../../history';

import { Button, Col, Container, Form, FormGroup, Input, Label, Row } from 'reactstrap';
import ReactTable from 'react-table';
import StoredLayout from 'layouts/StoredLayout';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import DialogModal from 'cyderComponents/modals/DialogModal';
import LinkButton from 'cyderComponents/buttons/LinkButton';
import StatusBadge from 'cyderComponents/common/StatusBadge';
import ExpenseTypeBadge from 'cyderComponents/common/ExpenseTypeBadge';
import { TablePagination } from 'cyderComponents/pagination/';

import {
    toggleCheckboxes,
    toggleCheckboxesAll,
    myReceiptsGetReceipts,
    myReceiptsRenderReceipts,
    myReceiptsDeleteReceipt,
    expenseMyReceiptsToggleModal,
} from 'actions/expenses/cyderExpenseMyReceiptsAction.js';
import { expenseSharedGetMileageCategories, expenseSharedGetExpenseCategories } from 'actions/expenses/cyderExpenseClaimSharedAction';
import { setJumbotronTitle } from 'actions/pagedata';

import cyderlib from 'js/cyderlib.js';
import reactTableFilter from 'js/reactTableFilter';
import { expenseDeletableStatuses, urlRecurring, urlReceipt, layoutLib } from 'js/constlib';

class MyReceiptsPage extends Component {
    state = {
        filterAll: '',
    };
    componentDidMount() {
        this.getReceipts();
    }
    componentDidUpdate(prevProps) {
        // needed because My Expenses and Recurring Charges uses the same Component class
        if (prevProps.recurring !== this.props.recurring) this.getReceipts();
    }
    getRecurringOrReceiptUrl = () => {
        return this.props.recurring ? urlRecurring : urlReceipt;
    };
    handleDelete = () => {
        this.props.handleDelete(this.props.recurring);
    };
    getReceipts = () => {
        this.props.getReceipts(this.props.recurring);
    };
    tabNav = (index, url) => {
        history.replace(url);
    };
    onFilteredChange = filtered => {
        this.setState(reactTableFilter.onFilteredChange(filtered));
    };
    filterAllFunc = e => {
        this.setState(reactTableFilter.filterAllFunc(e));
    };
    render() {
        const status = qs.parse(this.props.location.search).status;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        return (
            <Container className="wideContainer">
                <Row>
                    <Col xs={12} md={4}>
                        <Row>
                            <Col xs={6}>
                                <LinkButton
                                    color="success"
                                    altColor="light"
                                    to={
                                        this.props.recurring
                                            ? `/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/recurring/new`
                                            : `/${
                                                  window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'
                                              }/myreceipts/new/receipt`
                                    }
                                    materialIcon="add"
                                    text={this.props.recurring ? 'Recurring Charge' : 'Receipt'}
                                    className={'mr-2 mb-2 text-center'}
                                />
                            </Col>
                            {!this.props.recurring ? (
                                <Col xs={6}>
                                    <LinkButton
                                        color="success"
                                        altColor="light"
                                        to={`/${
                                            window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'
                                        }/myreceipts/new/mileage`}
                                        materialIcon="add"
                                        text="Mileage"
                                        className={'mr-2 mb-2 text-center'}
                                    />
                                </Col>
                            ) : null}
                        </Row>
                    </Col>
                    <Col xs={12} md={8}>
                        <Row>
                            {!this.props.recurring ? (
                                <Col xs={12} md={3}>
                                    <Input
                                        className={isMobile ? 'w-100p mr-2' : 'w-100 mr-2'}
                                        onChange={this.getReceipts}
                                        type="select"
                                        id="status"
                                        defaultValue={status}
                                    >
                                        <option>Attention</option>
                                        <option>AutoReceipt</option>
                                        <option>Ready</option>
                                        <option>Submitted</option>
                                        <option>Approved</option>
                                        <option>Returned</option>
                                        <option value="PendingPayment">Pending Payment</option>
                                        <option>Paid</option>
                                    </Input>
                                </Col>
                            ) : null}
                            <Col xs={12} md={9}>
                                <Input
                                    className={isMobile ? 'w-100p ' : 'w-300 '}
                                    placeholder="Search"
                                    onChange={this.filterAllFunc}
                                    value={this.state.filterAll}
                                />
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <CurrentExpenses
                            getRecurringOrReceiptUrl={this.getRecurringOrReceiptUrl}
                            recurring={this.props.recurring}
                            filtered={this.state.filtered}
                            onFilteredChange={this.onFilteredChange}
                            key={1}
                            getReceipts={this.getReceipts}
                            modalAction={this.props.modalAction}
                            modalOpen={this.props.modalOpen}
                            modalHeader={this.props.modalHeader}
                            modalMessage={this.props.modalMessage}
                            saved={this.props.saved}
                            saving={this.props.saving}
                            toggleModal={this.props.toggleModal}
                            handleDelete={this.handleDelete}
                            selectedItemsCount={this.props.selectedItemsCount}
                            loading={this.props.loading}
                            receipts={this.props.receipts}
                            selectedItems={this.props.selectedItems}
                            handleCheckbox={this.props.handleCheckbox}
                            handleCheckboxAll={this.props.handleCheckboxAll}
                            expenseCategoriesMap={this.props.expenseCategoriesMap}
                            mileageCategoriesMap={this.props.mileageCategoriesMap}
                            expenseSubmitableStatuses={this.props.expenseSubmitableStatuses}
                        />
                    </Col>
                </Row>
            </Container>
        );
    }
}

class CurrentExpenses extends Component {
    toggleAction = () => {
        const { modalAction, getReceipts } = this.props;
        this.handleToggleModal();
        if (modalAction === 'refresh') getReceipts();
        if (modalAction === 'redirect')
            history.push(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts`);
    };

    calcCurrentPositiveButtonText = () => {
        const { modalAction } = this.props;
        switch (modalAction) {
            case 'save':
                return 'Save';
            case 'delete':
                return 'Delete';
            default:
                return 'Ok';
        }
    };

    calcCurrentPositiveButtonAction = () => {
        const { modalAction } = this.props;
        switch (modalAction) {
            case 'delete':
                return this.props.handleDelete;
            case 'close':
                return this.handleToggleModal;
            case 'refresh':
                return () => {
                    this.props.getReceipts();
                    this.handleToggleModal();
                };
            case 'redirect':
                return () => {
                    history.push(`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreceipts`);
                    this.handleToggleModal();
                };
            default:
                return this.handleToggleModal;
        }
    };

    handleToggleModal = (modalAction, data) => {
        this.props.toggleModal(modalAction, data, this.props.recurring);
    };

    getTdProps = (state, rowInfo, column, instance) => {
        return {
            onClick: (e, handleOriginal) => {
                if (!rowInfo) return;
                const selectable =
                    this.props.expenseSubmitableStatuses[rowInfo.original.status.toLowerCase()] && rowInfo.original.rejected !== 'Y';
                const url = `${this.props.getRecurringOrReceiptUrl()}/${selectable ? 'edit' : 'view'}${
                    this.props.recurring ? '' : `/${rowInfo.original.type.toLowerCase()}`
                }`;
                let editUrl = withQuery(url, {
                    receiptid: rowInfo.original.id,
                });
                history.push(editUrl);
                if (handleOriginal) handleOriginal();
            },
        };
    };

    render() {
        const { receipts, selectedItems, loading, recurring } = this.props;
        const selected = Object.keys(selectedItems || {}).filter(key => selectedItems[key]);
        const selectedItemsCount = selected.length;
        const status = getStatus(recurring);
        const allowdToSubmitExpense = !this.props.recurring && ['Attention', 'Ready', 'AutoReceipt'].includes(status);
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        if (loading) return <LoadingSpinner center />;
        return (
            <div key="rt-table">
                <ReactTable
                    filtered={this.props.filtered}
                    onFilteredChange={this.onFilteredChange}
                    filterable={false}
                    defaultFilterMethod={reactTableFilter.defaultFilterMethod}
                    className="-highlight mb-2"
                    data={receipts}
                    minRows={1}
                    showPagination={!isMobile}
                    getTdProps={this.getTdProps}
                    PaginationComponent={TablePagination}
                    columns={getColumns(this.props, this.handleToggleModal)}
                    NoDataComponent={() => <div className="text-align-center">No record found</div>}
                />
                {allowdToSubmitExpense ? (
                    <Link to={`/${window.location.pathname.indexOf('mycorp') >= 0 ? 'mycorp' : 'expense'}/myreports/create`}>
                        <Button className={isMobile ? 'w-100p' : null} disabled={selectedItemsCount === 0}>
                            Submit Expense Report
                        </Button>
                    </Link>
                ) : null}
                <DialogModal
                    key="modal"
                    modal={this.props.modalOpen}
                    toggle={this.toggleAction}
                    customHeader={this.props.modalHeader || 'Confirmation'}
                    customModalBody={this.props.saving ? <LoadingSpinner /> : this.props.modalMessage}
                    positiveButtonAction={this.calcCurrentPositiveButtonAction()}
                    positiveButtonText={this.calcCurrentPositiveButtonText()}
                    onlyOneButton={this.props.saved}
                    buttonDisabler={this.props.saving}
                />
            </div>
        );
    }
}

const getStatus = recurring => {
    const status = recurring ? 'recurring' : document.getElementById('status') ? document.getElementById('status').value : 'Attention';
    return status;
};
const getColumns = (props, handleToggleModal) => {
    const {
        selectedAll,
        handleCheckboxAll,
        selectedItems,
        handleCheckbox,
        expenseSubmitableStatuses,
        mileageCategoriesMap,
        expenseCategoriesMap,
        getRecurringOrReceiptUrl,
        filtered,
        recurring,
    } = props;

    const status = getStatus(recurring);
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return [
        {
            accessor: 'receiptId',
            headerClassName: 'rt-no-sort-ind',
            width: 30,
            maxWidth: 30,
            show: ['Attention', 'Ready', 'AutoReceipt'].includes(status) && !isMobile,
            sortable: false,
            filterable: false,
            Header: () => {
                return (
                    <FormGroup check>
                        <Label>
                            <Input checked={selectedAll} type="checkbox" onChange={handleCheckboxAll} />
                            <div className="invisible"> Select All</div>
                        </Label>
                    </FormGroup>
                );
            },
            Cell: props => {
                const { status, rejected } = props.original;
                return expenseSubmitableStatuses[status.toLowerCase()] && rejected !== 'Y' ? (
                    <FormGroup check>
                        <Label>
                            <Input
                                type="checkbox"
                                checked={selectedItems[props.value] || false}
                                onClick={e => e.stopPropagation()}
                                onChange={() => handleCheckbox(props.value)}
                            />
                            <div className="invisible"> Selector</div>
                        </Label>
                    </FormGroup>
                ) : null;
            },
        },
        {
            Header: '',
            width: 50,
            filterable: false,
            show: false,
            sortable: true,
            accessor: 'type',
            Cell: props => <ExpenseTypeBadge type={props.value} />,
        },
        {
            Header: 'Date',
            accessor: 'expensedate',
            show: !isMobile,
            width: 100,
            filterable: false,
            sortMethod: (a, b, desc) => cyderlib.tableSortDate(a, b, desc, 'DD-MMM-YYYY'),
            Cell: props => moment(props.value).format('DD-MMM-YYYY'),
        },
        {
            Header: 'Expense ID',
            accessor: 'receiptId',
            show: !isMobile,
        },
        {
            show: !isMobile,
            Header: 'Particulars',
            Cell: props => {
                const { type, category, remarks } = props.original;
                return (
                    <div>
                        <div>
                            <strong>{type === 'Mileage' ? mileageCategoriesMap[category] : expenseCategoriesMap[category]}</strong>
                        </div>
                        {remarks}
                    </div>
                );
            },
        },
        {
            Header: 'Amount',
            show: !isMobile,
            accessor: 'amount',
            width: 100,
            className: 'text-right',
            headerClassName: 'text-right',
            filterMethod: cyderlib.filterCurrencyCent,
            Cell: props => 'S$' + (props.value / 100).toFixed(2),
        },
        {
            Header: 'Status',
            show: !isMobile,
            accessor: 'status',
            width: 100,
            filterable: false,
            filterMethod: cyderlib.filterIgnoreCase,
            Cell: props => <StatusBadge status={props.value} rejected={props.original.rejected} />,
        },
        {
            Header: '',
            show: !isMobile,
            maxWidth: 30,
            headerClassName: 'rt-no-sort-ind',
            accessor: 'attachmentId',
            sortable: false,
            filterable: false,
            Cell: props =>
                props.row._original.type === 'Receipt' && props.value ? (
                    <Button style={{ pointerEvents: 'none' }} size="sm" color="link">
                        <i className="align-middle material-icons text-muted">attach_file</i>
                    </Button>
                ) : null,
        },
        {
            Header: '',
            headerClassName: 'rt-no-sort-ind',
            accessor: 'id',
            sortable: false,
            show: isMobile,
            filterable: false,
            width: isMobile ? '100%' : 160,
            Cell: props => {
                const { status, rejected, type } = props.original;
                const selectable = expenseSubmitableStatuses[status.toLowerCase()] && rejected !== 'Y';
                const claimType = type.toLowerCase();

                // LINK
                const editUrl = withQuery(
                    `${getRecurringOrReceiptUrl()}/${selectable ? 'edit' : 'view'}${recurring ? '' : `/${claimType}`}`,
                    {
                        receiptid: props.value,
                    },
                );
                const copyUrl = withQuery(`${getRecurringOrReceiptUrl()}/new${recurring ? '' : `/${claimType}`}`, {
                    receiptid: props.value,
                    copy: true,
                });

                return (
                    <div className="boxShadow">
                        {['Attention', 'Ready', 'AutoReceipt'].includes(status) &&
                        expenseSubmitableStatuses[status.toLowerCase()] &&
                        rejected !== 'Y' ? (
                            <FormGroup check>
                                <Label>
                                    <Input
                                        type="checkbox"
                                        checked={selectedItems[props.original.receiptId] || false}
                                        onClick={e => e.stopPropagation()}
                                        onChange={() => handleCheckbox(props.original.receiptId)}
                                    />
                                    <div className="font14">
                                        <i>select to submit</i>
                                    </div>
                                </Label>
                            </FormGroup>
                        ) : null}
                        <div className="d-flex justify-content-between">
                            <strong>{moment(props.original.expensedate).format('DD MMM YYYY')}</strong>
                            <span>{cyderlib.formatCurrencyCent(props.original.amount)}</span>
                        </div>
                        <span>
                            {props.original.type === 'Mileage'
                                ? mileageCategoriesMap[props.original.category]
                                : expenseCategoriesMap[props.original.category]}
                        </span>
                        <br />
                        {props.original.remarks}
                        <br />
                        <div className="d-flex">
                            <StatusBadge status={props.original.status} />
                        </div>
                    </div>
                );
            },
        },
        reactTableFilter.generateAllColumnFilter(['remarks', 'category', 'amount', 'status', 'receiptId', 'expensedate'], {
            Mileage: mileageCategoriesMap,
            Receipt: expenseCategoriesMap,
        }),
    ];
};

const mapStateToProps = state => {
    return {
        ...state.cyderExpenseMyReceiptsReducer,
        ...state.cyderExpenseSharedReducer,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        toggleModal: (modalAction, data, recurring) => {
            dispatch(expenseMyReceiptsToggleModal(modalAction ? modalAction : null, data, recurring));
        },
        handleDelete: recurring => {
            dispatch(myReceiptsDeleteReceipt(recurring));
        },
        handleCheckbox: id => {
            dispatch(toggleCheckboxes(id));
        },
        handleCheckboxAll: () => {
            dispatch(toggleCheckboxesAll());
        },
        setJumbotronTitle: title => dispatch(setJumbotronTitle(title)),
        getReceipts: recurring => {
            const status = getStatus(recurring);
            const items = [
                dispatch(expenseSharedGetExpenseCategories()),
                dispatch(expenseSharedGetMileageCategories()),
                dispatch(myReceiptsGetReceipts(status)),
            ];
            Promise.all(items).then(res => {
                dispatch(myReceiptsRenderReceipts(res[2]));
            });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(MyReceiptsPage));
