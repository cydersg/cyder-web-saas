import React from 'react';
import Select from 'react-select';
import classnames from 'classnames';
import { Row, Col, Label, FormGroup } from 'reactstrap';

export const selectStyle = {
    control: styles => ({
        ...styles,
        borderRadius: '0px',
        background: 'white',
    }),
    indicatorSeparator: styles => ({
        ...styles,
        display: 'none',
    }),
};

const CyderSelect = ({
    label,
    className,
    colClassName,
    mandatory,
    readOnly,
    colstyle,
    labelClassName,
    customLabelComponent,
    formGroupClassName,
    error,
    inputProps,
}) => {
    const { value, id, disabled } = inputProps;

    // COMPONENT
    const PreviewComponent = <div>{value && value.label ? value.label : ''}</div>;
    const LabelComponent = customLabelComponent || (
        <Label
            for={id}
            className={classnames('text-bold', labelClassName, {
                mandatory: mandatory ? true : false,
            })}
        >
            {label} {mandatory && <span>*</span>}
        </Label>
    );

    return (
        <Row className={className}>
            <Col className={colClassName || 'col-12'} style={colstyle}>
                <FormGroup className={formGroupClassName}>
                    {label && LabelComponent}
                    {!readOnly && <Select {...inputProps} isDisabled={disabled} styles={selectStyle} />}
                    {readOnly && PreviewComponent}
                    {error && <span className="help-text">{error}</span>}
                </FormGroup>
            </Col>
        </Row>
    );
};

export default CyderSelect;
