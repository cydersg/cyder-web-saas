/**
 * Select Expense Category Form
 * 
 * Author: Fernando
 */

import React from 'react'
import {connect} from 'react-redux'
import '../../css/forms/react-select/default.css'
import ExpenseTypeSelect from '../../forms/expenses/ExpensesTypeSelect'
import {startExpense} from '../../actions/expenses/expenseCommonAction'

const SelectExpenseCategoryForm = ({start, expense}) => (
  <div>
    {!expense.inprogress && 
    <form onSubmit={e => e.preventDefault()} >
      <div>
        <div className="row">
          <div className="col-12 col-lg-6"> 
              <ExpenseTypeSelect />  
          </div>
        </div>
      </div>
      <div className="row">
          <div className="col-12 col-lg-6"> 
            <button
              className="btn btn-primary btn-rounded btn-outline"
              type="submit"
              onClick={() => start()}>
              Start
            </button>
          </div>
        </div>
    </form>
    }
  </div>
)

const mapStateToProps = (state, ownProps) => {
  return {
    expense: state.cyderExpenseReducer
  }
}
const mapDispatchToProps = dispatch => {
  return {  
    start: () => {
      dispatch(startExpense());
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(SelectExpenseCategoryForm)
