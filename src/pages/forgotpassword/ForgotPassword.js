import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import history from 'history.js';

import { Alert } from 'reactstrap';
import CyderForm from 'pages/CyderForm';
import EmptyView2 from 'layouts/EmptyView2';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import LinkButton from 'cyderComponents/buttons/LinkButton';
import MaterialDesignInput from 'cyderComponents/forms/MaterialDesignInput';

import { cyderForgotPasswordPageSetMessageAction, cyderForgotPasswordAction } from 'actions/security/cyderSecurityAction';

class ForgotPassword extends Component {
    validateThenSubmit = () => {
        this.props.clearErrorMessage();
        const email = document.getElementById(this.props.fields[0].id).value;
        if (!email) {
            this.props.setMessage('Please enter your email address');
            return false;
        }
        this.props.changePasswordSubmit(email);
    };

    render() {
        const { message, success, fields, requesting } = this.props;
        const style = message ? null : { display: 'none' };

        return (
            <div className="d-flex row justify-content-between" style={{ width: '100%' }}>
                <CyderForm
                    style={{ width: '100%' }}
                    title="Forgot Password"
                    className="align-self-center semi-transparent"
                    description={!success ? '' : null}
                >
                    <div className="form-group">
                        {requesting ? (
                            <LoadingSpinner />
                        ) : (
                            <div>
                                <Alert style={style} color={success ? 'success' : 'danger'} className="mb-5 text-bold">
                                    {message || null}
                                </Alert>
                                {!success ? <MaterialDesignInput field={fields[0]} /> : null}
                            </div>
                        )}
                    </div>
                    <div className="d-flex">
                        {!success && (
                            <LinkButton
                                notLink
                                text="Submit"
                                color="info"
                                altColor="light"
                                onClick={this.validateThenSubmit}
                                disabled={requesting}
                            />
                        )}
                        <Link className="align-self-center ml-auto text-muted text-bold" to={requesting ? '#' : '/'}>
                            Back
                        </Link>
                    </div>
                </CyderForm>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { ...state.cyderForgotPasswordReducer };
};

const mapDispatchToProps = dispatch => {
    return {
        clearErrorMessage: () => {
            dispatch(cyderForgotPasswordPageSetMessageAction(''));
        },
        setMessage: message => {
            dispatch(cyderForgotPasswordPageSetMessageAction(message));
        },
        changePasswordSubmit: email => {
            var navigateHome = () => {
                var navigate = '/home';
                history.replace(navigate);
            };
            var navigateLogin = () => {
                var navigate = '/';
                history.replace(navigate);
            };
            dispatch(cyderForgotPasswordAction(email, navigateHome, navigateLogin));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmptyView2(ForgotPassword));
