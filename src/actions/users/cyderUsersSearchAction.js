import {
    CYDER_USERS_SEARCH_SUCCESS,
    CYDER_USERS_SEARCH_REQUEST,
    CYDER_USERS_SEARCH_FAILURE,
    CYDER_USERS_SEARCH_SET_FORCE_REFRESH_FLAG,
} from '../constants/actionTypes';
import withQuery from 'with-query';

import { CYDER_COMMON_API_ROOT } from '../../config';

export function cyderUsersSearchAction(body) {
    return (dispatch, getState) => {
        dispatch({ type: CYDER_USERS_SEARCH_REQUEST });
        dispatch({ type: CYDER_USERS_SEARCH_SET_FORCE_REFRESH_FLAG, val: false });
        const url = withQuery(CYDER_COMMON_API_ROOT + '/users', {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'partialsearch',
        });
        const options = {
            body: JSON.stringify(body),
            method: 'POST',
            headers: {
                'x-api-key': getState().config.apiKeyCyder,
                'Content-Type': 'application/json',
                Authorization: getState().cyderProfileReducer.profile.authorizationToken,
            },
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        throw error;
                    });
                } else {
                    return response.json().then(response => {
                        const id = 1;
                        dispatch({ type: CYDER_USERS_SEARCH_SUCCESS, id, response });
                        return response;
                    });
                }
            })
            .catch(error => {
                dispatch({ type: CYDER_USERS_SEARCH_FAILURE, error });
                throw error;
            });
    };
}
