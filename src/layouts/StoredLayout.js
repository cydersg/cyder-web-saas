import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

const StoredLayout = compose(
    connect(state => {
        return {
            config: state.config,
        };
    }),
    lifecycle({
        componentDidMount() {
            let layout = this.props.config.layout;
            switch (layout) {
                case 'home':
                case 'empty-view-1':
                case 'empty-view-2':
                    layout = 'default-sidebar-1';
                    break;
                default:
                    break;
            }
            this.props.dispatch({
                type: 'SET_CONFIG',
                config: { layout },
            });
        },
    }),
);

export default StoredLayout;
