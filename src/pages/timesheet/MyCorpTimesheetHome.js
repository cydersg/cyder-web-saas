import React, { Fragment, Component, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import classnames from 'classnames';

import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import StoredLayout from 'layouts/StoredLayout';
import { Row, Col, Container, Button, FormGroup, Label, Input, Card, CardBody, Badge } from 'reactstrap';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import ButtonPrimary from 'cyderComponents/buttons/ButtonPrimary';
import ButtonSecondary from 'cyderComponents/buttons/ButtonSecondary';
import ButtonPrimaryRound from 'cyderComponents/buttons/ButtonPrimaryRound';
import ButtonPrimaryRoundImage from 'cyderComponents/buttons/ButtonPrimaryRoundImage';
import ButtonSecondaryRound from 'cyderComponents/buttons/ButtonSecondaryRound';
import {
    getData,
    clockIn,
    getLatestActivity,
    clockOut,
    setLoading,
    findLocation,
    addLocationToFavourite,
    removeLocationFromFavourite,
    getAllFavourites,
} from 'actions/timesheet/clockAction';
import { expenseSharedGetProjects } from 'actions/expenses/cyderExpenseClaimSharedAction';
import { setJumbotronTitle } from 'actions/pagedata';
import withQuery from 'with-query';
import GPlace from './GPlace';

import { CyderInput, CyderSelect } from 'cyderComponents/input';

// import QrReader from 'react-qr-reader';
import QrReader from 'cyderComponents/qrReader';
import { layoutLib } from 'js/constlib';

import MyCorpTimesheetBottomNav from './MyCorpTimesheetBottomNav';

class MyCorpTimesheetHome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            gpsMessage: 'Getting current position ...',
            actualLocation: {
                lat: null,
                lng: null,
                address: null,
            },
            chosenLocation: {
                lat: null,
                lng: null,
                address: null,
                name: null,
            },
            add: true,
            checkedin: false,
            myFavourites: [],
            showLatestCheckin: true,
        };
    }

    showLatestCheckin = show => {
        let newState = Object.assign({}, this.state);
        newState.showLatestCheckin = show;
        this.setState(newState);
    };

    addNew = () => {
        let newState = Object.assign({}, this.state);
        newState.add = true;
        newState.checkedin = false;
        this.setState(newState);
    };

    finishAdd = () => {
        this.componentDidMount();
    };

    viewMore = () => {
        let newState = Object.assign({}, this.state);
        newState.add = false;
        newState.checkedin = false;
        this.setState(newState);
    };

    addFavourite = async location => {
        await this.props.addFavourite(location);
        this.props.getData(this.props.profile.username);
    };

    removeFavourite = async id => {
        await this.props.removeFavourite(id);
        this.props.getAllFavourites(this.props.profile.username);

        // this.props.getData(this.props.profile.username);
    };

    confirmAdd = () => {
        let newState = Object.assign({}, this.state);
        newState.add = false;
        newState.checkedin = true;
        this.setState(newState);
    };

    setChosenLocation = position => {
        if (!position) {
            return;
        }
        let newState = Object.assign({}, this.state);
        newState.chosenLocation.lat = position.lat;
        newState.chosenLocation.lng = position.lng;
        newState.chosenLocation.address = position.address;
        newState.chosenLocation.name = position.name;
        this.setState(newState);
    };

    componentDidMount() {
        this.props.setJumbotronTitle('Check In/Out');
        this.props.getData(this.props.profile.username);

        const myThis = this;
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                let newState = Object.assign({}, myThis.state);
                newState.add = true;
                newState.checkedin = false;
                newState.actualLocation.lat = position.coords.latitude;
                newState.actualLocation.lng = position.coords.longitude;
                newState.gpsMessage = '';
                myThis.setState(newState);
            });
        } else {
            let newState = Object.assign({}, myThis.state);
            newState.add = true;
            newState.checkedin = false;
            newState.actualLocation.lat = null;
            newState.actualLocation.lng = null;
            newState.gpsMessage = 'GPS is not available';
            myThis.setState(newState);
        }
    }

    onClockIn = async () => {
        const project = document.getElementById('project').value;
        const remarks = document.getElementById('remarks').value;

        let myThis = this;

        if (this.state.chosenLocation == null || this.state.chosenLocation.lat == null) {
            if (!window.confirm('Location is unavailable, proceed?')) {
                return;
            }
        }
        const locationToSave = this.state.chosenLocation.lat ? this.state.chosenLocation : this.state.actualLocation;
        this.props
            .clockIn(
                project,
                remarks,
                locationToSave.lat,
                locationToSave.lng,
                this.state.actualLocation.lat,
                this.state.actualLocation.lng,
                locationToSave.name,
                locationToSave.address,
            )
            .then(e => {
                setTimeout(e => {
                    myThis.finishAdd();
                }, 0);
            });
    };

    render() {
        const { clockOut, projectsMap, activity, forceLoading, projects } = this.props;
        const usedProject = activity.map(x => x.project);
        const filteredProjects = projects.filter(x => !usedProject.includes(x.code));

        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        if (forceLoading) return <LoadingSpinner center />;

        return (
            <Fragment>
                <Row>
                    <Col>
                        <Row className="justify-content-center">
                            <Col
                                className="ph0"
                                style={{
                                    textAlign: 'center',
                                }}
                            >
                                {/* <b>MyCorp Check In</b> */}
                                <hr
                                    className="mycorp-hr-small"
                                    style={{
                                        marginBottom: 15,
                                    }}
                                />
                            </Col>
                        </Row>
                        {this.state.checkedin && (
                            <Row className="justify-content-center">
                                <Col className="mb-1 pb-1 ph0">You have successfully clocked-in to {this.state.project}</Col>
                            </Row>
                        )}
                        {this.state.add && (
                            <>
                                <Row className="justify-content-center mycorp-list">
                                    <Col className="mb-1 pb-1 ph0">
                                        <InputPanel
                                            {...this.state}
                                            actualLocation={this.state.actualLocation}
                                            onClockIn={this.onClockIn}
                                            finishAdd={this.finishAdd}
                                            filteredProjects={filteredProjects}
                                            setChosenLocation={this.setChosenLocation}
                                            findLocation={this.props.findLocation}
                                            myFavourites={this.props.allFavourites}
                                            history={this.props.history}
                                            removeFavourite={this.removeFavourite}
                                            profileName={this.props.profile.username}
                                            showLatestCheckin={this.showLatestCheckin}
                                        />
                                        {this.state.showLatestCheckin && (
                                            <Row>
                                                {activity.map((data, i) => {
                                                    const { project } = data;
                                                    data.project = projectsMap[project] || project;
                                                    if (i > 0) return null;
                                                    return (
                                                        <Col key={i} xs="12" className="ph0">
                                                            <ClockCard
                                                                data={data}
                                                                onClockOut={clockOut}
                                                                latest={i === 0}
                                                                addFavourite={this.addFavourite}
                                                                removeFavourite={this.removeFavourite}
                                                                myFavourites={this.props.allFavourites}
                                                            />
                                                        </Col>
                                                    );
                                                })}
                                            </Row>
                                        )}
                                    </Col>
                                </Row>
                            </>
                        )}
                        {!this.state.add && (
                            <div>
                                <div className="mycorp-add-bottom">
                                    <Button
                                        className="mycorp-add-button-circle"
                                        onClick={e => {
                                            this.addNew();
                                        }}
                                        color="success"
                                    >
                                        +
                                    </Button>
                                </div>

                                <Row className={classnames('clock-indicator-wrapper mycorp-list')}>
                                    {activity.map((data, i) => {
                                        const { project } = data;
                                        data.project = projectsMap[project] || project;
                                        return (
                                            <Col key={i} xs="12" className="ph0">
                                                <ClockCard
                                                    data={data}
                                                    onClockOut={clockOut}
                                                    latest={i === 0}
                                                    addFavourite={this.addFavourite}
                                                    removeFavourite={this.removeFavourite}
                                                    myFavourites={this.props.allFavourites}
                                                />
                                            </Col>
                                        );
                                    })}
                                    {activity.length === 0 ? <Col className="text-center text-muted">No activities found</Col> : null}
                                </Row>
                            </div>
                        )}
                    </Col>
                </Row>
                <MyCorpTimesheetBottomNav page="home" />
            </Fragment>
        );
    }
}

const InputPanel = props => {
    const {
        onClockIn,
        filteredProjects,
        actualLocation,
        chosenLocation,
        finishAdd,
        setChosenLocation,
        findLocation,
        myFavourites,
        profileName,
        history,
        showLatestCheckin,
        removeFavourite,
    } = props;
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    const [showQr, setShowQr] = useState(false);
    const [showFavourite, setShowFavourite] = useState(false);

    const [selectedFav, setSelectedFav] = useState(null);

    const [loadMap, setLoadMap] = useState(false);
    const [currentPlace, setCurrentPlace] = useState(null);

    const [favInput, setFavInput] = useState('');

    // API key of the google map
    const GOOGLE_MAP_API_KEY = 'AIzaSyBr4vviN07QGsh6OtH0mvRd9RRWL9YtSWo';

    const pickFavourites = () => {
        setShowFavourite(true);
        showLatestCheckin(false);
    };

    const scanQr = () => {
        setShowQr(true);
        showLatestCheckin(false);
    };

    const handleScan = async data => {
        if (data) {
            const location = await findLocation(data);
            if (location && location.ok && location.data && location.data.length > 0) {
                setCurrentPlace({
                    lat: location.data[0].lat,
                    lng: location.data[0].lng,
                    address: location.data[0].address && location.data[0].address !== 'null' ? location.data[0].address : '',
                    name: location.data[0].name && location.data[0].name !== 'null' ? location.data[0].name : '',
                });
                setShowQr(false);
            }
        }
    };

    const handleError = err => {
        setShowQr(false);
        alert(err);
    };

    const reverseGeoCode = () => {
        const { lat, lng } = actualLocation;
        if (!lat) {
            return Promise.resolve();
        }
        const url = withQuery(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${GOOGLE_MAP_API_KEY}`);
        const options = {
            method: 'GET',
        };
        return fetch(url, options)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(error => {
                        return error;
                    });
                } else {
                    return response.json().then(response => {
                        return response;
                    });
                }
            })
            .catch(error => {
                return error;
            });
    };

    useEffect(() => {
        loadGoogleMapScript(() => {
            setLoadMap(true);
        });
    }, []);

    useEffect(() => {
        setChosenLocation(currentPlace);
    }, [currentPlace]);

    useEffect(() => {
        reverseGeoCode().then(places => {
            if (places && places.status === 'OK') {
                const results = places.results;
                if (results && results.length > 0) {
                    setCurrentPlace({
                        lat: results[0].geometry.location.lat,
                        lng: results[0].geometry.location.lng,
                        address: results[0].formatted_address,
                        name: results[0].name,
                    });
                }
            }
        });
    }, [actualLocation.lat, actualLocation.lng]);

    // load google map script
    const loadGoogleMapScript = callback => {
        if (typeof window.google === 'object' && typeof window.google.maps === 'object') {
            callback();
        } else {
            const googleMapScript = document.createElement('script');
            googleMapScript.src = `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_MAP_API_KEY}&libraries=places`;
            window.document.body.appendChild(googleMapScript);
            googleMapScript.addEventListener('load', callback);
        }
    };

    const filteredFav = myFavourites ? myFavourites.filter(e => e.name.toLowerCase().indexOf(favInput.toLowerCase()) >= 0) : [];
    const sortedFav = filteredFav
        ? filteredFav.sort((a, b) => {
              if (a.name.toLowerCase() < b.name.toLowerCase()) {
                  return -1;
              } else if (a.name.toLowerCase() > b.name.toLowerCase()) {
                  return 1;
              } else {
                  return 0;
              }
          })
        : [];

    return (
        filteredProjects &&
        filteredProjects.length > 0 && (
            <Fragment>
                {showQr ? (
                    <>
                        <QrReader delay={300} onError={handleError} facingMode="user" onScan={handleScan} style={{ width: '100%' }} />
                        <br />

                        <ButtonPrimary
                            onClick={e => {
                                setShowQr(false);
                                showLatestCheckin(true);
                            }}
                            faIcon="times-circle"
                            label="Cancel"
                        />
                    </>
                ) : showFavourite ? (
                    <>
                        <i
                            onClick={e => {
                                setShowFavourite(false);
                                setSelectedFav(null);
                                showLatestCheckin(true);
                            }}
                            className="align-middle material-icons"
                            style={{
                                fontSize: 20,
                                marginBottom: 10,
                                marginTop: -10,
                            }}
                        >
                            arrow_back
                        </i>

                        <Input
                            id="favInput"
                            placeholder="Search from the list below"
                            value={favInput}
                            onChange={e => {
                                setFavInput(e.target.value);
                            }}
                        />
                        <div
                            style={{
                                background: '#f0f0f0',
                                padding: 5,
                                marginBottom: 5,
                                marginTop: 10,
                                fontSize: 14,
                            }}
                        >
                            <b>YOUR FAVOURITE PLACES</b>
                        </div>
                        {sortedFav &&
                            sortedFav.map((e, i) => {
                                return (
                                    <div
                                        style={{
                                            padding: 5,
                                            display: 'flex',
                                            width: '100%',
                                            borderBottom: '1px solid #f0f0f0',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <p
                                            className=" text-muted mycorp-p-slim "
                                            onClick={a => {
                                                handleScan(e.id);
                                                setShowFavourite(false);
                                                showLatestCheckin(true);
                                            }}
                                            style={{
                                                flex: 1,
                                            }}
                                        >
                                            <b>{e.name}</b>
                                            {e.address && e.address != 'null' && (
                                                <p
                                                    style={{
                                                        fontSize: 12,
                                                    }}
                                                    className=" mycorp-p-slim "
                                                >
                                                    {e.address}
                                                </p>
                                            )}
                                        </p>
                                        <i
                                            onClick={a => {
                                                removeFavourite(e.id);
                                            }}
                                            className="align-middle material-icons"
                                            style={{
                                                fontSize: 20,
                                                color: 'orange',
                                            }}
                                        >
                                            star
                                        </i>
                                    </div>
                                );
                            })}
                    </>
                ) : (
                    <>
                        <FormGroup>
                            <Label className="text-bold mandatory mycorp-form-group-label">
                                Project
                                <span> *</span>
                            </Label>
                            <div
                                style={{
                                    display: 'flex',
                                    width: '100%',
                                }}
                            >
                                <Input type="select" id="project">
                                    <option value={'ProjectX'}>ProjectX</option>
                                    {filteredProjects.map((item, i) => (
                                        <option key={i} value={item.code}>
                                            {item.name}
                                        </option>
                                    ))}
                                </Input>
                                <i
                                    class={`fa fa-cog fa-lg`}
                                    style={{
                                        fontSize: 30,
                                        marginTop: 5,
                                        marginLeft: 5,
                                    }}
                                    onClick={() => {
                                        history.push('/mycorp/timesheet/projects');
                                    }}
                                ></i>
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <Label className="text-bold mycorp-form-group-label">Remarks</Label>
                            <Input type="textarea" id="remarks" />
                        </FormGroup>
                        <FormGroup>
                            <Label className="text-bold mycorp-form-group-label">Location</Label>
                            {!loadMap ? (
                                <div>Loading...</div>
                            ) : (
                                <GPlace currentPlace={currentPlace} setChosenLocation={setChosenLocation} />
                            )}
                        </FormGroup>
                        <Row className="mycorp-big-icon-panel">
                            <ButtonPrimaryRoundImage onClick={onClockIn} faIcon="sign-in" label="Check in" />
                            <ButtonPrimaryRound disabled onClick={scanQr} faIcon="qrcode" label="Scan" />
                            <ButtonPrimaryRound onClick={pickFavourites} faIcon="star" label="Fav" />
                        </Row>
                    </>
                )}
            </Fragment>
        )
    );
};

const ClockCard = props => {
    const { onClockOut, latest, addFavourite, removeFavourite, myFavourites } = props;
    const { starttime, endtime, location_address, location_name, project, purpose, id, lat, lng } = props.data;

    const momentStartTime = moment(starttime, 'YYYY-MM-DD hh:mm:ss');
    const startTime = momentStartTime.format('h:mm a');

    // Auto clock out after 1 day
    // const diff = momentStartTime.diff(moment(), 'days');
    // if (diff <= -1) onClockOut(id);

    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    const favLocation =
        location_name &&
        myFavourites &&
        myFavourites.length > 0 &&
        myFavourites.filter(e => e.name.toLowerCase() === location_name.toLowerCase());
    const isFavourite = favLocation && favLocation.length > 0 ? true : false;

    return (
        <Card className="clock-indicator">
            <CardBody
                style={{
                    paddingTop: 10,
                    paddingBottom: 0,
                    paddingLeft: 0,
                    paddingRight: 0,
                }}
            >
                <Row>
                    <Col
                        className="d-flex align-items-center"
                        style={
                            isMobile
                                ? {
                                      justifyContent: 'space-between',
                                  }
                                : {}
                        }
                    >
                        <span
                            className="font-weight-bold"
                            style={{
                                fontSize: 16,
                            }}
                        >
                            {project}
                        </span>
                        {latest && (
                            <Badge
                                style={{
                                    marginRight: 7,
                                    color: '#ff9800',
                                    height: '100%',
                                }}
                                color="light"
                            >
                                Latest
                            </Badge>
                        )}
                    </Col>
                </Row>

                <Row>
                    <Col
                        className="d-flex align-items-center"
                        style={
                            isMobile
                                ? {
                                      justifyContent: 'space-between',
                                  }
                                : {}
                        }
                    >
                        <div>
                            <span
                                style={{
                                    fontSize: 12,
                                    marginRight: 10,
                                }}
                            >
                                {momentStartTime.format('DD MMM YYYY HH:mm')}
                            </span>

                            {purpose && (
                                <div
                                    style={{
                                        fontSize: 12,
                                    }}
                                >
                                    {purpose}
                                </div>
                            )}
                            {lat && lng && (
                                <div>
                                    <p
                                        className="text-muted"
                                        style={{
                                            fontSize: 14,
                                        }}
                                    >
                                        <b>{location_name || location_address}</b>
                                        {location_name && (
                                            <p
                                                style={{
                                                    fontSize: 12,
                                                }}
                                            >
                                                {location_address}
                                            </p>
                                        )}
                                    </p>
                                </div>
                            )}
                        </div>

                        {!endtime && (
                            <Row
                                style={{
                                    height: '100%',
                                }}
                            >
                                <Col className="mycorp-col-big-icon mr0">
                                    <Button className={' mycorp-add-button-circle'} onClick={() => onClockOut(id)} color="primary">
                                        <i class="fa fa-sign-out fa-lg  mycorp-big-icon"></i>
                                    </Button>
                                </Col>
                            </Row>
                        )}
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
};

const mapStateToProps = state => {
    return {
        ...state.clockReducer,
        profile: state.cyderProfileReducer.profile,
        projects: state.cyderExpenseSharedReducer.projects,
        projectsMap: state.cyderExpenseSharedReducer.projectsMap,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setJumbotronTitle: title => dispatch(setJumbotronTitle(title)),
        getData: owner => {
            const array = [dispatch(getLatestActivity()), dispatch(expenseSharedGetProjects()), dispatch(getAllFavourites(owner))];
            dispatch(setLoading(true)).then(() => Promise.all(array).then(() => dispatch(setLoading(false))));
        },
        findLocation: id => {
            return dispatch(findLocation(id));
        },
        getAllFavourites: owner => {
            return dispatch(getAllFavourites(owner));
        },
        addFavourite: location => {
            return dispatch(addLocationToFavourite(location));
        },
        removeFavourite: id => {
            return dispatch(removeLocationFromFavourite(id));
        },
        clockIn: (project, remarks, lat, lng, actualLat, actualLng, name, address) => {
            return dispatch(setLoading(true)).then(() =>
                dispatch(clockIn(project, remarks, lat, lng, actualLat, actualLng, name, address)).then(() =>
                    dispatch(getLatestActivity()).then(() => dispatch(setLoading(false))),
                ),
            );
        },
        clockOut: id => {
            dispatch(setLoading(true)).then(() =>
                dispatch(clockOut(id)).then(() => dispatch(getLatestActivity()).then(() => dispatch(setLoading(false)))),
            );
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(StoredLayout(MyCorpTimesheetHome));
