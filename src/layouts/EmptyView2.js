import { connect } from 'react-redux'
import { compose, lifecycle } from 'recompose'

const EmptyView2 = compose(
    connect(state => {
        return {
            config: state.config
        }
    }),
    lifecycle({
        componentDidMount() {
            this.props.dispatch({
                type: 'SET_CONFIG',
                config: {
                    layout: 'empty-view-2',
                }
            })
        }
    })
)

export default EmptyView2

