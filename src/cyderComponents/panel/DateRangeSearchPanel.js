import React, { Fragment, useState, useEffect } from 'react';
import { Container, Row, Col, Button, Label, Input } from 'reactstrap';
import { CyderInput } from 'cyderComponents/input/index';
import { getMonthOptions, getYearOptions } from 'js/generalUtils';
import { expenseDeletableStatuses, urlRecurring, urlReceipt, layoutLib } from 'js/constlib';
import CyderDatePicker from 'cyderComponents/forms/CyderDatePicker';
import ButtonPrimary from 'cyderComponents/buttons/ButtonPrimary';

const DateRangeSearchPanel = props => {
    const { onSearch, start, end } = props;
    const [startState, setStartState] = useState(null);
    const [endState, setEndState] = useState(null);

    const onSearchClicked = () => {
        if (!onSearch) return;
        onSearch(startState, endState);
    };

    useEffect(() => {
        setStartState(start);
        setEndState(end);
    }, [start, end]);

    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return (
        <Fragment>
            <div className="w-100p">
                <Row>
                    <Col
                        xs="12"
                        sm="5"
                        style={{
                            paddingLeft: 0,
                            paddingRight: 0,
                        }}
                    >
                        <CyderInput label="From" formGroupClassName="m-0">
                            <CyderDatePicker
                                value={startState}
                                placeholder="DD/MM/YYYY"
                                dateFormat="DD/MM/YYYY"
                                onChange={event => {
                                    setStartState(event.format('DD/MM/YYYY'));
                                }}
                            />
                        </CyderInput>
                    </Col>
                    <Col
                        xs="12"
                        sm="5"
                        style={{
                            paddingLeft: 0,
                            paddingRight: 0,
                        }}
                    >
                        <CyderInput label="Until" formGroupClassName="m-0">
                            <CyderDatePicker
                                value={endState}
                                placeholder="DD/MM/YYYY"
                                dateFormat="DD/MM/YYYY"
                                onChange={event => {
                                    setEndState(event.format('DD/MM/YYYY'));
                                }}
                            />
                        </CyderInput>
                    </Col>
                    <Col xs="12" md="2" className="mt-2 d-flex align-items-end">
                        <ButtonPrimary onClick={onSearchClicked} faIcon="search" label="Search" />
                    </Col>
                </Row>
            </div>
        </Fragment>
    );
};

export default DateRangeSearchPanel;
