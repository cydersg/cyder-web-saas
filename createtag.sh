#!/bin/bash

# check for argument
if [[ "$1" != "" ]]; then
    VERSION="$1"
else
    echo ERROR: argument 1 must be \$VERSION
    exit 1
fi
# check if version is valid semver
REGEX="^(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)(\\-[0-9A-Za-z-]+(\\.[0-9A-Za-z-]+)*)?(\\+[0-9A-Za-z-]+(\\.[0-9A-Za-z-]+)*)?$"
if [[ "$VERSION" =~ $REGEX ]]; then

    PACKAGE_VERSION=$(cat package.json \
        | grep version \
        | head -1 \
        | awk -F: '{ print $2 }' \
        | sed 's/[ ",]//g')

    if [ $PACKAGE_VERSION != $VERSION ]; then
        echo "ERROR: package.json version [$PACKAGE_VERSION] does not match specified version [$VERSION]"
        exit 1
    fi

    # this regex is for semver with no suffixes
    REGEX2="\\[\(0\|[1-9][0-9]*\)\\.\(0\|[1-9][0-9]*\)\\.\(0\|[1-9][0-9]*\)\\]"
    # check if changelog's latest version is specified version
    CLV=$(cat CHANGELOG.md | grep "$REGEX2" -o | sed -n 1p) 
    if [ "$CLV" != "[$VERSION]" ]; then
        echo "ERROR: changelog latest version $CLV does not match specified version [$VERSION]"
        exit 1
    else
            TAGNAME=$VERSION
            echo creating tag: $TAGNAME
            git tag $TAGNAME
            git push origin $TAGNAME
            echo pushed tag: $TAGNAME
    fi
else
    echo ERROR: $VERSION does not follow semver format
    exit 1
fi

