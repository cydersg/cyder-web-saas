import withQuery from 'with-query';
import fetchlib from 'js/fetchlib';
import { DJARVIS_COMMON_API_ROOT } from '../../config';

export function addLeaveType(code, description, requireapproval, amountCarryForward, defaultEntitlement) {
    return (dispatch, getState) => {
        const url = withQuery(`${DJARVIS_COMMON_API_ROOT}/leavetype`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'add',
        });
        const body = { code, description, requireapproval, amountCarryForward, defaultEntitlement };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}

export function updateLeaveType(id, description, requireapproval, amountCarryForward, defaultEntitlement) {
    return (dispatch, getState) => {
        const url = withQuery(`${DJARVIS_COMMON_API_ROOT}/leavetype`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'updatebyid',
        });
        const body = { id, description, requireapproval, amountCarryForward, defaultEntitlement };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}

export function getLeaveType(keyword) {
    return (dispatch, getState) => {
        const url = withQuery(`${DJARVIS_COMMON_API_ROOT}/leavetype`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'partialsearch',
        });
        const body = { keyword };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}

export function getLeaveTypeDetail(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${DJARVIS_COMMON_API_ROOT}/leavetype`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'findbyid',
            id,
        });
        return fetchlib.fetch(dispatch, getState, 'GET', url);
    };
}

export function deleteLeaveType(id) {
    return (dispatch, getState) => {
        const url = withQuery(`${DJARVIS_COMMON_API_ROOT}/leavetype`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'deletebyid',
        });
        const body = { id };
        return fetchlib.fetch(dispatch, getState, 'POST', url, body);
    };
}
