import React from 'react';

import { Badge, Row, Col, Table } from 'reactstrap';
import ReactTable from 'react-table';
import cyderlib from 'js/cyderlib';
import moment from 'moment';
import { expenseDeletableStatuses, urlRecurring, urlReceipt, layoutLib } from 'js/constlib';

const WorkflowHistoryTable = props => {
    const { workflowHistory } = props;

    return (
        <Row>
            <Col xs={12}>
                <h5>
                    <b>Item History</b>
                </h5>
                <ReactTable className="-highlight" data={workflowHistory} columns={getColumns()} minRows={0} showPagination={false} />
            </Col>
        </Row>
    );
};

const getColumns = () => {
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;

    return [
        {
            Header: 'Status',
            accessor: 'status',
            show: !isMobile,
            width: 100,
            Cell: props => {
                return (
                    <Badge color={cyderlib.statusBadgeColor(props.value)} className="mr-2">
                        {props.value}
                    </Badge>
                );
            },
        },
        {
            Header: 'Date',
            width: 100,
            show: !isMobile,
            Cell: props =>
                props.value
                    ? moment(props.value, 'YYYY-MM-DD HH:mm').format('DD/MM/YYYY')
                    : moment(props.original.createddt, 'YYYY-MM-DD HH:mm').format('DD/MM/YYYY'),
            accessor: 'owner_action_date',
        },
        {
            Header: 'Owner',
            show: !isMobile,
            width: 200,
            accessor: 'owner',
        },
        {
            Header: 'Remarks',
            show: !isMobile,
            accessor: 'remarks',
        },
        {
            // Header: 'Status',
            accessor: 'status',
            show: isMobile,
            Cell: props => {
                return (
                    <div className="boxShadow">
                        <span>
                            <strong>{props.original.owner}</strong>
                        </span>
                        <br />
                        <span>
                            {props.original.owner_action_date
                                ? moment(props.original.owner_action_date, 'YYYY-MM-DD HH:mm').format('DD/MM/YYYY')
                                : moment(props.original.createddt, 'YYYY-MM-DD HH:mm').format('DD/MM/YYYY')}
                        </span>
                        <br />
                        <span>{props.original.remarks}</span>
                        <br />
                        <Badge color={cyderlib.statusBadgeColor(props.original.status)} className="mr-2">
                            {props.original.status}
                        </Badge>
                    </div>
                );
            },
        },
    ];
};

export default WorkflowHistoryTable;
