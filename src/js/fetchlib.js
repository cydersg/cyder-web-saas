export default {
    fetch: async (
        dispatch,
        getState,
        method,
        url,
        body,
        actionTypeRequest,
        actionTypeSuccess,
        actionTypeFailure,
        useAPIKeyCyder, // Bool
        options, // Object
        customActionTypeSuccess, // Func
    ) => {
        const { apiKeyCyder, apiKey } = getState().config;
        const { authorizationToken } = getState().cyderProfileReducer.profile;

        // OPTIONS
        const opt = options || {
            method,
            body: method === 'POST' ? JSON.stringify(body || {}) : null,
            headers: {
                Authorization: authorizationToken,
                'x-api-key': useAPIKeyCyder ? apiKeyCyder : apiKey,
                'content-type': 'application/json',
            },
        };

        // REQUEST
        if (actionTypeRequest) dispatch({ type: actionTypeRequest });

        try {
            const res = await fetch(url, opt);
            const json = await res.json();

            // CONSTRUCT RESPONSE
            const response =
                !json.data && res.ok
                    ? {
                          data: json,
                          status: res.status,
                          ok: res.ok || true,
                      }
                    : {
                          ...json,
                          status: res.status,
                          ok: res.ok || true,
                      };

            // SUCCESS
            if (res.ok) {
                /**
                 * customActionTypeSuccess
                 * -----------------------------------------------------
                 * customActionTypeSuccess takes in a callback and return network
                 * response through argument to support multiple dispatches or data manipulations.
                 * */
                if (customActionTypeSuccess) {
                    customActionTypeSuccess(response);
                } else if (actionTypeSuccess) {
                    dispatch({
                        type: actionTypeSuccess,
                        response,
                    });
                }
            }

            // FAILURE
            if (!res.ok && actionTypeFailure) {
                dispatch({
                    type: actionTypeFailure,
                    error: response,
                });
            }
            return response;
        } catch (error) {
            // FAILURE
            if (actionTypeFailure) {
                dispatch({
                    type: actionTypeFailure,
                    error,
                });
            }
            return {
                ok: false,
                error: true,
                message: error,
            };
        }
    },
    /**
     *  Allow for direct change from 'fetchlib.fetch to 'mockAdapter', to have it mock fetch successfully
     */
    mockAdapter: (
        dispatch,
        getState,
        method,
        url,
        body,
        actionTypeRequest,
        actionTypeSuccess,
        actionTypeFailure,
        response = { data: [] },
        timeoutlen = 500,
    ) => {
        const res = new Promise((resolve, reject) => {
            dispatch({ type: actionTypeRequest });
            setTimeout(() => {
                dispatch({
                    type: actionTypeSuccess,
                    response,
                });
                reject(response);
            }, timeoutlen);
        });
        return res;
    },
};
