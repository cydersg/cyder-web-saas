import withQuery from 'with-query';

import fetchlib from '../../js/fetchlib';
import { CYDER_LEAVE_API_ROOT } from '../../config';

export function getActivityTotalTime() {
    return (dispatch, getState) => {
        const url = withQuery(`${CYDER_LEAVE_API_ROOT}/timesheet`, {
            appid: getState().cyderProfileReducer.profile.appId,
            action: 'gettotalactivityhour',
        });
        return fetchlib.fetch(dispatch, getState, 'GET', url);
    };
}
