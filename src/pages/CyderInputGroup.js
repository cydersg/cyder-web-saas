/**
 * Cyder Input Group
 * 
 * Author: Fernando
 */

import React from 'react'
import {connect} from 'react-redux'

const CyderInputGroup = ({field}) => {
  let hasErrors = field.errors.length > 0 ? true : false
    return (
        <div className={hasErrors ? 'form-group text-danger' : 'form-group'}>
            {field.rules.title ? (
                <label className="form-control-label">{field.rules.title}</label>
            ) : null}
            <div className="input-group">
                <span className="input-group-addon rounded-left">
                    <i
                        className={
                            hasErrors ? 'material-icons text-danger' : 'material-icons'
                        }>
                        {field.icon}
                    </i>
                </span>
                <input id={field.id}
                    placeholder={field.placeholder || ''}
                    type={field.type}
                    className={
                        hasErrors
                            ? 'form-control is-invalid rounded-right'
                            : 'form-control rounded-right'
                    }
                    name={field.name}
                />
            </div>
            {field.errors.map((error, i) => (
                <div key={i} className="form-text text-danger">
                    {error}
                </div>
            ))}
        </div>
    )
}

const mapStateToProps = (state, ownProps) => {
  return {}
}  
const mapDispatchToProps = dispatch => {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(CyderInputGroup)
