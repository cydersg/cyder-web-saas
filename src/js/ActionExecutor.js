export default {
    execute: async (loading, callback, catchCallback, finallyCallback) => {
        // ARGUMENTS VALIDATION
        const hasArgs = loading && callback;
        const areFunction = typeof loading === 'function' && typeof callback === 'function';
        if (!hasArgs) throw new Error('Require loading and action callback');
        if (!areFunction) throw new Error('Loading and action callback must be a type of function');

        // EXECUTION
        try {
            loading(true);
            const res = await callback();
            return res;
        } catch (error) {
            if (catchCallback && typeof catchCallback === 'function') catchCallback(error);
            throw error;
        } finally {
            if (finallyCallback && typeof finallyCallback === 'function') finallyCallback();
            loading(false);
        }
    },
};
