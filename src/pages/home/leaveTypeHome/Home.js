import React, { Fragment, Component, useEffect, useState, useCallback } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import { Row, Col, Container } from 'reactstrap';
import Calendar from 'react-awesome-calendar';
import LoadingSpinner from 'cyderComponents/loadingSpinner/LoadingSpinner';
import IconCardWidget from 'cyderComponents/cardWidget/IconCardWidget';
import RowColWrapper from 'cyderComponents/rowColWrapper/RowColWrapper';
import ConciergeHomeTable from 'pages/home/shared/ConciergeHomeTable';
import { SimplyLinkButton } from 'pages/home/shared/SimplyButtons';
import Jumbotron1 from 'elements/jumbotron/Jumbotron1';

import { getMonthlyLeaveSummary } from 'actions/timesheet/monthlyTimesheetAction';
import { getCurrentLeaveBalance } from 'actions/timesheet/staffLeaveBalanceAction';
import { getActivityTotalTime } from 'actions/activities/activitiesAction';
import { getAllHolidays } from 'actions/settings/cyderLeaveHolidaysAction';

import ActionExecutor from 'js/ActionExecutor';
import { capitalize, randDarkColor } from 'js/generalUtils';
import { element } from 'prop-types';
import { layoutLib } from 'js/constlib';
import LeaveCalendar from 'pages/leaves/LeaveCalendar';

class HomePage extends Component {
    state = {
        loading: false,
        currentLeaveBalance: [],
        activityTotalTime: [],
    };

    componentDidMount() {
        this.getCurrentLeaveBalance();
        this.getActivityTotalTime();
    }

    getCurrentLeaveBalance = () => {
        const callback = async () => {
            const { data } = await this.props.getCurrentLeaveBalance();
            if (data) this.setState({ currentLeaveBalance: data });
        };
        ActionExecutor.execute(this.showLoading, callback);
    };

    getActivityTotalTime = () => {
        const callback = async () => {
            const { data } = await this.props.getActivityTotalTime();
            if (data) this.setState({ activityTotalTime: data });
        };
        ActionExecutor.execute(this.showLoading, callback);
    };

    showLoading = loading => {
        this.setState({ loading });
    };

    render() {
        const { loading, currentLeaveBalance, activityTotalTime } = this.state;
        const isMobile = window.innerWidth <= layoutLib.mobileWidth;

        let cardData = [
            {
                icon: 'query_builder',
                title: activityTotalTime.totalTimeHour || 0,
                label: 'Clocked',
                color: 'orange',
            },
            {
                icon: 'alarm_add',
                title: activityTotalTime.totalOverTimeHour || 0,
                label: 'Overtime',
                color: 'indigo',
            },
        ];

        if (!isMobile) {
            cardData.push({
                icon: 'assignment',
                title: activityTotalTime.lastReport || '-',
                label: 'Last Report',
                color: 'blue',
            });
        }

        return (
            <Fragment>
                <Row className="mt-2 mb-3">
                    <Col xs={12} md={3}>
                        <QuickActionButtons {...this.props} />
                    </Col>
                    {(this.props.mode == null || this.props.mode === 'timesheet') && (
                        <Col xs={12} md={9} className="mt10">
                            <Row>
                                {cardData.map((data, i) => {
                                    return <IconCardWidget key={i} {...data} xs={6} sm={6} md={4} />;
                                })}
                            </Row>
                        </Col>
                    )}
                </Row>
                {(this.props.mode == null || this.props.mode === 'leave') && (
                    <RowColWrapper>
                        {loading ? <LoadingSpinner center /> : <LeaveTypeTablePanel data={currentLeaveBalance} />}
                    </RowColWrapper>
                )}
            </Fragment>
        );
    }
}

const QuickActionButtons = props => {
    return (
        <RowColWrapper colProps={{ className: 'col col-12 d-flex flex-column justify-content-center mt-2' }}>
            {(props == null || props.mode === 'timesheet') && (
                <>
                    <SimplyLinkButton text="Check In" iconName="access_time" to="../timesheet/clock" />
                    <SimplyLinkButton text="Check Out" iconName="access_alarm" to="../timesheet/clock" />
                    <br />
                    <SimplyLinkButton text="Activities" iconName="access_alarm" to="../timesheet/monthlytimesheet" />
                    <SimplyLinkButton text="Report" iconName="access_alarm" to="../timesheet/yearlytimesheet" />
                    <br />
                    <SimplyLinkButton text="Approval" iconName="access_alarm" to="../timesheet/timesheetapproval" />
                </>
            )}

            {(props == null || props.mode === 'leave') && (
                <>
                    <SimplyLinkButton text="Apply Leave" iconName="departure_board" to="../leaves/leaveplan/1" />
                    <SimplyLinkButton text="Leave Plan" iconName="departure_board" to="../leaves/leaveplan" />
                    <br />
                    <SimplyLinkButton text="Approval" iconName="departure_board" to="../leaves/leaveapproval" />
                </>
            )}
        </RowColWrapper>
    );
};

const LeaveTypeTablePanel = props => {
    const isMobile = window.innerWidth <= layoutLib.mobileWidth;
    const columns = [
        {
            Header: '',
            width: 50,
            show: !isMobile,
            accessor: 'code',
            Cell: props => {
                switch (props.value) {
                    case 'LEAVEINLIEU':
                    case 'WORKHOME':
                        return <i className="align-middle material-icons">home</i>;
                    case 'TRAINING':
                        return <i className="align-middle material-icons">group_work</i>;
                    case 'CHILDCARE':
                        return <i className="align-middle material-icons">child_care</i>;
                    case 'MEDICAL':
                        return <i className="align-middle material-icons">airline_seat_flat</i>;
                    case 'PATERNITY':
                    case 'MATERNITY':
                        return <i className="align-middle material-icons">child_friendly</i>;
                    case 'VACATION':
                        return <i className="align-middle material-icons">beach_access</i>;
                    case 'FAMILYMATTER':
                        return <i className="align-middle material-icons">group</i>;
                    default:
                        return null;
                }
            },
        },
        {
            Header: '',
            show: isMobile,
            accessor: 'code',
            Cell: props => {
                return (
                    <div className="boxShadow">
                        <strong>{props.original.code}</strong>
                        <br />
                        <div className="d-flex justify-content-between">
                            <span>Brought forward</span>
                            <span>{props.original.brought_forward}</span>
                        </div>
                        <div className="d-flex justify-content-between">
                            <span>Entitlement</span>
                            <span>{props.original.default_entitlement}</span>
                        </div>
                        <div className="d-flex justify-content-between">
                            <span>Used</span>
                            <span>{props.original.brought_forward + props.original.default_entitlement - props.original.balance}</span>
                        </div>
                        <div className="d-flex justify-content-between">
                            <span>Balance</span>
                            <span>{props.original.balance}</span>
                        </div>
                    </div>
                );
            },
        },
        {
            Header: 'Description',
            show: !isMobile,
            accessor: 'description',
        },
        {
            Header: 'Brought Forward',
            accessor: 'brought_forward',
            show: !isMobile,
        },
        {
            Header: 'Entitlement',
            accessor: 'default_entitlement',
            show: !isMobile,
        },
        {
            Header: 'Used',
            accessor: 'used',
            show: !isMobile,
            Cell: props => {
                return props.original.brought_forward + props.original.default_entitlement - props.original.balance;
            },
        },
        {
            Header: 'Balance',
            show: !isMobile,
            accessor: 'balance',
        },
    ];

    const { data } = props;
    return <ConciergeHomeTable title="Leave Balance" columns={columns} data={data} />;
};

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {
        getCurrentLeaveBalance: () => {
            return dispatch(getCurrentLeaveBalance());
        },
        getActivityTotalTime: () => {
            return dispatch(getActivityTotalTime());
        },
        getMonthlyLeaveSummary: body => {
            return dispatch(getMonthlyLeaveSummary(body));
        },
        getAllHolidays: year => {
            return dispatch(getAllHolidays(year));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
